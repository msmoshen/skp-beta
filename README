=======================================
SKP: The Shen-Kurohashi/Kawahara-Parser
=======================================
v0.7 
Edited by Mo Shen, 2014/05/15


What is SKP
-----------
SKP is a high accuracy multilingual dependency parser/parse reranker developed for the Kyoto Example-Based 
Dependency-to-Dependency Translation Framework (KyotoEBMT).

For more details, please refer to the upcoming paper:
Mo Shen, Daisuke Kawahara and Sadao Kurohashi. 
Dependency Parse Reranking with Rich Subtree Features.
IEEE Transactions on Audio, Speech, and Language Processing. (To Appear in 2014)


Compile
-----------
(working directory: SKP)

1. cd SKP/src

2. make


How to use
----------

1. Parser

1.1 Train:
	Usage: 	SKP -r 2 -t trainingDataFile -m modelFile -a -s

1.2 Parse new sentences:
	Usage: 	SKP -r 2 -m modelFile < inputPath  > outputPath

	Note: The input file should be in the CONLL data format; A line in this format begins with the line number and 
should include the word form and the POS tag:

		10	研究	_	NN	_	_	_	_

1.3 Evaluation:
	(in SKP folder)
	Usage: python script/evaluate_joint_parse.py outputFile refFile


2. Procude K-best parses

2.1 K-best parsing:
	SKP -r 2 -K 5 -m modelFile  < inputPath > outputPath

	This will produce the 5-best parses for the input sentences.

2.2 Train cross-validate parsers using part of the training data:
	Usage: 	SKP -r 2 -t trainingDataFile -m outputModelFile -v 10 -z 1 -a -s

	This will train a model without using the first 10% sentences in the trainning data. Modify the values of the 
options -v and -z to change which part of the training data you want to set aside.

2.3 Produce k-best list on set-aside data:
	Usage: 	SKP -r 2 -o trainingDataFile -g kbestOutputFile -m modelFile -k 50 -v 10 -z 1

	This will parse the set aside train data using the model trained on the rest of the sentences.

2.4 Convert K-best parses into parse forests:
	(in SKP folder)
	src/SKP -r 2 -K 5 -m modelFile  < conllFormatInput | python script/nbest_to_packed_forest.py > packedForestOutput

(All above processes assume data/feature.dp.def as the default feature template)


3. Reranker

3.1 Train:
	Usage: 	SKP -r 0 -y trainingDataFile -m outputModelFile -k 10

3.2 Test:
	Usage: 	SKP -x -y testingDataFile -m ModelFile -k 20
	
	The model used should be trained using complete training data in stage 1. The K need not to be identical with 
that in the training. Empirically use a K twice as large as that in the training can gain the best performace.


4. Parse reranking
	Usage: 	SKP -p 2 -o inputData -m parsingModelFile -q rerankingModelFile -k 5

	The input file should be in the CONLL data format; A line in this format begins with the line number as follows:

		10	研究	_	NN	_	_	_	_

	The output of this module will overwrite the input file to add predicted dependency tags:

		10	研究	_	NN	_	_	11	_
	 
	WARNING: The output will OVERWRITE the input file. Please backup your input file beforehand.

	An example: copy the following directories into a working directory (suppose it's called "work_dir"):

		orchid:~shen/SKP_complementary/test

		orchid:~shen/SKP_complementary/model

	and do the following:

	 	SKP -p 2 -o work_dir/test/CTB5.test.transparent.conll.dep.UTF8 -m work_dir/model/model.parse.CTB.base2.dat -q work_dir/model/model.rerank.CTB.base2.k5.dat -k 5

	The file: work_dir/test/CTB5.test.transparent.conll.dep.UTF8 will be overwritten  with dependency tags after 
the processing is finished. You can check the accuracy by compare the tags with original test data (the file 
work_dir/test/CTB5.test.conll.dep.UTF8): under the directory work_dir/test, run

		perl check_accuracy.pl

	If you wish to repeat the test, do the following under the directory work_dir/test :

		perl make_transparent_data.pl

	This will remove the dependency tags in work_dir/test/CTB5.test.transparent.conll.dep.UTF8 .


5. Options:
-t		path to input training data file
-y		path to input k-best training data file		
-m		path to (input/output) (parsing/reranking) model file
-q		path to input reranking model file
-e		path to input testing data file
-o		path to input data ready for producing k-best list
-r		order of parser; default 1
-x		re-ranker testing mode; default false
-p		order of parser in parse reranking output mode; default false
-g		directory of output k-best training data file
-v		number of folds in cross-validate training; default 0
-z		set the n-th fold of data aside for producing k-best list and others for training a base parser, used 
together with the "-v" option; default 0
-i		number of training iterations;default 10
-a		trigger averaged perceptron; default false
-s		shuffle training data; default false
-c		path to input data file in configuration mode; default false 
-k		specify the K in producing the K-best candidate parse tree list (Use this option for the parse 
reranker); default 1
-K		specify the K in producing the K-best parses (Use this option when you simply want the K-best output); 
default 1
-u		output model files after each iteration to the path specified in the argument; default false
-l		output model files only for the last iteration to the path specified in the argument -u, newer output 
will overwrite the older one; default false
-n		using alternative decoding algorithm in 2nd-order parser (recommended for training 2nd-order parser); 
default false
-O		parse testing data and output the result in CONLL format file in specified path
-w		(experimental) trigger exhaustive training; default false
-F		(experimental) trigger word-cluster feature set; default false


TODO
----
- Semi-supervised learning
- Exhaustive training
- Dynamic subtree context in reranking


Authors
-------
Mo Shen, Daisuke Kawahara, and Sadao Kurohashi


Contact
-------
Mo Shen <shen@nlp.ist.i.kyoto-u.ac.jp>