#!/usr/bin/python

import sys
import codecs

# Convert N-best dependency parses to packed forest 
# Usage: python [thisScript] < [N-bestParsesFile] > [packedForestFile]

def main():
    sys.stdin = codecs.getreader('utf-8')(sys.stdin)
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr)

    makeForest()


def makeForest():
    autoList = []    
    nodeList = []
    spanList = []
    newSentence = ''
    lineNum = 1
    build_new_forest = True
    raw_sent_ctr = 0

    for line in sys.stdin:
        if line == '\n' :
            newSentBundle = newSentence, nodeList
            autoList.append(newSentBundle)
            spanSingleList = []
            spanMapReverse = {}
            depMap = {}
            sys.stdout.write(autoList[0][0] + '\n')
            for oneSent in autoList:
#                 sys.stdout.write('#' + autoList[0][0] + '\n')
                findSpans(oneSent, spanList)
            spanCtr = 1
            for i in xrange(0, len(spanList) ):
                for k in spanList[i].iterkeys():
                    newSpan = [k, spanList[i][k]]
                    spanSingleList.append(newSpan)
                    newReverseSpan = [spanCtr, spanList[i][k]]
                    spanCtr += 1
                    spanMapReverse[k] = newReverseSpan
            printSpans(spanSingleList)
            mapSpans(spanMapReverse, depMap, autoList)
            printDependencies(spanMapReverse, depMap)
            autoList = []
            build_new_forest = True
        else:
            if line.startswith('#'):
#                 ll = line.rstrip()
#                 sys.stdout.write(ll + '\n')
                raw_sent_ctr = 0
                if build_new_forest:
                    build_new_forest = False
                else:
                    newSentBundle = newSentence, nodeList
                    autoList.append(newSentBundle)
                nodeList = []
                spanList = []
                newSentence = ''
                lineNum = 1
            else:
                ll = line.rstrip()
                elements = ll.split('\t')
                if raw_sent_ctr > 0:
                    newSentence += ' '
                newSentence += elements[1]
                newSentence += '_'
                newSentence += elements[3]
                raw_sent_ctr += 1                
                node = creatNode(newSentence, elements, lineNum)
                nodeList.append(node)
                if len(spanList) < lineNum:
                    newSpanMap = {}
                    spanList.append(newSpanMap)
                lineNum += 1


def findSpans(oneSent, spanList):
    for oneNode in oneSent[1]:
        if  oneNode[3] > 0 and oneNode[3] < oneNode[0]:
            oneSent[1][oneNode[3] - 1][4][1].append(oneNode[0])
        elif  oneNode[3] > oneNode[0]:
            oneSent[1][oneNode[3] - 1][4][0].append(oneNode[0])
    for oneNode in oneSent[1]:
        oneNode[5][0] = findLeftSpan(oneNode, oneSent[1])
        oneNode[5][1] = findRightSpan(oneNode, oneSent[1])
        oneNode[6] = str(oneNode[5][0]) + '_' + str(oneNode[5][1]) + '_' + oneNode[1] + '_' + oneNode[2]
        spanList[oneNode[0] - 1][oneNode[6]] = [oneNode[1], oneNode[2]]
#         printDetail(oneNode)


def findLeftSpan(node, nodeList):
    if len(node[4][0]) == 0:
        return node[0] - 1
    else:
        return findLeftSpan(nodeList[node[4][0][0] - 1], nodeList)


def findRightSpan(node, nodeList):
    if len(node[4][1]) == 0:
        return node[0]
    else:
        return findRightSpan(nodeList[node[4][1][-1] - 1], nodeList)


def creatNode(newSentence, elements, lineNum):
    surface = elements[1]
    PoS = elements[3]
    dep = int(elements[6])
    span = [0, 0]
    children_left = []
    children_right = []
    children = children_left, children_right
    signature = ''
    node = [lineNum, surface, PoS, dep, children, span, signature]
    return node


def printSpans(spanSingleList):
    sys.stdout.write('<spans>\n')
    for i in xrange(0, len(spanSingleList) ):
        spanElements = spanSingleList[i][0].split('_')
        sys.stdout.write(str(i + 1) + ':\t')
        sys.stdout.write(spanElements[0] + ',' + spanElements[1] + '\t' + spanElements[2] + '\t' + spanElements[3] + '\n')
#         sys.stdout.write(spanSingleList[i][0] + '\n')
    sys.stdout.write('</spans>\n')


def mapSpans(spanMapReverse, depMap, autoList):
    for oneSent in autoList:
        for oneNode in oneSent[1]:
            combinedSign = ''
            ctr = 0
            for leftChild in oneNode[4][0]:
                if ctr > 0:
                    combinedSign += ','
                ctr += 1
                combinedSign += str(spanMapReverse[oneSent[1][leftChild - 1][6]][0])
            for rightChild in oneNode[4][1]:
                if ctr > 0:
                    combinedSign += ','
                ctr += 1
                combinedSign += str(spanMapReverse[oneSent[1][rightChild - 1][6]][0])
            if combinedSign != '':
                if oneNode[6] not in depMap:
                    newComSignMap = {combinedSign:1}
                    depMap[oneNode[6]] = newComSignMap
                else:
                    depMap[oneNode[6]][combinedSign] = 1


def printDependencies(spanMapReverse, depMap):
    sys.stdout.write('<dependencies>\n')            
    for ss in depMap.iterkeys():
        if len(depMap[ss].keys()) > 0:
            for pp in depMap[ss].iterkeys():
                sys.stdout.write(str(spanMapReverse[ss][0])  + ':\t' + pp  + '\n')
    sys.stdout.write('</dependencies>\n')
    sys.stdout.write('\n')


def printDetail(oneNode):
    sys.stdout.write(str(oneNode[0]) + '\t' + oneNode[1] + '\t' + oneNode[2] + '\t' + str(oneNode[3]) + '\n')
    sys.stdout.write('\t' + str(oneNode[5][0]) + ',' + str(oneNode[5][1]) + '\n')
    iter_ctr = 0
    sys.stdout.write('\t')
    for oneSign in oneNode[4][0]:
        if iter_ctr > 0:
            sys.stdout.write(',')
        iter_ctr += 1
        sys.stdout.write(str(oneSign))
    sys.stdout.write(' | ')
    iter_ctr = 0
    for oneSign in oneNode[4][1]:
        if iter_ctr > 0:
            sys.stdout.write(',')
        iter_ctr += 1
        sys.stdout.write(str(oneSign))
    sys.stdout.write('\n')


if __name__ == '__main__':
    main()
