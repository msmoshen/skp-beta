#!/usr/bin/python

import sys
import codecs
import re

# Convert xml files into CoNLL-style N-best dependency parses; Coupled with script 'nbest_tp_packed_forest_py'
# Usage: python [thisScript] < [xmlFile] > [N-bestParsesFile]

def main():
    sys.stdin = codecs.getreader('utf-8')(sys.stdin)
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
    convertFormat()


def convertFormat():
    node = []
    node = initNode(node)
    for line in sys.stdin:
        ll = line.lstrip()
        if (re.search(r'^</sentence>', ll)):
            sys.stdout.write('\n')
            continue
        if (re.search(r'^<j_data', ll)):
            prob = re.search(r'prob="(.+?)"', ll)
            sys.stdout.write('#' + prob.group(1) + '\n')
            continue
        if (re.search(r'^<phrase', ll)):
            pnum = re.search(r'pnum="(.+?)"', ll)
            dpnd = re.search(r'dpnd="(.+?)"', ll)
            node[0] = int(pnum.group(1)) + 1
            node[3] = int(dpnd.group(1)) + 1
            continue
        if (re.search(r'^<word', ll)):
            surface = re.search(r'>(.+?)</word>', ll)
            if not surface:
                surface = re.search(r'lem="(.+?)"', ll)
            pos = re.search(r'pos="(.+?)"', ll)
            node[1] = surface.group(1)
            node[2] = pos.group(1)
            sys.stdout.write(str(node[0]) + '\t' + node[1] + '\t_\t' + node[2] + '\t' + node[2] + '\t_\t' + str(node[3]) + '\t_\n')
            node = initNode(node)
            continue


def initNode(node):
    del node
    lineNum = -1
    wordForm = ''
    PoS = ''
    dep = -1
    node = [lineNum, wordForm, PoS, dep]
    return node


if __name__ == '__main__':
    main()
