#!/usr/bin/python

import sys
import codecs

# Usage: python [thisScript] < input > outputConllFormat
def main():

    sys.stdin = codecs.getreader('utf-8')(sys.stdin)
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr)

    stdin_buffer = sys.stdin.readlines()
    transFormToConll(stdin_buffer)


def transFormToConll(fileHandle):
#    fileHandle = sys.stdin.read()
#    fileHandle = fileHandle.decode('utf8')

    for line in fileHandle:
        ll = line.rstrip()
        if not ll.startswith('#'):
            pairs = ll.split(' ')
            for i in xrange(0, len(pairs)):
                word_pos = pairs[i].split('_')
                if len(word_pos) == 2:
                    sys.stdout.write(str(i+1) +  '\t' + word_pos[0] +  '\t' + '_' +  '\t' + word_pos[1] +  '\t' + word_pos[1] +  '\t' + '_'+  '\t' + '_'+  '\t' + '_'+ '\n')
                else:
                    sys.stderr.write('err: not splitible in: ' + pairs[i] + '\n')
            sys.stdout.write('\n')


if __name__ == '__main__':
    main()

