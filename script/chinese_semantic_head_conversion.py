#!/usr/bin/python

import sys
import codecs

def main():
#Usage: python [ThisScript] [ModalList] < InputConllFile > OutputConllFile 

    sys.stdin = codecs.getreader('utf-8')(sys.stdin)
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr)

    modalFilePath = sys.argv[1]
    stdin_buffer = sys.stdin.readlines()
    loadTreebank(modalFilePath, stdin_buffer)


def loadTreebank(modalFilePath, fileHandle):
#    fileHandle = sys.stdin.read()
#    fileHandle = fileHandle.decode('utf8')
    modalFileHandle = codecs.open(modalFilePath, 'rU', 'utf-8')
    
    modalDict = {}
    wordEntry = []
    modalLineNum = []

    for line in modalFileHandle:
        ll = line.rstrip()
        modalDict[ll] = 0

    for line in fileHandle:
        if line == '\n':
            for modalLine in modalLineNum:
                shiftHead(modalLine, wordEntry)
            for blocks in wordEntry:
                endCtr = 0
                for oneBlock in blocks:
                    endCtr += 1
                    sys.stdout.write(oneBlock)
                    if endCtr < len(blocks):
                        sys.stdout.write('\t')
                sys.stdout.write('\n')
            sys.stdout.write('\n')
            del wordEntry[:]
            del modalLineNum[:]
        else:
            ll = line.rstrip()
            if not ll.startswith('#'):
                block = ll.split('\t')
                #if the word is a modal
                if block[1] in modalDict and block[3] == 'VV':
                    modalLineNum.append(int(block[0]))
                wordEntry.append(block)


def shiftHead(modalLine, wordEntry):
    childList = []
    predicateIsFound = False

    for blocks in wordEntry:
        if int(blocks[0]) < modalLine:
            if int(blocks[6]) == modalLine:
                childList.append(int(blocks[0]))
        elif int(blocks[0]) == modalLine:
            head = int(blocks[6])
        else:
            if int(blocks[6]) == modalLine:
#                sys.stderr.write(blocks[3] + '\n')
                if not predicateIsFound and blocks[3] == 'VV':
                    predicateIsFound = True
                    predicateLine = int(blocks[0])
                else:
                    childList.append(int(blocks[0]))

    if predicateIsFound:
#        sys.stderr.write(wordEntry[modalLine - 1][1] + ' ' + wordEntry[modalLine - 1][3] + '\n')
        wordEntry[modalLine - 1][6] = str(predicateLine)
        for childLine in childList:
            wordEntry[childLine - 1][6] = str(predicateLine)
        wordEntry[predicateLine - 1][6] = str(head)



if __name__ == '__main__':
    main()
