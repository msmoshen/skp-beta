#!/usr/bin/python

import sys
import string
import codecs

# Script for evaluating joint segmentation, PoS-tagging and dependency parsing
# Usage: python [thisScript] [outputFile] [goldStandardFile]

def main():
    sys.stdin = codecs.getreader('utf-8')(sys.stdin)
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr)

    autoName = sys.argv[1]
    goldName = sys.argv[2]

    autoList = []
    goldList = []

    loadAuto(autoList, autoName)
    loadGold(goldList, goldName)
    evaluate(autoList, goldList)


def loadAuto(autoList, autoFilePath):
    nodeList = []
    newSentence = ''
    leftIndex = 0
    rightIndex = 0

    autoFileHandle = codecs.open(autoFilePath, 'rU', 'utf-8')

    for line in autoFileHandle:
        if not line.startswith('#'):
            if line != '\n' :
                ll = line.rstrip()
                elements = ll.split('\t')
                newSentence += elements[1]
                rightIndex += len(elements[1])
                boundaryIndices = leftIndex, rightIndex
                surface = elements[1]
                PoS = elements[3]
                dep = int(elements[6])
                node = boundaryIndices, surface, PoS, dep
                nodeList.append(node)
                leftIndex = rightIndex
            else:
                newSentBundle = newSentence, nodeList
                autoList.append(newSentBundle)
                nodeList = []
                newSentence = ''
                leftIndex = 0
                rightIndex = 0


def loadGold(goldList, goldFilePath):
    nodeList = []
    newSentence = ''
    leftIndex = 0
    rightIndex = 0

    goldFileHandle = codecs.open(goldFilePath, 'rU', 'utf-8')

    for line in goldFileHandle:
        if not line.startswith('#'):
            if line != '\n' :
                ll = line.rstrip()
                elements = ll.split('\t')
                newSentence += elements[1]
                rightIndex += len(elements[1])
                boundaryIndices = leftIndex, rightIndex
                surface = elements[1]
                PoS = elements[3]
                dep = int(elements[6])
                node = boundaryIndices, surface, PoS, dep
                nodeList.append(node)
                leftIndex = rightIndex
            else:
                newSentBundle = newSentence, nodeList
                goldList.append(newSentBundle)
                nodeList = []
                newSentence = ''
                leftIndex = 0
                rightIndex = 0


def evaluate(autoList, goldList):
#    fileHandle = sys.stdin.read()
#    fileHandle = fileHandle.decode('utf8')
    if len(autoList) != len(goldList):
        sys.stderr.write(';;err: number of sentences not match\n')
        sys.exit(0)

    denom = 0
    nom = 0
    
    for i in xrange(0, len(autoList)):
        if autoList[i][0] != goldList[i][0]:
            sys.stderr.write(';;err: sentence not match:\n')
            sys.stderr.write('\t' + autoList[i][0] + '\n')
            sys.stderr.write('\t' + goldList[i][0] + '\n')
        else:
            gold_iter = 0
            for s in xrange(0, len(autoList[i][1])):
                while goldList[i][1][gold_iter][0][0] < autoList[i][1][s][0][0]:
                    gold_iter += 1
                if goldList[i][1][gold_iter][-1] == 0 or goldList[i][1][gold_iter][2] == 'PU':
                    continue
                else:
                    denom += 1
                    if autoList[i][1][s][0][0] == goldList[i][1][gold_iter][0][0] and autoList[i][1][s][0][1] == goldList[i][1][gold_iter][0][1]:
                        autoHeadIndex = autoList[i][1][s][-1] - 1
                        goldHeadIndex = goldList[i][1][gold_iter][-1] - 1
                        if autoList[i][1][autoHeadIndex][1] == goldList[i][1][goldHeadIndex][1]:
                            nom += 1
#                            sys.stderr.write(autoList[i][1][autoHeadIndex][1] + '\t' + goldList[i][1][goldHeadIndex][1] + '\n')
#                    sys.stderr.write(str(autoList[i][1][s][0][0]) + '\t'+ str(goldList[i][1][gold_iter][0][0]) + '\t' + str(autoList[i][1][s][0][1]) + '\t'+ str(goldList[i][1][gold_iter][0][1]) + '\n')
    prec = 100 * float(nom)/float(denom)
    prec = round(prec, 2)
    sys.stderr.write('Precision: ' + str(prec) + '\n')



if __name__ == '__main__':
    main()
