//============================================================================
// Name        : SKP.cc
// Author      : Mo Shen, Daisuke Kawahara, Sadao Kurohashi
// Version     : 1.0
// Description : A dependency parse reranking system
//============================================================================

#include "common.h"
#include "tagger.h"
#include "depParser.h"
#include "cmdline.h"
#include "factor.h"

bool MODE_TRAIN = false;
bool MODE_EXHAUST = false;
bool MODE_EXAM = false;
bool MODE_PARSE = false;
bool MODE_CONFIG = false;
bool MODE_RERANK = false;
bool MODE_KBEST = false;
bool MODE_RETEST = false;
bool WEIGHT_AVERAGED = false;
bool MODE_SHUFFLE = false;
bool MODE_VERBOSE = false;
bool MODE_NEO = true;
bool MODE_TRACE = false;
bool MODE_TRACE_LAST = false;
bool MODE_MODEL_REFINE = false;
bool MODE_PARSE_RERANK = false;
bool MODE_SEQUENCE_RERANK = false;
bool MODE_WORD_CLUSTER = false;
bool MODE_OUTPUT = false;
bool MODE_OUTPUT_STREAM = true;
bool MODE_INPUT_STREAM = true;

size_t training_data_division = 0;
size_t training_data_spare = 0;

std::map<std::string, double> feature_weight;
std::map<std::string, double> feature_weight_sum;

std::map<std::string, double> feature_weight_dep;
std::map<std::string, double> feature_weight_sum_dep;
std::map<std::string, double> feature_weight_rerank;

size_t total_sentence_num;

std::string tracingHead;

void option_proc(cmdline::parser &option, int argc, char **argv) {
	option.add<unsigned int>("sequence-rerank", 'b', "sequence-rerank order",
			false);
	option.add<unsigned int>("parse-rerank", 'p', "parse-rerank order", false);
	option.add<std::string>("model", 'm', "model filename", false);

	option.add<std::string>("reranker_model", 'q', "reranker model filename",
			false);
	option.add<std::string>("feature", 'f', "feature template filename", false,
			"../data/feature.dp.def");
	option.add<std::string>("semi-feature", 'F', "semi-supervised feature template filename", false,
			"../data/feature.dp.cl.def");
	option.add<std::string>("train", 't', "training filename", false);
	option.add<std::string>("k-best_output", 'g', "k-best output filename",
			false);

	option.add<std::string>("config", 'c', "config filename", false);

	option.add<std::string>("data", 'o', "data filename", false);

	option.add<std::string>("output", 'O', "output filename", false);

	option.add<unsigned int>("kbest", 'k', "best k output", false, 5);
	option.add<unsigned int>("kbest_outstream", 'K', "best k output", false, 5);

	option.add<std::string>("rerank", 'y', "rerank k-best list", false);
	option.add<unsigned int>("iteration", 'i', "iteration number for training",
			false, 10);
//	option.add<unsigned int>("unk_max_length", 'l',
//			"maximum length of unknown word detection", false, 3);

	option.add<unsigned int>("parse", 'r', "parsing order", false);

	option.add<unsigned int>("division", 'v', "divide data", false);

	option.add<unsigned int>("spare", 'z', "data set aside", false);

	option.add("exhaust", 'w', "exhausted training ");

	option.add("rerank_test", 'x', "reranker test");

	option.add("averaged", 'a', "use averaged perceptron for training");
	option.add("shuffle", 's', "shuffle training data for each iteration");
//	option.add("unknown", 'u',
//			"apply unknown word detection (obsolete; already default)");
	option.add("debug", '\0', "debug mode");

	option.add<std::string>("model_base", 'j', "basement model filename",
			false);

	option.add("verbose", '1', "verbose mode");

	option.add("examine", 'e', "test filename");

	option.add("neo", 'n', "alternative decoding mode");

	option.add<std::string>("trace", 'u', "trace iteration", false);

	option.add("last", 'l', "trace last iteration");

	option.add("help", 'h', "print this message");
	option.parse_check(argc, argv);

	if (option.exist("train")) {
		MODE_TRAIN = true;
	}

	if (option.exist("exhaust")) {
		MODE_EXHAUST = true;
	}

	if (option.exist("examine")) {
		MODE_EXAM = true;
	}
	if (option.exist("parse")) {
		MODE_PARSE = true;
	}

	if (option.exist("config")) {
		MODE_CONFIG = true;
	}

	if (option.exist("rerank")) {
		MODE_RERANK = true;
	}

	if (option.exist("kbest")) {
		MODE_KBEST = true;
	}

	if (option.exist("rerank_test")) {
		MODE_RETEST = true;
	}

	if (option.exist("averaged")) {
		WEIGHT_AVERAGED = true;
	}

	if (option.exist("shuffle")) {
		MODE_SHUFFLE = true;
	}

	if (option.exist("verbose")) {
		MODE_VERBOSE = true;
	}

	if (option.exist("neo")) {
		MODE_NEO = true;
	}

	if (option.exist("trace")) {
		MODE_TRACE = true;
		tracingHead = option.get<std::string>("trace");
	}

	if (option.exist("last")) {
		MODE_TRACE_LAST = true;
	}

	if (option.exist("model_base")) {
		MODE_MODEL_REFINE = true;
	}

	if (option.exist("parse-rerank")) {
		MODE_PARSE_RERANK = true;
	}

	if (option.exist("sequence-rerank")) {
		MODE_SEQUENCE_RERANK = true;
	}
	if (option.exist("semi-feature")) {
		MODE_WORD_CLUSTER = true;
	}
	if (option.exist("output")) {
		MODE_OUTPUT = true;
	}


}

// write feature weights
bool write_model_file(const std::string &model_filename) {
	if (MODE_PARSE) {
		std::ofstream model_out(model_filename.c_str(), std::ios::out);
		model_out.precision(15);
		if (!model_out.is_open()) {
			cerr << ";; cannot open " << model_filename << " for writing"
					<< endl;
			return false;
		}
		for (std::map<std::string, double>::iterator it =
				feature_weight_dep.begin(); it != feature_weight_dep.end();
				it++) {
			model_out << it->first << " " << it->second << endl;
		}
		model_out.close();
	} else {
		std::ofstream model_out(model_filename.c_str(), std::ios::out);
		if (!model_out.is_open()) {
			cerr << ";; cannot open " << model_filename << " for writing"
					<< endl;
			return false;
		}
		for (std::map<std::string, double>::iterator it =
				feature_weight.begin(); it != feature_weight.end(); it++) {
			model_out << it->first << " " << it->second << endl;
		}
		model_out.close();
	}
	return true;
}

bool write_model_file(const std::string &model_filename,
		std::map<std::string, double>& featWeight) {
	if (MODE_PARSE) {
		std::ofstream model_out(model_filename.c_str(), std::ios::out);
		model_out.precision(15);
		if (!model_out.is_open()) {
			cerr << ";; cannot open " << model_filename << " for writing"
					<< endl;
			return false;
		}
		for (std::map<std::string, double>::iterator it = featWeight.begin();
				it != featWeight.end(); it++) {
			model_out << it->first << " " << it->second << endl;
		}
		model_out.close();
	} else {
		return false;
	}
	return true;
}

// read feature weights
bool read_model_file(const std::string &model_filename) {

	std::ifstream model_in(model_filename.c_str(), std::ios::in);
	model_in.precision(15);

	if (!model_in.is_open()) {
		cerr << ";; cannot open model file " << model_filename << " for reading" << endl;
		return false;
	} else {
		cerr << "Reading model " << model_filename
				<< " ..." << endl;
	}

	std::string buffer;
	while (getline(model_in, buffer)) {
		std::vector<std::string> line;
		Morph::split_string(buffer, " ", line);

		feature_weight_dep[line[0]] = atof(
				static_cast<const char *>(line[1].c_str()));

	}

	//cerr << "The line Hp:JJ has the value " << feature_weight_dep["Hp:JJ"]<< endl;

	model_in.close();
	return true;
}

bool read_rerank_model_file(const std::string &model_filename) {

	std::ifstream model_in(model_filename.c_str(), std::ios::in);
	model_in.precision(15);

	if (!model_in.is_open()) {
		cerr << ";; cannot open " << model_filename << " for reading" << endl;
		return false;
	} else {
		cerr << "Reading reranking model " << model_filename
				<< " ..." << endl;
	}
	std::string buffer;
	while (getline(model_in, buffer)) {
		std::vector<std::string> line;
		Morph::split_string(buffer, " ", line);
		feature_weight_rerank[line[0]] = atof(
				static_cast<const char *>(line[1].c_str()));
	}
	model_in.close();
	return true;
}

int main(int argc, char** argv) {
	cout.precision(15);
	cerr.precision(15);
	cmdline::parser option;
	option_proc(option, argc, argv);

	Morph::Parameter param(option.get<std::string>("feature"), 10, false);

//	Morph::Parameter param(option.get<std::string>("feature"),option.get<std::string>("semi-feature"), 10, false);

//  Morph::Parameter param("../data/feature.dp.def", 10, false);

	Morph::DepParser depParser(&param);

	if (MODE_TRAIN)
	{

		if (MODE_PARSE) {

			if (option.exist("division")) {
				training_data_division = option.get<unsigned int>("division");
			}

			if (option.exist("spare")) {
				training_data_spare = option.get<unsigned int>("spare");
			}

			if (MODE_EXHAUST) {
				depParser.exhaust_train(option.get<std::string>("train"),
						option.get<unsigned int>("parse"),
						option.get<unsigned int>("iteration"));
			} else if (MODE_MODEL_REFINE) {
				read_model_file(option.get<std::string>("model_base"));
				depParser.train(option.get<std::string>("train"),
						option.get<unsigned int>("parse"),
						option.get<unsigned int>("iteration"));
			} else {
				depParser.train(option.get<std::string>("train"),
						option.get<unsigned int>("parse"),
						option.get<unsigned int>("iteration"));
			}
			write_model_file(option.get<std::string>("model"));
			feature_weight_dep.clear();
			feature_weight_sum_dep.clear();
		} else {
//			tagger.train(option.get<std::string>("train"));
//			write_model_file(option.get<std::string>("model"));
//			feature_weight_sum.clear();
		}
	} else if (MODE_CONFIG) {
		if (MODE_PARSE) {
			depParser.config(option.get<std::string>("config"),
					option.get<unsigned int>("parse"));
			write_model_file(option.get<std::string>("model"));
			feature_weight_dep.clear();
			feature_weight_sum_dep.clear();
		} else {
			cerr << "config mode not implemented." << endl;
		}
	} else if (MODE_PARSE_RERANK) {

		read_model_file(option.get<std::string>("model"));
		depParser.sequence_parse(option.get<std::string>("data"),
				option.get<unsigned int>("kbest"),
				option.get<unsigned int>("parse-rerank"));
		feature_weight_dep.clear();

		read_rerank_model_file(option.get<std::string>("reranker_model"));
		depParser.complete(option.get<std::string>("data"),
				option.get<unsigned int>("kbest"),
				option.get<unsigned int>("parse-rerank"));
		feature_weight_rerank.clear();

	} else if (MODE_SEQUENCE_RERANK) {

		read_model_file(option.get<std::string>("model"));
		depParser.sequence_parse(option.get<std::string>("data"),
				option.get<unsigned int>("kbest"),
				option.get<unsigned int>("sequence-rerank"));
		feature_weight_dep.clear();

		read_rerank_model_file(option.get<std::string>("reranker_model"));
		depParser.sequence_rerank(option.get<std::string>("data"),
				option.get<unsigned int>("kbest"),
				option.get<unsigned int>("sequence-rerank"));
		feature_weight_rerank.clear();

	} else if (MODE_RERANK) {
		if (MODE_PARSE) {

			if (MODE_MODEL_REFINE) {
				read_model_file(option.get<std::string>("model_base"));
				depParser.rerank(option.get<std::string>("rerank"),
						option.get<unsigned int>("kbest"),
						option.get<unsigned int>("iteration"));
			} else {
				depParser.rerank(option.get<std::string>("rerank"),
						option.get<unsigned int>("kbest"),
						option.get<unsigned int>("iteration"));
			}

			write_model_file(option.get<std::string>("model"));
			feature_weight_dep.clear();
			feature_weight_sum_dep.clear();
		} else if (MODE_RETEST) {
			read_model_file(option.get<std::string>("model"));
			depParser.rerank_test(option.get<std::string>("rerank"),
					option.get<unsigned int>("kbest"));
			feature_weight_dep.clear();
		} else {
			cerr << "re-rank mode not implemented." << endl;
		}
	} else if (MODE_KBEST) {
		if (MODE_PARSE) {

			if (option.exist("division")) {
				training_data_division = option.get<unsigned int>("division");
			}

			if (option.exist("spare")) {
				training_data_spare = option.get<unsigned int>("spare");
			}

			read_model_file(option.get<std::string>("model"));
			depParser.kbest(option.get<std::string>("data"),
					option.get<std::string>("k-best_output"),
					option.get<unsigned int>("kbest"),
					option.get<unsigned int>("parse"));
			feature_weight_dep.clear();
		} else {
			cerr << "kbest mode not implemented." << endl;
		}
	} else if (MODE_EXAM) {

			if (option.exist("division")) {
				training_data_division = option.get<unsigned int>("division");
			}

			if (option.exist("spare")) {
				training_data_spare = option.get<unsigned int>("spare");
			}

			read_model_file(option.get<std::string>("model"));
			depParser.test(option.get<unsigned int>("parse"));
			feature_weight_dep.clear();

	} else {

		MODE_OUTPUT = true;

		read_model_file(option.get<std::string>("model"));
		if (MODE_OUTPUT_STREAM) {
			if (MODE_INPUT_STREAM){
				if (option.exist("kbest_outstream")) {
					depParser.kbest_iostream(option.get<unsigned int>("kbest_outstream"),
										   option.get<unsigned int>("parse"));
				} else {
					depParser.io_stream(option.get<unsigned int>("parse"));
				}
			}
			else {
				depParser.output_stream(option.get<unsigned int>("parse"));
			}
		} else {
			depParser.output(option.get<unsigned int>("parse"));
		}
		feature_weight_dep.clear();

	}
	return 0;
}
