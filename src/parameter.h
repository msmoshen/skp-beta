#ifndef PARAMETER_H
#define PARAMETER_H

#include "common.h"

namespace Morph {

class Parameter {
public:
	bool unknown_word_detection;
	bool shuffle_training_data;
	bool debug;
	unsigned int unk_max_length;
	unsigned int iteration_num;
	std::string darts_filename;
	std::string dic_filename;
	std::string pos_filename;
	std::string ftmpl_filename;

	std::string semiftmpl_filename;

	std::vector<unsigned short> unk_pos;
	std::vector<unsigned short> unk_figure_pos;

	Parameter(const std::string &in_darts_filename,
			const std::string &in_dic_filename,
			const std::string &in_pos_filename,
			const std::string &in_ftmpl_filename, const int in_iteration_num,
			const bool in_unknown_word_detection,
			const bool in_shuffle_training_data,
			const unsigned int in_unk_max_length, const bool in_debug) {
		darts_filename = in_darts_filename;
		dic_filename = in_dic_filename;
		pos_filename = in_pos_filename;
		ftmpl_filename = in_ftmpl_filename;
		iteration_num = in_iteration_num;
		unknown_word_detection = in_unknown_word_detection;
		shuffle_training_data = in_shuffle_training_data;
		unk_max_length = in_unk_max_length;
		debug = in_debug;
	}


	Parameter(const std::string &in_ftmpl_filename, const std::string &in_semiftmpl_filename, const int in_iteration_num,
			const bool in_shuffle_training_data) {
		ftmpl_filename = in_ftmpl_filename;
		semiftmpl_filename = in_semiftmpl_filename;
		iteration_num = in_iteration_num;
		shuffle_training_data = in_shuffle_training_data;
	}


	Parameter(const std::string &in_ftmpl_filename, const int in_iteration_num,
			const bool in_shuffle_training_data) {
		ftmpl_filename = in_ftmpl_filename;
		iteration_num = in_iteration_num;
		shuffle_training_data = in_shuffle_training_data;
	}
};

}

#endif
