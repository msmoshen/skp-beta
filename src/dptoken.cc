#include "common.h"
#include "dptoken.h"

namespace Morph
{
Dptoken::Dptoken(std::string w,std::string p,size_t i,size_t d)
{
    Dptoken::word=w;
    Dptoken::pos=p;
    Dptoken::position=i;
    Dptoken::dep=d;
}

Dptoken::~Dptoken()
{
    //dtor
}
void Dptoken::setLeftPos(std::string p)
{
    leftPos=p;
}
void Dptoken::setRightPos(std::string p)
{
    rightPos=p;
}

 Dependence::Dependence( Dptoken* head, Dptoken* modifier){
	 headPtr=head;
	 modifierPtr=modifier;
 }
Dependence::~Dependence(){

}


void Dependence::minus_feature_from_weight(std::map<std::string, double> &in_feature_weight, size_t factor){
	//TODO
}

void Dependence::minus_feature_from_weight(std::map<std::string, double> &in_feature_weight){
	//TODO
}

void Dependence::plus_feature_from_weight(std::map<std::string, double> &in_feature_weight, size_t factor){
	//TODO
}

void Dependence::plus_feature_from_weight(std::map<std::string, double> &in_feature_weight){
	//TODO
}

}
