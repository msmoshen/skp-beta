#include "depParser.h"
#include "common.h"
#include "dptoken.h"
#include "dpsentence.h"
#include "parameter.h"

namespace Morph {

DepParser::DepParser(Parameter *in_param) {
	param = in_param;
	ftmpl.open(param->ftmpl_filename);
//	wcftmpl.open(param->semiftmpl_filename);
	dpsentences_for_train_num = 0;
}

DepParser::~DepParser() {
	// TODO Auto-generated destructor stub
}

bool DepParser::train(const std::string &gsd_file, unsigned int order,
		unsigned int iteration) {

	cout.precision(15);
	cerr.precision(15);

	if (MODE_MODEL_REFINE) {
		cerr << "Mode: refine model" << endl;
	}

	if (order == 1) {

		cerr << "Mode: 1st-order" << endl;

		if (training_data_division > 0 && training_data_spare > 0) {
			cerr << "Mode: Jack-Knifing" << endl;
			read_gold_data(gsd_file, training_data_division,
					training_data_spare);
		} else {
			read_gold_data(gsd_file);
		}

		total_sentence_num = 1;

		//Morph::Decoder edgeParser;

		//         for iteration 1..5 {
		//                 for sentence 1..N {
		//                          tagger.analyzer(sentence chunked);
		//                          - collected feature;
		//                          feature gold standard;
		//                          +
		//                     }
		//             }

		cerr << "Mode: train" << endl;

		if (WEIGHT_AVERAGED) {
			cerr << "Mode: average" << endl;
		}

		if (MODE_TRACE) {
			cerr << "Mode: trace" << endl;
		}

		for (size_t t = 0; t < iteration; t++) {

//			if (t > 0 && t % 10 == 0) {
//				std::string tempModelName = "/home/shen/data/tempModels/";
//				tempModelName.append(int2string(t));
//				write_temp_model_file(tempModelName);
//			}

			if (MODE_TRACE && t > 0) {

				std::string tempModelName = tracingHead;
				tempModelName.append("/");

				if (MODE_TRACE_LAST) {

					if (training_data_division > 0 && training_data_spare > 0) {
						tempModelName.append(
								int2string(training_data_division));
						tempModelName.append(".");
						tempModelName.append(int2string(training_data_spare));
						tempModelName.append(".");
					}
					tempModelName.append("last");

				} else {
					if (training_data_division > 0 && training_data_spare > 0) {
						tempModelName.append(
								int2string(training_data_division));
						tempModelName.append(".");
						tempModelName.append(int2string(training_data_spare));
						tempModelName.append(".");
					}
					tempModelName.append(int2string(t));
				}

				write_temp_model_file(tempModelName);

			}

			cerr << "ITERATION:" << t << endl;
			//		if (param->shuffle_training_data) {// shuffle training data
			//			random_shuffle(dpsentences_for_train.begin(),
			//					dpsentences_for_train.end());
			//		}

			if (MODE_SHUFFLE) { // shuffle training data
				random_shuffle(dpsentences_for_train.begin(),
						dpsentences_for_train.end());

				cerr << "Mode:shuffle" << endl;
			}

			dpsentences_for_train[0]->printHeadIndex();

			for (std::vector<Dpsentence*>::iterator it =
					dpsentences_for_train.begin();
					it != dpsentences_for_train.end(); it++) {
				//new_sentence_analyze((*it)->get_sentence()); // get the best path

				if (total_sentence_num % 100 == 0)
					cerr << "sentence:" << total_sentence_num << endl;

				(*it)->initializeScoreMatrix();

				if (MODE_WORD_CLUSTER) {
					(*it)->evaluateEdgeScoreMatrixWc(&ftmpl, &wcftmpl,
							total_sentence_num);
				} else {
					(*it)->evaluateEdgeScoreMatrix(&ftmpl, total_sentence_num);
				}

				(*it)->decodeEdgeFactors();
				//edgeParser.setSize((*it)->getLength());

				//			edgeParser.decodeEdgeFactors((*it)->getLength(), (*it)->scoreMtx,
				//					&t,&ftmpl);

				//edgeParser.printPredict();
				//			(*it)->printPredict();
				//			(*it)->printHeadIndex();
				if (MODE_VERBOSE)
					(*it)->comparePrint();

				//						FeatureSet featureSet(&ftmpl);
				//						Dptoken new_dptoken1("a", "B", 1, 2);
				//						Dptoken new_dptoken2("c", "D", 2, 3);
				//						featureSet.extract_edge_feature(&new_dptoken1, &new_dptoken2);
				//						cerr << "we're here!edgepass" << t << endl;
				//						featureSet.extract_context_feature(&new_dptoken1, &new_dptoken2,&((*it)->dpTokenArray));
				//						cerr << "we're here!contextpass" << t << endl;

				for (int pIter = 1; pIter < (*it)->getLength() + 1; pIter++) {
					if ((*it)->getPredict()->at(pIter) == 0) {
						continue;
					} else {
						//						Morph::Dependence depEdge(&((*it)->getDpTokenArray()->at(
						//								edgeParser.predict[pIter])),
						//								&((*it)->getDpTokenArray()->at(pIter)));

						if (MODE_WORD_CLUSTER) {

							FeatureSet *f = new FeatureSet(&ftmpl, &wcftmpl);

							f->extract_edge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)));

							f->extract_context_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)),
									(*it)->getDpTokenArray());

							f->extract_wcedge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)));

							f->extract_wccontext_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)),
									(*it)->getDpTokenArray());

							if (WEIGHT_AVERAGED) {

								f->minus_feature_from_weight(feature_weight_dep,
										1);

								f->minus_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1); // - prediction

								//						f->minus_feature_from_weight(feature_weight_dep,
								//								feature_weight_sum_dep,
								//								feature_count_lastupdate, total_sentence_num, 1);
							} else {
								f->minus_feature_from_weight(feature_weight_dep,
										1);
							}
							delete f;

						} else {
							FeatureSet *f = new FeatureSet(&ftmpl);

							f->extract_edge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)));

							f->extract_context_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)),
									(*it)->getDpTokenArray());

							if (WEIGHT_AVERAGED) {

								f->minus_feature_from_weight(feature_weight_dep,
										1);

								f->minus_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1); // - prediction

								//						f->minus_feature_from_weight(feature_weight_dep,
								//								feature_weight_sum_dep,
								//								feature_count_lastupdate, total_sentence_num, 1);
							} else {
								f->minus_feature_from_weight(feature_weight_dep,
										1);
							}
							delete f;

						}

					}
				}
				for (int aIter = 0; aIter < (*it)->getLength(); aIter++) {
					if ((*it)->getHeadIndex()->at(aIter) == 0) {
						continue;
					} else {
						//						Morph::Dependence depEdge(&((*it)->getDpTokenArray()->at(
						//								(*it)->headIndex[aIter])),
						//								&((*it)->getDpTokenArray()->at(aIter)));

						if (MODE_WORD_CLUSTER) {
							FeatureSet *f = new FeatureSet(&ftmpl, &wcftmpl);

							f->extract_edge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)));

							f->extract_context_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)),
									(*it)->getDpTokenArray());

							f->extract_wcedge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)));

							f->extract_wccontext_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)),
									(*it)->getDpTokenArray());

							if (WEIGHT_AVERAGED) {

								f->plus_feature_from_weight(feature_weight_dep,
										1);

								f->plus_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1); // + gold

								//						f->plus_feature_from_weight(feature_weight_dep,
								//								feature_weight_sum_dep,
								//								feature_count_lastupdate, total_sentence_num, 1);
							} else {
								f->plus_feature_from_weight(feature_weight_dep,
										1);
							}
							delete f;

						} else {
							FeatureSet *f = new FeatureSet(&ftmpl);

							f->extract_edge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)));

							f->extract_context_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)),
									(*it)->getDpTokenArray());

							if (WEIGHT_AVERAGED) {

								f->plus_feature_from_weight(feature_weight_dep,
										1);

								f->plus_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1); // + gold

								//						f->plus_feature_from_weight(feature_weight_dep,
								//								feature_weight_sum_dep,
								//								feature_count_lastupdate, total_sentence_num, 1);
							} else {
								f->plus_feature_from_weight(feature_weight_dep,
										1);
							}
							delete f;
						}

					}
				}
				//            if (WEIGHT_AVERAGED) { // for average
				//                sentence->minus_feature_from_weight(feature_weight_sum, total_iteration_num); // - prediction
				//                (*it)->get_feature()->plus_feature_from_weight(feature_weight_sum, total_iteration_num); // + gold standard
				//            }

				//			if (WEIGHT_AVERAGED) {
				//				for (std::map<std::string, double>::iterator it =
				//						feature_weight.begin(); it != feature_weight.end(); it++) {
				//					//					if (feature_weight_sum.find(it->first)
				//					//							== feature_weight.end())
				//					//						feature_weight_sum[it->first] = it->second;
				//					//					else
				//					feature_weight_sum[it->first] += it->second;
				//				}
				//			}

				(*it)->dpsentenceClear();
				total_sentence_num++;
			}
		}

		if (MODE_TRACE) {
			std::string tempModelName = tracingHead;
			tempModelName.append("/");

			if (training_data_division > 0 && training_data_spare > 0) {
				tempModelName.append(int2string(training_data_division));
				tempModelName.append(".");
				tempModelName.append(int2string(training_data_spare));
				tempModelName.append(".");
			}
			tempModelName.append("end");

			write_temp_model_file(tempModelName);
		}

		if (WEIGHT_AVERAGED) {
			for (std::map<std::string, double>::iterator it =
					feature_weight_sum_dep.begin();
					it != feature_weight_sum_dep.end(); it++) {
				if (feature_weight_dep.find(it->first)
						== feature_weight_dep.end()) {
					cerr << "error: cannot match feature weight and weight sum"
							<< endl;
				} else {
					feature_weight_dep[it->first] =
							feature_weight_dep[it->first]
									- it->second / total_sentence_num;
				}
			}
		}
		clear_gold_data();
		return true;

	} else if (order == 2) {

		cerr << "Mode: 2nd-order" << endl;

		if (training_data_division > 0 && training_data_spare > 0) {
			cerr << "Mode: Jack-Knifing" << endl;
			read_gold_data(gsd_file, training_data_division,
					training_data_spare);
		} else {
			read_gold_data(gsd_file);
		}
		total_sentence_num = 1;

		//Morph::Decoder edgeParser;

		//         for iteration 1..5 {
		//                 for sentence 1..N {
		//                          tagger.analyzer(sentence chunked);
		//                          - collected feature;
		//                          feature gold standard;
		//                          +
		//                     }
		//             }

		cerr << "Mode: train" << endl;

		if (WEIGHT_AVERAGED) {
			cerr << "Mode: average" << endl;
		}

		if (MODE_TRACE) {
			cerr << "Mode: trace" << endl;
		}

		if (MODE_NEO) {
			cerr << "Mode: alternative decoding" << endl;
		}

		for (size_t t = 0; t < iteration; t++) {

			if (MODE_TRACE && t > 0) {

				std::string tempModelName = tracingHead;
				tempModelName.append("/");

				if (MODE_TRACE_LAST) {

					if (training_data_division > 0 && training_data_spare > 0) {
						tempModelName.append(
								int2string(training_data_division));
						tempModelName.append(".");
						tempModelName.append(int2string(training_data_spare));
						tempModelName.append(".");
					}
					tempModelName.append("last");

				} else {
					if (training_data_division > 0 && training_data_spare > 0) {
						tempModelName.append(
								int2string(training_data_division));
						tempModelName.append(".");
						tempModelName.append(int2string(training_data_spare));
						tempModelName.append(".");
					}
					tempModelName.append(int2string(t));
				}

				write_temp_model_file(tempModelName);

			}

			cerr << "ITERATION:" << t << endl;
			//		if (param->shuffle_training_data) {// shuffle training data
			//			random_shuffle(dpsentences_for_train.begin(),
			//					dpsentences_for_train.end());
			//		}

			if (MODE_SHUFFLE) { // shuffle training data
				random_shuffle(dpsentences_for_train.begin(),
						dpsentences_for_train.end());

				cerr << "Mode:shuffle" << endl;
			}

			dpsentences_for_train[0]->printHeadIndex();

			for (std::vector<Dpsentence*>::iterator it =
					dpsentences_for_train.begin();
					it != dpsentences_for_train.end(); it++) {
				//new_sentence_analyze((*it)->get_sentence()); // get the best path

				if (total_sentence_num % 100 == 0)
					cerr << "sentence:" << total_sentence_num << endl;

				//cerr << "sequence started" << endl;
				(*it)->initializeScoreCube();
				//cerr << "initialized" << endl;

				if (MODE_WORD_CLUSTER) {
					(*it)->evaluateSiblingScoreMatrixWc(&ftmpl, &wcftmpl,
							total_sentence_num);
				} else {
					(*it)->evaluateSiblingScoreMatrix(&ftmpl,
							total_sentence_num);
				}

				//cerr << "evaluated" << endl;

				if (MODE_NEO) {
					(*it)->decodeSiblingFactorsNeo();
				} else {
					(*it)->decodeSiblingFactors();
				}
				//cerr << "decoded" << endl;
				//edgeParser.setSize((*it)->getLength());

				//			edgeParser.decodeEdgeFactors((*it)->getLength(), (*it)->scoreMtx,
				//					&t,&ftmpl);

				//edgeParser.printPredict();
				//			(*it)->printPredict();
				//			(*it)->printHeadIndex();
				if (MODE_VERBOSE)
					(*it)->comparePrint();

				//						FeatureSet featureSet(&ftmpl);
				//						Dptoken new_dptoken1("a", "B", 1, 2);
				//						Dptoken new_dptoken2("c", "D", 2, 3);
				//						featureSet.extract_edge_feature(&new_dptoken1, &new_dptoken2);
				//						cerr << "we're here!edgepass" << t << endl;
				//						featureSet.extract_context_feature(&new_dptoken1, &new_dptoken2,&((*it)->dpTokenArray));
				//						cerr << "we're here!contextpass" << t << endl;

				for (int pIter = 1; pIter < (*it)->getLength() + 1; pIter++) {
					if ((*it)->getPredict()->at(pIter) == 0) {
						continue;
					} else {
						//						Morph::Dependence depEdge(&((*it)->getDpTokenArray()->at(
						//								edgeParser.predict[pIter])),
						//								&((*it)->getDpTokenArray()->at(pIter)));

						if (MODE_WORD_CLUSTER) {

							FeatureSet *f = new FeatureSet(&ftmpl, &wcftmpl);

							int sibBias = 0;

							f->extract_edge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)));

							f->extract_context_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)),
									(*it)->getDpTokenArray());

							f->extract_wcedge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)));

							f->extract_wccontext_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)),
									(*it)->getDpTokenArray());

							if ((*it)->getPredict()->at(pIter) < pIter) {

								for (int sibIter = pIter - 1;
										sibIter > (*it)->getPredict()->at(pIter);
										sibIter--) {
									if ((*it)->getPredict()->at(sibIter)
											== (*it)->getPredict()->at(pIter)) {
										sibBias = sibIter;
										break;
									}
								}

								if (sibBias > 0) {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)),
											&((*it)->getDpTokenArray()->at(
													sibBias - 1)));

									f->extract_wcsibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)),
											&((*it)->getDpTokenArray()->at(
													sibBias - 1)));

								} else {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)));

									f->extract_wcsibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)));
								}

							} else if ((*it)->getPredict()->at(pIter) > pIter) {

								for (int sibIter = pIter + 1;
										sibIter < (*it)->getPredict()->at(pIter);
										sibIter++) {
									if ((*it)->getPredict()->at(sibIter)
											== (*it)->getPredict()->at(pIter)) {
										sibBias = sibIter;
										break;
									}
								}

								if (sibBias > 0) {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)),
											&((*it)->getDpTokenArray()->at(
													sibBias - 1)));

									f->extract_wcsibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)),
											&((*it)->getDpTokenArray()->at(
													sibBias - 1)));

								} else {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)));

									f->extract_wcsibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)));
								}

							} else {
								cerr << "error: modifier equals to head;; "
										<< endl;
							}

							//						cout<<"predict score "<<pIter<<"\t"<<(*it)->getPredict()->at(pIter)<<"\t"<<sibBias<<"\t"<<f->calc_inner_product_with_weight(
							//								feature_weight_dep) <<endl;

							if (WEIGHT_AVERAGED) {

								f->minus_feature_from_weight(feature_weight_dep,
										1);

								f->minus_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1); // - prediction

								//						f->minus_feature_from_weight(feature_weight_dep,
								//								feature_weight_sum_dep,
								//								feature_count_lastupdate, total_sentence_num, 1);
							} else {
								f->minus_feature_from_weight(feature_weight_dep,
										1);
							}
							delete f;

						} else {

							FeatureSet *f = new FeatureSet(&ftmpl);

							int sibBias = 0;

							f->extract_edge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)));

							f->extract_context_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getPredict()->at(pIter) - 1)),
									&((*it)->getDpTokenArray()->at(pIter - 1)),
									(*it)->getDpTokenArray());

							if ((*it)->getPredict()->at(pIter) < pIter) {

								for (int sibIter = pIter - 1;
										sibIter > (*it)->getPredict()->at(pIter);
										sibIter--) {
									if ((*it)->getPredict()->at(sibIter)
											== (*it)->getPredict()->at(pIter)) {
										sibBias = sibIter;
										break;
									}
								}

								if (sibBias > 0) {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)),
											&((*it)->getDpTokenArray()->at(
													sibBias - 1)));
								} else {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)));
								}

							} else if ((*it)->getPredict()->at(pIter) > pIter) {

								for (int sibIter = pIter + 1;
										sibIter < (*it)->getPredict()->at(pIter);
										sibIter++) {
									if ((*it)->getPredict()->at(sibIter)
											== (*it)->getPredict()->at(pIter)) {
										sibBias = sibIter;
										break;
									}
								}

								if (sibBias > 0) {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)),
											&((*it)->getDpTokenArray()->at(
													sibBias - 1)));
								} else {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getPredict()->at(
															pIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													pIter - 1)));
								}

							} else {
								cerr << "error: modifier equals to head;; "
										<< endl;
							}

							//						cout<<"predict score "<<pIter<<"\t"<<(*it)->getPredict()->at(pIter)<<"\t"<<sibBias<<"\t"<<f->calc_inner_product_with_weight(
							//								feature_weight_dep) <<endl;

							if (WEIGHT_AVERAGED) {

								f->minus_feature_from_weight(feature_weight_dep,
										1);

								f->minus_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1); // - prediction

								//						f->minus_feature_from_weight(feature_weight_dep,
								//								feature_weight_sum_dep,
								//								feature_count_lastupdate, total_sentence_num, 1);
							} else {
								f->minus_feature_from_weight(feature_weight_dep,
										1);
							}
							delete f;

						}
					}
				}
				for (int aIter = 0; aIter < (*it)->getLength(); aIter++) {
					if ((*it)->getHeadIndex()->at(aIter) == 0) {
						continue;
					} else {
						//						Morph::Dependence depEdge(&((*it)->getDpTokenArray()->at(
						//								(*it)->headIndex[aIter])),
						//								&((*it)->getDpTokenArray()->at(aIter)));

						if (MODE_WORD_CLUSTER) {

							FeatureSet *f = new FeatureSet(&ftmpl, &wcftmpl);

							int sibBias = -1;

							f->extract_edge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)));

							f->extract_context_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)),
									(*it)->getDpTokenArray());

							f->extract_wcedge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)));

							f->extract_wccontext_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)),
									(*it)->getDpTokenArray());

							if ((*it)->getHeadIndex()->at(aIter) < aIter + 1) {

								for (int sibIter = aIter - 1;
										sibIter
												> (*it)->getHeadIndex()->at(
														aIter) - 1; sibIter--) {
									if ((*it)->getHeadIndex()->at(sibIter)
											== (*it)->getHeadIndex()->at(
													aIter)) {
										sibBias = sibIter;
										break;
									}
								}

								if (sibBias >= 0) {

									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)),
											&((*it)->getDpTokenArray()->at(
													sibBias)));

									f->extract_wcsibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)),
											&((*it)->getDpTokenArray()->at(
													sibBias)));

								} else {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)));

									f->extract_wcsibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)));

								}

							} else if ((*it)->getHeadIndex()->at(aIter)
									> aIter + 1) {

								for (int sibIter = aIter + 1;
										sibIter
												< (*it)->getHeadIndex()->at(
														aIter) - 1; sibIter++) {
									if ((*it)->getHeadIndex()->at(sibIter)
											== (*it)->getHeadIndex()->at(
													aIter)) {
										sibBias = sibIter;
										break;
									}
								}

								if (sibBias >= 0) {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)),
											&((*it)->getDpTokenArray()->at(
													sibBias)));

									f->extract_wcsibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)),
											&((*it)->getDpTokenArray()->at(
													sibBias)));

								} else {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)));

									f->extract_wcsibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)));

								}

							} else {
								cerr << "error: modifier equals to head;; "
										<< endl;
							}

							//						cout<<"gold score "<<aIter+1<<"\t"<<(*it)->getHeadIndex()->at(aIter) <<"\t"<<sibBias+1<<"\t"<<f->calc_inner_product_with_weight(
							//								feature_weight_dep) <<endl;

							if (WEIGHT_AVERAGED) {

								f->plus_feature_from_weight(feature_weight_dep,
										1);

								f->plus_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1); // + gold

								//						f->plus_feature_from_weight(feature_weight_dep,
								//								feature_weight_sum_dep,
								//								feature_count_lastupdate, total_sentence_num, 1);
							} else {
								f->plus_feature_from_weight(feature_weight_dep,
										1);
							}
							delete f;

						} else {
							FeatureSet *f = new FeatureSet(&ftmpl);

							int sibBias = -1;

							f->extract_edge_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)));

							f->extract_context_feature(
									&((*it)->getDpTokenArray()->at(
											(*it)->getHeadIndex()->at(aIter)
													- 1)),
									&((*it)->getDpTokenArray()->at(aIter)),
									(*it)->getDpTokenArray());

							if ((*it)->getHeadIndex()->at(aIter) < aIter + 1) {

								for (int sibIter = aIter - 1;
										sibIter
												> (*it)->getHeadIndex()->at(
														aIter) - 1; sibIter--) {
									if ((*it)->getHeadIndex()->at(sibIter)
											== (*it)->getHeadIndex()->at(
													aIter)) {
										sibBias = sibIter;
										break;
									}
								}

								if (sibBias >= 0) {

									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)),
											&((*it)->getDpTokenArray()->at(
													sibBias)));
								} else {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)));

								}

							} else if ((*it)->getHeadIndex()->at(aIter)
									> aIter + 1) {

								for (int sibIter = aIter + 1;
										sibIter
												< (*it)->getHeadIndex()->at(
														aIter) - 1; sibIter++) {
									if ((*it)->getHeadIndex()->at(sibIter)
											== (*it)->getHeadIndex()->at(
													aIter)) {
										sibBias = sibIter;
										break;
									}
								}

								if (sibBias >= 0) {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)),
											&((*it)->getDpTokenArray()->at(
													sibBias)));

								} else {
									f->extract_sibling_feature(
											&((*it)->getDpTokenArray()->at(
													(*it)->getHeadIndex()->at(
															aIter) - 1)),
											&((*it)->getDpTokenArray()->at(
													aIter)));

								}

							} else {
								cerr << "error: modifier equals to head;; "
										<< endl;
							}

							//						cout<<"gold score "<<aIter+1<<"\t"<<(*it)->getHeadIndex()->at(aIter) <<"\t"<<sibBias+1<<"\t"<<f->calc_inner_product_with_weight(
							//								feature_weight_dep) <<endl;

							if (WEIGHT_AVERAGED) {

								f->plus_feature_from_weight(feature_weight_dep,
										1);

								f->plus_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1); // + gold

								//						f->plus_feature_from_weight(feature_weight_dep,
								//								feature_weight_sum_dep,
								//								feature_count_lastupdate, total_sentence_num, 1);
							} else {
								f->plus_feature_from_weight(feature_weight_dep,
										1);
							}
							delete f;
						}

					}
				}
				//            if (WEIGHT_AVERAGED) { // for average
				//                sentence->minus_feature_from_weight(feature_weight_sum, total_iteration_num); // - prediction
				//                (*it)->get_feature()->plus_feature_from_weight(feature_weight_sum, total_iteration_num); // + gold standard
				//            }

				//			if (WEIGHT_AVERAGED) {
				//				for (std::map<std::string, double>::iterator it =
				//						feature_weight.begin(); it != feature_weight.end(); it++) {
				//					//					if (feature_weight_sum.find(it->first)
				//					//							== feature_weight.end())
				//					//						feature_weight_sum[it->first] = it->second;
				//					//					else
				//					feature_weight_sum[it->first] += it->second;
				//				}
				//			}

				(*it)->dpsentenceClear2nd();
				total_sentence_num++;
			}
		}

		if (MODE_TRACE) {
			std::string tempModelName = tracingHead;
			tempModelName.append("/");

			if (training_data_division > 0 && training_data_spare > 0) {
				tempModelName.append(int2string(training_data_division));
				tempModelName.append(".");
				tempModelName.append(int2string(training_data_spare));
				tempModelName.append(".");
			}
			tempModelName.append("end");

			write_temp_model_file(tempModelName);
		}

		if (WEIGHT_AVERAGED) {
			for (std::map<std::string, double>::iterator it =
					feature_weight_sum_dep.begin();
					it != feature_weight_sum_dep.end(); it++) {
				if (feature_weight_dep.find(it->first)
						== feature_weight_dep.end()) {
					cerr << "error: cannot match feature weight and weight sum"
							<< endl;
				} else {
					feature_weight_dep[it->first] =
							feature_weight_dep[it->first]
									- it->second / total_sentence_num;
				}
			}
		}
		clear_gold_data();
		return true;

	}

	cerr << "error: higher-order parsing mode not implemented" << endl;
	return false;
}

bool DepParser::exhaust_train(const std::string &gsd_file, unsigned int order,
		unsigned int iteration) {

	if (order == 1) {
//todo
	} else if (order == 2) {

		cerr << "Mode: 2nd-order" << endl;
		cerr << "Mode: exhaust" << endl;

		if (training_data_division > 0 && training_data_spare > 0) {
			cerr << "Mode: Jack-Knifing" << endl;
			read_gold_data(gsd_file, training_data_division,
					training_data_spare);
		} else {
			read_gold_data(gsd_file);
		}
		total_sentence_num = 1;

		int sentence_num_count = 0;
		int sentence_num_count_side = 0;

		std::vector<std::vector<std::vector<std::vector<FeatureSet> > > > fmtx;

		for (std::vector<Dpsentence*>::iterator it =
				dpsentences_for_train.begin();
				it != dpsentences_for_train.end(); it++) {

			std::vector<std::vector<std::vector<FeatureSet> > > fsentence;

			for (int i = 0; i < (*it)->getLength(); i++) {
				std::vector<std::vector<FeatureSet> > fcoll;
				for (int j = 0; j < (*it)->getLength(); j++) {
					std::vector<FeatureSet> fvec;
					if (i == j) {
						FeatureSet f = FeatureSet(&ftmpl);
						fvec.push_back(f);
					} else {
						for (int k = 0; k < abs(i - j); k++) {
							FeatureSet f = FeatureSet(&ftmpl);
							fvec.push_back(f);
						}
					}
					fcoll.push_back(fvec);
				}
				fsentence.push_back(fcoll);
			}
			fmtx.push_back(fsentence);

			sentence_num_count++;

			cerr << "prepared " << sentence_num_count << " sentences" << endl;
		}

		for (std::vector<Dpsentence*>::iterator it =
				dpsentences_for_train.begin();
				it != dpsentences_for_train.end(); it++) {

			(*it)->collectSiblingFeatureStrings(&ftmpl,
					fmtx[sentence_num_count_side]);

			sentence_num_count_side++;

			cerr << "collected " << sentence_num_count_side << " sentences"
					<< endl;
		}

		//Morph::Decoder edgeParser;

		//         for iteration 1..5 {
		//                 for sentence 1..N {
		//                          tagger.analyzer(sentence chunked);
		//                          - collected feature;
		//                          feature gold standard;
		//                          +
		//                     }
		//             }

		cerr << "Mode: train" << endl;
		if (param->shuffle_training_data) {
			cerr << "Mode: shuffle" << endl;
		}
		if (WEIGHT_AVERAGED) {
			cerr << "Mode: average" << endl;
		}

		for (size_t t = 0; t < iteration; t++) {

			int sentence_num_count_iter = 0;
//			if (t > 0 && t % 10 == 0) {
//				std::string tempModelName = "/home/shen/data/tempModels/";
//				tempModelName.append(int2string(t));
//				write_temp_model_file(tempModelName);
//			}

			cerr << "ITERATION:" << t << endl;
			//		if (param->shuffle_training_data) {// shuffle training data
			//			random_shuffle(dpsentences_for_train.begin(),
			//					dpsentences_for_train.end());
			//		}

			if (MODE_SHUFFLE) { // shuffle training data
				random_shuffle(dpsentences_for_train.begin(),
						dpsentences_for_train.end());

				cerr << "Mode:shuffle" << endl;
			}

			dpsentences_for_train[0]->printHeadIndex();

			for (std::vector<Dpsentence*>::iterator it =
					dpsentences_for_train.begin();
					it != dpsentences_for_train.end(); it++) {
				//new_sentence_analyze((*it)->get_sentence()); // get the best path

				if (total_sentence_num % 100 == 0)
					cerr << "sentence:" << total_sentence_num << endl;

				//cerr << "sequence started" << endl;
				(*it)->initializeScoreCube();
				//cerr << "initialized" << endl;
				(*it)->evaluateScoreFormCollection(
						fmtx[sentence_num_count_iter]);
				sentence_num_count_iter++;
				//cerr << "evaluated" << endl;

				//(*it)->decodeSiblingFactorsNeo();
				(*it)->decodeSiblingFactors();

				//cerr << "decoded" << endl;
				//edgeParser.setSize((*it)->getLength());

				//			edgeParser.decodeEdgeFactors((*it)->getLength(), (*it)->scoreMtx,
				//					&t,&ftmpl);

				//edgeParser.printPredict();
				//			(*it)->printPredict();
				//			(*it)->printHeadIndex();
				//(*it)->comparePrint();

				//						FeatureSet featureSet(&ftmpl);
				//						Dptoken new_dptoken1("a", "B", 1, 2);
				//						Dptoken new_dptoken2("c", "D", 2, 3);
				//						featureSet.extract_edge_feature(&new_dptoken1, &new_dptoken2);
				//						cerr << "we're here!edgepass" << t << endl;
				//						featureSet.extract_context_feature(&new_dptoken1, &new_dptoken2,&((*it)->dpTokenArray));
				//						cerr << "we're here!contextpass" << t << endl;

				for (int pIter = 1; pIter < (*it)->getLength() + 1; pIter++) {
					if ((*it)->getPredict()->at(pIter) == 0) {
						continue;
					} else {
						//						Morph::Dependence depEdge(&((*it)->getDpTokenArray()->at(
						//								edgeParser.predict[pIter])),
						//								&((*it)->getDpTokenArray()->at(pIter)));

						FeatureSet *f = new FeatureSet(&ftmpl);

						int sibBias = 0;

						f->extract_edge_feature(
								&((*it)->getDpTokenArray()->at(
										(*it)->getPredict()->at(pIter) - 1)),
								&((*it)->getDpTokenArray()->at(pIter - 1)));

						f->extract_context_feature(
								&((*it)->getDpTokenArray()->at(
										(*it)->getPredict()->at(pIter) - 1)),
								&((*it)->getDpTokenArray()->at(pIter - 1)),
								(*it)->getDpTokenArray());

						if ((*it)->getPredict()->at(pIter) < pIter) {

							for (int sibIter = pIter - 1;
									sibIter > (*it)->getPredict()->at(pIter);
									sibIter--) {
								if ((*it)->getPredict()->at(sibIter)
										== (*it)->getPredict()->at(pIter)) {
									sibBias = sibIter;
									break;
								}
							}

							if (sibBias > 0) {
								f->extract_sibling_feature(
										&((*it)->getDpTokenArray()->at(
												(*it)->getPredict()->at(pIter)
														- 1)),
										&((*it)->getDpTokenArray()->at(
												pIter - 1)),
										&((*it)->getDpTokenArray()->at(
												sibBias - 1)));
							} else {
								f->extract_sibling_feature(
										&((*it)->getDpTokenArray()->at(
												(*it)->getPredict()->at(pIter)
														- 1)),
										&((*it)->getDpTokenArray()->at(
												pIter - 1)));
							}

						} else if ((*it)->getPredict()->at(pIter) > pIter) {

							for (int sibIter = pIter + 1;
									sibIter < (*it)->getPredict()->at(pIter);
									sibIter++) {
								if ((*it)->getPredict()->at(sibIter)
										== (*it)->getPredict()->at(pIter)) {
									sibBias = sibIter;
									break;
								}
							}

							if (sibBias > 0) {
								f->extract_sibling_feature(
										&((*it)->getDpTokenArray()->at(
												(*it)->getPredict()->at(pIter)
														- 1)),
										&((*it)->getDpTokenArray()->at(
												pIter - 1)),
										&((*it)->getDpTokenArray()->at(
												sibBias - 1)));
							} else {
								f->extract_sibling_feature(
										&((*it)->getDpTokenArray()->at(
												(*it)->getPredict()->at(pIter)
														- 1)),
										&((*it)->getDpTokenArray()->at(
												pIter - 1)));
							}

						} else {
							cerr << "error: modifier equals to head;; " << endl;
						}

//						cout<<"predict score "<<pIter<<"\t"<<(*it)->getPredict()->at(pIter)<<"\t"<<sibBias<<"\t"<<f->calc_inner_product_with_weight(
//								feature_weight_dep) <<endl;

						if (WEIGHT_AVERAGED) {

							f->minus_feature_from_weight(feature_weight_dep, 1);

							f->minus_feature_from_weight(feature_weight_sum_dep,
									total_sentence_num - 1); // - prediction

							//						f->minus_feature_from_weight(feature_weight_dep,
							//								feature_weight_sum_dep,
							//								feature_count_lastupdate, total_sentence_num, 1);
						} else {
							f->minus_feature_from_weight(feature_weight_dep, 1);
						}
						delete f;
					}
				}
				for (int aIter = 0; aIter < (*it)->getLength(); aIter++) {
					if ((*it)->getHeadIndex()->at(aIter) == 0) {
						continue;
					} else {
						//						Morph::Dependence depEdge(&((*it)->getDpTokenArray()->at(
						//								(*it)->headIndex[aIter])),
						//								&((*it)->getDpTokenArray()->at(aIter)));

						FeatureSet *f = new FeatureSet(&ftmpl);

						int sibBias = -1;

						f->extract_edge_feature(
								&((*it)->getDpTokenArray()->at(
										(*it)->getHeadIndex()->at(aIter) - 1)),
								&((*it)->getDpTokenArray()->at(aIter)));

						f->extract_context_feature(
								&((*it)->getDpTokenArray()->at(
										(*it)->getHeadIndex()->at(aIter) - 1)),
								&((*it)->getDpTokenArray()->at(aIter)),
								(*it)->getDpTokenArray());

						if ((*it)->getHeadIndex()->at(aIter) < aIter + 1) {

							for (int sibIter = aIter - 1;
									sibIter
											> (*it)->getHeadIndex()->at(aIter)
													- 1; sibIter--) {
								if ((*it)->getHeadIndex()->at(sibIter)
										== (*it)->getHeadIndex()->at(aIter)) {
									sibBias = sibIter;
									break;
								}
							}

							if (sibBias >= 0) {

								f->extract_sibling_feature(
										&((*it)->getDpTokenArray()->at(
												(*it)->getHeadIndex()->at(aIter)
														- 1)),
										&((*it)->getDpTokenArray()->at(aIter)),
										&((*it)->getDpTokenArray()->at(sibBias)));
							} else {
								f->extract_sibling_feature(
										&((*it)->getDpTokenArray()->at(
												(*it)->getHeadIndex()->at(aIter)
														- 1)),
										&((*it)->getDpTokenArray()->at(aIter)));

							}

						} else if ((*it)->getHeadIndex()->at(aIter)
								> aIter + 1) {

							for (int sibIter = aIter + 1;
									sibIter
											< (*it)->getHeadIndex()->at(aIter)
													- 1; sibIter++) {
								if ((*it)->getHeadIndex()->at(sibIter)
										== (*it)->getHeadIndex()->at(aIter)) {
									sibBias = sibIter;
									break;
								}
							}

							if (sibBias >= 0) {
								f->extract_sibling_feature(
										&((*it)->getDpTokenArray()->at(
												(*it)->getHeadIndex()->at(aIter)
														- 1)),
										&((*it)->getDpTokenArray()->at(aIter)),
										&((*it)->getDpTokenArray()->at(sibBias)));

							} else {
								f->extract_sibling_feature(
										&((*it)->getDpTokenArray()->at(
												(*it)->getHeadIndex()->at(aIter)
														- 1)),
										&((*it)->getDpTokenArray()->at(aIter)));

							}

						} else {
							cerr << "error: modifier equals to head;; " << endl;
						}

//						cout<<"gold score "<<aIter+1<<"\t"<<(*it)->getHeadIndex()->at(aIter) <<"\t"<<sibBias+1<<"\t"<<f->calc_inner_product_with_weight(
//								feature_weight_dep) <<endl;

						if (WEIGHT_AVERAGED) {

							f->plus_feature_from_weight(feature_weight_dep, 1);

							f->plus_feature_from_weight(feature_weight_sum_dep,
									total_sentence_num - 1); // + gold

							//						f->plus_feature_from_weight(feature_weight_dep,
							//								feature_weight_sum_dep,
							//								feature_count_lastupdate, total_sentence_num, 1);
						} else {
							f->plus_feature_from_weight(feature_weight_dep, 1);
						}
						delete f;
					}
				}
				//            if (WEIGHT_AVERAGED) { // for average
				//                sentence->minus_feature_from_weight(feature_weight_sum, total_iteration_num); // - prediction
				//                (*it)->get_feature()->plus_feature_from_weight(feature_weight_sum, total_iteration_num); // + gold standard
				//            }

				//			if (WEIGHT_AVERAGED) {
				//				for (std::map<std::string, double>::iterator it =
				//						feature_weight.begin(); it != feature_weight.end(); it++) {
				//					//					if (feature_weight_sum.find(it->first)
				//					//							== feature_weight.end())
				//					//						feature_weight_sum[it->first] = it->second;
				//					//					else
				//					feature_weight_sum[it->first] += it->second;
				//				}
				//			}

				(*it)->dpsentenceClear2nd();
				total_sentence_num++;
			}
		}

		if (WEIGHT_AVERAGED) {
			for (std::map<std::string, double>::iterator it =
					feature_weight_sum_dep.begin();
					it != feature_weight_sum_dep.end(); it++) {
				if (feature_weight_dep.find(it->first)
						== feature_weight_dep.end()) {
					cerr << "error: cannot match feature weight and weight sum"
							<< endl;
				} else {
					feature_weight_dep[it->first] =
							feature_weight_dep[it->first]
									- it->second / total_sentence_num;
				}
			}
		}
		clear_gold_data();
		return true;

	}

	cerr << "error: higher-order parsing mode not implemented" << endl;
	return false;
}

bool DepParser::test(unsigned int order) {

	size_t accu_nom = 0;
	size_t accu_denom = 0;

	if (training_data_division > 0 && training_data_spare > 0) {
		cerr << "Mode: Jack-Knifing" << endl;
		read_complement_gold_data(training_data_division, training_data_spare);
	} else {
		read_gold_data();
	}

	total_sentence_num = 0;

	//Morph::Decoder edgeParser;

	//         for iteration 1..5 {
	//                 for sentence 1..N {
	//                          tagger.analyzer(sentence chunked);
	//                          - collected feature;
	//                          feature gold standard;
	//                          +
	//                     }
	//             }

	cerr << "Mode: test" << endl;

	if (MODE_NEO) {
		cerr << "Mode: alternative decoding" << endl;
	}

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	for (std::vector<Dpsentence*>::iterator it = dpsentences_for_train.begin();
			it != dpsentences_for_train.end(); it++) {
		//new_sentence_analyze((*it)->get_sentence()); // get the best path

		//if (total_sentence_num % 100 == 0)
		//cerr << "sentence:" << total_sentence_num << endl;
		if (order == 1) {

			(*it)->initializeScoreMatrix();

			if (MODE_WORD_CLUSTER) {
				(*it)->evaluateEdgeScoreMatrixWc(&ftmpl, &wcftmpl,
						total_sentence_num);
			} else {
				(*it)->evaluateEdgeScoreMatrix(&ftmpl, total_sentence_num);
			}

			(*it)->decodeEdgeFactors();

		} else if (order == 2) {

			(*it)->initializeScoreCube();

			if (MODE_WORD_CLUSTER) {
				(*it)->evaluateSiblingScoreMatrixWc(&ftmpl, &wcftmpl,
						total_sentence_num);
			} else {
				(*it)->evaluateSiblingScoreMatrix(&ftmpl, total_sentence_num);
			}

			if (MODE_NEO) {
				double showScore = (*it)->decodeSiblingFactorsNeo();
				cerr << showScore << endl;
			} else {
				(*it)->decodeSiblingFactors();
			}

		}

		//(*it)->printPredict();
		//(*it)->printHeadIndex();
		(*it)->comparePrint();
		//(*it)->printResult();

		(*it)->checkAccuracy(accu_nom, accu_denom);
		double accu_percentage;
		if (accu_denom > 0)
			accu_percentage = accu_nom / accu_denom;
		cerr << "accuracy: " << accu_nom << "\t" << accu_denom << endl;

		if (order == 1)
			(*it)->dpsentenceClear();
		else if (order == 2)
			(*it)->dpsentenceClear2nd();

		total_sentence_num++;
	}

	//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
	//         new_sentence_analyze((*it)->get_sentence()); // get the best path
	//         print_best_path();
	//         sentence_clear();
	//     }

	clear_gold_data();
	return true;
}

bool DepParser::rerank_test(const std::string &gsd_file,
		unsigned int k_testing) {

	size_t accu_nom = 0;
	size_t accu_denom = 0;

	size_t oracle_accu_nom = 0;
	size_t oracle_accu_denom = 0;

	read_rerank_gold_data(gsd_file);

	total_sentence_num = 0;

	size_t length_threshold = 0;

	cerr << "Mode: rerank test" << endl;

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	for (std::vector<Dpsentence*>::iterator it = dpsentences_for_train.begin();
			it != dpsentences_for_train.end(); it++) {
		//new_sentence_analyze((*it)->get_sentence()); // get the best path

		//if (total_sentence_num % 100 == 0)
		//cerr << "sentence:" << total_sentence_num << endl;
		if ((*it)->getLength() > length_threshold) {

			size_t topK = k_testing;
			if (topK > (*it)->getOracle()->size()) {
				topK = (*it)->getOracle()->size();
			}

			double MaxScore = -1e16;
			size_t picked_oracle = 0;
			double MaxAccu = 0;
			size_t oracle_position = 0;

			//FeatureSet *f = new FeatureSet(&ftmpl)[(*it)->getOracle()->size()];
//			std::vector<Morph::FeatureSet> rf;
//			for (int i = 0; i < topK; i++) {
//				FeatureSet f(&ftmpl);
//				rf.push_back(f);
//				//cerr << (*it)->getDepTree()->size() << endl;
//			}

			for (int k_iter = 0; k_iter < topK; k_iter++) {
				Dptree* dptree = new Dptree();
				dptree->buildTree((*it)->getDpTokenArray(),
						&((*it)->getDepTree()->at(k_iter)));
				//cerr << "we're here" << endl;

				double tempScore = (*it)->getParseScore()->at(
						k_iter)/PARSE_SCORE_FACTOR;

				cerr << tempScore << " ";

				for (std::vector<Morph::DptreeNode>::iterator tn_it =
						dptree->dpTreeNodeAry.begin();
						tn_it != dptree->dpTreeNodeAry.end(); tn_it++) {

					FeatureSet f = FeatureSet(&ftmpl);

					f.extract_rerank_feature(dptree, &(*tn_it), 5, 5, 3);
					f.extract_depth_feature(dptree, &(*tn_it), 4);
					f.extract_ancestor_feature(dptree, &(*tn_it), 4);

					tempScore += f.calc_rerank_inner_product_with_weight(
							feature_weight_dep);

				}

				cerr << tempScore << endl;

				if (MaxScore < tempScore) {
					MaxScore = tempScore;
					picked_oracle = k_iter;
				}

				if (MaxAccu < (*it)->getPredictAccu()->at(k_iter)) {
					MaxAccu = (*it)->getPredictAccu()->at(k_iter);
					oracle_position = k_iter;
				}

				delete dptree;
				//cerr << "position reached" << endl;
			}

			(*it)->comparePrint((*it)->getDepTree()->at(picked_oracle),
					(*it)->getDepTree()->at(oracle_position));

			(*it)->checkAccuracy(accu_nom, accu_denom,
					(*it)->getDepTree()->at(picked_oracle));

			(*it)->checkAccuracy(oracle_accu_nom, oracle_accu_denom,
					(*it)->getDepTree()->at(oracle_position));

			double accu_percentage;
			if (accu_denom > 0)
				accu_percentage = accu_nom / accu_denom;
			cerr << "accuracy: " << accu_nom << "\t" << accu_denom << endl;

			double oracle_accu_percentage;
			if (oracle_accu_denom > 0)
				oracle_accu_percentage = oracle_accu_nom / oracle_accu_denom;
			cerr << "oracle accuracy: " << oracle_accu_nom << "\t"
					<< oracle_accu_denom << endl;

			cerr << "sentence:" << total_sentence_num << "\t" << "origin "
					<< (*it)->getPredictAccu()->at(0) << "\t" << "picked "
					<< (*it)->getPredictAccu()->at(picked_oracle) << endl;
			cerr << "" << endl;
		} else {

			//(*it)->comparePrint();

			(*it)->checkAccuracy(accu_nom, accu_denom,
					(*it)->getDepTree()->at(0));

			double accu_percentage;
			if (accu_denom > 0)
				accu_percentage = accu_nom / accu_denom;
			cerr << "accuracy: " << accu_nom << "\t" << accu_denom << endl;

		}
		total_sentence_num++;
	}

	clear_gold_data();
	return true;
}

bool DepParser::rerank(const std::string &gsd_file, unsigned int k_training,
		unsigned int iteration) {

	if (MODE_MODEL_REFINE) {
		cerr << "Mode: refine model" << endl;
	}

	read_rerank_gold_data(gsd_file);

	total_sentence_num = 1;

	cerr << "Mode: rerank" << endl;

	if (WEIGHT_AVERAGED) {

		cerr << "Mode: average" << endl;
	}

	int successed = 0;
	int failed = 0;
	//Morph::Decoder edgeParser;

	//         for iteration 1..5 {
	//                 for sentence 1..N {
	//                          tagger.analyzer(sentence chunked);
	//                          - collected feature;
	//                          feature gold standard;
	//                          +
	//                     }
	//             }
	for (size_t t = 0; t < iteration; t++) {

		//			if (t > 0 && t % 10 == 0) {
		//				std::string tempModelName = "/home/shen/data/tempModels/";
		//				tempModelName.append(int2string(t));
		//				write_temp_model_file(tempModelName);
		//			}

		cerr << "ITERATION:" << t << endl;
		if (MODE_SHUFFLE) { // shuffle training data
			random_shuffle(dpsentences_for_train.begin(),
					dpsentences_for_train.end());

			cerr << "Mode:shuffle" << endl;
		}
		//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

		for (std::vector<Dpsentence*>::iterator it =
				dpsentences_for_train.begin();
				it != dpsentences_for_train.end(); it++) {
			//new_sentence_analyze((*it)->get_sentence()); // get the best path

			//if (total_sentence_num % 100 == 0)
			//cerr << "sentence:" << total_sentence_num << endl;

			size_t topK = k_training;

			if (topK > (*it)->getOracle()->size()) {
				topK = (*it)->getOracle()->size();
			}

			double MaxScore = -1e16;
			double MaxAccu = 0;
			size_t picked_oracle = 0;

			//FeatureSet *f = new FeatureSet(&ftmpl)[(*it)->getOracle()->size()];
			std::vector<std::vector<Morph::FeatureSet> > rfMtx;

			for (int i = 0; i < topK; i++) {
				std::vector<Morph::FeatureSet> rfVec;
				rfMtx.push_back(rfVec);
				//cerr << (*it)->getDepTree()->size() << endl;
			}

			for (int k_iter = 0; k_iter < topK; k_iter++) {

				Dptree* dptree = new Dptree();
				dptree->buildTree((*it)->getDpTokenArray(),
						&((*it)->getDepTree()->at(k_iter)));
				//cerr << "we're here" << endl;
				//f->extract_depth_feature((*it)->getDpTokenArray());
				//f->extract_breath_feature((*it)->getDpTokenArray());

				double tempScore = (*it)->getParseScore()->at(
						k_iter)/PARSE_SCORE_FACTOR;

				for (std::vector<Morph::DptreeNode>::iterator tn_it =
						dptree->dpTreeNodeAry.begin();
						tn_it != dptree->dpTreeNodeAry.end(); tn_it++) {

					FeatureSet f = FeatureSet(&ftmpl);

					f.extract_rerank_feature(dptree, &(*tn_it), 5, 5, 3);
					f.extract_depth_feature(dptree, &(*tn_it), 4);
					f.extract_ancestor_feature(dptree, &(*tn_it), 4);

					tempScore += f.calc_rerank_inner_product_with_weight(
							feature_weight_dep);

					rfMtx[k_iter].push_back(f);
				}

//				for (std::set<std::string>::iterator it = rf[k_iter].rfset.begin(); it != rf[k_iter].rfset.end();
//							it++) {
//						cout<<(*it)<<endl;
//					}
//				cout<<endl;

				cerr << tempScore << endl;

				if (MaxScore < tempScore) {
					MaxScore = tempScore;
					picked_oracle = k_iter;
				}

				if (MaxAccu < (*it)->getPredictAccu()->at(k_iter)) {
					MaxAccu = (*it)->getPredictAccu()->at(k_iter);
				}

//				for (int p_iter = 0; p_iter < (*it)->getDpTokenArray()->size();
//						p_iter++) {
//					rfMtx[k_iter][p_iter].plus_rerank_feature_from_weight(
//							feature_weight_dep, 1);
//				}

				delete dptree;
				//cerr << "position reached" << endl;
			}

			if ((*it)->getPredictAccu()->at(picked_oracle) < MaxAccu) {

				int oracleMark;

				int oracleCount = 0;

				for (int i = 0; i < topK; i++) {
					if ((*it)->getPredictAccu()->at(i) == MaxAccu) {

						oracleCount++;

					}
				}

				if (oracleCount == 0)
					cerr << "oracle not selected" << endl;

				double updateStepSize = 1 / oracleCount;

				double sumStepSize = total_sentence_num / oracleCount;

				if ((*it)->getPredictAccu()->at(picked_oracle) >= MaxAccu)
					cerr << "E: picked large than oracle" << endl;

				if (WEIGHT_AVERAGED) {

					for (int p_iter = 0;
							p_iter < (*it)->getDpTokenArray()->size();
							p_iter++) {
						rfMtx[picked_oracle][p_iter].minus_rerank_feature_from_weight(
								feature_weight_dep, 1);

						rfMtx[picked_oracle][p_iter].minus_rerank_feature_from_weight(
								feature_weight_sum_dep, total_sentence_num - 1);
					}

					//						f->plus_feature_from_weight(feature_weight_dep,
					//								feature_weight_sum_dep,
					//								feature_count_lastupdate, total_sentence_num, 1);
				} else {
					for (int p_iter = 0;
							p_iter < (*it)->getDpTokenArray()->size();
							p_iter++) {
						rfMtx[picked_oracle][p_iter].minus_rerank_feature_from_weight(
								feature_weight_dep, 1);
					}
				}

				for (int i = 0; i < topK; i++) {
					if ((*it)->getPredictAccu()->at(i) >= MaxAccu) {

						if ((*it)->getPredictAccu()->at(i) > MaxAccu)
							cerr << "oracle not selected" << endl;

						oracleMark = i;

						if (WEIGHT_AVERAGED) {
//							rf[i].plus_rerank_feature_from_weight(
//									feature_weight_dep, updateStepSize);
//
//							rf[i].plus_rerank_feature_from_weight(
//									feature_weight_sum_dep, sumStepSize);

							for (int p_iter = 0;
									p_iter < (*it)->getDpTokenArray()->size();
									p_iter++) {
								rfMtx[i][p_iter].plus_rerank_feature_from_weight(
										feature_weight_dep, 1);

								rfMtx[i][p_iter].plus_rerank_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num - 1);
							}

						} else {
							for (int p_iter = 0;
									p_iter < (*it)->getDpTokenArray()->size();
									p_iter++) {
								rfMtx[i][p_iter].plus_rerank_feature_from_weight(
										feature_weight_dep, 1);
							}
						}
						break;
					}
				}

				if ((*it)->getPredictAccu()->at(picked_oracle)
						> (*it)->getPredictAccu()->at(0)) {
					cerr << "sentence:" << total_sentence_num
							<< "  successful modification"
							<< (*it)->getPredictAccu()->at(picked_oracle)
							<< "\t" << "origin "
							<< (*it)->getPredictAccu()->at(0) << "\t"
							<< "oracle "
							<< (*it)->getPredictAccu()->at(oracleMark) << "\t"
							<< "good" << endl;

					successed++;

				} else if ((*it)->getPredictAccu()->at(picked_oracle)
						< (*it)->getPredictAccu()->at(0)) {
					cerr << "sentence:" << total_sentence_num
							<< "  failed modification"
							<< (*it)->getPredictAccu()->at(picked_oracle)
							<< "\t" << "origin "
							<< (*it)->getPredictAccu()->at(0) << "\t"
							<< "oracle "
							<< (*it)->getPredictAccu()->at(oracleMark) << "\t"
							<< "bad" << endl;

					failed++;

				} else {
					cerr << "sentence:" << total_sentence_num
							<< "  unchanged ranking"
							<< (*it)->getPredictAccu()->at(picked_oracle)
							<< "\t" << "origin "
							<< (*it)->getPredictAccu()->at(0) << "\t"
							<< "oracle "
							<< (*it)->getPredictAccu()->at(oracleMark) << "\t"
							<< "bad" << endl;

				}

			} else {
				if ((*it)->getPredictAccu()->at(picked_oracle)
						> (*it)->getPredictAccu()->at(0)) {
					cerr << "sentence:" << total_sentence_num
							<< "  successful modification" << "\t" << "origin "
							<< (*it)->getPredictAccu()->at(0) << "\t"
							<< "oracle "
							<< (*it)->getPredictAccu()->at(picked_oracle)
							<< "\t" << "good" << endl;

					successed++;

				} else {
					cerr << "sentence:" << total_sentence_num
							<< "  unchanged ranking" << "\t" << "origin "
							<< (*it)->getPredictAccu()->at(0) << "\t"
							<< "oracle "
							<< (*it)->getPredictAccu()->at(picked_oracle)
							<< "\t" << "good" << endl;
				}
			}

			cerr << "S: " << successed << " F: " << failed << endl;

			total_sentence_num++;
		}
	}

	if (WEIGHT_AVERAGED) {
		for (std::map<std::string, double>::iterator it =
				feature_weight_sum_dep.begin();
				it != feature_weight_sum_dep.end(); it++) {
			if (feature_weight_dep.find(it->first)
					== feature_weight_dep.end()) {
				cerr << "error: cannot match feature weight and weight sum"
						<< endl;
			} else {
				feature_weight_dep[it->first] = feature_weight_dep[it->first]
						- it->second / total_sentence_num;
			}
		}
	}

	clear_gold_data();
	return true;
}

bool DepParser::config(const std::string &gsd_file, unsigned int order) {

	read_rerank_gold_data(gsd_file);

	total_sentence_num = 0;

	cerr << "Mode: config" << endl;

	if (WEIGHT_AVERAGED) {

		cerr << "Mode: average" << endl;
	}

	//Morph::Decoder edgeParser;

	//         for iteration 1..5 {
	//                 for sentence 1..N {
	//                          tagger.analyzer(sentence chunked);
	//                          - collected feature;
	//                          feature gold standard;
	//                          +
	//                     }
	//             }
	for (size_t t = 0; t < 1; t++) {

		//			if (t > 0 && t % 10 == 0) {
		//				std::string tempModelName = "/home/shen/data/tempModels/";
		//				tempModelName.append(int2string(t));
		//				write_temp_model_file(tempModelName);
		//			}

		cerr << "ITERATION:" << t << endl;
		if (MODE_SHUFFLE) { // shuffle training data
			random_shuffle(dpsentences_for_train.begin(),
					dpsentences_for_train.end());

			cerr << "Mode:shuffle" << endl;
		}
		//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

		for (std::vector<Dpsentence*>::iterator it =
				dpsentences_for_train.begin();
				it != dpsentences_for_train.end(); it++) {
			//new_sentence_analyze((*it)->get_sentence()); // get the best path

			//if (total_sentence_num % 100 == 0)
			//cerr << "sentence:" << total_sentence_num << endl;

			size_t topK = 1;

			if (topK > (*it)->getOracle()->size()) {
				topK = (*it)->getOracle()->size();
			}

			double MaxScore = -1e9;
			double MaxAccu = 0;
			size_t picked_oracle = 0;

			//FeatureSet *f = new FeatureSet(&ftmpl)[(*it)->getOracle()->size()];
			std::vector<std::vector<Morph::FeatureSet> > rfMtx;

			for (int i = 0; i < topK; i++) {
				std::vector<Morph::FeatureSet> rfVec;
				rfMtx.push_back(rfVec);
				//cerr << (*it)->getDepTree()->size() << endl;
			}

			for (int k_iter = 0; k_iter < topK; k_iter++) {

				Dptree* dptree = new Dptree();
				dptree->buildTree((*it)->getDpTokenArray(),
						&((*it)->getDepTree()->at(k_iter)));
				//cerr << "we're here" << endl;
				//f->extract_depth_feature((*it)->getDpTokenArray());
				//f->extract_breath_feature((*it)->getDpTokenArray());

				double tempScore = (*it)->getParseScore()->at(k_iter);

				for (std::vector<Morph::DptreeNode>::iterator tn_it =
						dptree->dpTreeNodeAry.begin();
						tn_it != dptree->dpTreeNodeAry.end(); tn_it++) {

					FeatureSet f = FeatureSet(&ftmpl);

					f.config_rerank_feature(dptree, &(*tn_it), 10, 10, 10);
					f.config_depth_feature(dptree, &(*tn_it), 6);
					f.config_ancestor_feature(dptree, &(*tn_it), 6);

					tempScore += f.calc_rerank_inner_product_with_weight(
							feature_weight_dep);

					rfMtx[k_iter].push_back(f);
				}

//				for (std::set<std::string>::iterator it = rf[k_iter].rfset.begin(); it != rf[k_iter].rfset.end();
//							it++) {
//						cout<<(*it)<<endl;
//					}
//				cout<<endl;

				cerr << tempScore << endl;

				if (MaxScore < tempScore) {
					MaxScore = tempScore;
					picked_oracle = k_iter;
				}

				if (MaxAccu < (*it)->getPredictAccu()->at(k_iter)) {
					MaxAccu = (*it)->getPredictAccu()->at(k_iter);
				}

				for (int p_iter = 0; p_iter < (*it)->getDpTokenArray()->size();
						p_iter++) {
					rfMtx[k_iter][p_iter].plus_rerank_feature_from_weight(
							feature_weight_dep, 1);
				}

				delete dptree;
				//cerr << "position reached" << endl;
			}

			if ((*it)->getPredictAccu()->at(picked_oracle) < MaxAccu) {

				int oracleMark;

				int oracleCount = 0;

				for (int i = 0; i < topK; i++) {
					if ((*it)->getPredictAccu()->at(i) == MaxAccu) {

						oracleCount++;

					}
				}

				if (oracleCount == 0)
					cerr << "oracle not selected" << endl;

				double updateStepSize = 1 / oracleCount;

				double sumStepSize = total_sentence_num / oracleCount;

				if ((*it)->getPredictAccu()->at(picked_oracle) >= MaxAccu)
					cerr << "E: picked large than oracle" << endl;

				if (WEIGHT_AVERAGED) {

					for (int p_iter = 0;
							p_iter < (*it)->getDpTokenArray()->size();
							p_iter++) {
						rfMtx[picked_oracle][p_iter].minus_rerank_feature_from_weight(
								feature_weight_dep, 1);

						rfMtx[picked_oracle][p_iter].minus_rerank_feature_from_weight(
								feature_weight_sum_dep, total_sentence_num);
					}

					//						f->plus_feature_from_weight(feature_weight_dep,
					//								feature_weight_sum_dep,
					//								feature_count_lastupdate, total_sentence_num, 1);
				} else {
					for (int p_iter = 0;
							p_iter < (*it)->getDpTokenArray()->size();
							p_iter++) {
						rfMtx[picked_oracle][p_iter].minus_rerank_feature_from_weight(
								feature_weight_dep, 1);
					}
				}

				for (int i = 0; i < topK; i++) {
					if ((*it)->getPredictAccu()->at(i) >= MaxAccu) {

						if ((*it)->getPredictAccu()->at(i) > MaxAccu)
							cerr << "oracle not selected" << endl;

						oracleMark = i;

						if (WEIGHT_AVERAGED) {
//							rf[i].plus_rerank_feature_from_weight(
//									feature_weight_dep, updateStepSize);
//
//							rf[i].plus_rerank_feature_from_weight(
//									feature_weight_sum_dep, sumStepSize);

							for (int p_iter = 0;
									p_iter < (*it)->getDpTokenArray()->size();
									p_iter++) {
								rfMtx[i][p_iter].plus_rerank_feature_from_weight(
										feature_weight_dep, 1);

								rfMtx[i][p_iter].plus_rerank_feature_from_weight(
										feature_weight_sum_dep,
										total_sentence_num);
							}

						} else {
							for (int p_iter = 0;
									p_iter < (*it)->getDpTokenArray()->size();
									p_iter++) {
								rfMtx[i][p_iter].plus_rerank_feature_from_weight(
										feature_weight_dep, 1);
							}
						}
						break;
					}
				}

				if ((*it)->getPredictAccu()->at(picked_oracle)
						>= (*it)->getPredictAccu()->at(0)) {
					cerr << "sentence:" << total_sentence_num << "  picked "
							<< (*it)->getPredictAccu()->at(picked_oracle)
							<< "\t" << "origin "
							<< (*it)->getPredictAccu()->at(0) << "\t"
							<< "oracle "
							<< (*it)->getPredictAccu()->at(oracleMark) << "\t"
							<< "good" << endl;

				} else {
					cerr << "sentence:" << total_sentence_num << "  picked "
							<< (*it)->getPredictAccu()->at(picked_oracle)
							<< "\t" << "origin "
							<< (*it)->getPredictAccu()->at(0) << "\t"
							<< "oracle "
							<< (*it)->getPredictAccu()->at(oracleMark) << "\t"
							<< "bad" << endl;

				}

			} else {
				cerr << "sentence:" << total_sentence_num << "  correct" << "\t"
						<< "origin " << (*it)->getPredictAccu()->at(0) << "\t"
						<< "oracle "
						<< (*it)->getPredictAccu()->at(picked_oracle) << "\t"
						<< "good" << endl;
			}
			total_sentence_num++;
		}
	}

	if (WEIGHT_AVERAGED) {
		for (std::map<std::string, double>::iterator it =
				feature_weight_sum_dep.begin();
				it != feature_weight_sum_dep.end(); it++) {
			if (feature_weight_dep.find(it->first)
					== feature_weight_dep.end()) {
				cerr << "error: cannot match feature weight and weight sum"
						<< endl;
			} else {
				feature_weight_dep[it->first] = feature_weight_dep[it->first]
						- it->second / total_sentence_num;
			}
		}
	}

	clear_gold_data();
	return true;
}

bool DepParser::complete(const std::string &gsd_file, size_t K,
		unsigned int order) {

	std::string kbo_file = "tempKbestOut.";
	kbo_file += int2string((int) K);
	kbo_file += ".";
	kbo_file += int2string((int) order);
	kbo_file += ".dat";

	size_t accu_nom = 0;
	size_t accu_denom = 0;

	size_t oracle_accu_nom = 0;
	size_t oracle_accu_denom = 0;

	read_rerank_gold_data(kbo_file);

	total_sentence_num = 0;

	size_t length_threshold = 0;

	std::vector<std::vector<int> > predictedTags;

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	for (std::vector<Dpsentence*>::iterator it = dpsentences_for_train.begin();
			it != dpsentences_for_train.end(); it++) {
		//new_sentence_analyze((*it)->get_sentence()); // get the best path

		//if (total_sentence_num % 100 == 0)
		//cerr << "sentence:" << total_sentence_num << endl;

		size_t topK = K;
		if (topK > (*it)->getOracle()->size()) {
			topK = (*it)->getOracle()->size();
		}

		double MaxScore = -1e16;
		size_t picked_oracle = 0;
		double MaxAccu = 0;

		//FeatureSet *f = new FeatureSet(&ftmpl)[(*it)->getOracle()->size()];
		//			std::vector<Morph::FeatureSet> rf;
		//			for (int i = 0; i < topK; i++) {
		//				FeatureSet f(&ftmpl);
		//				rf.push_back(f);
		//				//cerr << (*it)->getDepTree()->size() << endl;
		//			}

		for (int k_iter = 0; k_iter < topK; k_iter++) {
			Dptree* dptree = new Dptree();
			dptree->buildTree((*it)->getDpTokenArray(),
					&((*it)->getDepTree()->at(k_iter)));
			//cerr << "we're here" << endl;

			double tempScore = (*it)->getParseScore()->at(
					k_iter)/PARSE_SCORE_FACTOR;

			cerr << tempScore << " ";

			for (std::vector<Morph::DptreeNode>::iterator tn_it =
					dptree->dpTreeNodeAry.begin();
					tn_it != dptree->dpTreeNodeAry.end(); tn_it++) {

				FeatureSet f = FeatureSet(&ftmpl);

				f.extract_rerank_feature(dptree, &(*tn_it), 5, 5, 3);
				f.extract_depth_feature(dptree, &(*tn_it), 4);
				f.extract_ancestor_feature(dptree, &(*tn_it), 4);

				tempScore += f.calc_rerank_inner_product_with_weight(
						feature_weight_rerank);

			}

			cerr << tempScore << endl;

			if (MaxScore < tempScore) {
				MaxScore = tempScore;
				picked_oracle = k_iter;
			}

			delete dptree;
			//cerr << "position reached" << endl;
		}

		predictedTags.push_back((*it)->getDepTree()->at(picked_oracle));

//				(*it)->comparePrint((*it)->getDepTree()->at(picked_oracle),
//						(*it)->getDepTree()->at(oracle_position));

//				(*it)->checkAccuracy(accu_nom, accu_denom,
//						(*it)->getDepTree()->at(picked_oracle));

//				(*it)->checkAccuracy(oracle_accu_nom, oracle_accu_denom,
//						(*it)->getDepTree()->at(oracle_position));

		cerr << "sentence:" << total_sentence_num << "\t" << "origin "
				<< (*it)->getPredictAccu()->at(0) << "\t" << "picked "
				<< (*it)->getPredictAccu()->at(picked_oracle) << endl;
		cerr << "" << endl;
		total_sentence_num++;
	}

	clear_gold_data();

	overwrite_dependency_tags(gsd_file, predictedTags);

	return true;

}

bool DepParser::sequence_parse(const std::string &gsd_file, size_t K,
		unsigned int order) {

	std::string kbo_file = "tempKbestOut.";
	kbo_file += int2string((int) K);
	kbo_file += ".";
	kbo_file += int2string((int) order);
	kbo_file += ".dat";

	std::vector<std::vector<std::string> > goldSentences;

	read_kbest_gold_data(goldSentences, gsd_file, K);

	total_sentence_num = 0;

	cerr << "Mode: Parse-rerank" << endl;

	if (order == 1) {

		cerr << "Mode: 1st-order parsing" << endl;
		cerr << "Mode: Rerank " << K << "-best candidate(s)" << endl;

		for (std::vector<KbestDpsentence*>::iterator it =
				kbest_dpsentences_for_train.begin();
				it != kbest_dpsentences_for_train.end(); it++) {
			//new_sentence_analyze((*it)->get_sentence()); // get the best path

			//if (total_sentence_num % 100 == 0)
			//cerr << "sentence:" << total_sentence_num << endl;

			std::vector<double> parseScore;

			(*it)->initializeScoreMatrix();
			(*it)->evaluateEdgeScoreMatrix(&ftmpl, total_sentence_num);
			parseScore = (*it)->decodeEdgeFactors((*it)->getK());

			if (it == kbest_dpsentences_for_train.begin()) {
				check_file_exist(kbo_file);
			}

			if (training_data_division > 0 && training_data_spare > 0) {
				write_kbest_data(goldSentences.at(total_sentence_num), gsd_file,
						kbo_file, (*it)->predict,
						(*it)->getOracleList((*it)->getK()), parseScore,
						(*it)->getRatioList((*it)->getK()), (*it)->getK(),
						(*it)->getLength(),
						total_sentence_num
								+ floor(
										(double) (kbest_gold_data_sentence_count
												* (training_data_spare - 1))
												/ (double) training_data_division));

			} else {
				write_kbest_data(goldSentences.at(total_sentence_num), gsd_file,
						kbo_file, (*it)->predict,
						(*it)->getOracleList((*it)->getK()), parseScore,
						(*it)->getRatioList((*it)->getK()), (*it)->getK(),
						(*it)->getLength(), total_sentence_num);

			}

			(*it)->dpsentenceClear();
			total_sentence_num++;
		}

	} else if (order == 2) {

		cerr << "Mode: 2nd-order parsing" << endl;
		cerr << "Mode: Rerank " << K << "-best candidate(s)" << endl;

		for (std::vector<KbestDpsentence*>::iterator it =
				kbest_dpsentences_for_train.begin();
				it != kbest_dpsentences_for_train.end(); it++) {
			//new_sentence_analyze((*it)->get_sentence()); // get the best path

			//if (total_sentence_num % 100 == 0)
			//cerr << "sentence:" << total_sentence_num << endl;

			std::vector<double> parseScore;

			(*it)->initializeScoreCube();
			(*it)->evaluateSiblingScoreMatrix(&ftmpl, total_sentence_num);
			parseScore = (*it)->decodeSiblingFactorsNeo((*it)->getK());

			if (it == kbest_dpsentences_for_train.begin()) {
				check_file_exist(kbo_file);
			}

			if (training_data_division > 0 && training_data_spare > 0) {
				write_kbest_data(goldSentences.at(total_sentence_num), gsd_file,
						kbo_file, (*it)->predict,
						(*it)->getOracleList((*it)->getK()), parseScore,
						(*it)->getRatioList((*it)->getK()), (*it)->getK(),
						(*it)->getLength(),
						total_sentence_num
								+ floor(
										(double) (kbest_gold_data_sentence_count
												* (training_data_spare - 1))
												/ (double) training_data_division));

			} else {
				write_kbest_data(goldSentences.at(total_sentence_num), gsd_file,
						kbo_file, (*it)->predict,
						(*it)->getOracleList((*it)->getK()), parseScore,
						(*it)->getRatioList((*it)->getK()), (*it)->getK(),
						(*it)->getLength(), total_sentence_num);

			}

			(*it)->dpsentenceClear2nd();
			total_sentence_num++;
		}

	} else {
		cerr << "E: higher order model not implemented. DepParser::kbest. "
				<< endl;
	}

	//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
	//         new_sentence_analyze((*it)->get_sentence()); // get the best path
	//         print_best_path();
	//         sentence_clear();
	//     }

	clear_kbest_gold_data();

	return true;

}

bool DepParser::sequence_rerank(const std::string &gsd_file, size_t K,
		unsigned int order) {

	std::string kbo_file = "tempKbestOut.";
	kbo_file += int2string((int) K);
	kbo_file += ".";
	kbo_file += int2string((int) order);
	kbo_file += ".dat";

	size_t accu_nom = 0;
	size_t accu_denom = 0;

	size_t oracle_accu_nom = 0;
	size_t oracle_accu_denom = 0;

	read_rerank_gold_data(kbo_file);

	total_sentence_num = 0;

	size_t length_threshold = 0;

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	for (std::vector<Dpsentence*>::iterator it = dpsentences_for_train.begin();
			it != dpsentences_for_train.end(); it++) {
		//new_sentence_analyze((*it)->get_sentence()); // get the best path

		//if (total_sentence_num % 100 == 0)
		//cerr << "sentence:" << total_sentence_num << endl;
		if ((*it)->getLength() > length_threshold) {

			size_t topK = K;
			if (topK > (*it)->getOracle()->size()) {
				topK = (*it)->getOracle()->size();
			}

			double MaxScore = -1e16;
			size_t picked_oracle = 0;
			double MaxAccu = 0;
			size_t oracle_position = 0;

			//FeatureSet *f = new FeatureSet(&ftmpl)[(*it)->getOracle()->size()];
			//			std::vector<Morph::FeatureSet> rf;
			//			for (int i = 0; i < topK; i++) {
			//				FeatureSet f(&ftmpl);
			//				rf.push_back(f);
			//				//cerr << (*it)->getDepTree()->size() << endl;
			//			}

			for (int k_iter = 0; k_iter < topK; k_iter++) {
				Dptree* dptree = new Dptree();
				dptree->buildTree((*it)->getDpTokenArray(),
						&((*it)->getDepTree()->at(k_iter)));
				//cerr << "we're here" << endl;

				double tempScore = (*it)->getParseScore()->at(
						k_iter)/PARSE_SCORE_FACTOR;

				cerr << tempScore << " ";

				for (std::vector<Morph::DptreeNode>::iterator tn_it =
						dptree->dpTreeNodeAry.begin();
						tn_it != dptree->dpTreeNodeAry.end(); tn_it++) {

					FeatureSet f = FeatureSet(&ftmpl);

					f.extract_rerank_feature(dptree, &(*tn_it), 5, 5, 3);
					f.extract_depth_feature(dptree, &(*tn_it), 4);
					f.extract_ancestor_feature(dptree, &(*tn_it), 4);

					tempScore += f.calc_rerank_inner_product_with_weight(
							feature_weight_rerank);

				}

				cerr << tempScore << endl;

				if (MaxScore < tempScore) {
					MaxScore = tempScore;
					picked_oracle = k_iter;
				}

				if (MaxAccu < (*it)->getPredictAccu()->at(k_iter)) {
					MaxAccu = (*it)->getPredictAccu()->at(k_iter);
					oracle_position = k_iter;
				}

				delete dptree;
				//cerr << "position reached" << endl;
			}

			(*it)->comparePrint((*it)->getDepTree()->at(picked_oracle),
					(*it)->getDepTree()->at(oracle_position));

			(*it)->checkAccuracy(accu_nom, accu_denom,
					(*it)->getDepTree()->at(picked_oracle));

			(*it)->checkAccuracy(oracle_accu_nom, oracle_accu_denom,
					(*it)->getDepTree()->at(oracle_position));

			double accu_percentage;
			if (accu_denom > 0)
				accu_percentage = accu_nom / accu_denom;
			cerr << "accuracy: " << accu_nom << "\t" << accu_denom << endl;

			double oracle_accu_percentage;
			if (oracle_accu_denom > 0)
				oracle_accu_percentage = oracle_accu_nom / oracle_accu_denom;
			cerr << "oracle accuracy: " << oracle_accu_nom << "\t"
					<< oracle_accu_denom << endl;

			cerr << "sentence:" << total_sentence_num << "\t" << "origin "
					<< (*it)->getPredictAccu()->at(0) << "\t" << "picked "
					<< (*it)->getPredictAccu()->at(picked_oracle) << endl;
			cerr << "" << endl;
		} else {

			//(*it)->comparePrint();

			(*it)->checkAccuracy(accu_nom, accu_denom,
					(*it)->getDepTree()->at(0));

			double accu_percentage;
			if (accu_denom > 0)
				accu_percentage = accu_nom / accu_denom;
			cerr << "accuracy: " << accu_nom << "\t" << accu_denom << endl;

		}
		total_sentence_num++;
	}

	clear_gold_data();

	return true;

}

bool DepParser::output_stream(unsigned int order) {
	std::vector<std::vector<std::string> > goldSentences;

	size_t accu_nom = 0;
	size_t accu_denom = 0;

	read_gold_data();

	total_sentence_num = 0;

	//Morph::Decoder edgeParser;

	//         for iteration 1..5 {
	//                 for sentence 1..N {
	//                          tagger.analyzer(sentence chunked);
	//                          - collected feature;
	//                          feature gold standard;
	//                          +
	//                     }
	//             }

	cerr << "Mode: output stream" << endl;

	if (MODE_NEO) {
		cerr << "Mode: alternative decoding" << endl;
	}

	std::vector<std::vector<int> > predictedTags;

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	for (std::vector<Dpsentence*>::iterator it = dpsentences_for_train.begin();
			it != dpsentences_for_train.end(); it++) {
		//new_sentence_analyze((*it)->get_sentence()); // get the best path

		if (total_sentence_num % 100 == 0)
			cerr << "sentence:" << total_sentence_num << endl;

		if (order == 1) {

			(*it)->initializeScoreMatrix();

			if (MODE_WORD_CLUSTER) {
				(*it)->evaluateEdgeScoreMatrixWc(&ftmpl, &wcftmpl,
						total_sentence_num);
			} else {
				(*it)->evaluateEdgeScoreMatrix(&ftmpl, total_sentence_num);
			}

			(*it)->decodeEdgeFactors();

		} else if (order == 2) {

			(*it)->initializeScoreCube();

			if (MODE_WORD_CLUSTER) {
				(*it)->evaluateSiblingScoreMatrixWc(&ftmpl, &wcftmpl,
						total_sentence_num);
			} else {
				(*it)->evaluateSiblingScoreMatrix(&ftmpl, total_sentence_num);
			}

			if (MODE_NEO) {
				double showScore = (*it)->decodeSiblingFactorsNeo();
				cerr << showScore << endl;
			} else {
				(*it)->decodeSiblingFactors();
			}

		}

		predictedTags.push_back((*(*it)->getPredict()));

		if ((*it)->getDpTokenArray()->size() + 1
				!= (*(*it)->getPredict()).size()) {
			cerr << ";;err: sentences not aligned; write_dependency_tags"
					<< endl;
			return false;
		}

		for (int p = 0; p < (*it)->getDpTokenArray()->size(); p++) {

			int line_No = p + 1;

			cout << line_No << "\t";
			cout << (*it)->getDpTokenArray()->at(p).word << "\t";
			cout << "_\t";
			cout << (*it)->getDpTokenArray()->at(p).pos << "\t";
			cout << (*it)->getDpTokenArray()->at(p).pos << "\t";
			cout << "_\t";
			cout << (*(*it)->getPredict())[p + 1] << "\t";
			cout << "_" << endl;

		}
		cout << endl;

		if (order == 1)
			(*it)->dpsentenceClear();
		else if (order == 2)
			(*it)->dpsentenceClear2nd();

		total_sentence_num++;
	}

	//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
	//         new_sentence_analyze((*it)->get_sentence()); // get the best path
	//         print_best_path();
	//         sentence_clear();
	//     }

	clear_gold_data();

	return true;

}

bool DepParser::kbest_iostream(size_t K, unsigned int order) {

	std::vector<std::string> inputSentence;
//	std::vector<std::vector<std::string> > goldSentences;

	cerr << "Mode: K-best output" << endl;

	size_t sentence_count = 0;

	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	KbestDpsentence *new_dpsentence = new KbestDpsentence(K);
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

//	std::vector<std::string> lines;

	while (getline(cin, buffer)) {

		if (buffer == "") {

//			goldSen.push_back(lines);
//			lines.clear();

			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			if (line_count == 1)
				new_dpsentence->setK(1);
			else if (line_count == 2)
				new_dpsentence->setK(2);
			else if (line_count == 3)
				new_dpsentence->setK(7);
			else if (K > pow(2, line_count) - 1)
				new_dpsentence->setK(pow(2, line_count) - 1);

//			dpsentences_for_train_num++;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			std::vector<double> parseScore;
			if (sentence_count % 100 == 0)
				cerr << "sentence:" << sentence_count << endl;

			if (order == 1) {

					//new_sentence_analyze((*it)->get_sentence()); // get the best path

					new_dpsentence->initializeScoreMatrix();
//					(*it)->initializeScoreMatrix();

					new_dpsentence->evaluateEdgeScoreMatrix(&ftmpl, sentence_count);
//					(*it)->evaluateEdgeScoreMatrix(&ftmpl, total_sentence_num);

					parseScore = new_dpsentence->decodeEdgeFactors(new_dpsentence->getK());
//					parseScore = (*it)->decodeEdgeFactors((*it)->getK());

			} else if (order == 2) {

				new_dpsentence->initializeScoreCube();

				new_dpsentence->evaluateSiblingScoreMatrix(&ftmpl, sentence_count);

				parseScore = new_dpsentence->decodeSiblingFactorsNeo(new_dpsentence->getK());

			}

//			predictedTags.push_back((*(*it)->getPredict()));

//			if (new_dpsentence->getDpTokenArray()->size() + 1
//					!= (*new_dpsentence->getPredict()).size()) {
//				cerr << ";;err: sentences not aligned; write_dependency_tags"
//						<< endl;
//				return false;
//			}


//			for (std::vector<std::string>::iterator str_it = inputSentence.begin();
//					str_it != inputSentence.end(); str_it++) {
//				cout << (*str_it) << endl;
//			}

//			cout << "Sentence No." << total_sentence_num << endl;
			for (size_t rank = 0; rank < std::min(K, new_dpsentence->getK()); rank++) {
//				cout << "------" << endl;
				cout << "#" << parseScore[rank] << endl;
				for (size_t i = 0; i < new_dpsentence->getLength(); i++) {

					int line_No = i + 1;

					cout << line_No << "\t";
					cout << new_dpsentence->getDpTokenArray()->at(i).word << "\t";
					cout << "_\t";
					cout << new_dpsentence->getDpTokenArray()->at(i).pos << "\t";
					cout << new_dpsentence->getDpTokenArray()->at(i).pos << "\t";
					cout << "_\t";
					cout << new_dpsentence->predict[rank][i + 1] << "\t";
					cout << "_" << endl;

				}
			}
			cout << endl;

			if (order == 1)
				new_dpsentence->dpsentenceClear();
			else if (order == 2)
				new_dpsentence->dpsentenceClear2nd();

			//delete new_dpsentence;

			inputSentence.clear();

			new_dpsentence = new KbestDpsentence(K);
			continue;
		}

		inputSentence.push_back(buffer);

		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	delete new_dpsentence;


	//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
	//         new_sentence_analyze((*it)->get_sentence()); // get the best path
	//         print_best_path();
	//         sentence_clear();
	//     }

	clear_kbest_gold_data();

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;


	return true;
}


bool DepParser::io_stream(unsigned int order) {
	std::vector<std::vector<std::string> > goldSentences;

	size_t accu_nom = 0;
	size_t accu_denom = 0;

	total_sentence_num = 0;

	//Morph::Decoder edgeParser;

	//         for iteration 1..5 {
	//                 for sentence 1..N {
	//                          tagger.analyzer(sentence chunked);
	//                          - collected feature;
	//                          feature gold standard;
	//                          +
	//                     }
	//             }

	cerr << "Mode: I/O stream" << endl;

	if (MODE_NEO) {
		cerr << "Mode: alternative decoding" << endl;
	}

//	std::vector<std::vector<int> > predictedTags;

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	size_t sentence_count = 0;

	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	Dpsentence *new_dpsentence = new Dpsentence();
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;
	double parseScore = 0;

	while (getline(cin, buffer)) {

		if (buffer == "") {
			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			if (sentence_count % 100 == 0)
				cerr << "sentence:" << sentence_count << endl;

			if (order == 1) {

				new_dpsentence->initializeScoreMatrix();

				if (MODE_WORD_CLUSTER) {
					new_dpsentence->evaluateEdgeScoreMatrixWc(&ftmpl, &wcftmpl,
							sentence_count);
				} else {
					new_dpsentence->evaluateEdgeScoreMatrix(&ftmpl, sentence_count);
				}

				new_dpsentence->decodeEdgeFactors();

			} else if (order == 2) {

				new_dpsentence->initializeScoreCube();

				if (MODE_WORD_CLUSTER) {
					new_dpsentence->evaluateSiblingScoreMatrixWc(&ftmpl, &wcftmpl,
							sentence_count);
				} else {
					new_dpsentence->evaluateSiblingScoreMatrix(&ftmpl, sentence_count);
				}

				if (MODE_NEO) {
					parseScore = new_dpsentence->decodeSiblingFactorsNeo();
//					cerr << parseScore << endl;
				} else {
					new_dpsentence->decodeSiblingFactors();
				}

			}

//			predictedTags.push_back((*(*it)->getPredict()));

			if (new_dpsentence->getDpTokenArray()->size() + 1
					!= (*new_dpsentence->getPredict()).size()) {
				cerr << ";;err: sentences not aligned; write_dependency_tags"
						<< endl;
				return false;
			}


			cout << "#" << parseScore << endl;
			for (int p = 0; p < new_dpsentence->getDpTokenArray()->size(); p++) {

				int line_No = p + 1;

				cout << line_No << "\t";
				cout << new_dpsentence->getDpTokenArray()->at(p).word << "\t";
				cout << "_\t";
				cout << new_dpsentence->getDpTokenArray()->at(p).pos << "\t";
				cout << new_dpsentence->getDpTokenArray()->at(p).pos << "\t";
				cout << "_\t";
				cout << (*new_dpsentence->getPredict())[p + 1] << "\t";
				cout << "_" << endl;

			}
			cout << endl;

			if (order == 1)
				new_dpsentence->dpsentenceClear();
			else if (order == 2)
				new_dpsentence->dpsentenceClear2nd();


			//delete new_dpsentence;

			new_dpsentence = new Dpsentence();
			continue;
		}
		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	delete new_dpsentence;


	//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
	//         new_sentence_analyze((*it)->get_sentence()); // get the best path
	//         print_best_path();
	//         sentence_clear();
	//     }

	clear_gold_data();

	return true;

}


bool DepParser::output(unsigned int order) {
	std::vector<std::vector<std::string> > goldSentences;

	size_t accu_nom = 0;
	size_t accu_denom = 0;

	read_gold_data();

	total_sentence_num = 0;

	//Morph::Decoder edgeParser;

	//         for iteration 1..5 {
	//                 for sentence 1..N {
	//                          tagger.analyzer(sentence chunked);
	//                          - collected feature;
	//                          feature gold standard;
	//                          +
	//                     }
	//             }

	cerr << "Mode: output" << endl;

	if (MODE_NEO) {
		cerr << "Mode: alternative decoding" << endl;
	}

	std::vector<std::vector<int> > predictedTags;

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	for (std::vector<Dpsentence*>::iterator it = dpsentences_for_train.begin();
			it != dpsentences_for_train.end(); it++) {
		//new_sentence_analyze((*it)->get_sentence()); // get the best path

		if (total_sentence_num % 100 == 0)
			cerr << "sentence:" << total_sentence_num << endl;

		if (order == 1) {

			(*it)->initializeScoreMatrix();

			if (MODE_WORD_CLUSTER) {
				(*it)->evaluateEdgeScoreMatrixWc(&ftmpl, &wcftmpl,
						total_sentence_num);
			} else {
				(*it)->evaluateEdgeScoreMatrix(&ftmpl, total_sentence_num);
			}

			(*it)->decodeEdgeFactors();

		} else if (order == 2) {

			(*it)->initializeScoreCube();

			if (MODE_WORD_CLUSTER) {
				(*it)->evaluateSiblingScoreMatrixWc(&ftmpl, &wcftmpl,
						total_sentence_num);
			} else {
				(*it)->evaluateSiblingScoreMatrix(&ftmpl, total_sentence_num);
			}

			if (MODE_NEO) {
				double showScore = (*it)->decodeSiblingFactorsNeo();
				cerr << showScore << endl;
			} else {
				(*it)->decodeSiblingFactors();
			}

		}

		predictedTags.push_back((*(*it)->getPredict()));

		if (order == 1)
			(*it)->dpsentenceClear();
		else if (order == 2)
			(*it)->dpsentenceClear2nd();

		total_sentence_num++;
	}

	//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
	//         new_sentence_analyze((*it)->get_sentence()); // get the best path
	//         print_best_path();
	//         sentence_clear();
	//     }

	write_dependency_tags(predictedTags);

	clear_gold_data();

	return true;

}

bool DepParser::output(const std::string &gsd_file, const std::string &o_file,
		unsigned int order) {
	std::vector<std::vector<std::string> > goldSentences;

	size_t accu_nom = 0;
	size_t accu_denom = 0;

	if (training_data_division > 0 && training_data_spare > 0) {
		cerr << "Mode: Jack-Knifing" << endl;
		read_gold_data_slice(gsd_file, training_data_division,
				training_data_spare);
	} else {
		read_gold_data(gsd_file);
	}

	total_sentence_num = 0;

	//Morph::Decoder edgeParser;

	//         for iteration 1..5 {
	//                 for sentence 1..N {
	//                          tagger.analyzer(sentence chunked);
	//                          - collected feature;
	//                          feature gold standard;
	//                          +
	//                     }
	//             }

	cerr << "Mode: output" << endl;

	if (MODE_NEO) {
		cerr << "Mode: alternative decoding" << endl;
	}

	std::vector<std::vector<int> > predictedTags;

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	for (std::vector<Dpsentence*>::iterator it = dpsentences_for_train.begin();
			it != dpsentences_for_train.end(); it++) {
		//new_sentence_analyze((*it)->get_sentence()); // get the best path

		if (total_sentence_num % 100 == 0)
			cerr << "sentence:" << total_sentence_num << endl;

		if (order == 1) {

			(*it)->initializeScoreMatrix();

			if (MODE_WORD_CLUSTER) {
				(*it)->evaluateEdgeScoreMatrixWc(&ftmpl, &wcftmpl,
						total_sentence_num);
			} else {
				(*it)->evaluateEdgeScoreMatrix(&ftmpl, total_sentence_num);
			}

			(*it)->decodeEdgeFactors();

		} else if (order == 2) {

			(*it)->initializeScoreCube();

			if (MODE_WORD_CLUSTER) {
				(*it)->evaluateSiblingScoreMatrixWc(&ftmpl, &wcftmpl,
						total_sentence_num);
			} else {
				(*it)->evaluateSiblingScoreMatrix(&ftmpl, total_sentence_num);
			}

			if (MODE_NEO) {
				double showScore = (*it)->decodeSiblingFactorsNeo();
				cerr << showScore << endl;
			} else {
				(*it)->decodeSiblingFactors();
			}

		}

		predictedTags.push_back((*(*it)->getPredict()));

		if (order == 1)
			(*it)->dpsentenceClear();
		else if (order == 2)
			(*it)->dpsentenceClear2nd();

		total_sentence_num++;
	}

	//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
	//         new_sentence_analyze((*it)->get_sentence()); // get the best path
	//         print_best_path();
	//         sentence_clear();
	//     }

	write_dependency_tags(o_file, predictedTags);

	clear_gold_data();

	return true;

}

bool DepParser::kbest(const std::string &gsd_file, const std::string &kbo_file,
		size_t K, unsigned int order) {

	std::vector<std::vector<std::string> > goldSentences;

	if (training_data_division > 0 && training_data_spare > 0) {
		cerr << "Mode: Jack-Knifing" << endl;
		read_complement_kbest_gold_data(goldSentences, gsd_file, K,
				training_data_division, training_data_spare);
	} else {
		read_kbest_gold_data(goldSentences, gsd_file, K);
	}

	total_sentence_num = 0;

	cerr << "Mode: kbest" << endl;

	//cerr << feature_weight_dep.begin()->first <<" "<<feature_weight_dep.begin()->second << endl;

	if (order == 1) {
		for (std::vector<KbestDpsentence*>::iterator it =
				kbest_dpsentences_for_train.begin();
				it != kbest_dpsentences_for_train.end(); it++) {
			//new_sentence_analyze((*it)->get_sentence()); // get the best path

			//if (total_sentence_num % 100 == 0)
			//cerr << "sentence:" << total_sentence_num << endl;

			std::vector<double> parseScore;

			(*it)->initializeScoreMatrix();
			(*it)->evaluateEdgeScoreMatrix(&ftmpl, total_sentence_num);
			parseScore = (*it)->decodeEdgeFactors((*it)->getK());

			if (it == kbest_dpsentences_for_train.begin()) {
				check_file_exist(kbo_file);
			}

			if (training_data_division > 0 && training_data_spare > 0) {
				write_kbest_data(goldSentences.at(total_sentence_num), gsd_file,
						kbo_file, (*it)->predict,
						(*it)->getOracleList((*it)->getK()), parseScore,
						(*it)->getRatioList((*it)->getK()), (*it)->getK(),
						(*it)->getLength(),
						total_sentence_num
								+ floor(
										(double) (kbest_gold_data_sentence_count
												* (training_data_spare - 1))
												/ (double) training_data_division));

			} else {
				write_kbest_data(goldSentences.at(total_sentence_num), gsd_file,
						kbo_file, (*it)->predict,
						(*it)->getOracleList((*it)->getK()), parseScore,
						(*it)->getRatioList((*it)->getK()), (*it)->getK(),
						(*it)->getLength(), total_sentence_num);

			}

			(*it)->dpsentenceClear();
			total_sentence_num++;
		}

	} else if (order == 2) {
		for (std::vector<KbestDpsentence*>::iterator it =
				kbest_dpsentences_for_train.begin();
				it != kbest_dpsentences_for_train.end(); it++) {
			//new_sentence_analyze((*it)->get_sentence()); // get the best path

			//if (total_sentence_num % 100 == 0)
			//cerr << "sentence:" << total_sentence_num << endl;

			std::vector<double> parseScore;

			(*it)->initializeScoreCube();
			(*it)->evaluateSiblingScoreMatrix(&ftmpl, total_sentence_num);
			parseScore = (*it)->decodeSiblingFactorsNeo((*it)->getK());

			if (it == kbest_dpsentences_for_train.begin()) {
				check_file_exist(kbo_file);
			}

			if (training_data_division > 0 && training_data_spare > 0) {
				write_kbest_data(goldSentences.at(total_sentence_num), gsd_file,
						kbo_file, (*it)->predict,
						(*it)->getOracleList((*it)->getK()), parseScore,
						(*it)->getRatioList((*it)->getK()), (*it)->getK(),
						(*it)->getLength(),
						total_sentence_num
								+ floor(
										(double) (kbest_gold_data_sentence_count
												* (training_data_spare - 1))
												/ (double) training_data_division));

			} else {
				write_kbest_data(goldSentences.at(total_sentence_num), gsd_file,
						kbo_file, (*it)->predict,
						(*it)->getOracleList((*it)->getK()), parseScore,
						(*it)->getRatioList((*it)->getK()), (*it)->getK(),
						(*it)->getLength(), total_sentence_num);

			}

			(*it)->dpsentenceClear2nd();
			total_sentence_num++;
		}

	} else {
		cerr << "E: higher order model not implemented. DepParser::kbest. "
				<< endl;
	}

	//     for (std::vector<Sentence *>::iterator it = sentences_for_train.begin(); it != sentences_for_train.end(); it++) {
	//         new_sentence_analyze((*it)->get_sentence()); // get the best path
	//         print_best_path();
	//         sentence_clear();
	//     }

	clear_kbest_gold_data();
	return true;
}

// read gold standard data
bool DepParser::read_gold_data(const std::string &gsd_file) {
	return read_gold_data(gsd_file.c_str());
}

bool DepParser::read_gold_data(const std::string &gsd_file, size_t division,
		size_t spare) {
	return read_gold_data(gsd_file.c_str(), division, spare);
}

bool DepParser::read_complement_gold_data(const std::string &gsd_file,
		size_t division, size_t spare) {
	return read_complement_gold_data(gsd_file.c_str(), division, spare);
}

bool DepParser::read_gold_data_slice(const std::string &gsd_file,
		size_t division, size_t spare) {
	return read_gold_data_slice(gsd_file.c_str(), division, spare);
}

bool DepParser::read_kbest_gold_data(
		std::vector<std::vector<std::string> >& goldSen,
		const std::string &gsd_file, size_t K, size_t division, size_t spare) {

	return read_kbest_gold_data(goldSen, gsd_file.c_str(), K, division, spare);

}

bool DepParser::read_rerank_gold_data(const std::string &gsd_file) {
	return read_rerank_gold_data(gsd_file.c_str());
}

// read gold standard data from input stream
bool DepParser::read_gold_data() {
	size_t sentence_count = 0;

	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	Dpsentence *new_dpsentence = new Dpsentence();
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	while (getline(cin, buffer)) {

		if (buffer == "") {
			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			DepParser::add_one_sentence_for_train(new_dpsentence);
			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new Dpsentence();
			continue;
		}
		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	delete new_dpsentence;
	return true;
}

// read gold standard data
bool DepParser::read_gold_data(const char *gsd_file) {
	size_t sentence_count = 0;

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}
	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	Dpsentence *new_dpsentence = new Dpsentence();
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	while (getline(gsd_in, buffer)) {

		if (buffer == "") {
			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			DepParser::add_one_sentence_for_train(new_dpsentence);
			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new Dpsentence();
			continue;
		}
		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	gsd_in.close();
	delete new_dpsentence;
	return true;
}

bool DepParser::read_gold_data(const char *gsd_file, size_t division,
		size_t spare) {

	std::vector<Dpsentence *> full_dpsentences_for_train;

	size_t sentence_count = 0;

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}
	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	Dpsentence *new_dpsentence = new Dpsentence();
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	while (getline(gsd_in, buffer)) {

		if (buffer == "") {
			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			full_dpsentences_for_train.push_back(new_dpsentence);

			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new Dpsentence();
			continue;
		}
		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	gsd_in.close();
	delete new_dpsentence;

	size_t nominator_sentence_count = 0;
	for (std::vector<Dpsentence *>::iterator st_it =
			full_dpsentences_for_train.begin();
			st_it != full_dpsentences_for_train.end(); st_it++) {
		nominator_sentence_count++;
		if ((double) nominator_sentence_count
				> floor(
						(double) (sentence_count * (spare - 1))
								/ (double) division)
				&& (double) nominator_sentence_count
						<= floor(
								(double) (sentence_count * spare)
										/ (double) division)) {
			continue;
		}
		DepParser::add_one_sentence_for_train(*st_it);
	}

	return true;
}

bool DepParser::read_complement_gold_data(size_t division, size_t spare) {

	std::vector<Dpsentence *> full_dpsentences_for_train;

	size_t sentence_count = 0;

	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	Dpsentence *new_dpsentence = new Dpsentence();
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	while (getline(cin, buffer)) {

		if (buffer == "") {
			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			full_dpsentences_for_train.push_back(new_dpsentence);

			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new Dpsentence();
			continue;
		}
		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	delete new_dpsentence;

	size_t nominator_sentence_count = 0;
	for (std::vector<Dpsentence *>::iterator st_it =
			full_dpsentences_for_train.begin();
			st_it != full_dpsentences_for_train.end(); st_it++) {
		nominator_sentence_count++;
		if ((double) nominator_sentence_count
				> floor(
						(double) (sentence_count * (spare - 1))
								/ (double) division)
				&& (double) nominator_sentence_count
						<= floor(
								(double) (sentence_count * spare)
										/ (double) division)) {
			DepParser::add_one_sentence_for_train(*st_it);
		}
	}

	return true;
}

bool DepParser::read_complement_gold_data(const char *gsd_file, size_t division,
		size_t spare) {

	std::vector<Dpsentence *> full_dpsentences_for_train;

	size_t sentence_count = 0;

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}
	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	Dpsentence *new_dpsentence = new Dpsentence();
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	while (getline(gsd_in, buffer)) {

		if (buffer == "") {
			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			full_dpsentences_for_train.push_back(new_dpsentence);

			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new Dpsentence();
			continue;
		}
		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	gsd_in.close();
	delete new_dpsentence;

	size_t nominator_sentence_count = 0;
	for (std::vector<Dpsentence *>::iterator st_it =
			full_dpsentences_for_train.begin();
			st_it != full_dpsentences_for_train.end(); st_it++) {
		nominator_sentence_count++;
		if ((double) nominator_sentence_count
				> floor(
						(double) (sentence_count * (spare - 1))
								/ (double) division)
				&& (double) nominator_sentence_count
						<= floor(
								(double) (sentence_count * spare)
										/ (double) division)) {
			DepParser::add_one_sentence_for_train(*st_it);
		}
	}

	return true;
}

bool DepParser::read_gold_data_slice(const char *gsd_file, size_t division,
		size_t spare) {

	size_t sentence_count = 0;

	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}

	std::string travler;
	size_t line_marker = 0;
	std::vector<int> sentence_index;

	sentence_index.push_back(0);

	while (getline(gsd_in, travler)) {
		line_marker++;
		if (travler == "") {
			sentence_count++;

			sentence_index.push_back(line_marker);

			cerr << "Now " << sentence_count << " sentences" << endl;
		}

	}
	travler.clear();

	gsd_in.close();
	gsd_in.open(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}

	cerr << "Totally " << sentence_count << " sentences" << endl;

	Dpsentence *new_dpsentence = new Dpsentence();
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	size_t line_checker = 0;

	while (getline(gsd_in, buffer)) {
		line_checker++;

		if (line_checker
				> sentence_index[floor(
						(double) (sentence_count * (spare - 1))
								/ (double) division)]
				&& line_checker
						<= sentence_index[floor(
								(double) (sentence_count * spare)
										/ (double) division)]) {

//			if(line_checker > sentence_index[1624097] && line_checker < sentence_index[1624297] ){
//				continue;
//			}

			if (buffer == "") {

				new_dpsentence->setHeadIndex(&depArray);

				new_dpsentence->setSurroundingPos();

				new_dpsentence->setLength(line_count);

				DepParser::add_one_sentence_for_train(new_dpsentence);

				dpsentences_for_train_num++;

				cerr << "Now using " << dpsentences_for_train_num
						<< " sentences" << endl;

				line_count = 0;
				depArray.clear();

				//delete new_dpsentence;

				new_dpsentence = new Dpsentence();
				continue;
			}
			std::vector<std::string> dpToken;
			split_string(buffer, "\t", dpToken);

			ss << dpToken.at(6);
			ss >> goldDep;
			ss.str("");
			ss.clear();

			depArray.push_back(goldDep);

			Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
					line_count, goldDep);

			new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
			new_dpsentence->appendDpToken(new_dptoken);
			delete new_dptoken;
			//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
			//            new_sentence->lookup_gold_data(*it);
			//        }
			//        new_sentence->find_best_path();
			//        new_sentence->set_feature();
			//        new_sentence->clear_nodes();
			// new_sentence->feature_print();
			//        sentences_for_train_num++;
			line_count++;
		} else {
			continue;
		}

	}

	gsd_in.close();
	delete new_dpsentence;

	cerr << "Totally " << sentence_count << " sentences" << endl;
	cerr << "Using " << dpsentences_for_train_num << " sentences" << endl;

	return true;
}

bool DepParser::read_kbest_gold_data(
		std::vector<std::vector<std::string> >& goldSen, const char *gsd_file,
		size_t K, size_t division, size_t spare) {

	std::vector<KbestDpsentence *> full_dpsentences_for_train;
	size_t sentence_count = 0;

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}
	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	KbestDpsentence *new_dpsentence = new KbestDpsentence(K);
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	while (getline(gsd_in, buffer)) {

		if (buffer == "") {
			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			if (line_count == 1)
				new_dpsentence->setK(1);
			else if (line_count == 2)
				new_dpsentence->setK(2);
			else if (line_count == 3)
				new_dpsentence->setK(7);
			else if (K > pow(2, line_count) - 1)
				new_dpsentence->setK(pow(2, line_count) - 1);

			full_dpsentences_for_train.push_back(new_dpsentence);
			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new KbestDpsentence(K);
			continue;
		}
		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	gsd_in.close();
	delete new_dpsentence;

	size_t nominator_sentence_count = 0;
	for (std::vector<KbestDpsentence *>::iterator st_it =
			full_dpsentences_for_train.begin();
			st_it != full_dpsentences_for_train.end(); st_it++) {
		nominator_sentence_count++;
		if ((double) nominator_sentence_count
				> floor(
						(double) (sentence_count * (spare - 1))
								/ (double) division)
				&& (double) nominator_sentence_count
						<= floor(
								(double) (sentence_count * spare)
										/ (double) division)) {
			continue;
		}
		DepParser::add_one_sentence_for_train(*st_it);
	}

	kbest_gold_data_sentence_count = sentence_count;
	return true;
}

bool DepParser::read_complement_kbest_gold_data(
		std::vector<std::vector<std::string> >& goldSen,
		const std::string &gsd_file, size_t K, size_t division, size_t spare) {
	return read_complement_kbest_gold_data(goldSen, gsd_file.c_str(), K,
			division, spare);
}

bool DepParser::read_complement_kbest_gold_data(
		std::vector<std::vector<std::string> >& goldSen, const char *gsd_file,
		size_t K, size_t division, size_t spare) {

	std::vector<KbestDpsentence *> full_dpsentences_for_train;
	std::vector<std::vector<std::string> > full_goldSen;

	size_t sentence_count = 0;

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}
	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	KbestDpsentence *new_dpsentence = new KbestDpsentence(K);
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	std::vector<std::string> lines;

	while (getline(gsd_in, buffer)) {

		if (buffer == "") {

			full_goldSen.push_back(lines);
			lines.clear();

			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			if (line_count == 1)
				new_dpsentence->setK(1);
			else if (line_count == 2)
				new_dpsentence->setK(2);
			else if (line_count == 3)
				new_dpsentence->setK(7);
			else if (K > pow(2, line_count) - 1)
				new_dpsentence->setK(pow(2, line_count) - 1);

			full_dpsentences_for_train.push_back(new_dpsentence);
			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new KbestDpsentence(K);
			continue;
		}

		lines.push_back(buffer);

		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	gsd_in.close();
	delete new_dpsentence;

	size_t nominator_sentence_count = 0;
	for (std::vector<KbestDpsentence *>::iterator st_it =
			full_dpsentences_for_train.begin();
			st_it != full_dpsentences_for_train.end(); st_it++) {
		nominator_sentence_count++;
		if ((double) nominator_sentence_count
				> floor(
						(double) (sentence_count * (spare - 1))
								/ (double) division)
				&& (double) nominator_sentence_count
						<= floor(
								(double) (sentence_count * spare)
										/ (double) division)) {
			DepParser::add_one_sentence_for_train(*st_it);
		}
	}

	nominator_sentence_count = 0;
	for (std::vector<std::vector<std::string> >::iterator gs_it =
			full_goldSen.begin(); gs_it != full_goldSen.end(); gs_it++) {
		nominator_sentence_count++;
		if ((double) nominator_sentence_count
				> floor(
						(double) (sentence_count * (spare - 1))
								/ (double) division)
				&& (double) nominator_sentence_count
						<= floor(
								(double) (sentence_count * spare)
										/ (double) division)) {
			goldSen.push_back(*gs_it);
		}
	}

	kbest_gold_data_sentence_count = sentence_count;
	return true;

}

bool DepParser::read_kbest_gold_data(
		std::vector<std::vector<std::string> >& goldSen,
		const std::string &gsd_file, size_t K) {
	return read_kbest_gold_data(goldSen, gsd_file.c_str(), K);
}

bool DepParser::read_kbest_gold_data(
		std::vector<std::vector<std::string> >& goldSen, const char *gsd_file,
		size_t K) {
	size_t sentence_count = 0;

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}
	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	KbestDpsentence *new_dpsentence = new KbestDpsentence(K);
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	std::vector<std::string> lines;

	while (getline(gsd_in, buffer)) {

		if (buffer == "") {

			goldSen.push_back(lines);
			lines.clear();

			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			if (line_count == 1)
				new_dpsentence->setK(1);
			else if (line_count == 2)
				new_dpsentence->setK(2);
			else if (line_count == 3)
				new_dpsentence->setK(7);
			else if (K > pow(2, line_count) - 1)
				new_dpsentence->setK(pow(2, line_count) - 1);

			DepParser::add_one_sentence_for_train(new_dpsentence);
			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new KbestDpsentence(K);
			continue;
		}

		lines.push_back(buffer);

		std::vector<std::string> dpToken;
		split_string(buffer, "\t", dpToken);

		ss << dpToken.at(6);
		ss >> goldDep;
		ss.str("");
		ss.clear();

		depArray.push_back(goldDep);

		Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
				line_count, goldDep);

		new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
		new_dpsentence->appendDpToken(new_dptoken);
		delete new_dptoken;
		//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
		//            new_sentence->lookup_gold_data(*it);
		//        }
		//        new_sentence->find_best_path();
		//        new_sentence->set_feature();
		//        new_sentence->clear_nodes();
		// new_sentence->feature_print();
		//        sentences_for_train_num++;
		line_count++;
	}

	gsd_in.close();
	delete new_dpsentence;

	kbest_gold_data_sentence_count = sentence_count;
	return true;
}

bool DepParser::read_rerank_gold_data(const char *gsd_file) {
	size_t sentence_count = 0;

	std::ifstream gsd_in(gsd_file, std::ios::in);
	if (!gsd_in.is_open()) {
		cerr << ";; cannot open gold data for reading" << endl;
		return false;
	}
	// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

	Dpsentence *new_dpsentence = new Dpsentence();
	std::string buffer;
	std::stringstream ss;
	int goldDep;
	std::vector<int> depArray;
	size_t line_count = 0;

	std::vector<int> incomingTree;
	std::stringstream its;
	int predDep;

	//std::vector<int> oracles;
	std::stringstream ocs;
	int orc;

	//std::vector<double> parseScores;
	std::stringstream pss;
	double parSc;

	//std::vector<double> predAccuracies;
	std::stringstream pas;
	double predAc;

	int read_mode = 1; // i:sentence 2:sentence number header(pass) 3 sentence number(pass) 4:header 5:predicted tree

	while (getline(gsd_in, buffer)) {

		if (buffer == "Sentence No.") {

			read_mode = 2;

			continue;
		}

		if (read_mode == 4 && buffer == "------") {

			continue;

		} else if (read_mode == 5 && buffer == "------") {
			read_mode = 4;

			new_dpsentence->pushTreeList(incomingTree);

			incomingTree.clear();

			continue;
		}

		if (buffer == "") {

			read_mode = 1;

			new_dpsentence->pushTreeList(incomingTree);

			incomingTree.clear();

			new_dpsentence->setHeadIndex(&depArray);

			new_dpsentence->setSurroundingPos();

			new_dpsentence->setLength(line_count);

			DepParser::add_one_sentence_for_train(new_dpsentence);
			dpsentences_for_train_num++;
			//delete new_dpsentence;

			line_count = 0;
			sentence_count++;
			depArray.clear();

			//delete new_dpsentence;

			new_dpsentence = new Dpsentence();
			continue;
		}

		if (read_mode == 1) {
			std::vector<std::string> dpToken;
			split_string(buffer, "\t", dpToken);

			ss << dpToken.at(6);
			ss >> goldDep;
			ss.str("");
			ss.clear();

			depArray.push_back(goldDep);

			Dptoken *new_dptoken = new Dptoken(dpToken.at(1), dpToken.at(3),
					line_count, goldDep);

			new_dpsentence->getHeadIndex()->push_back(new_dptoken->dep);
			new_dpsentence->appendDpToken(new_dptoken);
			delete new_dptoken;
			//        for (std::vector<std::string>::iterator it = word_pos_pairs.begin(); it != word_pos_pairs.end(); it++) {
			//            new_sentence->lookup_gold_data(*it);
			//        }
			//        new_sentence->find_best_path();
			//        new_sentence->set_feature();
			//        new_sentence->clear_nodes();
			// new_sentence->feature_print();
			//        sentences_for_train_num++;
			line_count++;

		} else if (read_mode == 2) {
			read_mode = 3;
			continue;

		} else if (read_mode == 3) {
			read_mode = 4;
			continue;

		} else if (read_mode == 4) {
			read_mode = 5;
			std::vector<std::string> header;
			split_string(buffer, "\t", header);

			ocs << header[0];
			ocs >> orc;
			ocs.str("");
			ocs.clear();
			new_dpsentence->pushOracleList(orc);

			pss << header[1];
			pss >> parSc;
			pss.str("");
			pss.clear();
			new_dpsentence->pushParseScoreList(parSc);

			pas << header[2];
			pas >> predAc;
			pas.str("");
			pas.clear();
			new_dpsentence->pushPredAccuList(predAc);
			//todo

		} else if (read_mode == 5) {
			its << buffer;
			its >> predDep;
			its.str("");
			its.clear();
			incomingTree.push_back(predDep);

		} else {

		}

	}

	gsd_in.close();
	delete new_dpsentence;
	return true;
}

bool DepParser::add_one_sentence_for_train(Dpsentence *in_sentence) {
	dpsentences_for_train.push_back(in_sentence);
	return true;
}

bool DepParser::add_one_sentence_for_train(KbestDpsentence *in_sentence) {
	kbest_dpsentences_for_train.push_back(in_sentence);
	return true;
}

// clear gold standard data
void DepParser::clear_gold_data() {
	for (std::vector<Dpsentence *>::iterator it = dpsentences_for_train.begin();
			it != dpsentences_for_train.end(); it++) {
		delete *it;
	}
	dpsentences_for_train.clear();
}

void DepParser::clear_kbest_gold_data() {
	for (std::vector<KbestDpsentence *>::iterator it =
			kbest_dpsentences_for_train.begin();
			it != kbest_dpsentences_for_train.end(); it++) {
		delete *it;
	}
	kbest_dpsentences_for_train.clear();
}

}
