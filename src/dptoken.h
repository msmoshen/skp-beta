#ifndef DPTOKEN_H
#define DPTOKEN_H

#include "common.h"
#include "feature.h"

namespace Morph {

template<class T>
inline std::string re_int2string(const T i) {
	std::ostringstream o;

	o << i;
	return o.str();
}

class Dptoken {
public:
	std::string word;
	std::string pos;
	std::string leftPos;
	std::string rightPos;
	size_t position;
	size_t dep;

	std::string shortBits;
	std::string longBits;
	std::string fullBits;

	Dptoken();
	Dptoken(std::string w, std::string p, size_t i, size_t d);
	virtual ~Dptoken();

	void setLeftPos(std::string p);
	void setRightPos(std::string p);

};
class Dependence {
	Dptoken* headPtr;
	Dptoken* modifierPtr;
public:
	Dependence();
	Dependence(Dptoken* head, Dptoken* modifier);
	virtual ~Dependence();

	void minus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight, size_t factor);

	void minus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight);

	void plus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight, size_t factor);

	void plus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight);

};

class DptreeNode {
	int ID;
	int isRoot;
	int isLeaf;
	int distanceToRoot;
	int nodesToRoot;
	int distanceToHead;
	int isRightBranch;
	int isAnchor;
	size_t level;
	size_t valence;
	size_t subTreeDepth;
	Morph::Dptoken* nodePtr;
	Morph::Dptoken* parentPtr;
	std::vector<Morph::DptreeNode*> leftChildList;
	std::vector<Morph::DptreeNode*> rightChildList;
	std::deque<Morph::DptreeNode*> leftBranchSequence;
	std::deque<Morph::DptreeNode*> rightBranchSequence;

public:
	//std::set<std::string> ch_rfset;
	//std::set<std::string> lt_rfset;
	std::vector<std::vector<std::vector<std::vector<DptreeNode*> > > > trimSubTab;
	std::string rootStr1;
	std::string rootStr2;
	std::string nodeStr0;
	std::string nodeStr1;
	std::string chStr0;
	std::string chStr1;

	DptreeNode() {
	}
	;

	DptreeNode(Morph::Dptoken* token, Morph::Dptoken* parent, int id, int leaf,
			int root, int dist2root, int dist2head, int isRB, int isACor,
			size_t lv, size_t cdNum) {
		ID = id;
		nodePtr = token;
		parentPtr = parent;
		isLeaf = leaf;
		isRoot = root;
		distanceToRoot = dist2root;
		distanceToHead = dist2head;
		isRightBranch = isRB;
		isAnchor = isACor;
		level = lv;
		valence = cdNum;
		subTreeDepth = 0;
	}
	;
	virtual ~DptreeNode() {
	}
	;
	std::deque<Morph::DptreeNode*>* getLeftBranchSequence() {
		return &leftBranchSequence;
	}

	std::deque<Morph::DptreeNode*>* getRightBranchSequence() {
		return &rightBranchSequence;
	}

	std::vector<Morph::DptreeNode*>* getLeftChildList() {
		return &leftChildList;
	}

	void appendLeftChildList(Morph::DptreeNode* newChildNode) {
		leftChildList.push_back(newChildNode);
	}

	std::vector<Morph::DptreeNode*>* getRightChildList() {
		return &rightChildList;
	}

	void appendRightChildList(Morph::DptreeNode* newChildNode) {
		rightChildList.push_back(newChildNode);
	}

	inline Morph::Dptoken* getNode() {
		return nodePtr;
	}

	void setNode(Morph::Dptoken* targetNode) {
		nodePtr = targetNode;
	}

	inline Morph::Dptoken* getParentNode() {
		return parentPtr;
	}
	void setParentNode(Morph::Dptoken* targetParentNode) {
		parentPtr = targetParentNode;
	}

	inline size_t getLevel() {
		return level;
	}

	inline void setLevel(size_t lv) {
		level = lv;
	}

	inline void incrementLevel() {
		level++;
	}

	inline size_t getValence() {
		return valence;
	}

	inline void setValence(size_t cn) {
		valence = cn;
	}

	inline size_t getSubTreeDepth() {
		return subTreeDepth;
	}

	inline void setSubTreeDepth(size_t subtd) {
		subTreeDepth = subtd;
	}

	inline void incrementValence() {
		valence++;
	}

	inline int getDistanceToRoot() {
		return distanceToRoot;
	}

	inline void setDistanceToRoot(int dtr) {
		distanceToRoot = dtr;
	}

	inline int getNodesToRoot() {
		return nodesToRoot;
	}

	inline void setNodesToRoot(int ntr) {
		nodesToRoot = ntr;
	}

	inline void incrementNodesToRoot(int step) {
		nodesToRoot += step;
	}

	inline int getDistanceToHead() {
		return distanceToHead;
	}

	inline void setDistanceToHead(int dth) {
		distanceToHead = dth;
	}

	inline int getIsRightBranch() {
		return isRightBranch;
	}

	inline void setIsRightBranch(int iRB) {
		isRightBranch = iRB;
	}

	inline int getIsAnchor() {
		return isAnchor;
	}

	inline void setIsAnchor(int iAor) {
		isAnchor = iAor;
	}

	inline int getID() {
		return ID;
	}

	inline void setID(int iD) {
		ID = iD;
	}

	inline int getIsLeaf() {
		return isLeaf;
	}

	inline void setIsLeaf(int il) {
		isLeaf = il;
	}

	inline int getIsRoot() {
		return isRoot;
	}

	inline void setIsRoot(int ir) {
		isRoot = ir;
	}

	void exploreSubtree(Morph::DptreeNode* rootNode, size_t left_boundry,
			size_t right_boundry, size_t bottom_boundry) {

		rootNode->rootStr1.clear();
		rootNode->rootStr1 += "Tm:{";
		rootNode->rootStr1 += rootNode->getNode()->pos;
		rootNode->rootStr1 += "}";
		rootNode->rootStr2.clear();
		rootNode->rootStr2 += "Tm:{";
		rootNode->rootStr2 += rootNode->getNode()->pos;
		rootNode->rootStr2 += ",";
		rootNode->rootStr2 += rootNode->getNode()->word;
		rootNode->rootStr2 += "}";

		for (std::vector<Morph::DptreeNode*>::iterator cd_it =
				rootNode->getLeftChildList()->begin();
				cd_it != rootNode->getLeftChildList()->end(); cd_it++) {

			//appendNodeFeature(f1, 1, rootNode, *cd_it);
			//appendNodeFeature(f2, 2, rootNode, *cd_it);
			leftBranchTravel(rootNode, (*cd_it), 1, left_boundry,
					bottom_boundry);
		}

		for (std::vector<Morph::DptreeNode*>::iterator cd_it =
				rootNode->getRightChildList()->begin();
				cd_it != rootNode->getRightChildList()->end(); cd_it++) {
			//appendNodeFeature(f1, 1, rootNode, *cd_it);
			//appendNodeFeature(f2, 2, rootNode, *cd_it);
			rightBranchTravel(rootNode, (*cd_it), 1, right_boundry,
					bottom_boundry);
		}

	}

	void leftBranchTravel(Morph::DptreeNode* rootNode,
			Morph::DptreeNode* headNode, int isAchr, size_t left_boundry,
			size_t bottom_boundry) {
		if (headNode->getLevel() - rootNode->getLevel() < bottom_boundry) {
			if (subTreeDepth < headNode->getLevel() - rootNode->getLevel())
				subTreeDepth = headNode->getLevel() - rootNode->getLevel();

			for (std::vector<Morph::DptreeNode*>::iterator cd_it =
					headNode->getLeftChildList()->begin();
					cd_it != headNode->getLeftChildList()->end(); cd_it++) {

				//appendNodeFeature(f1, 1, rootNode, *cd_it);
				//appendNodeFeature(f2, 2, rootNode, *cd_it);
				leftBranchTravel(rootNode, (*cd_it), isAchr, left_boundry,
						bottom_boundry);
			}

			headNode->setIsAnchor(isAchr);

			headNode->nodeStr0.clear();
			headNode->nodeStr0 += "[_,";
			headNode->nodeStr0 += re_int2string(headNode->getIsRightBranch());
			headNode->nodeStr0 += ",";
			headNode->nodeStr0 += re_int2string(
					(int) (headNode->getLevel() - rootNode->getLevel()));
			headNode->nodeStr0 += "]";

			headNode->nodeStr1.clear();
			headNode->nodeStr1 += "[";
			headNode->nodeStr1 += headNode->getNode()->pos;
			headNode->nodeStr1 += ",";
			headNode->nodeStr1 += re_int2string(headNode->getIsRightBranch());
			headNode->nodeStr1 += ",";
			headNode->nodeStr1 += re_int2string(
					(int) (headNode->getLevel() - rootNode->getLevel()));
			headNode->nodeStr1 += "]";

			leftBranchSequence.push_back(headNode);

			for (std::vector<Morph::DptreeNode*>::iterator cd_it =
					headNode->getRightChildList()->begin();
					cd_it != headNode->getRightChildList()->end(); cd_it++) {
				//appendNodeFeature(f1, 1, rootNode, *cd_it);
				//appendNodeFeature(f2, 2, rootNode, *cd_it);
				leftBranchTravel(rootNode, (*cd_it), 0, left_boundry,
						bottom_boundry);
			}
		}
	}

	void rightBranchTravel(Morph::DptreeNode* rootNode,
			Morph::DptreeNode* headNode, int isAchr, size_t right_boundry,
			size_t bottom_boundry) {
		if (headNode->getLevel() - rootNode->getLevel() < bottom_boundry) {
			if (subTreeDepth < headNode->getLevel() - rootNode->getLevel())
				subTreeDepth = headNode->getLevel() - rootNode->getLevel();

			for (std::vector<Morph::DptreeNode*>::iterator cd_it =
					headNode->getLeftChildList()->begin();
					cd_it != headNode->getLeftChildList()->end(); cd_it++) {

				//appendNodeFeature(f1, 1, rootNode, *cd_it);
				//appendNodeFeature(f2, 2, rootNode, *cd_it);
				rightBranchTravel(rootNode, (*cd_it), 0, right_boundry,
						bottom_boundry);
			}

			headNode->setIsAnchor(isAchr);

			headNode->nodeStr0.clear();
			headNode->nodeStr0 += "[_,";
			headNode->nodeStr0 += re_int2string(headNode->getIsRightBranch());
			headNode->nodeStr0 += ",";
			headNode->nodeStr0 += re_int2string(
					(int) (headNode->getLevel() - rootNode->getLevel()));
			headNode->nodeStr0 += "]";

			headNode->nodeStr1.clear();
			headNode->nodeStr1 += "[";
			headNode->nodeStr1 += headNode->getNode()->pos;
			headNode->nodeStr1 += ",";
			headNode->nodeStr1 += re_int2string(headNode->getIsRightBranch());
			headNode->nodeStr1 += ",";
			headNode->nodeStr1 += re_int2string(
					(int) (headNode->getLevel() - rootNode->getLevel()));
			headNode->nodeStr1 += "]";

			rightBranchSequence.push_front(headNode);

			for (std::vector<Morph::DptreeNode*>::iterator cd_it =
					headNode->getRightChildList()->begin();
					cd_it != headNode->getRightChildList()->end(); cd_it++) {
				//appendNodeFeature(f1, 1, rootNode, *cd_it);
				//appendNodeFeature(f2, 2, rootNode, *cd_it);
				rightBranchTravel(rootNode, (*cd_it), isAchr, right_boundry,
						bottom_boundry);
			}
		}
	}

};

class DpNodeChain {
public:
	std::vector<Morph::DptreeNode*> dpTreeNodeAry;
	Morph::DptreeNode* subRootNode;
	DpNodeChain();
	virtual ~DpNodeChain();

};

class Dptree {
public:
	unsigned int height;
	std::vector<Morph::Dptoken>* dpTokenAryPtr;
	std::vector<Morph::DptreeNode> dpTreeNodeAry;
	Morph::DptreeNode* rootNode;

	//std::vector<std::string> ch_fset;
	std::set<std::string> ch_fset;
	std::set<std::string> lt_fset;

	Dptree() {

	}
	;
	virtual ~Dptree() {
	}
	;

//	void depthFirstTravel(Morph::DptreeNode* subRoot) {
//		for (std::vector<Morph::DptreeNode*>::iterator node_it =
//				subRoot->getLeftChildList()->begin();
//				node_it != subRoot->getLeftChildList()->end(); node_it++) {
//			depthFirstTravel((*node_it));
//		}
//
//		for (std::vector<Morph::DptreeNode*>::iterator node_it =
//				subRoot->getRightChildList()->begin();
//				node_it != subRoot->getRightChildList()->end(); node_it++) {
//			depthFirstTravel((*node_it));
//		}
//	}

	//todo
	void travelAndStat(Morph::DptreeNode* subRoot, size_t ht) {
		subRoot->setLevel(ht);
		for (std::vector<Morph::DptreeNode*>::iterator node_it =
				subRoot->getLeftChildList()->begin();
				node_it != subRoot->getLeftChildList()->end(); node_it++) {
			travelAndStat((*node_it), ht + 1);
		}

		for (std::vector<Morph::DptreeNode*>::iterator node_it =
				subRoot->getRightChildList()->begin();
				node_it != subRoot->getRightChildList()->end(); node_it++) {
			travelAndStat((*node_it), ht + 1);
		}
	}

	void buildTree(std::vector<Morph::Dptoken>* dpTokenArray,
			std::vector<int>* predictVector) {
		size_t rootIndex = 0;

		for (std::vector<Morph::Dptoken>::iterator tk_it =
				dpTokenArray->begin(); tk_it != dpTokenArray->end(); tk_it++) {
			if ((*predictVector).at(tk_it->position) != 0) {
				Morph::DptreeNode newTreeNode(
						&(*tk_it),
						&(dpTokenArray->at(
								(*predictVector).at(tk_it->position) - 1)), tk_it->position, 1,
						0, 0, 0, 0, 1, 0, 0);

				dpTreeNodeAry.push_back(newTreeNode);
			} else {
				rootIndex = tk_it->position;

				Morph::DptreeNode newTreeNode(&(*tk_it), NULL, tk_it->position, 1, 1, 0, 0, 0, 1,
						0, 0);

				dpTreeNodeAry.push_back(newTreeNode);
			}
		}

		for (std::vector<Morph::Dptoken>::iterator tk_it =
				dpTokenArray->begin(); tk_it != dpTokenArray->end(); tk_it++) {
			if ((*predictVector).at(tk_it->position) != 0) {
				dpTreeNodeAry.at((*predictVector).at(tk_it->position) - 1).setIsLeaf(
						0);

				if (tk_it->position
						< (*predictVector).at(tk_it->position) - 1) {

					dpTreeNodeAry.at(tk_it->position).setIsRightBranch(-1);

					dpTreeNodeAry.at((*predictVector).at(tk_it->position) - 1).appendLeftChildList(
							&(dpTreeNodeAry.at(tk_it->position)));

				} else if (tk_it->position
						> (*predictVector).at(tk_it->position) - 1) {

					dpTreeNodeAry.at(tk_it->position).setIsRightBranch(1);

					dpTreeNodeAry.at((*predictVector).at(tk_it->position) - 1).appendRightChildList(
							&(dpTreeNodeAry.at(tk_it->position)));
				} else {
					std::cerr << "error:dep equals to node" << std::endl;
				}

				dpTreeNodeAry.at((*predictVector).at(tk_it->position) - 1).incrementValence();

				dpTreeNodeAry.at(tk_it->position).setDistanceToHead(
						abs(
								(int) ((tk_it->position + 1)
										- (*predictVector).at(tk_it->position))));

				dpTreeNodeAry.at(tk_it->position).setDistanceToRoot(
						abs((int) (tk_it->position - rootIndex)));

			} else {
				rootNode = &(dpTreeNodeAry.at(tk_it->position));
			}
		}

		//???
		travelAndStat(rootNode, 0);

	}

	void appendNodeFeature(std::string& f, int complexity,
			Morph::DptreeNode* rootNode, Morph::DptreeNode* headNode) {

		if (complexity == 1) {
			f += "[";
			f += "ISR::";
			f += re_int2string(headNode->getIsRightBranch());
			f += ",";
			f += "RLV::";
			f += re_int2string(
					(int) (headNode->getLevel() - rootNode->getLevel()));
			f += "]";
		} else if (complexity == 2) {
			f += "[";
			f += "P::";
			f += headNode->getNode()->pos;
			f += ",";
			f += "ISR::";
			f += re_int2string(headNode->getIsRightBranch());
			f += ",";
			f += "RLV::";
			f += re_int2string(
					(int) (headNode->getLevel() - rootNode->getLevel()));
			f += "]";
		} else if (complexity == 3) {
			f += "[";
			f += "P::";
			f += headNode->getNode()->pos;
			f += ",";
			f += "W::";
			f += headNode->getNode()->word;
			f += ",";
			f += "ISR::";
			f += re_int2string(headNode->getIsRightBranch());
			f += ",";
			f += "RLV::";
			f += re_int2string(
					(int) (headNode->getLevel() - rootNode->getLevel()));
			f += "]";
		} else if (complexity == 4) {
			f += "[";
			f += "P::";
			f += headNode->getNode()->pos;
			f += ",";
			f += "W::";
			f += headNode->getNode()->word;
			f += ",";
			f += "LF::";
			f += re_int2string(headNode->getIsLeaf());
			f += ",";
			f += "ISR::";
			f += re_int2string(headNode->getIsRightBranch());
			f += ",";
			f += "D2H::";
			f += re_int2string(headNode->getDistanceToHead());
			f += ",";
			f += "D2S::";
			f += re_int2string(
					(int) (headNode->getNode()->position
							- rootNode->getNode()->position));
			f += ",";

			f += "D2R::";
			f += re_int2string(headNode->getDistanceToRoot());
			f += ",";

			f += "ALV::";
			f += re_int2string((int) (headNode->getLevel()));
			f += ",";

			f += "RLV::";
			f += re_int2string(
					(int) (headNode->getLevel() - rootNode->getLevel()));

			f += ",";
			f += "VA::";
			f += re_int2string((int) (headNode->getValence()));
			f += "]";
		} else {

		}
	}

	void appendRootFeature(std::string& f, int complexity,
			Morph::DptreeNode* dpNode) {
		if (complexity == 1) {
			f += "{";
			f += "P::";
			f += dpNode->getNode()->pos;
			f += "}";
		} else if (complexity == 2) {
			f += "{";
			f += "P::";
			f += dpNode->getNode()->pos;
			f += ",";
			f += "W::";
			f += dpNode->getNode()->word;
			f += "}";

		} else if (complexity == 3) {
			f += "{";
			f += "P::";
			f += dpNode->getNode()->pos;
			f += ",";
			f += "W::";
			f += dpNode->getNode()->word;
			f += ",";
			f += "VA::";
			f += re_int2string((int) (dpNode->getValence()));
			f += "}";

		} else if (complexity == 4) {
			f += "{";
			f += "P::";
			f += dpNode->getNode()->pos;
			f += ",";
			f += "W::";
			f += dpNode->getNode()->word;
			f += ",";
			f += "VA::";
			f += re_int2string((int) (dpNode->getValence()));
			f += "}";

		}
	}

	void extract_depth_feature(size_t length_limit) {

		for (std::vector<Morph::DptreeNode>::iterator tn_it =
				dpTreeNodeAry.begin(); tn_it != dpTreeNodeAry.end(); tn_it++) {

			if (tn_it->getIsRoot())
				continue;

			int chainLength = 0;

			std::deque<Morph::DptreeNode*> chain;

			Morph::DptreeNode* currentTreeNodePtr = &(*tn_it);

			chain.push_front(currentTreeNodePtr);

			chainLength++;

			//		while (chainLength <= length_limit && !(currentTreeNodePtr->getIsRoot())) {
			//			currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
			//					currentTreeNodePtr->getParentNode()->position));
			//			chain.push_front(currentTreeNodePtr);
			//			chainLength++;
			//		}

			while (!(currentTreeNodePtr->getIsRoot())) {
				currentTreeNodePtr = &(dpTreeNodeAry.at(
						currentTreeNodePtr->getParentNode()->position));
				chain.push_front(currentTreeNodePtr);
			}

			for (int i = 0; i < chain.size() - 1; i++) {
				std::string r1;
				std::string r2;
				std::string f1;
				std::string f2;
				std::string c11;
				std::string c12;
				std::string c21;
				std::string c22;

				appendRootFeature(r1, 1, chain.at(i));
				appendRootFeature(r2, 2, chain.at(i));

				int stopPosition = chain.size() - 1 - i;
				if (stopPosition > length_limit) {
					stopPosition = length_limit;
				}
				for (int k = i + 1; k <= i + stopPosition; k++) {
					appendNodeFeature(f1, 1, chain.at(i), chain.at(k));
					appendNodeFeature(f2, 2, chain.at(i), chain.at(k));
				}

				c11 = "Ch:" + r1 + f1;
				c12 = "Ch:" + r1 + f2;
				c21 = "Ch:" + r2 + f1;
				c22 = "Ch:" + r2 + f2;
				ch_fset.insert(c11);
				ch_fset.insert(c12);
				ch_fset.insert(c21);
				ch_fset.insert(c22);
			}
			//		if (currentToken == NULL)
			//			cerr << "NullPtrErr : extract_depth_feature" << endl;
			//
			//		if (tk_it->position != currentToken->position)
			//			cerr << "MissMatch : extract_depth_feature" << endl;
			//		if (stopSign != 0)
			//			cerr << "root not reached : extract_depth_feature" << endl;

		}
	}

	void extract_rerank_feature(size_t left_boundry, size_t right_boundry,
			size_t bottom_boundry) {

		for (std::vector<Morph::DptreeNode>::iterator tn_it =
				dpTreeNodeAry.begin(); tn_it != dpTreeNodeAry.end(); tn_it++) {

			tn_it->exploreSubtree(&(*tn_it), left_boundry, right_boundry,
					bottom_boundry);

			std::vector<int> leftMileStones;
			std::vector<int> rightMileStones;

			size_t leftPen = 1;
			size_t rightPen = 1;

			if (tn_it->getLeftBranchSequence()->size() > 0) {

				leftMileStones.push_back(0);

				while (leftPen < tn_it->getLeftBranchSequence()->size()) {
					if (tn_it->getLeftBranchSequence()->at(leftPen)->getLevel()
							> tn_it->getLeftBranchSequence()->at(
									leftMileStones.back())->getLevel()) {
						leftPen++;
					} else {
						leftMileStones.push_back(leftPen);
						leftPen++;
					}
				}
			}

			if (tn_it->getRightBranchSequence()->size() > 0) {

				rightMileStones.push_back(0);

				while (rightPen < tn_it->getRightBranchSequence()->size()) {
					if (tn_it->getRightBranchSequence()->at(rightPen)->getLevel()
							> tn_it->getRightBranchSequence()->at(
									rightMileStones.back())->getLevel()) {
						rightPen++;
					} else {
						rightMileStones.push_back(rightPen);
						rightPen++;
					}
				}
			}

			if (tn_it->getLeftBranchSequence()->size() == 0) {
				for (std::vector<int>::iterator rb_it = rightMileStones.begin();
						rb_it != rightMileStones.end(); rb_it++) {
					for (size_t bb = 0; bb <= tn_it->getSubTreeDepth(); bb++) {

						std::string r1;
						std::string r2;
						std::string f1;
						std::string f2;
						std::string c11;
						std::string c12;
						std::string c21;
						std::string c22;

						appendRootFeature(r1, 1, &(*tn_it));
						appendRootFeature(r2, 2, &(*tn_it));

						for (size_t right_it = (*rb_it);
								right_it
										< tn_it->getRightBranchSequence()->size();
								right_it++) {
							if (tn_it->getRightBranchSequence()->at(right_it)->getLevel()
									- tn_it->getLevel() < bb) {
								appendNodeFeature(
										f1,
										1,
										&(*tn_it),
										tn_it->getRightBranchSequence()->at(
												right_it));
								appendNodeFeature(
										f2,
										2,
										&(*tn_it),
										tn_it->getRightBranchSequence()->at(
												right_it));
							}
						}

						c11 = r1 + f1;
						c12 = r1 + f2;
						c21 = r2 + f1;
						c22 = r2 + f2;
						lt_fset.insert(c11);
						lt_fset.insert(c12);
						lt_fset.insert(c21);
						lt_fset.insert(c22);

					}
				}
			}
			if (tn_it->getRightBranchSequence()->size() == 0) {
				for (std::vector<int>::iterator lb_it = leftMileStones.begin();
						lb_it != leftMileStones.end(); lb_it++) {
					for (size_t bb = 0; bb <= tn_it->getSubTreeDepth(); bb++) {

						std::string r1;
						std::string r2;
						std::string f1;
						std::string f2;
						std::string c11;
						std::string c12;
						std::string c21;
						std::string c22;

						appendRootFeature(r1, 1, &(*tn_it));
						appendRootFeature(r2, 2, &(*tn_it));

						for (size_t left_it = (*lb_it);
								left_it < tn_it->getLeftBranchSequence()->size();
								left_it++) {

							if (tn_it->getLeftBranchSequence()->at(left_it)->getLevel()
									- tn_it->getLevel() < bb) {
								appendNodeFeature(
										f1,
										1,
										&(*tn_it),
										tn_it->getLeftBranchSequence()->at(
												left_it));
								appendNodeFeature(
										f2,
										2,
										&(*tn_it),
										tn_it->getLeftBranchSequence()->at(
												left_it));
							}

						}

						c11 = r1 + f1;
						c12 = r1 + f2;
						c21 = r2 + f1;
						c22 = r2 + f2;
						lt_fset.insert(c11);
						lt_fset.insert(c12);
						lt_fset.insert(c21);
						lt_fset.insert(c22);

					}
				}
			}

			for (std::vector<int>::iterator lb_it = leftMileStones.begin();
					lb_it != leftMileStones.end(); lb_it++) {
				for (std::vector<int>::iterator rb_it = rightMileStones.begin();
						rb_it != rightMileStones.end(); rb_it++) {
					for (size_t bb = 0; bb <= tn_it->getSubTreeDepth(); bb++) {

						std::string r1;
						std::string r2;
						std::string f1;
						std::string f2;
						std::string c11;
						std::string c12;
						std::string c21;
						std::string c22;

						appendRootFeature(r1, 1, &(*tn_it));
						appendRootFeature(r2, 2, &(*tn_it));

						for (size_t left_it = (*lb_it);
								left_it < tn_it->getLeftBranchSequence()->size();
								left_it++) {

							if (tn_it->getLeftBranchSequence()->at(left_it)->getLevel()
									- tn_it->getLevel() < bb) {
								appendNodeFeature(
										f1,
										1,
										&(*tn_it),
										tn_it->getLeftBranchSequence()->at(
												left_it));
								appendNodeFeature(
										f2,
										2,
										&(*tn_it),
										tn_it->getLeftBranchSequence()->at(
												left_it));
							}

						}
						for (size_t right_it = (*rb_it);
								right_it
										< tn_it->getRightBranchSequence()->size();
								right_it++) {
							if (tn_it->getRightBranchSequence()->at(right_it)->getLevel()
									- tn_it->getLevel() < bb) {
								appendNodeFeature(
										f1,
										1,
										&(*tn_it),
										tn_it->getRightBranchSequence()->at(
												right_it));
								appendNodeFeature(
										f2,
										2,
										&(*tn_it),
										tn_it->getRightBranchSequence()->at(
												right_it));
							}
						}

						c11 = r1 + f1;
						c12 = r1 + f2;
						c21 = r2 + f1;
						c22 = r2 + f2;
						lt_fset.insert(c11);
						lt_fset.insert(c12);
						lt_fset.insert(c21);
						lt_fset.insert(c22);

					}
				}
			}

		}

	}

	double calc_ch_inner_product_with_weight(
			std::map<std::string, double> &in_rerank_feature_weight) {
		double sum = 0;
		for (std::set<std::string>::iterator it = ch_fset.begin();
				it != ch_fset.end(); it++) {
			if (in_rerank_feature_weight.find(*it)
					!= in_rerank_feature_weight.end()) {
				sum += in_rerank_feature_weight[*it];
			}
		}
		return sum;
	}

	double calc_lt_inner_product_with_weight(
			std::map<std::string, double> &in_rerank_feature_weight) {
		double sum = 0;
		for (std::set<std::string>::iterator it = lt_fset.begin();
				it != lt_fset.end(); it++) {
			if (in_rerank_feature_weight.find(*it)
					!= in_rerank_feature_weight.end()) {
				sum += in_rerank_feature_weight[*it];
			}
		}
		return sum;
	}

};

}
#endif // DPTOKEN_H
