#include <cstdlib>
#include "common.h"
#include "pos.h"
#include "sentence.h"
#include "dptoken.h"
#include "feature.h"
//#include "dpsentence.h"

namespace Morph {

FeatureSet::FeatureSet(FeatureTemplateSet *in_ftmpl) {
	ftmpl = in_ftmpl;
}

FeatureSet::FeatureSet(FeatureTemplateSet *in_ftmpl,FeatureTemplateSet *in_wcftmpl) {
	ftmpl = in_ftmpl;
	wcftmpl = in_wcftmpl;
}

FeatureSet::~FeatureSet() {
	fset.clear();
}

void FeatureSet::extract_unigram_feature(Node *node) {
	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			ftmpl->get_templates()->begin();
			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
		if (!((*tmpl_it)->get_is_unigram())) // skip bigram feature template
			continue;
		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			if (*it == FEATURE_MACRO_WORD)
				f += *(node->string);
			else if (*it == FEATURE_MACRO_POS)
				f += *(node->pos);
			else if (*it == FEATURE_MACRO_LENGTH)
				f += int2string(node->get_char_num());
			else if (*it == FEATURE_MACRO_BEGINNING_CHAR)
				f.append(node->get_first_char(),
						(node->stat & MORPH_PSEUDO_NODE) ?
								strlen(node->get_first_char()) :
								utf8_bytes(
										(unsigned char *) node->get_first_char()));
			else if (*it == FEATURE_MACRO_ENDING_CHAR) {
				f += *(node->end_string);
				// cerr << *(node->string) << " : " << *(node->string_for_print) << " : " << f << endl;
			} else if (*it == FEATURE_MACRO_BEGINNING_CHAR_TYPE)
				f += int2string(node->char_family);
			else if (*it == FEATURE_MACRO_ENDING_CHAR_TYPE)
				f += int2string(node->end_char_family);
			else if (*it == FEATURE_MACRO_FEATURE1) // Wikipedia (test)
				f += int2string(node->lcAttr);
		}
		fset.push_back(f);
	}
}

void FeatureSet::extract_bigram_feature(Node *l_node, Node *r_node) {
	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			ftmpl->get_templates()->begin();
			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
		if ((*tmpl_it)->get_is_unigram()) // skip unigram feature template
			continue;
		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			// left
			if (*it == FEATURE_MACRO_LEFT_WORD)
				f += *(l_node->string);
			else if (*it == FEATURE_MACRO_LEFT_POS)
				f += *(l_node->pos);
			else if (*it == FEATURE_MACRO_LEFT_LENGTH)
				f += int2string(l_node->get_char_num());
			else if (*it == FEATURE_MACRO_LEFT_BEGINNING_CHAR)
				f.append(l_node->get_first_char(),
						(l_node->stat & MORPH_PSEUDO_NODE) ?
								strlen(l_node->get_first_char()) :
								utf8_bytes(
										(unsigned char *) l_node->get_first_char()));
			else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR) {
				f += *(l_node->end_string);
				// cerr << *(l_node->string) << " : " << *(l_node->string_for_print) << " : " << f << endl;
			} else if (*it == FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE)
				f += int2string(l_node->char_family);
			else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE)
				f += int2string(l_node->end_char_family);
			// right
			else if (*it == FEATURE_MACRO_RIGHT_WORD)
				f += *(r_node->string);
			else if (*it == FEATURE_MACRO_RIGHT_POS)
				f += *(r_node->pos);
			else if (*it == FEATURE_MACRO_RIGHT_LENGTH)
				f += int2string(r_node->get_char_num());
			else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR)
				f.append(r_node->get_first_char(),
						(r_node->stat & MORPH_PSEUDO_NODE) ?
								strlen(r_node->get_first_char()) :
								utf8_bytes(
										(unsigned char *) r_node->get_first_char()));
			else if (*it == FEATURE_MACRO_RIGHT_ENDING_CHAR)
				f += *(r_node->end_string);
			else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE)
				f += int2string(r_node->char_family);
			else if (*it == FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE)
				f += int2string(r_node->end_char_family);
		}
		fset.push_back(f);
	}
}

void FeatureSet::extract_edge_feature(Dptoken* head, Dptoken* modifier) {

	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			ftmpl->get_templates()->begin();
			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
		if ((*tmpl_it)->get_type() != "EDGE") // skip other feature template
			continue;
		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			// left
			if (*it == DPFEATURE_MACRO_HEAD_WORD)
				f += head->word;
			else if (*it == DPFEATURE_MACRO_HEAD_POS)
				f += head->pos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_WORD)
				f += modifier->word;
			else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
				f += modifier->pos;

			else if (*it == DPFEATURE_MACRO_HEAD_LEFT_POS)
				f += head->leftPos;
			else if (*it == DPFEATURE_MACRO_HEAD_RIGHT_POS)
				f += head->rightPos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_LEFT_POS)
				f += modifier->leftPos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_RIGHT_POS)
				f += modifier->rightPos;

			else if (*it == DPFEATURE_MACRO_DIRECTION) {
				if (head->position < modifier->position)
					f += "#LEFT";
				else if (head->position > modifier->position)
					f += "#RIGHT";
				else
					cerr << "error: head identical with modifier" << endl;
			} else if (*it == DPFEATURE_MACRO_DISTANCE) {
				int tempDist = abs((int) (head->position - modifier->position));
				if (tempDist < 6)
					f += int2string(tempDist);
				else if (tempDist < 11)
					f += int2string(10);
				else
					f += int2string(50);
			}
		}
		fset.push_back(f);
	}
}

void FeatureSet::extract_context_feature(Dptoken* head, Dptoken* modifier,
		std::vector<Morph::Dptoken> *dpTokenArray) {
	if (head->position < modifier->position) {

		for (std::vector<FeatureTemplate *>::iterator tmpl_it =
				ftmpl->get_templates()->begin();
				tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {

			if ((*tmpl_it)->get_type() != "CONTEXT") // skip other feature template
				continue;
			if (head->position == modifier->position - 1) {
				std::string f = (*tmpl_it)->get_name() + ":";
				for (std::vector<unsigned int>::iterator it =
						(*tmpl_it)->get_features()->begin();
						it != (*tmpl_it)->get_features()->end(); it++) {

					if (it != (*tmpl_it)->get_features()->begin())
						f += ",";
					// left
					if (*it == DPFEATURE_MACRO_HEAD_POS)
						f += head->pos;
					else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
						f += modifier->pos;
					else if (*it == DPFEATURE_MACRO_INBETWEEN_POS)
						f += "#FIRST_DEP";
					else if (*it == DPFEATURE_MACRO_DIRECTION) {
						if (head->position < modifier->position)
							f += "#LEFT";
						else if (head->position > modifier->position)
							f += "#RIGHT";
						else
							cerr << "error: head identical with modifier"
									<< endl;
					} else if (*it == DPFEATURE_MACRO_DISTANCE) {
						int tempDist = abs(
								(int) (head->position - modifier->position));
						if (tempDist < 6)
							f += int2string(tempDist);
						else if (tempDist < 11)
							f += int2string(10);
						else
							f += int2string(50);
					}
				}
				fset.push_back(f);
			}
			for (size_t ibt_it = head->position + 1;
					ibt_it != modifier->position; ibt_it++) {
				std::string f = (*tmpl_it)->get_name() + ":";
				for (std::vector<unsigned int>::iterator it =
						(*tmpl_it)->get_features()->begin();
						it != (*tmpl_it)->get_features()->end(); it++) {

					if (it != (*tmpl_it)->get_features()->begin())
						f += ",";
					// left
					if (*it == DPFEATURE_MACRO_HEAD_POS)
						f += head->pos;
					else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
						f += modifier->pos;
					else if (*it == DPFEATURE_MACRO_INBETWEEN_POS)
						f += (*dpTokenArray)[ibt_it].pos;
					else if (*it == DPFEATURE_MACRO_DIRECTION) {
						if (head->position < modifier->position)
							f += "#LEFT";
						else if (head->position > modifier->position)
							f += "#RIGHT";
						else
							cerr << "error: head identical with modifier"
									<< endl;
					} else if (*it == DPFEATURE_MACRO_DISTANCE) {
						int tempDist = abs(
								(int) (head->position - modifier->position));
						if (tempDist < 6)
							f += int2string(tempDist);
						else if (tempDist < 11)
							f += int2string(10);
						else
							f += int2string(50);
					}

				}
				fset.push_back(f);
			}
		}
	} else if (head->position > modifier->position) {
		for (std::vector<FeatureTemplate *>::iterator tmpl_it =
				ftmpl->get_templates()->begin();
				tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {

			if ((*tmpl_it)->get_type() != "CONTEXT") // skip other feature template
				continue;
			if (head->position == modifier->position + 1) {
				std::string f = (*tmpl_it)->get_name() + ":";
				for (std::vector<unsigned int>::iterator it =
						(*tmpl_it)->get_features()->begin();
						it != (*tmpl_it)->get_features()->end(); it++) {

					if (it != (*tmpl_it)->get_features()->begin())
						f += ",";
					// left
					if (*it == DPFEATURE_MACRO_HEAD_POS)
						f += head->pos;
					else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
						f += modifier->pos;
					else if (*it == DPFEATURE_MACRO_INBETWEEN_POS)
						f += "#FIRST_DEP";
					else if (*it == DPFEATURE_MACRO_DIRECTION) {
						if (head->position < modifier->position)
							f += "#LEFT";
						else if (head->position > modifier->position)
							f += "#RIGHT";
						else
							cerr << "error: head identical with modifier"
									<< endl;
					} else if (*it == DPFEATURE_MACRO_DISTANCE) {
						int tempDist = abs(
								(int) (head->position - modifier->position));
						if (tempDist < 6)
							f += int2string(tempDist);
						else if (tempDist < 11)
							f += int2string(10);
						else
							f += int2string(50);
					}
				}
				fset.push_back(f);
			}
			for (size_t ibt_it = modifier->position + 1;
					ibt_it != head->position; ibt_it++) {
				std::string f = (*tmpl_it)->get_name() + ":";
				for (std::vector<unsigned int>::iterator it =
						(*tmpl_it)->get_features()->begin();
						it != (*tmpl_it)->get_features()->end(); it++) {

					if (it != (*tmpl_it)->get_features()->begin())
						f += ",";
					// left
					if (*it == DPFEATURE_MACRO_HEAD_POS)
						f += head->pos;
					else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
						f += modifier->pos;
					else if (*it == DPFEATURE_MACRO_INBETWEEN_POS)
						f += (*dpTokenArray)[ibt_it].pos;
					else if (*it == DPFEATURE_MACRO_DIRECTION) {
						if (head->position < modifier->position)
							f += "#LEFT";
						else if (head->position > modifier->position)
							f += "#RIGHT";
						else
							cerr << "error: head identical with modifier"
									<< endl;
					} else if (*it == DPFEATURE_MACRO_DISTANCE) {
						int tempDist = abs(
								(int) (head->position - modifier->position));
						if (tempDist < 6)
							f += int2string(tempDist);
						else if (tempDist < 11)
							f += int2string(10);
						else
							f += int2string(50);
					}
				}
				fset.push_back(f);
			}
		}
	} else {
		cerr
				<< "error: head identical with modifier when extracting in-between feature "
				<< head->position << " " << modifier->position << endl;
	}
}

void FeatureSet::extract_wcedge_feature(Dptoken* head, Dptoken* modifier) {

	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			wcftmpl->get_templates()->begin();
			tmpl_it != wcftmpl->get_templates()->end(); tmpl_it++) {
		if ((*tmpl_it)->get_type() != "EDGE") // skip other feature template
			continue;
		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			// left
			if (*it == DPFEATURE_MACRO_HEAD_WORD)
				f += head->word;
			else if (*it == DPFEATURE_MACRO_HEAD_POS)
				f += head->pos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_WORD)
				f += modifier->word;
			else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
				f += modifier->pos;

			else if (*it == DPFEATURE_MACRO_HEAD_LEFT_POS)
				f += head->leftPos;
			else if (*it == DPFEATURE_MACRO_HEAD_RIGHT_POS)
				f += head->rightPos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_LEFT_POS)
				f += modifier->leftPos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_RIGHT_POS)
				f += modifier->rightPos;

			else if (*it == DPFEATURE_MACRO_HEAD_SB)
				f += head->shortBits;
			else if (*it == DPFEATURE_MACRO_HEAD_MB)
				f += head->longBits;
			else if (*it == DPFEATURE_MACRO_HEAD_FB)
				f += head->fullBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_SB)
				f += modifier->shortBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_MB)
				f += modifier->longBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_FB)
				f += modifier->fullBits;

			else if (*it == DPFEATURE_MACRO_DIRECTION) {
				if (head->position < modifier->position)
					f += "#LEFT";
				else if (head->position > modifier->position)
					f += "#RIGHT";
				else
					cerr << "error: head identical with modifier" << endl;
			} else if (*it == DPFEATURE_MACRO_DISTANCE) {
				int tempDist = abs((int) (head->position - modifier->position));
				if (tempDist < 6)
					f += int2string(tempDist);
				else if (tempDist < 11)
					f += int2string(10);
				else
					f += int2string(50);
			}
		}
		fset.push_back(f);
	}
}

void FeatureSet::extract_wccontext_feature(Dptoken* head, Dptoken* modifier,
		std::vector<Morph::Dptoken> *dpTokenArray) {
	if (head->position < modifier->position) {

		for (std::vector<FeatureTemplate *>::iterator tmpl_it =
				wcftmpl->get_templates()->begin();
				tmpl_it != wcftmpl->get_templates()->end(); tmpl_it++) {

			if ((*tmpl_it)->get_type() != "CONTEXT") // skip other feature template
				continue;
			if (head->position == modifier->position - 1) {
				std::string f = (*tmpl_it)->get_name() + ":";
				for (std::vector<unsigned int>::iterator it =
						(*tmpl_it)->get_features()->begin();
						it != (*tmpl_it)->get_features()->end(); it++) {

					if (it != (*tmpl_it)->get_features()->begin())
						f += ",";
					// left
					if (*it == DPFEATURE_MACRO_HEAD_POS)
						f += head->pos;
					else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
						f += modifier->pos;

					else if (*it == DPFEATURE_MACRO_HEAD_SB)
						f += head->shortBits;
					else if (*it == DPFEATURE_MACRO_HEAD_MB)
						f += head->longBits;
					else if (*it == DPFEATURE_MACRO_HEAD_FB)
						f += head->fullBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_SB)
						f += modifier->shortBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_MB)
						f += modifier->longBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_FB)
						f += modifier->fullBits;

					else if (*it == DPFEATURE_MACRO_INBETWEEN_POS)
						f += "#FIRST_DEP";
					else if (*it == DPFEATURE_MACRO_DIRECTION) {
						if (head->position < modifier->position)
							f += "#LEFT";
						else if (head->position > modifier->position)
							f += "#RIGHT";
						else
							cerr << "error: head identical with modifier"
									<< endl;
					} else if (*it == DPFEATURE_MACRO_DISTANCE) {
						int tempDist = abs(
								(int) (head->position - modifier->position));
						if (tempDist < 6)
							f += int2string(tempDist);
						else if (tempDist < 11)
							f += int2string(10);
						else
							f += int2string(50);
					}
				}
				fset.push_back(f);
			}
			for (size_t ibt_it = head->position + 1;
					ibt_it != modifier->position; ibt_it++) {
				std::string f = (*tmpl_it)->get_name() + ":";
				for (std::vector<unsigned int>::iterator it =
						(*tmpl_it)->get_features()->begin();
						it != (*tmpl_it)->get_features()->end(); it++) {

					if (it != (*tmpl_it)->get_features()->begin())
						f += ",";
					// left
					if (*it == DPFEATURE_MACRO_HEAD_POS)
						f += head->pos;
					else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
						f += modifier->pos;

					else if (*it == DPFEATURE_MACRO_HEAD_SB)
						f += head->shortBits;
					else if (*it == DPFEATURE_MACRO_HEAD_MB)
						f += head->longBits;
					else if (*it == DPFEATURE_MACRO_HEAD_FB)
						f += head->fullBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_SB)
						f += modifier->shortBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_MB)
						f += modifier->longBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_FB)
						f += modifier->fullBits;

					else if (*it == DPFEATURE_MACRO_INBETWEEN_POS)
						f += (*dpTokenArray)[ibt_it].pos;
					else if (*it == DPFEATURE_MACRO_DIRECTION) {
						if (head->position < modifier->position)
							f += "#LEFT";
						else if (head->position > modifier->position)
							f += "#RIGHT";
						else
							cerr << "error: head identical with modifier"
									<< endl;
					} else if (*it == DPFEATURE_MACRO_DISTANCE) {
						int tempDist = abs(
								(int) (head->position - modifier->position));
						if (tempDist < 6)
							f += int2string(tempDist);
						else if (tempDist < 11)
							f += int2string(10);
						else
							f += int2string(50);
					}

				}
				fset.push_back(f);
			}
		}
	} else if (head->position > modifier->position) {
		for (std::vector<FeatureTemplate *>::iterator tmpl_it =
				wcftmpl->get_templates()->begin();
				tmpl_it != wcftmpl->get_templates()->end(); tmpl_it++) {

			if ((*tmpl_it)->get_type() != "CONTEXT") // skip other feature template
				continue;
			if (head->position == modifier->position + 1) {
				std::string f = (*tmpl_it)->get_name() + ":";
				for (std::vector<unsigned int>::iterator it =
						(*tmpl_it)->get_features()->begin();
						it != (*tmpl_it)->get_features()->end(); it++) {

					if (it != (*tmpl_it)->get_features()->begin())
						f += ",";
					// left
					if (*it == DPFEATURE_MACRO_HEAD_POS)
						f += head->pos;
					else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
						f += modifier->pos;

					else if (*it == DPFEATURE_MACRO_HEAD_SB)
						f += head->shortBits;
					else if (*it == DPFEATURE_MACRO_HEAD_MB)
						f += head->longBits;
					else if (*it == DPFEATURE_MACRO_HEAD_FB)
						f += head->fullBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_SB)
						f += modifier->shortBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_MB)
						f += modifier->longBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_FB)
						f += modifier->fullBits;

					else if (*it == DPFEATURE_MACRO_INBETWEEN_POS)
						f += "#FIRST_DEP";
					else if (*it == DPFEATURE_MACRO_DIRECTION) {
						if (head->position < modifier->position)
							f += "#LEFT";
						else if (head->position > modifier->position)
							f += "#RIGHT";
						else
							cerr << "error: head identical with modifier"
									<< endl;
					} else if (*it == DPFEATURE_MACRO_DISTANCE) {
						int tempDist = abs(
								(int) (head->position - modifier->position));
						if (tempDist < 6)
							f += int2string(tempDist);
						else if (tempDist < 11)
							f += int2string(10);
						else
							f += int2string(50);
					}
				}
				fset.push_back(f);
			}
			for (size_t ibt_it = modifier->position + 1;
					ibt_it != head->position; ibt_it++) {
				std::string f = (*tmpl_it)->get_name() + ":";
				for (std::vector<unsigned int>::iterator it =
						(*tmpl_it)->get_features()->begin();
						it != (*tmpl_it)->get_features()->end(); it++) {

					if (it != (*tmpl_it)->get_features()->begin())
						f += ",";
					// left
					if (*it == DPFEATURE_MACRO_HEAD_POS)
						f += head->pos;
					else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
						f += modifier->pos;

					else if (*it == DPFEATURE_MACRO_HEAD_SB)
						f += head->shortBits;
					else if (*it == DPFEATURE_MACRO_HEAD_MB)
						f += head->longBits;
					else if (*it == DPFEATURE_MACRO_HEAD_FB)
						f += head->fullBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_SB)
						f += modifier->shortBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_MB)
						f += modifier->longBits;
					else if (*it == DPFEATURE_MACRO_MODIFIER_FB)
						f += modifier->fullBits;

					else if (*it == DPFEATURE_MACRO_INBETWEEN_POS)
						f += (*dpTokenArray)[ibt_it].pos;
					else if (*it == DPFEATURE_MACRO_DIRECTION) {
						if (head->position < modifier->position)
							f += "#LEFT";
						else if (head->position > modifier->position)
							f += "#RIGHT";
						else
							cerr << "error: head identical with modifier"
									<< endl;
					} else if (*it == DPFEATURE_MACRO_DISTANCE) {
						int tempDist = abs(
								(int) (head->position - modifier->position));
						if (tempDist < 6)
							f += int2string(tempDist);
						else if (tempDist < 11)
							f += int2string(10);
						else
							f += int2string(50);
					}
				}
				fset.push_back(f);
			}
		}
	} else {
		cerr
				<< "error: head identical with modifier when extracting in-between feature "
				<< head->position << " " << modifier->position << endl;
	}
}

//void FeatureSet::extract_sibling_feature(Dptoken* head, Dptoken* modifier) {
//
//	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
//			ftmpl->get_templates()->begin();
//			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
//		if ((*tmpl_it)->get_type() != "SIBLING") // skip other feature template
//			continue;
//		std::string f = (*tmpl_it)->get_name() + ":";
//		for (std::vector<unsigned int>::iterator it =
//				(*tmpl_it)->get_features()->begin();
//				it != (*tmpl_it)->get_features()->end(); it++) {
//			if (it != (*tmpl_it)->get_features()->begin())
//				f += ",";
//			// left
//			if (*it == DPFEATURE_MACRO_HEAD_WORD)
//				f += head->word;
//			else if (*it == DPFEATURE_MACRO_HEAD_POS)
//				f += head->pos;
//			else if (*it == DPFEATURE_MACRO_MODIFIER_WORD)
//				f += modifier->word;
//			else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
//				f += modifier->pos;
//
//			else if (*it == DPFEATURE_MACRO_SIBLING_WORD)
//				f += "#FSW";
//			else if (*it == DPFEATURE_MACRO_SIBLING_POS)
//				f += "#FSP";
//
//			else if (*it == DPFEATURE_MACRO_DIRECTION) {
//				if (head->position < modifier->position)
//					f += "#LEFT";
//				else if (head->position > modifier->position)
//					f += "#RIGHT";
//				else
//					std::cerr << "error: head identical with modifier"
//							<< std::endl;
//			} else if (*it == DPFEATURE_MACRO_DISTANCE) {
//				int tempDist = abs((int) (head->position - modifier->position));
//				if (tempDist < 6)
//					f += int2string(tempDist);
//				else if (tempDist < 11)
//					f += int2string(10);
//				else
//					f += int2string(50);
//			}
//		}
//		fset.push_back(f);
//	}
//
//}

void FeatureSet::extract_sibling_feature(Dptoken* head, Dptoken* modifier,
		Dptoken* sibling) {

	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			ftmpl->get_templates()->begin();
			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
		if ((*tmpl_it)->get_type() != "SIBLING") // skip other feature template
			continue;
		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			// left
			if (*it == DPFEATURE_MACRO_HEAD_WORD)
				f += head->word;
			else if (*it == DPFEATURE_MACRO_HEAD_POS)
				f += head->pos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_WORD)
				f += modifier->word;
			else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
				f += modifier->pos;

			else if (*it == DPFEATURE_MACRO_SIBLING_WORD)
				f += sibling->word;
			else if (*it == DPFEATURE_MACRO_SIBLING_POS)
				f += sibling->pos;

			else if (*it == DPFEATURE_MACRO_DIRECTION) {
				if (sibling->position < modifier->position)
					f += "#LEFT";
				else if (sibling->position > modifier->position)
					f += "#RIGHT";
				else
					std::cerr << "error: head identical with modifier"
							<< std::endl;
			} else if (*it == DPFEATURE_MACRO_DISTANCE) {
				int tempDist = abs(
						(int) (sibling->position - modifier->position));
				if (tempDist < 6)
					f += int2string(tempDist);
				else if (tempDist < 11)
					f += int2string(10);
				else
					f += int2string(50);
			}
		}
		fset.push_back(f);
	}

}

void FeatureSet::extract_sibling_feature(Dptoken* head, Dptoken* modifier) {
	std::string h1 = "HpSpMp";
	std::string h2 = "SpMp";
	std::string h3 = "SpMw";
	std::string h4 = "SwMp";
	std::string h5 = "SwMw";

	std::string hp = head->pos;

	std::string mp = modifier->pos;

	std::string mw = modifier->word;

	if (head->position < modifier->position) {

		fset.push_back(h1 + ":" + hp + "," + "#FRSP" + "," + mp);
		fset.push_back(h2 + ":" + "#FRSP" + "," + mp);
		fset.push_back(h3 + ":" + "#FRSP" + "," + mw);
		fset.push_back(h4 + ":" + "#FRSW" + "," + mp);
		fset.push_back(h5 + ":" + "#FRSW" + "," + mw);

	} else if (head->position > modifier->position) {

		fset.push_back(h1 + ":" + hp + "," + "#FLSP" + "," + mp);
		fset.push_back(h2 + ":" + "#FLSP" + "," + mp);
		fset.push_back(h3 + ":" + "#FLSP" + "," + mw);
		fset.push_back(h4 + ":" + "#FLSW" + "," + mp);
		fset.push_back(h5 + ":" + "#FLSW" + "," + mw);

	} else {
		std::cerr << "error: head identical with modifier" << std::endl;
	}

}

void FeatureSet::extract_wcsibling_feature(Dptoken* head, Dptoken* modifier,
		Dptoken* sibling) {

	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			wcftmpl->get_templates()->begin();
			tmpl_it != wcftmpl->get_templates()->end(); tmpl_it++) {
		if ((*tmpl_it)->get_type() != "SIBLING") // skip other feature template
			continue;
		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			// left
			if (*it == DPFEATURE_MACRO_HEAD_WORD)
				f += head->word;
			else if (*it == DPFEATURE_MACRO_HEAD_POS)
				f += head->pos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_WORD)
				f += modifier->word;
			else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
				f += modifier->pos;

			else if (*it == DPFEATURE_MACRO_SIBLING_WORD)
				f += sibling->word;
			else if (*it == DPFEATURE_MACRO_SIBLING_POS)
				f += sibling->pos;

			else if (*it == DPFEATURE_MACRO_HEAD_SB)
				f += head->shortBits;
			else if (*it == DPFEATURE_MACRO_HEAD_MB)
				f += head->longBits;
			else if (*it == DPFEATURE_MACRO_HEAD_FB)
				f += head->fullBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_SB)
				f += modifier->shortBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_MB)
				f += modifier->longBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_FB)
				f += modifier->fullBits;
			else if (*it == DPFEATURE_MACRO_SIBLING_SB)
				f += sibling->shortBits;
			else if (*it == DPFEATURE_MACRO_SIBLING_MB)
				f += sibling->longBits;
			else if (*it == DPFEATURE_MACRO_SIBLING_FB)
				f += sibling->fullBits;

			else if (*it == DPFEATURE_MACRO_DIRECTION) {
				if (sibling->position < modifier->position)
					f += "#LEFT";
				else if (sibling->position > modifier->position)
					f += "#RIGHT";
				else
					std::cerr << "error: head identical with modifier"
							<< std::endl;
			} else if (*it == DPFEATURE_MACRO_DISTANCE) {
				int tempDist = abs(
						(int) (sibling->position - modifier->position));
				if (tempDist < 6)
					f += int2string(tempDist);
				else if (tempDist < 11)
					f += int2string(10);
				else
					f += int2string(50);
			}
		}
		fset.push_back(f);
	}

}

void FeatureSet::extract_wcsibling_feature(Dptoken* head, Dptoken* modifier) {


	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			wcftmpl->get_templates()->begin();
			tmpl_it != wcftmpl->get_templates()->end(); tmpl_it++) {
		if ((*tmpl_it)->get_type() != "SIBLING") // skip other feature template
			continue;

		if((*tmpl_it)->get_features()->back()==DPFEATURE_MACRO_DISTANCE ||(*tmpl_it)->get_features()->back()==DPFEATURE_MACRO_DIRECTION)
			continue;

		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			// left
			if (*it == DPFEATURE_MACRO_HEAD_WORD)
				f += head->word;
			else if (*it == DPFEATURE_MACRO_HEAD_POS)
				f += head->pos;
			else if (*it == DPFEATURE_MACRO_MODIFIER_WORD)
				f += modifier->word;
			else if (*it == DPFEATURE_MACRO_MODIFIER_POS)
				f += modifier->pos;

			else if (*it == DPFEATURE_MACRO_SIBLING_WORD){
				if (head->position < modifier->position)
					f += "#FRSW";
				else if (head->position > modifier->position)
					f += "#FLSW";
				else
					std::cerr << "error: head identical with modifier" << std::endl;
			}
			else if (*it == DPFEATURE_MACRO_SIBLING_POS){
				if (head->position < modifier->position)
					f += "#FRSP";
				else if (head->position > modifier->position)
					f += "#FLSP";
				else
					std::cerr << "error: head identical with modifier" << std::endl;
			}

			else if (*it == DPFEATURE_MACRO_HEAD_SB)
				f += head->shortBits;
			else if (*it == DPFEATURE_MACRO_HEAD_MB)
				f += head->longBits;
			else if (*it == DPFEATURE_MACRO_HEAD_FB)
				f += head->fullBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_SB)
				f += modifier->shortBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_MB)
				f += modifier->longBits;
			else if (*it == DPFEATURE_MACRO_MODIFIER_FB)
				f += modifier->fullBits;

		}
		fset.push_back(f);
	}

}

void FeatureSet::extract_sibling_feature_complex(Dptoken* head,
		Dptoken* modifier) {
	std::string h1 = "HpSpMp";
	std::string h2 = "SpMp";
	std::string h3 = "SpMw";
	std::string h4 = "SwMp";
	std::string h5 = "SwMw";
	std::string h11 = "HpSpMpDirDis";
	std::string h12 = "SpMpDirDis";
	std::string h13 = "SpMwDirDis";
	std::string h14 = "SwMpDirDis";
	std::string h15 = "SwMwDirDis";

	std::string hp = head->pos;

	std::string mp = modifier->pos;

	std::string mw = modifier->word;

	fset.push_back(h1 + ":" + hp + "," + "#FSP" + "," + mp);
	fset.push_back(h2 + ":" + "#FSP" + "," + mp);
	fset.push_back(h3 + ":" + "#FSP" + "," + mw);
	fset.push_back(h4 + ":" + "#FSW" + "," + mp);
	fset.push_back(h5 + ":" + "#FSW" + "," + mw);

	if (head->position < modifier->position) {

		fset.push_back(
				h11 + ":" + hp + "," + "#FSP" + "," + mp + "," + "#RIGHT" + ","
						+ "0");
		fset.push_back(
				h12 + ":" + "#FSP" + "," + mp + "," + "#RIGHT" + "," + "0");
		fset.push_back(
				h13 + ":" + "#FSP" + "," + mw + "," + "#RIGHT" + "," + "0");
		fset.push_back(
				h14 + ":" + "#FSW" + "," + mp + "," + "#RIGHT" + "," + "0");
		fset.push_back(
				h15 + ":" + "#FSW" + "," + mw + "," + "#RIGHT" + "," + "0");

	} else if (head->position > modifier->position) {

		fset.push_back(
				h11 + ":" + hp + "," + "#FSP" + "," + mp + "," + "#LEFT" + ","
						+ "0");
		fset.push_back(
				h12 + ":" + "#FSP" + "," + mp + "," + "#LEFT" + "," + "0");
		fset.push_back(
				h13 + ":" + "#FSP" + "," + mw + "," + "#LEFT" + "," + "0");
		fset.push_back(
				h14 + ":" + "#FSW" + "," + mp + "," + "#LEFT" + "," + "0");
		fset.push_back(
				h15 + ":" + "#FSW" + "," + mw + "," + "#LEFT" + "," + "0");

	} else {
		std::cerr << "error: head identical with modifier" << std::endl;
	}

}
//void FeatureSet::extract_sibling_feature(Dptoken* head, Dptoken* modifier,
//		Dptoken* sibling) {
//	std::string h1 = "HpSpMp";
//	std::string h2 = "SpMp";
//	std::string h3 = "SpMw";
//	std::string h4 = "SwMp";
//	std::string h5 = "SwMw";
//
//	std::string f1 = ":";
//	std::string f2 = ":";
//	std::string f3 = ":";
//	std::string f4 = ":";
//	std::string f5 = ":";
//
//	f1 += head->pos;
//	f1 += ",";
//	f1 += sibling->pos;
//	f1 += ",";
//	f1 += modifier->pos;
//
//	f2 += sibling->pos;
//	f2 += ",";
//	f2 += modifier->pos;
//
//	f3 += sibling->pos;
//	f3 += ",";
//	f3 += modifier->word;
//
//	f4 += sibling->word;
//	f4 += ",";
//	f4 += modifier->pos;
//
//	f5 += sibling->word;
//	f5 += ",";
//	f5 += modifier->word;
//
//	fset.push_back(h1 + f1);
//	fset.push_back(h2 + f2);
//	fset.push_back(h3 + f3);
//	fset.push_back(h4 + f4);
//	fset.push_back(h5 + f5);
//
//	h1 += "DirDis";
//	h2 += "DirDis";
//	h3 += "DirDis";
//	h4 += "DirDis";
//	h5 += "DirDis";
//
//	f1 += ",";
//	f2 += ",";
//	f3 += ",";
//	f4 += ",";
//	f5 += ",";
//
//	if (sibling->position < modifier->position) {
//		f1 += "#LEFT";
//		f2 += "#LEFT";
//		f3 += "#LEFT";
//		f4 += "#LEFT";
//		f5 += "#LEFT";
//	} else if (sibling->position > modifier->position) {
//		f1 += "#RIGHT";
//		f2 += "#RIGHT";
//		f3 += "#RIGHT";
//		f4 += "#RIGHT";
//		f5 += "#RIGHT";
//	} else {
//		std::cerr << "error: head identical with modifier" << std::endl;
//	}
//
//	f1 += ",";
//	f2 += ",";
//	f3 += ",";
//	f4 += ",";
//	f5 += ",";
//
//	int tempDist = abs((int) (sibling->position - modifier->position));
//
//	if (tempDist < 6) {
//		f1 += int2string(tempDist);
//		f2 += int2string(tempDist);
//		f3 += int2string(tempDist);
//		f4 += int2string(tempDist);
//		f5 += int2string(tempDist);
//	} else if (tempDist < 11) {
//		f1 += int2string(10);
//		f2 += int2string(10);
//		f3 += int2string(10);
//		f4 += int2string(10);
//		f5 += int2string(10);
//	} else {
//		f1 += int2string(50);
//		f2 += int2string(50);
//		f3 += int2string(50);
//		f4 += int2string(50);
//		f5 += int2string(50);
//	}
//
//	fset.push_back(h1 + f1);
//	fset.push_back(h2 + f2);
//	fset.push_back(h3 + f3);
//	fset.push_back(h4 + f4);
//	fset.push_back(h5 + f5);
//
//}

//void FeatureSet::findAllChains(Morph::DptreeNode* subRoot, size_t n) {
//
//	chain.at(currentLevel) = subRoot;
//
//	if (subRoot->getIsLeaf() || n >= currentLevel) {
//		extract_depth_feature(&(chain));
//	} else {
//		currentLevel++;
//		for (std::vector<Morph::DptreeNode*>::iterator node_it =
//				subRoot->getLeftChildList()->begin();
//				node_it != subRoot->getLeftChildList()->end(); node_it++) {
//			subRoot->depthFirstTravel((*node_it));
//		}
//
//		for (std::vector<Morph::DptreeNode*>::iterator node_it =
//				subRoot->getRightChildList()->begin();
//				node_it != subRoot->getRightChildList()->end(); node_it++) {
//			subRoot->depthFirstTravel((*node_it));
//		}
//	}
//}

//void FeatureSet::depthFirstTravel(std::string &f1, std::string &f2,
//		Morph::DptreeNode* rootNode, Morph::DptreeNode* headNode,
//		size_t left_boundry, size_t right_boundry, size_t bottom_boundry) {
//
//	if (headNode->getLevel() - rootNode->getLevel() < bottom_boundry) {
//
//		for (std::vector<Morph::DptreeNode*>::iterator cd_it =
//				headNode->getLeftChildList()->begin();
//				cd_it != headNode->getLeftChildList()->end(); cd_it++) {
//			if (abs(
//					(*cd_it)->getDistanceToRoot()
//							- rootNode->getDistanceToRoot()) < left_boundry) {
//				appendNodeFeature(f1, 1, rootNode, *cd_it);
//				appendNodeFeature(f2, 2, rootNode, *cd_it);
//				if (!(*cd_it)->getIsLeaf()) {
//					depthFirstTravel(f1,f2,rootNode, (*cd_it), left_boundry,
//							right_boundry, bottom_boundry);
//				}
//			}
//		}
//
//		for (std::vector<Morph::DptreeNode*>::iterator cd_it =
//				headNode->getRightChildList()->begin();
//				cd_it != headNode->getRightChildList()->end(); cd_it++) {
//			if (abs(
//					(*cd_it)->getDistanceToRoot()
//							- rootNode->getDistanceToRoot()) < right_boundry) {
//				appendNodeFeature(f1, 1, rootNode, *cd_it);
//				appendNodeFeature(f2, 2, rootNode, *cd_it);
//				if (!(*cd_it)->getIsLeaf()) {
//					depthFirstTravel(f1,f2,rootNode, (*cd_it), left_boundry,
//							right_boundry, bottom_boundry);
//				}
//			}
//		}
//
//	}
//
//}

void FeatureSet::depthFirstTravel(std::string &f1, std::string &f2,
		Morph::DptreeNode* rootNode, Morph::DptreeNode* headNode,
		size_t left_boundry, size_t right_boundry, size_t bottom_boundry) {

	if (headNode->getLevel() - rootNode->getLevel() < bottom_boundry) {

		for (std::vector<Morph::DptreeNode*>::iterator cd_it =
				headNode->getLeftChildList()->begin();
				cd_it != headNode->getLeftChildList()->end(); cd_it++) {
			if (abs(
					(*cd_it)->getDistanceToRoot()
							- rootNode->getDistanceToRoot()) < left_boundry) {
				appendNodeFeature(f1, 1, rootNode, *cd_it);
				appendNodeFeature(f2, 2, rootNode, *cd_it);
				if (!(*cd_it)->getIsLeaf()) {
					depthFirstTravel(f1, f2, rootNode, (*cd_it), left_boundry,
							right_boundry, bottom_boundry);
				}
			}
		}

		for (std::vector<Morph::DptreeNode*>::iterator cd_it =
				headNode->getRightChildList()->begin();
				cd_it != headNode->getRightChildList()->end(); cd_it++) {
			if (abs(
					(*cd_it)->getDistanceToRoot()
							- rootNode->getDistanceToRoot()) < right_boundry) {
				appendNodeFeature(f1, 1, rootNode, *cd_it);
				appendNodeFeature(f2, 2, rootNode, *cd_it);
				if (!(*cd_it)->getIsLeaf()) {
					depthFirstTravel(f1, f2, rootNode, (*cd_it), left_boundry,
							right_boundry, bottom_boundry);
				}
			}
		}

	}

}

void FeatureSet::appendNodeFeature(std::string& f, int complexity,
		Morph::DptreeNode* rootNode, Morph::DptreeNode* headNode) {

	if (complexity == 1) {
		f += "[";
		f += "_,";
//		f += "ISR::";
		f += int2string(headNode->getIsRightBranch());
		f += ",";
//		f += "RLV::";
		f += int2string((int) (headNode->getLevel() - rootNode->getLevel()));
		f += "]";
	} else if (complexity == 2) {
		f += "[";
//		f += "P::";
		f += headNode->getNode()->pos;
		f += ",";
//		f += "ISR::";
		f += int2string(headNode->getIsRightBranch());
		f += ",";
//		f += "RLV::";
		f += int2string((int) (headNode->getLevel() - rootNode->getLevel()));
		f += "]";
	} else if (complexity == 3) {
		f += "[";
		f += "P::";
		f += headNode->getNode()->pos;
		f += ",";
		f += "W::";
		f += headNode->getNode()->word;
		f += ",";
		f += "ISR::";
		f += int2string(headNode->getIsRightBranch());
		f += ",";
		f += "RLV::";
		f += int2string((int) (headNode->getLevel() - rootNode->getLevel()));
		f += ",";
		f += "Anchor::";
		f += int2string(headNode->getIsAnchor());
		f += "]";
	} else if (complexity == 4) {
		f += "[";
		f += "P::";
		f += headNode->getNode()->pos;
		f += ",";
		f += "W::";
		f += headNode->getNode()->word;
		f += ",";
		f += "LF::";
		f += int2string(headNode->getIsLeaf());
		f += ",";
		f += "ISR::";
		f += int2string(headNode->getIsRightBranch());
		f += ",";
		f += "D2H::";
		f += int2string(headNode->getDistanceToHead());
		f += ",";
		f += "D2S::";
		f += int2string(
				(int) (headNode->getNode()->position
						- rootNode->getNode()->position));
		f += ",";

		f += "D2R::";
		f += int2string(headNode->getDistanceToRoot());
		f += ",";

		f += "ALV::";
		f += int2string((int) (headNode->getLevel()));
		f += ",";

		f += "RLV::";
		f += int2string((int) (headNode->getLevel() - rootNode->getLevel()));

		f += ",";
		f += "VA::";
		f += int2string((int) (headNode->getValence()));
		f += "]";
	} else {

	}
}

void FeatureSet::appendRootFeature(std::string& f, int complexity,
		Morph::DptreeNode* dpNode) {
	if (complexity == 1) {
		f += "{";
//		f += "P::";
		f += dpNode->getNode()->pos;
		f += "}";
	} else if (complexity == 2) {
		f += "{";
//		f += "P::";
		f += dpNode->getNode()->pos;
		f += ",";
//		f += "W::";
		f += dpNode->getNode()->word;
		f += "}";

	} else if (complexity == 3) {
		f += "{";
		f += "P::";
		f += dpNode->getNode()->pos;
		f += ",";
		f += "W::";
		f += dpNode->getNode()->word;
		f += ",";
		f += "VA::";
		f += int2string((int) (dpNode->getValence()));
		f += "}";

	} else if (complexity == 4) {
		f += "{";
		f += "P::";
		f += dpNode->getNode()->pos;
		f += ",";
		f += "W::";
		f += dpNode->getNode()->word;
		f += ",";
		f += "VA::";
		f += int2string((int) (dpNode->getValence()));
		f += "}";

	}
}

void FeatureSet::appendDepthNodeFeature(std::string& f, int complexity,
		Morph::DptreeNode* headNode) {

	if (complexity == 1) {
		f += "[";
		f += "_,";
		f += int2string(headNode->getIsRightBranch());
		f += "]";
	} else if (complexity == 2) {
		f += "[";
		f += headNode->getNode()->pos;
		f += ",";
		f += int2string(headNode->getIsRightBranch());
		f += "]";
	} else if (complexity == 3) {
		f += "[";
		f += headNode->getNode()->pos;
		f += ",";
		f += headNode->getNode()->word;
		f += ",";
		f += int2string(headNode->getIsRightBranch());
		f += "]";
	} else {

	}
}

void FeatureSet::appendAncestorNodeFeature(std::string& f, int complexity,
		Morph::DptreeNode* modifierNode, Morph::DptreeNode* headNode) {

	if (complexity == 1) {
		f += "[";
		f += "_,";
		if (modifierNode->getNode()->position < headNode->getNode()->position) {
			f += "-1";
		} else if (modifierNode->getNode()->position
				> headNode->getNode()->position) {
			f += "1";
		} else {
			cerr << "E: ancestor feature: identical positions" << endl;
		}
		f += "]";
	} else if (complexity == 2) {
		f += "[";
		f += headNode->getNode()->pos;
		f += ",";
		if (modifierNode->getNode()->position < headNode->getNode()->position) {
			f += "-1";
		} else if (modifierNode->getNode()->position
				> headNode->getNode()->position) {
			f += "1";
		} else {
			cerr << "E: ancestor feature: identical positions" << endl;
		}
		f += "]";
	} else if (complexity == 3) {
		f += "[";
		f += headNode->getNode()->pos;
		f += ",";
		f += headNode->getNode()->word;
		f += ",";
		if (modifierNode->getNode()->position < headNode->getNode()->position) {
			f += "-1";
		} else if (modifierNode->getNode()->position
				> headNode->getNode()->position) {
			f += "1";
		} else {
			cerr << "E: ancestor feature: identical positions" << endl;
		}
		f += "]";
	} else {

	}
}

void FeatureSet::appendUnitNodeFeature(std::string &f,
		Morph::DptreeNode* dpNode) {

	f += "P::";
	f += dpNode->getNode()->pos;
	f += ",";
	f += "W::";
	f += dpNode->getNode()->word;
	f += ",";
	f += "VA::";
	f += int2string((int) (dpNode->getValence()));

	//temp
	f += ",";
	f += "MD::";
	f += int2string((int) (dpNode->getValence()));
	f += ",";
	f += "AD::";
	f += int2string((int) (dpNode->getValence()));
	f += ",";
	f += "LC::";
	f += int2string((int) (dpNode->getValence()));
	f += ",";
	f += "RC::";
	f += int2string((int) (dpNode->getValence()));

}

//void FeatureSet::extract_rerank_feature(Dptree* dptree, size_t left_boundry,
//		size_t right_boundry, size_t bottom_boundry) {
//
//	for (std::vector<Morph::DptreeNode>::iterator tn_it =
//			dptree->dpTreeNodeAry.begin(); tn_it != dptree->dpTreeNodeAry.end();
//			tn_it++) {
//		for (size_t lb = 0; lb < left_boundry; lb++) {
//			for (size_t rb = 0; rb < right_boundry; rb++) {
//				for (size_t bb = 0; bb < bottom_boundry; bb++) {
//
//					if (lb == 0 && rb == 0 && bb == 0)
//						continue;
//
//					std::string r1;
//					std::string r2;
//					std::string f1;
//					std::string f2;
//					std::string c11;
//					std::string c12;
//					std::string c21;
//					std::string c22;
//
//					appendRootFeature(r1, 1, &(*tn_it));
//					appendRootFeature(r2, 2, &(*tn_it));
//
//					depthFirstTravel(f1, f2, &(*tn_it), &(*tn_it), lb, rb, bb);
//
//					c11 = r1 + f1;
//					c12 = r1 + f2;
//					c21 = r2 + f1;
//					c22 = r2 + f2;
//					rfset.insert(c11);
//					rfset.insert(c12);
//					rfset.insert(c21);
//					rfset.insert(c22);
//				}
//			}
//		}
//	}
//
//}

//void FeatureSet::extract_rerank_feature(Dptree* dptree, size_t left_boundry,
//		size_t right_boundry, size_t bottom_boundry) {
//
//	for (std::vector<Morph::DptreeNode>::iterator tn_it =
//			dptree->dpTreeNodeAry.begin(); tn_it != dptree->dpTreeNodeAry.end();
//			tn_it++) {
//
//		tn_it->exploreSubtree(&(*tn_it), left_boundry, right_boundry,
//				bottom_boundry);
//
//		std::vector<int> leftMileStones;
//		std::vector<int> rightMileStones;
//
//		size_t leftPen = 1;
//		size_t rightPen = 1;
//
//		if (tn_it->getLeftBranchSequence()->size() > 0) {
//
//			leftMileStones.push_back(0);
//
//			while (leftPen < tn_it->getLeftBranchSequence()->size()) {
//				if (tn_it->getLeftBranchSequence()->at(leftPen)->getLevel()
//						> tn_it->getLeftBranchSequence()->at(
//								leftMileStones.back())->getLevel()) {
//					leftPen++;
//				} else {
//					leftMileStones.push_back(leftPen);
//					leftPen++;
//				}
//			}
//		}
//
//		if (tn_it->getRightBranchSequence()->size() > 0) {
//
//			rightMileStones.push_back(0);
//
//			while (rightPen < tn_it->getRightBranchSequence()->size()) {
//				if (tn_it->getRightBranchSequence()->at(rightPen)->getLevel()
//						> tn_it->getRightBranchSequence()->at(
//								rightMileStones.back())->getLevel()) {
//					rightPen++;
//				} else {
//					rightMileStones.push_back(rightPen);
//					rightPen++;
//				}
//			}
//		}
//
//		if (tn_it->getLeftBranchSequence()->size() == 0) {
//			for (std::vector<int>::iterator rb_it = rightMileStones.begin();
//					rb_it != rightMileStones.end(); rb_it++) {
//				for (size_t bb = 0; bb <= tn_it->getSubTreeDepth(); bb++) {
//
//					std::string r1;
//					std::string r2;
//					std::string f1;
//					std::string f2;
//					std::string c11;
//					std::string c12;
//					std::string c21;
//					std::string c22;
//
//					appendRootFeature(r1, 1, &(*tn_it));
//					appendRootFeature(r2, 2, &(*tn_it));
//
//					for (size_t right_it = (*rb_it);
//							right_it < tn_it->getRightBranchSequence()->size();
//							right_it++) {
//						if (tn_it->getRightBranchSequence()->at(right_it)->getLevel()
//								- tn_it->getLevel() <= bb) {
//							appendNodeFeature(f1, 1, &(*tn_it),
//									tn_it->getRightBranchSequence()->at(
//											right_it));
//							appendNodeFeature(f2, 2, &(*tn_it),
//									tn_it->getRightBranchSequence()->at(
//											right_it));
//						}
//					}
//
//					c11 = r1 + f1;
//					c12 = r1 + f2;
//					c21 = r2 + f1;
//					c22 = r2 + f2;
//					rfset.insert(c11);
//					rfset.insert(c12);
//					rfset.insert(c21);
//					rfset.insert(c22);
//
//				}
//			}
//		}
//		if (tn_it->getRightBranchSequence()->size() == 0) {
//			for (std::vector<int>::iterator lb_it = leftMileStones.begin();
//					lb_it != leftMileStones.end(); lb_it++) {
//				for (size_t bb = 0; bb <= tn_it->getSubTreeDepth(); bb++) {
//
//					std::string r1;
//					std::string r2;
//					std::string f1;
//					std::string f2;
//					std::string c11;
//					std::string c12;
//					std::string c21;
//					std::string c22;
//
//					appendRootFeature(r1, 1, &(*tn_it));
//					appendRootFeature(r2, 2, &(*tn_it));
//
//					for (size_t left_it = (*lb_it);
//							left_it < tn_it->getLeftBranchSequence()->size();
//							left_it++) {
//
//						if (tn_it->getLeftBranchSequence()->at(left_it)->getLevel()
//								- tn_it->getLevel() <= bb) {
//							appendNodeFeature(f1, 1, &(*tn_it),
//									tn_it->getLeftBranchSequence()->at(
//											left_it));
//							appendNodeFeature(f2, 2, &(*tn_it),
//									tn_it->getLeftBranchSequence()->at(
//											left_it));
//						}
//
//					}
//
//					c11 = r1 + f1;
//					c12 = r1 + f2;
//					c21 = r2 + f1;
//					c22 = r2 + f2;
//					rfset.insert(c11);
//					rfset.insert(c12);
//					rfset.insert(c21);
//					rfset.insert(c22);
//
//				}
//			}
//		}
//
//		for (std::vector<int>::iterator lb_it = leftMileStones.begin();
//				lb_it != leftMileStones.end(); lb_it++) {
//			for (std::vector<int>::iterator rb_it = rightMileStones.begin();
//					rb_it != rightMileStones.end(); rb_it++) {
//				for (size_t bb = 0; bb <= tn_it->getSubTreeDepth(); bb++) {
//
//					std::string r1;
//					std::string r2;
//					std::string f1;
//					std::string f2;
//					std::string c11;
//					std::string c12;
//					std::string c21;
//					std::string c22;
//
//					appendRootFeature(r1, 1, &(*tn_it));
//					appendRootFeature(r2, 2, &(*tn_it));
//
//					for (size_t left_it = (*lb_it);
//							left_it < tn_it->getLeftBranchSequence()->size();
//							left_it++) {
//
//						if (tn_it->getLeftBranchSequence()->at(left_it)->getLevel()
//								- tn_it->getLevel() <= bb) {
//							appendNodeFeature(f1, 1, &(*tn_it),
//									tn_it->getLeftBranchSequence()->at(
//											left_it));
//							appendNodeFeature(f2, 2, &(*tn_it),
//									tn_it->getLeftBranchSequence()->at(
//											left_it));
//						}
//
//					}
//					for (size_t right_it = (*rb_it);
//							right_it < tn_it->getRightBranchSequence()->size();
//							right_it++) {
//						if (tn_it->getRightBranchSequence()->at(right_it)->getLevel()
//								- tn_it->getLevel() <= bb) {
//							appendNodeFeature(f1, 1, &(*tn_it),
//									tn_it->getRightBranchSequence()->at(
//											right_it));
//							appendNodeFeature(f2, 2, &(*tn_it),
//									tn_it->getRightBranchSequence()->at(
//											right_it));
//						}
//					}
//
//					c11 = r1 + f1;
//					c12 = r1 + f2;
//					c21 = r2 + f1;
//					c22 = r2 + f2;
//					rfset.insert(c11);
//					rfset.insert(c12);
//					rfset.insert(c21);
//					rfset.insert(c22);
//
//				}
//			}
//		}
//
//	}
//
//}
//
//void FeatureSet::extract_depth_feature(Dptree* dptree, size_t length_limit) {
//
//	for (std::vector<Morph::DptreeNode>::iterator tn_it =
//			dptree->dpTreeNodeAry.begin(); tn_it != dptree->dpTreeNodeAry.end();
//			tn_it++) {
//
//		if (tn_it->getIsRoot())
//			continue;
//
//		int chainLength = 0;
//
//		std::deque<Morph::DptreeNode*> chain;
//
//		Morph::DptreeNode* currentTreeNodePtr = &(*tn_it);
//
//		chain.push_front(currentTreeNodePtr);
//
//		chainLength++;
//
////		while (chainLength <= length_limit && !(currentTreeNodePtr->getIsRoot())) {
////			currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
////					currentTreeNodePtr->getParentNode()->position));
////			chain.push_front(currentTreeNodePtr);
////			chainLength++;
////		}
//
//		while (!(currentTreeNodePtr->getIsRoot())) {
//			currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
//					currentTreeNodePtr->getParentNode()->position));
//			chain.push_front(currentTreeNodePtr);
//		}
//
//		for (int i = 0; i < chain.size() - 1; i++) {
//			std::string r1;
//			std::string r2;
//			std::string f1;
//			std::string f2;
//			std::string c11;
//			std::string c12;
//			std::string c21;
//			std::string c22;
//
//			appendRootFeature(r1, 1, chain.at(i));
//			appendRootFeature(r2, 2, chain.at(i));
//
//			int stopPosition = chain.size() - 1 - i;
//			if (stopPosition > length_limit) {
//				stopPosition = length_limit;
//			}
//			for (int k = i + 1; k <= i + stopPosition; k++) {
//				appendNodeFeature(f1, 1, chain.at(i), chain.at(k));
//				appendNodeFeature(f2, 2, chain.at(i), chain.at(k));
//			}
//
//			c11 = "Ch:" + r1 + f1;
//			c12 = "Ch:" + r1 + f2;
//			c21 = "Ch:" + r2 + f1;
//			c22 = "Ch:" + r2 + f2;
//			rfset.insert(c11);
//			rfset.insert(c12);
//			rfset.insert(c21);
//			rfset.insert(c22);
//		}
////		if (currentToken == NULL)
////			cerr << "NullPtrErr : extract_depth_feature" << endl;
////
////		if (tk_it->position != currentToken->position)
////			cerr << "MissMatch : extract_depth_feature" << endl;
////		if (stopSign != 0)
////			cerr << "root not reached : extract_depth_feature" << endl;
//
//	}
//}

void FeatureSet::config_rerank_feature(Dptree* dptree, DptreeNode* subroot,
		size_t left_boundry, size_t right_boundry, size_t bottom_boundry) {

	subroot->exploreSubtree(subroot, left_boundry, right_boundry,
			bottom_boundry);

	std::vector<int> leftAnchors;
	std::vector<int> rightAnchors;

	int leftPen = 0;
	int rightPen = 0;

	std::vector<DptreeNode*> trimSeed;
	std::vector<std::vector<DptreeNode*> > trimTabLv1;
	std::vector<std::vector<std::vector<DptreeNode*> > > trimTabLv2;

	trimSeed.push_back(subroot);

	trimTabLv1.push_back(trimSeed);

	for (std::deque<Morph::DptreeNode*>::iterator lbs_it =
			subroot->getLeftBranchSequence()->begin();
			lbs_it != subroot->getLeftBranchSequence()->end(); lbs_it++) {
		if ((*lbs_it)->getIsAnchor() == 1) {
			leftAnchors.push_back(leftPen);
			trimTabLv1.push_back(trimSeed);
			//cerr << leftPen << " ";
		}
		leftPen++;
	}
	//cerr << endl;

	trimTabLv2.push_back(trimTabLv1);

	for (std::deque<Morph::DptreeNode*>::iterator rbs_it =
			subroot->getRightBranchSequence()->begin();
			rbs_it != subroot->getRightBranchSequence()->end(); rbs_it++) {
		if ((*rbs_it)->getIsAnchor() == 1) {
			rightAnchors.push_back(rightPen);
			trimTabLv2.push_back(trimTabLv1);
		}
		rightPen++;
	}

	//cerr << "depth from node: " << subroot->getSubTreeDepth() << endl;

	for (int bb = 0; bb <= subroot->getSubTreeDepth(); bb++) {
		subroot->trimSubTab.push_back(trimTabLv2);
	}

//  cerr << "trimmed sub-tree: " << endl;
//	for (int i = 0; i < subroot->trimSubTab.size(); i++) {
//		for (int j = 0; j < subroot->trimSubTab.at(i).size(); j++) {
//			for (int k = 0; k < subroot->trimSubTab.at(i).at(j).size(); k++) {
//				cerr << subroot->trimSubTab[i][j][k][0]->getNode()->word;
//			}
//			cerr << " ";
//		}
//		cerr << endl;
//	}
//	cerr << endl;

	if (leftPen != subroot->getLeftBranchSequence()->size()
			|| rightPen != subroot->getRightBranchSequence()->size()) {
		cerr << "E: pen not equal with sequence size." << endl;
	}

	for (int i = 0; i < subroot->trimSubTab.size(); i++) {
		for (int j = 0; j < subroot->trimSubTab.at(i).size(); j++) {
			for (int k = 0; k < subroot->trimSubTab.at(i).at(j).size(); k++) {

				if (subroot->getLeftBranchSequence()->size() != 0 && k > 0) {
					for (int left_it = leftAnchors.at(leftAnchors.size() - k);
							left_it < subroot->getLeftBranchSequence()->size();
							left_it++) {

						//cerr << "entered lv 1" << endl;

						if (subroot->getLeftBranchSequence()->at(left_it)->getLevel()
								- subroot->getLevel() <= i) {

							//cerr << "entered lv 2" << endl;

							subroot->trimSubTab[i][j][k].push_back(
									subroot->getLeftBranchSequence()->at(
											left_it));
						}
					}
				}

				//cerr << "entered lv 3" << endl;

				if (subroot->getRightBranchSequence()->size() != 0 && j > 0) {

					//cerr << "entered lv 4" << endl;

					for (int right_it =
							subroot->getRightBranchSequence()->size() - 1;
							right_it >= rightAnchors.at(j - 1); right_it--) {
						if (subroot->getRightBranchSequence()->at(right_it)->getLevel()
								- subroot->getLevel() <= i) {
							subroot->trimSubTab[i][j][k].push_back(
									subroot->getRightBranchSequence()->at(
											right_it));
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < subroot->trimSubTab.size(); i++) {
		for (int j = 0; j < subroot->trimSubTab.at(i).size(); j++) {
			for (int k = 0; k < subroot->trimSubTab.at(i).at(j).size(); k++) {

//				cout << "i " << i << "j " << j << "k " << k;
				for (int p = 0; p <= i; p++) {
					for (int q = 0; q <= j; q++) {
						for (int s = 0; s <= k; s++) {

							std::string f1 =
									subroot->trimSubTab[i][j][k][0]->rootStr1;
							std::string f2 =
									subroot->trimSubTab[i][j][k][0]->rootStr2;

							int sideIndex = 1;

							for (int ts_it = 1;
									ts_it < subroot->trimSubTab[i][j][k].size();
									ts_it++) {

								if (sideIndex
										< subroot->trimSubTab[p][q][s].size()) {
									if (subroot->trimSubTab[i][j][k][ts_it]->getID()
											== subroot->trimSubTab[p][q][s][sideIndex]->getID()) {

										f1 +=
												subroot->trimSubTab[i][j][k][ts_it]->nodeStr1;
										f2 +=
												subroot->trimSubTab[i][j][k][ts_it]->nodeStr1;

										sideIndex++;
									} else {
										f1 +=
												subroot->trimSubTab[i][j][k][ts_it]->nodeStr0;
										f2 +=
												subroot->trimSubTab[i][j][k][ts_it]->nodeStr0;
									}
								} else {
									f1 +=
											subroot->trimSubTab[i][j][k][ts_it]->nodeStr0;
									f2 +=
											subroot->trimSubTab[i][j][k][ts_it]->nodeStr0;
								}
							}

							rfset.insert(f1);
							rfset.insert(f2);
						}
					}
				}

//				cout << subroot->trimSubTab[i][j][k][0]->rootStr2;
//				for (int ts_it = 1; ts_it < subroot->trimSubTab[i][j][k].size();
//						ts_it++) {
//					cout << subroot->trimSubTab[i][j][k][ts_it]->nodeStr1;
//				}
//				cout << endl;
			}
		}
	}
//	cout << endl;

//	if (subroot->getLeftBranchSequence()->size() == 0) {
//		for (std::vector<int>::iterator rb_it = rightAnchors.begin();
//				rb_it != rightAnchors.end(); rb_it++) {
//			for (int bb = 0; bb <= subroot->getSubTreeDepth(); bb++) {
//
//				std::string r2;
//				std::string f2;
//				std::string c22;
//
//				appendRootFeature(r2, 2, subroot);
//
//				for (int right_it = subroot->getRightBranchSequence()->size()
//						- 1; right_it >= (*rb_it); right_it--) {
//
//					cerr
//							<< subroot->getRightBranchSequence()->at(right_it)->getNode()->word
//							<< " ";
//
//					if (subroot->getRightBranchSequence()->at(right_it)->getLevel()
//							- subroot->getLevel() <= bb) {
//
//						appendNodeFeature(
//								f2,
//								3,
//								subroot,
//								subroot->getRightBranchSequence()->at(
//										right_it));
//					}
//				}
//				c22 = r2 + f2;
//
//				rfset.insert(c22);
//
//				cerr << c22 << endl;
//			}
//			cerr << endl;
//		}
//		cerr << endl;
//	}
//
//	else if (subroot->getRightBranchSequence()->size() == 0) {
//		for (std::vector<int>::iterator lb_it = leftAnchors.begin();
//				lb_it != leftAnchors.end(); lb_it++) {
//			for (int bb = 0; bb <= subroot->getSubTreeDepth(); bb++) {
//
//				std::string r2;
//				std::string f2;
//				std::string c22;
//
//				appendRootFeature(r2, 2, subroot);
//
//				for (int left_it = (*lb_it);
//						left_it < subroot->getLeftBranchSequence()->size();
//						left_it++) {
//
//					cerr
//							<< subroot->getLeftBranchSequence()->at(left_it)->getNode()->word
//							<< " ";
//
//					if (subroot->getLeftBranchSequence()->at(left_it)->getLevel()
//							- subroot->getLevel() <= bb) {
//
//						appendNodeFeature(f2, 3, subroot,
//								subroot->getLeftBranchSequence()->at(left_it));
//					}
//
//				}
//
//				c22 = r2 + f2;
//
//				rfset.insert(c22);
//
//				cerr << c22 << endl;
//
//			}
//			cerr << endl;
//		}
//		cerr << endl;
//	} else {
//
//		for (int bb = 0; bb <= subroot->getSubTreeDepth(); bb++) {
//			for (std::vector<int>::iterator rb_it = rightAnchors.begin();
//					rb_it != rightAnchors.end(); rb_it++) {
//
//				std::string r2;
//				std::string f2;
//				std::string c22;
//
//				appendRootFeature(r2, 2, subroot);
//
//				for (int right_it = subroot->getRightBranchSequence()->size()
//						- 1; right_it >= (*rb_it); right_it--) {
//
//					cerr
//							<< subroot->getRightBranchSequence()->at(right_it)->getNode()->word
//							<< " ";
//
//					if (subroot->getRightBranchSequence()->at(right_it)->getLevel()
//							- subroot->getLevel() <= bb) {
//
//						appendNodeFeature(
//								f2,
//								3,
//								subroot,
//								subroot->getRightBranchSequence()->at(
//										right_it));
//					}
//				}
//				c22 = r2 + f2;
//
//				rfset.insert(c22);
//
//				cerr << c22 << endl;
//			}
//			cerr << endl;
//
//			for (std::vector<int>::iterator lb_it = leftAnchors.begin();
//					lb_it != leftAnchors.end(); lb_it++) {
//
//				std::string r2;
//				std::string f2;
//				std::string c22;
//
//				appendRootFeature(r2, 2, subroot);
//
//				for (int left_it = (*lb_it);
//						left_it < subroot->getLeftBranchSequence()->size();
//						left_it++) {
//
//					cerr
//							<< subroot->getLeftBranchSequence()->at(left_it)->getNode()->word
//							<< " ";
//
//					if (subroot->getLeftBranchSequence()->at(left_it)->getLevel()
//							- subroot->getLevel() <= bb) {
//
//						appendNodeFeature(f2, 3, subroot,
//								subroot->getLeftBranchSequence()->at(left_it));
//					}
//
//				}
//
//				c22 = r2 + f2;
//
//				rfset.insert(c22);
//
//				cerr << c22 << endl;
//			}
//			cerr << endl;
//
//			for (std::vector<int>::iterator lb_it = leftAnchors.begin();
//					lb_it != leftAnchors.end(); lb_it++) {
//				for (std::vector<int>::iterator rb_it = rightAnchors.begin();
//						rb_it != rightAnchors.end(); rb_it++) {
//
//					std::string r2;
//					std::string f2;
//					std::string c22;
//
//					appendRootFeature(r2, 2, subroot);
//
//					for (int left_it = (*lb_it);
//							left_it < subroot->getLeftBranchSequence()->size();
//							left_it++) {
//
//						cerr
//								<< subroot->getLeftBranchSequence()->at(left_it)->getNode()->word
//								<< " ";
//
//						if (subroot->getLeftBranchSequence()->at(left_it)->getLevel()
//								- subroot->getLevel() <= bb) {
//
//							appendNodeFeature(
//									f2,
//									3,
//									subroot,
//									subroot->getLeftBranchSequence()->at(
//											left_it));
//						}
//
//					}
//
//					for (int right_it =
//							subroot->getRightBranchSequence()->size() - 1;
//							right_it >= (*rb_it); right_it--) {
//
//						cerr
//								<< subroot->getRightBranchSequence()->at(
//										right_it)->getNode()->word << " ";
//
//						if (subroot->getRightBranchSequence()->at(right_it)->getLevel()
//								- subroot->getLevel() <= bb) {
//
//							appendNodeFeature(
//									f2,
//									3,
//									subroot,
//									subroot->getRightBranchSequence()->at(
//											right_it));
//						}
//					}
//
//					c22 = r2 + f2;
//
//					rfset.insert(c22);
//					cerr << c22 << endl;
//				}
//			}
//		}
//	}

}

void FeatureSet::config_depth_feature(Dptree* dptree, DptreeNode* subroot,
		size_t length_limit) {

	int chainLength = 0;

	std::deque<Morph::DptreeNode*> chain;

	Morph::DptreeNode* currentTreeNodePtr = subroot;

	chain.push_front(currentTreeNodePtr);

	chainLength++;

//		while (chainLength <= length_limit && !(currentTreeNodePtr->getIsRoot())) {
//			currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
//					currentTreeNodePtr->getParentNode()->position));
//			chain.push_front(currentTreeNodePtr);
//			chainLength++;
//		}

	while (!(currentTreeNodePtr->getIsRoot()) && chainLength <= length_limit) {
		currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
				currentTreeNodePtr->getParentNode()->position));
		chain.push_front(currentTreeNodePtr);
		chainLength++;
	}

//		std::string r1 = "Ch:"+chain.at(0)->rootStr1;
//		std::string r2 = "Ch:"+chain.at(0)->rootStr2;

	std::string f1 = "Ch:";
	std::string f2 = "Ch:";

	appendRootFeature(f1, 1, chain.at(0));
	appendRootFeature(f2, 2, chain.at(0));

	while (chain.size() > 2) {

		for (int i = 0; i < chain.size(); i++) {
			for (int j = 1; j < chain.size(); j++) {
				if (j == i) {
					appendDepthNodeFeature(f1, 2, chain.at(j));
					appendDepthNodeFeature(f2, 2, chain.at(j));
				} else {
					appendDepthNodeFeature(f1, 1, chain.at(j));
					appendDepthNodeFeature(f2, 1, chain.at(j));
				}
			}
			rfset.insert(f1);
			rfset.insert(f2);
			f1 = "Ch:";
			f2 = "Ch:";
			appendRootFeature(f1, 1, chain.at(0));
			appendRootFeature(f2, 2, chain.at(0));
		}

		chain.pop_front();

	}

//		for (int i = 0; i < chain.size() - 1; i++) {
//
//					std::string r2;
//
//					std::string f2;
//
//					std::string c22;
//
//					appendRootFeature(r2, 2, chain.at(i));
//
//					int stopPosition = chain.size() - 1 - i;
//					if (stopPosition > length_limit) {
//						stopPosition = length_limit;
//					}
//					for (int k = i + 1; k <= i + stopPosition; k++) {
//						appendNodeFeature(f2, 3, chain.at(i), chain.at(k));
//					}
//
//					c22 = "Ch:" + r2 + f2;
//
//					rfset.insert(c22);
//				}

//		if (currentToken == NULL)
//			cerr << "NullPtrErr : extract_depth_feature" << endl;
//
//		if (tk_it->position != currentToken->position)
//			cerr << "MissMatch : extract_depth_feature" << endl;
//		if (stopSign != 0)
//			cerr << "root not reached : extract_depth_feature" << endl;
}

void FeatureSet::config_ancestor_feature(Dptree* dptree, DptreeNode* subroot,
		size_t length_limit) {

	int chainLength = 0;

	std::deque<Morph::DptreeNode*> chain;

	Morph::DptreeNode* currentTreeNodePtr = subroot;

	chain.push_front(currentTreeNodePtr);

	chainLength++;

//		while (chainLength <= length_limit && !(currentTreeNodePtr->getIsRoot())) {
//			currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
//					currentTreeNodePtr->getParentNode()->position));
//			chain.push_front(currentTreeNodePtr);
//			chainLength++;
//		}

	while (!(currentTreeNodePtr->getIsRoot()) && chainLength <= length_limit) {
		currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
				currentTreeNodePtr->getParentNode()->position));
		chain.push_back(currentTreeNodePtr);
		chainLength++;
	}

	std::string f1 = "An:";
	std::string f2 = "An:";

	appendRootFeature(f1, 1, chain.at(0));
	appendRootFeature(f2, 2, chain.at(0));

	while (chain.size() > 2) {

		for (int i = 0; i < chain.size(); i++) {
			for (int j = 1; j < chain.size(); j++) {
				if (j == i) {
					appendAncestorNodeFeature(f1, 2, chain.at(j - 1),
							chain.at(j));
					appendAncestorNodeFeature(f2, 2, chain.at(j - 1),
							chain.at(j));
				} else {
					appendAncestorNodeFeature(f1, 1, chain.at(j - 1),
							chain.at(j));
					appendAncestorNodeFeature(f2, 1, chain.at(j - 1),
							chain.at(j));
				}
			}
			rfset.insert(f1);
			rfset.insert(f2);
			f1 = "An:";
			f2 = "An:";
			appendRootFeature(f1, 1, chain.at(0));
			appendRootFeature(f2, 2, chain.at(0));
		}

		chain.pop_back();

	}
}

void FeatureSet::extract_rerank_feature(Dptree* dptree, DptreeNode* subroot,
		size_t left_boundry, size_t right_boundry, size_t bottom_boundry) {

	subroot->exploreSubtree(subroot, left_boundry, right_boundry,
			bottom_boundry);

	std::vector<int> leftAnchors;
	std::vector<int> rightAnchors;

	int leftPen = 0;
	int rightPen = 0;

	std::vector<DptreeNode*> trimSeed;
	std::vector<std::vector<DptreeNode*> > trimTabLv1;
	std::vector<std::vector<std::vector<DptreeNode*> > > trimTabLv2;

	trimSeed.push_back(subroot);

	trimTabLv1.push_back(trimSeed);

	for (std::deque<Morph::DptreeNode*>::iterator lbs_it =
			subroot->getLeftBranchSequence()->begin();
			lbs_it != subroot->getLeftBranchSequence()->end(); lbs_it++) {
		if ((*lbs_it)->getIsAnchor() == 1) {
			leftAnchors.push_back(leftPen);
			trimTabLv1.push_back(trimSeed);
			//cerr << leftPen << " ";
		}
		leftPen++;
	}
	//cerr << endl;

	trimTabLv2.push_back(trimTabLv1);

	for (std::deque<Morph::DptreeNode*>::iterator rbs_it =
			subroot->getRightBranchSequence()->begin();
			rbs_it != subroot->getRightBranchSequence()->end(); rbs_it++) {
		if ((*rbs_it)->getIsAnchor() == 1) {
			rightAnchors.push_back(rightPen);
			trimTabLv2.push_back(trimTabLv1);
		}
		rightPen++;
	}

	//cerr << "depth from node: " << subroot->getSubTreeDepth() << endl;

	for (int bb = 0; bb <= subroot->getSubTreeDepth(); bb++) {
		subroot->trimSubTab.push_back(trimTabLv2);
	}

//  cerr << "trimmed sub-tree: " << endl;
//	for (int i = 0; i < subroot->trimSubTab.size(); i++) {
//		for (int j = 0; j < subroot->trimSubTab.at(i).size(); j++) {
//			for (int k = 0; k < subroot->trimSubTab.at(i).at(j).size(); k++) {
//				cerr << subroot->trimSubTab[i][j][k][0]->getNode()->word;
//			}
//			cerr << " ";
//		}
//		cerr << endl;
//	}
//	cerr << endl;

	if (leftPen != subroot->getLeftBranchSequence()->size()
			|| rightPen != subroot->getRightBranchSequence()->size()) {
		cerr << "E: pen not equal with sequence size." << endl;
	}

	for (int i = 0; i < subroot->trimSubTab.size(); i++) {
		for (int j = 0; j < subroot->trimSubTab.at(i).size(); j++) {
			for (int k = 0; k < subroot->trimSubTab.at(i).at(j).size(); k++) {

				if (subroot->getLeftBranchSequence()->size() != 0 && k > 0) {
					for (int left_it = leftAnchors.at(leftAnchors.size() - k);
							left_it < subroot->getLeftBranchSequence()->size();
							left_it++) {

						//cerr << "entered lv 1" << endl;

						if (subroot->getLeftBranchSequence()->at(left_it)->getLevel()
								- subroot->getLevel() <= i) {

							//cerr << "entered lv 2" << endl;

							subroot->trimSubTab[i][j][k].push_back(
									subroot->getLeftBranchSequence()->at(
											left_it));
						}
					}
				}

				//cerr << "entered lv 3" << endl;

				if (subroot->getRightBranchSequence()->size() != 0 && j > 0) {

					//cerr << "entered lv 4" << endl;

					for (int right_it =
							subroot->getRightBranchSequence()->size() - 1;
							right_it >= rightAnchors.at(j - 1); right_it--) {
						if (subroot->getRightBranchSequence()->at(right_it)->getLevel()
								- subroot->getLevel() <= i) {
							subroot->trimSubTab[i][j][k].push_back(
									subroot->getRightBranchSequence()->at(
											right_it));
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < subroot->trimSubTab.size(); i++) {
		for (int j = 0; j < subroot->trimSubTab.at(i).size(); j++) {
			for (int k = 0; k < subroot->trimSubTab.at(i).at(j).size(); k++) {

//				cout << "i " << i << "j " << j << "k " << k;
				for (int p = 0; p <= i; p++) {
					for (int q = 0; q <= j; q++) {
						for (int s = 0; s <= k; s++) {

							std::string f1 =
									subroot->trimSubTab[i][j][k][0]->rootStr1;
							std::string f2 =
									subroot->trimSubTab[i][j][k][0]->rootStr2;

							int sideIndex = 1;

							for (int ts_it = 1;
									ts_it < subroot->trimSubTab[i][j][k].size();
									ts_it++) {

								if (sideIndex
										< subroot->trimSubTab[p][q][s].size()) {
									if (subroot->trimSubTab[i][j][k][ts_it]->getID()
											== subroot->trimSubTab[p][q][s][sideIndex]->getID()) {

										f1 +=
												subroot->trimSubTab[i][j][k][ts_it]->nodeStr1;
										f2 +=
												subroot->trimSubTab[i][j][k][ts_it]->nodeStr1;

										sideIndex++;
									} else {
										f1 +=
												subroot->trimSubTab[i][j][k][ts_it]->nodeStr0;
										f2 +=
												subroot->trimSubTab[i][j][k][ts_it]->nodeStr0;
									}
								} else {
									f1 +=
											subroot->trimSubTab[i][j][k][ts_it]->nodeStr0;
									f2 +=
											subroot->trimSubTab[i][j][k][ts_it]->nodeStr0;
								}
							}

							rfset.insert(f1);
							rfset.insert(f2);
						}
					}
				}

//				cout << subroot->trimSubTab[i][j][k][0]->rootStr2;
//				for (int ts_it = 1; ts_it < subroot->trimSubTab[i][j][k].size();
//						ts_it++) {
//					cout << subroot->trimSubTab[i][j][k][ts_it]->nodeStr1;
//				}
//				cout << endl;
			}
		}
	}
//	cout << endl;

}

void FeatureSet::extract_depth_feature(Dptree* dptree, DptreeNode* subroot,
		size_t length_limit) {

	int chainLength = 0;

	std::deque<Morph::DptreeNode*> chain;

	Morph::DptreeNode* currentTreeNodePtr = subroot;

	chain.push_front(currentTreeNodePtr);

	chainLength++;

//		while (chainLength <= length_limit && !(currentTreeNodePtr->getIsRoot())) {
//			currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
//					currentTreeNodePtr->getParentNode()->position));
//			chain.push_front(currentTreeNodePtr);
//			chainLength++;
//		}

	while (!(currentTreeNodePtr->getIsRoot()) && chainLength <= length_limit) {
		currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
				currentTreeNodePtr->getParentNode()->position));
		chain.push_front(currentTreeNodePtr);
		chainLength++;
	}

//		std::string r1 = "Ch:"+chain.at(0)->rootStr1;
//		std::string r2 = "Ch:"+chain.at(0)->rootStr2;

	std::string f1 = "Ch:";
	std::string f2 = "Ch:";

	appendRootFeature(f1, 1, chain.at(0));
	appendRootFeature(f2, 2, chain.at(0));

	while (chain.size() > 2) {

		for (int i = 0; i < chain.size(); i++) {
			for (int j = 1; j < chain.size(); j++) {
				if (j == i) {
					appendDepthNodeFeature(f1, 2, chain.at(j));
					appendDepthNodeFeature(f2, 2, chain.at(j));
				} else {
					appendDepthNodeFeature(f1, 1, chain.at(j));
					appendDepthNodeFeature(f2, 1, chain.at(j));
				}
			}
			rfset.insert(f1);
			rfset.insert(f2);
			f1 = "Ch:";
			f2 = "Ch:";
			appendRootFeature(f1, 1, chain.at(0));
			appendRootFeature(f2, 2, chain.at(0));
		}

		chain.pop_front();

	}

//		for (int i = 0; i < chain.size() - 1; i++) {
//
//					std::string r2;
//
//					std::string f2;
//
//					std::string c22;
//
//					appendRootFeature(r2, 2, chain.at(i));
//
//					int stopPosition = chain.size() - 1 - i;
//					if (stopPosition > length_limit) {
//						stopPosition = length_limit;
//					}
//					for (int k = i + 1; k <= i + stopPosition; k++) {
//						appendNodeFeature(f2, 3, chain.at(i), chain.at(k));
//					}
//
//					c22 = "Ch:" + r2 + f2;
//
//					rfset.insert(c22);
//				}

//		if (currentToken == NULL)
//			cerr << "NullPtrErr : extract_depth_feature" << endl;
//
//		if (tk_it->position != currentToken->position)
//			cerr << "MissMatch : extract_depth_feature" << endl;
//		if (stopSign != 0)
//			cerr << "root not reached : extract_depth_feature" << endl;
}

void FeatureSet::extract_ancestor_feature(Dptree* dptree, DptreeNode* subroot,
		size_t length_limit) {

	int chainLength = 0;

	std::deque<Morph::DptreeNode*> chain;

	Morph::DptreeNode* currentTreeNodePtr = subroot;

	chain.push_front(currentTreeNodePtr);

	chainLength++;

//		while (chainLength <= length_limit && !(currentTreeNodePtr->getIsRoot())) {
//			currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
//					currentTreeNodePtr->getParentNode()->position));
//			chain.push_front(currentTreeNodePtr);
//			chainLength++;
//		}

	while (!(currentTreeNodePtr->getIsRoot()) && chainLength <= length_limit) {
		currentTreeNodePtr = &(dptree->dpTreeNodeAry.at(
				currentTreeNodePtr->getParentNode()->position));
		chain.push_back(currentTreeNodePtr);
		chainLength++;
	}

	std::string f1 = "An:";
	std::string f2 = "An:";

	appendRootFeature(f1, 1, chain.at(0));
	appendRootFeature(f2, 2, chain.at(0));

	while (chain.size() > 2) {

		for (int i = 0; i < chain.size(); i++) {
			for (int j = 1; j < chain.size(); j++) {
				if (j == i) {
					appendAncestorNodeFeature(f1, 2, chain.at(j - 1),
							chain.at(j));
					appendAncestorNodeFeature(f2, 2, chain.at(j - 1),
							chain.at(j));
				} else {
					appendAncestorNodeFeature(f1, 1, chain.at(j - 1),
							chain.at(j));
					appendAncestorNodeFeature(f2, 1, chain.at(j - 1),
							chain.at(j));
				}
			}
			rfset.insert(f1);
			rfset.insert(f2);
			f1 = "An:";
			f2 = "An:";
			appendRootFeature(f1, 1, chain.at(0));
			appendRootFeature(f2, 2, chain.at(0));
		}

		chain.pop_back();

	}
}

//void FeatureSet::extract_breath_feature(
//		std::vector<Morph::Dptoken> *dpTokenArray) {
//
//	std::set<size_t> checkList;
//
//	for (std::vector<Morph::Dptoken>::iterator tk_it = dpTokenArray->begin();
//			tk_it != dpTokenArray->end(); tk_it++) {
//
//		if (tk_it->dep == 0)
//			continue;
//
//		if (checkList.find(tk_it->dep) != checkList.end()) {
//			continue;
//		} else {
//
//			std::string f = "BREATH:";
//			f += int2string((int) (dpTokenArray->size()));
//			f += "_";
//
//			checkList.insert(tk_it->dep);
//			size_t collection = tk_it->dep;
//			int memberCount = 0;
//
//			for (std::vector<Morph::Dptoken>::iterator L1_it = tk_it;
//					L1_it != dpTokenArray->end(); L1_it++) {
//				if (L1_it->dep == collection) {
//					f += "(";
//					f += L1_it->pos;
//					f += ",";
//					f += L1_it->word;
//					f += ",";
//					f += int2string((int) (L1_it->dep - (L1_it->position + 1)));
//					f += ")";
//					f += "_";
//					memberCount++;
//				}
//			}
//			f += "[";
//			f += dpTokenArray->at(collection - 1).pos;
//			f += ",";
//			f += dpTokenArray->at(collection - 1).word;
//			f += ",";
//			f += int2string(memberCount);
//			f += "]";
//
//			fset.push_back(f);
//		}
//
//	}
//}

double FeatureSet::calc_inner_product_with_weight() {
	double sum = 0;
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {
		if (feature_weight.find(*it) != feature_weight.end()) {
			sum += feature_weight[*it];
		}
	}
	return sum;
}

double FeatureSet::calc_inner_product_with_weight(
		std::map<std::string, double> &in_feature_weight) {
	double sum = 0;
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {

		//cerr << "trying to find: " << *it <<  endl;

		if (in_feature_weight.find(*it) != in_feature_weight.end()) {

			//cerr << "feature in using: " << *it << "\t" << in_feature_weight[*it]<< endl;

			sum += in_feature_weight[*it];
		}
	}
	return sum;
}

double FeatureSet::calc_rerank_inner_product_with_weight(
		std::map<std::string, double> &in_rerank_feature_weight) {
	double sum = 0;
	for (std::set<std::string>::iterator it = rfset.begin(); it != rfset.end();
			it++) {
		if (in_rerank_feature_weight.find(*it)
				!= in_rerank_feature_weight.end()) {
			sum += in_rerank_feature_weight[*it];
		}
	}
	return sum;
}

bool FeatureSet::print() {
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {
		cout << *it << " ";
	}
	cout << endl;
	return true;
}

FeatureTemplate::FeatureTemplate(std::string &in_name,
		std::string &feature_string, bool in_is_unigram) {
	is_unigram = in_is_unigram;
	name = in_name;
	std::vector<std::string> line;
	split_string(feature_string, ",", line);
	for (std::vector<std::string>::iterator it = line.begin(); it != line.end();
			it++) {
		unsigned int macro_id = interpret_macro(*it);
		if (macro_id)
			features.push_back(macro_id);
	}
}

FeatureTemplate::FeatureTemplate(std::string &in_name,
		std::string &feature_string, std::string &feature_type) {
	type = feature_type;
	name = in_name;
	std::vector<std::string> line;
	split_string(feature_string, ",", line);
	for (std::vector<std::string>::iterator it = line.begin(); it != line.end();
			it++) {
		unsigned int macro_id = interpret_macro(*it);
		if (macro_id)
			features.push_back(macro_id);
	}
}

unsigned int FeatureTemplate::interpret_macro(std::string &macro) {
	// unigram
	if (type == "EDGE") {
//		if (macro == FEATURE_MACRO_STRING_WORD)
//			return FEATURE_MACRO_WORD;
//		else if (macro == FEATURE_MACRO_STRING_POS)
//			return FEATURE_MACRO_POS;
//		else if (macro == FEATURE_MACRO_STRING_LENGTH)
//			return FEATURE_MACRO_LENGTH;
//		else if (macro == FEATURE_MACRO_STRING_BEGINNING_CHAR)
//			return FEATURE_MACRO_BEGINNING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_ENDING_CHAR)
//			return FEATURE_MACRO_ENDING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_BEGINNING_CHAR_TYPE)
//			return FEATURE_MACRO_BEGINNING_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_ENDING_CHAR_TYPE)
//			return FEATURE_MACRO_ENDING_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_FEATURE1)
//			return FEATURE_MACRO_FEATURE1;
		//dpfeature
		if (macro == DPFEATURE_MACRO_STRING_HEAD_WORD)
			return DPFEATURE_MACRO_HEAD_WORD;
		else if (macro == DPFEATURE_MACRO_STRING_HEAD_POS)
			return DPFEATURE_MACRO_HEAD_POS;
		else if (macro == DPFEATURE_MACRO_STRING_MODIFIER_WORD)
			return DPFEATURE_MACRO_MODIFIER_WORD;
		else if (macro == DPFEATURE_MACRO_STRING_MODIFIER_POS)
			return DPFEATURE_MACRO_MODIFIER_POS;
		else if (macro == DPFEATURE_MACRO_STRING_HEAD_LEFT_POS)
			return DPFEATURE_MACRO_HEAD_LEFT_POS;
		else if (macro == DPFEATURE_MACRO_STRING_HEAD_RIGHT_POS)
			return DPFEATURE_MACRO_HEAD_RIGHT_POS;
		else if (macro == DPFEATURE_MACRO_STRING_MODIFIER_LEFT_POS)
			return DPFEATURE_MACRO_MODIFIER_LEFT_POS;
		else if (macro == DPFEATURE_MACRO_STRING_MODIFIER_RIGHT_POS)
			return DPFEATURE_MACRO_MODIFIER_RIGHT_POS;
		else if (macro == DPFEATURE_MACRO_STRING_DIRECTION)
			return DPFEATURE_MACRO_DIRECTION;
		else if (macro == DPFEATURE_MACRO_STRING_DISTANCE)
			return DPFEATURE_MACRO_DISTANCE;
	}
	// bigram
	else if (type == "CONTEXT") {
		// bigram: left
//		if (macro == FEATURE_MACRO_STRING_LEFT_WORD)
//			return FEATURE_MACRO_LEFT_WORD;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_POS)
//			return FEATURE_MACRO_LEFT_POS;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_LENGTH)
//			return FEATURE_MACRO_LEFT_LENGTH;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR)
//			return FEATURE_MACRO_LEFT_BEGINNING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_ENDING_CHAR)
//			return FEATURE_MACRO_LEFT_ENDING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR_TYPE)
//			return FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_ENDING_CHAR_TYPE)
//			return FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE;
//		// bigram: right
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_WORD)
//			return FEATURE_MACRO_RIGHT_WORD;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_POS)
//			return FEATURE_MACRO_RIGHT_POS;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_LENGTH)
//			return FEATURE_MACRO_RIGHT_LENGTH;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR)
//			return FEATURE_MACRO_RIGHT_BEGINNING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR)
//			return FEATURE_MACRO_RIGHT_ENDING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR_TYPE)
//			return FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR_TYPE)
//			return FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE;
		//dpfeature
		if (macro == DPFEATURE_MACRO_STRING_HEAD_POS)
			return DPFEATURE_MACRO_HEAD_POS;
		else if (macro == DPFEATURE_MACRO_STRING_MODIFIER_POS)
			return DPFEATURE_MACRO_MODIFIER_POS;
		else if (macro == DPFEATURE_MACRO_STRING_DIRECTION)
			return DPFEATURE_MACRO_DIRECTION;
		else if (macro == DPFEATURE_MACRO_STRING_DISTANCE)
			return DPFEATURE_MACRO_DISTANCE;
		else if (macro == DPFEATURE_MACRO_STRING_INBETWEEN_POS)
			return DPFEATURE_MACRO_INBETWEEN_POS;
	} else if (type == "SIBLING") {
		if (macro == DPFEATURE_MACRO_STRING_HEAD_WORD)
			return DPFEATURE_MACRO_HEAD_WORD;
		else if (macro == DPFEATURE_MACRO_STRING_HEAD_POS)
			return DPFEATURE_MACRO_HEAD_POS;
		else if (macro == DPFEATURE_MACRO_STRING_MODIFIER_WORD)
			return DPFEATURE_MACRO_MODIFIER_WORD;
		else if (macro == DPFEATURE_MACRO_STRING_MODIFIER_POS)
			return DPFEATURE_MACRO_MODIFIER_POS;
		else if (macro == DPFEATURE_MACRO_STRING_SIBLING_POS)
			return DPFEATURE_MACRO_SIBLING_POS;
		else if (macro == DPFEATURE_MACRO_STRING_SIBLING_WORD)
			return DPFEATURE_MACRO_SIBLING_WORD;
		else if (macro == DPFEATURE_MACRO_STRING_DIRECTION)
			return DPFEATURE_MACRO_DIRECTION;
		else if (macro == DPFEATURE_MACRO_STRING_DISTANCE)
			return DPFEATURE_MACRO_DISTANCE;
	}

	cerr << ";; cannot understand macro: " << macro << endl;
	return 0;
}

FeatureTemplate *FeatureTemplateSet::interpret_template(
		std::string &template_string, bool is_unigram) {
	std::vector<std::string> line;
	split_string(template_string, ":", line);
	return new FeatureTemplate(line[0], line[1], is_unigram);
}

FeatureTemplate *FeatureTemplateSet::interpret_template(
		std::string &template_string, std::string &feature_type) {
	std::vector<std::string> line;
	split_string(template_string, ":", line);
	return new FeatureTemplate(line[0], line[1], feature_type);
}

bool FeatureTemplateSet::open(const std::string &template_filename) {
	std::ifstream ft_in(template_filename.c_str(), std::ios::in);
	if (!ft_in.is_open()) {
		cerr << ";; cannot open " << template_filename << " for reading"
				<< endl;
		return false;
	}

	std::string buffer;
	while (getline(ft_in, buffer)) {
		if (buffer.at(0) == '#') // comment line
			continue;
		std::vector<std::string> line;

		//		cerr << buffer<< endl;

		split_string(buffer, " ", line);

		if (line[0] == "UNIGRAM") {
			templates.push_back(interpret_template(line[1], true));
		} else if (line[0] == "BIGRAM") {
			templates.push_back(interpret_template(line[1], false));
		} else if (line[0] == "EDGE") {
			templates.push_back(interpret_template(line[1], line[0]));
		} else if (line[0] == "CONTEXT") {
			templates.push_back(interpret_template(line[1], line[0]));
		} else if (line[0] == "SIBLING") {
			templates.push_back(interpret_template(line[1], line[0]));
		} else {
			cerr << ";; cannot understand: " << line[0] << " " << line[1]
					<< endl;
		}
	}

	return true;
}

}
