#ifndef DIC_H
#define DIC_H

#include "common.h"
#include "darts.h"
#include "mmap.h"
#include "pos.h"
#include "node.h"
#include "feature.h"
#include "parameter.h"

namespace Morph {

class Dic {
    Parameter *param;
    Darts::DoubleArray darts;
    Mmap<char> *dmmap;
    const Token *token_head;
    Pos posid2pos;
    FeatureTemplateSet *ftmpl;
  public:
    Dic() {}
    Dic(Parameter *in_param, FeatureTemplateSet *in_ftmpl);
    ~Dic();
    bool open(Parameter *in_param, FeatureTemplateSet *in_ftmpl);
    Node *lookup(const char *start_str);
    Node *lookup(const char *start_str, unsigned int specified_length, std::string *specified_pos);
    Node *lookup(const char *start_str, unsigned int specified_length, unsigned short specified_posid);
    Node *make_unk_pseudo_node(const char *start_str, int byte_len);
    Node *make_unk_pseudo_node(const char *start_str, int byte_len, std::string &specified_pos);
    Node *make_unk_pseudo_node(const char *start_str, int byte_len, unsigned short specified_posid);
    Node *make_unk_pseudo_node_list_some_pos(const char *start_str, int byte_len, unsigned short specified_posid, std::vector<unsigned short> *specified_unk_pos);
    Node *make_unk_pseudo_node_list(const char *start_str, unsigned int min_char_num, unsigned int max_char_num);
    Node *make_unk_pseudo_node_list(const char *start_str, unsigned int min_char_num, unsigned int max_char_num, unsigned short specified_posid);
    Node *make_specified_pseudo_node(const char *start_str, unsigned int specified_length, std::string *specified_pos, std::vector<unsigned short> *specified_unk_pos, unsigned int type_family);
    Node *make_specified_pseudo_node(const char *start_str, unsigned int specified_length, unsigned short specified_posid, std::vector<unsigned short> *specified_unk_pos, unsigned int type_family);

    const Token *get_token(const Darts::DoubleArray::result_pair_type &n) const {
        return token_head + (n.value >> 8);
    }
    size_t token_size(const Darts::DoubleArray::result_pair_type &n) const { return 0xff & n.value; }

    void inline read_node_info(const Token &token, Node **node);
};

#define MMAP_OPEN(type, map, file) do {         \
    map = new Mmap<type>;                       \
    if (!map->open(file)) {                     \
      return false;                             \
    }                                           \
  } while (0)

#define MMAP_CLOSE(type, map) do {              \
    delete map; } while (0)
}

#endif
