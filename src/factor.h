#ifndef FACTOR_H
#define FACTOR_H

#include "common.h"
#include "sentence.h"

namespace Morph
{

class Factor
{



public:
std::string pWord;
std::string cWord;
std::string pPos;
std::string cPos;
std::string pLeftPos;
std::string cLeftPos;
std::string pRightPos;
std::string cRightPos;
std::string* bPosList;
bool directionRight;
size_t distance;

    Factor(int num);
    virtual ~Factor();
    void PrintTHWMessage();
    void setFactor(Morph::Sentence &Sentence, int pIndex, int cIndex){

    }
    void computeEdgeScoreMatrix(Morph::Sentence &sentence, double** emptyMtx);
};


}
#endif // FACTOR_H
