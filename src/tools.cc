#include "common.h"

namespace Morph {

// return the bytes of a char
int utf8_bytes(unsigned char *ucp) {
    unsigned char c = *ucp;

    if (c > 0xfb) { /* 6 bytes */
	return 6;
    }
    else if (c > 0xf7) { /* 5 bytes */
	return 5;
    }
    else if (c > 0xef) { /* 4 bytes */
	return 4;
    }
    else if (c > 0xdf) { /* 3 bytes */
	return 3;
    }
    else if (c > 0x7f) { /* 2 bytes */
	return 2;
    }
    else { /* 1 byte */
	return 1;
    }
}

// return the bytes of a char
unsigned short utf8_chars(unsigned char *ucp, size_t byte_len) {
    unsigned short char_num = 0;
    for (size_t pos = 0; pos < byte_len; pos += utf8_bytes(ucp + pos)) {
        char_num++;
    }
    return char_num;
}

// return the pointer of the specified char (if 1 is specified, return the second char pointer)
unsigned char *get_specified_char_pointer(unsigned char *ucp, size_t byte_len, unsigned short specified_char_num) {
    unsigned short char_num = 0;
    size_t last_pos = 0;
    for (size_t pos = 0; pos < byte_len; pos += utf8_bytes(ucp + pos)) {
        if (char_num == specified_char_num) {
            return ucp + pos;
        }
        char_num++;
        last_pos = pos;
    }
    return ucp + last_pos;
}

unsigned int check_unicode_char_type(int code) {
    /* SPACE */
    if (code == 0x20 || code == 0x3000) {
	return TYPE_SPACE;
    }
    /* IDEOGRAPHIC PUNCTUATIONS (、。) */
    else if (code > 0x3000 && code < 0x3003) {
	return TYPE_IDEOGRAPHIC_PUNC;
    }
    /* HIRAGANA */
    else if (code > 0x303f && code < 0x30a0) {
	return TYPE_HIRAGANA;
    }
    /* KATAKANA and "ー"(0x30fc) */
    else if (code > 0x309f && code < 0x30fb || code == 0x30fc) {
	return TYPE_KATAKANA;
    }
    /* "・"(0x30fb) */
    else if (code == 0x30fb) {
	return TYPE_MIDDLE_DOT;
    }
    /* "，"(0xff0c) */
    else if (code == 0xff0c) {
        return TYPE_COMMA;
    }
    /* "／"(0xff0f) */
    else if (code == 0xff0f) {
        return TYPE_SLASH;
    }
    /* "："(0xff1a) */
    else if (code == 0xff1a) {
        return TYPE_COLON;
    }
    /* PRIOD */
    else if (code == 0xff0e) {
	return TYPE_PERIOD;
    }
    /* FIGURE (0-9, ０-９) */
    else if ((code > 0x2f && code < 0x3a) || 
             (code > 0xff0f && code < 0xff1a)) {
	return TYPE_FIGURE;
    }
    /* KANJI_FIGURE (○一七万三九二五亿六八十千四百零, ％, 点余多) */
    else if (code == 0x25cb || code == 0x4e00 || code == 0x4e03 || code == 0x4e07 || code == 0x4e09 || code == 0x4e5d || 
             code == 0x4e8c || code == 0x4e94 || code == 0x4ebf || code == 0x516b || code == 0x516d || 
             code == 0x5341 || code == 0x5343 || code == 0x56db || code == 0x767e || code == 0x96f6 || 
             code == 0xff05 || 
             // 年月日: code == 0x5e74 || code == 0x6708 || code == 0x65e5 || 
             code == 0x4f59 || code == 0x591a || code == 0x70b9) {
	return TYPE_KANJI_FIGURE;
    }
    /* ALPHABET (A-Z, a-z, Umlaut etc., Ａ-Ｚ, ａ-ｚ) */
    else if ((code > 0x40 && code < 0x5b) || 
             (code > 0x60 && code < 0x7b) || 
             (code > 0xbf && code < 0x0100) || 
             (code > 0xff20 && code < 0xff3b) || 
	     (code > 0xff40 && code < 0xff5b)) {
	return TYPE_ALPH;
    }
    /* CJK Unified Ideographs and "々" */
    else if ((code > 0x4dff && code < 0xa000) || code == 0x3005) {
	return TYPE_KANJI;
    }
    else {
	return TYPE_SYMBOL;
    }
}

// check the code of a char
unsigned int check_utf8_char_type(unsigned char *ucp) {
    int code = 0;
    int unicode;

    unsigned char c = *ucp;
    if (c > 0xfb) { /* 6 bytes */
	code = 0;
    }
    else if (c > 0xf7) { /* 5 bytes */
	code = 0;
    }
    else if (c > 0xef) { /* 4 bytes */
	code = 0;
    }
    else if (c > 0xdf) { /* 3 bytes */
	unicode = (c & 0x0f) << 12;
	c = *(ucp + 1);
	unicode += (c & 0x3f) << 6;
	c = *(ucp + 2);
	unicode += c & 0x3f;
	code = check_unicode_char_type(unicode);
    }
    else if (c > 0x7f) { /* 2 bytes */
	unicode = (c & 0x1f) << 6;
	c = *(ucp + 1);
	unicode += c & 0x3f;
	code = check_unicode_char_type(unicode);
    }
    else { /* 1 byte */
	code = check_unicode_char_type(c);
    }

    return code;
}

unsigned int check_char_family(unsigned char *ucp) {
    return check_char_family(check_utf8_char_type(ucp));
}

unsigned int check_char_family(unsigned int char_type) {
    if (char_type & TYPE_FAMILY_KANJI) {
        return TYPE_FAMILY_KANJI;
    }
    else if (char_type & TYPE_FAMILY_SPACE) {
        return TYPE_FAMILY_SPACE;
    }
    else if (char_type & TYPE_FAMILY_ALPH) {
        return TYPE_FAMILY_ALPH;
    }
    else if (char_type & TYPE_FIGURE) { // figure only
        return TYPE_FIGURE;
    }
    else { // char_type & TYPE_FAMILY_SYMBOL
        return TYPE_FAMILY_SYMBOL;
    }
}

bool compare_char_type_in_family(unsigned int char_type, unsigned int family_pattern) {
    if (char_type & family_pattern) {
        return true;
    }
    else {
        return false;
    }
}

// exceptional expression
bool check_exceptional_two_chars_in_figure(const char *cp, unsigned int rest_byte_len) {
    if (rest_byte_len >= EXCEPTIONAL_FIGURE_EXPRESSION_LENGTH && 
        !strncmp(cp, EXCEPTIONAL_FIGURE_EXPRESSION, EXCEPTIONAL_FIGURE_EXPRESSION_LENGTH)) {
        return true;
    }
    else {
        return false;
    }
}

}
