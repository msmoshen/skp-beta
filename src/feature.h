#ifndef FEATURE_H
#define FEATURE_H

#include "common.h"
#include "node.h"
#include "dptoken.h"
//#include "dpsentence.h"

namespace Morph {

#define FEATURE_MACRO_STRING_WORD "%w"
#define FEATURE_MACRO_WORD 1
#define FEATURE_MACRO_STRING_POS "%p"
#define FEATURE_MACRO_POS 2
#define FEATURE_MACRO_STRING_LENGTH "%l"
#define FEATURE_MACRO_LENGTH 3
#define FEATURE_MACRO_STRING_BEGINNING_CHAR "%bc"
#define FEATURE_MACRO_BEGINNING_CHAR 4
#define FEATURE_MACRO_STRING_ENDING_CHAR "%ec"
#define FEATURE_MACRO_ENDING_CHAR 5
#define FEATURE_MACRO_STRING_BEGINNING_CHAR_TYPE "%bt"
#define FEATURE_MACRO_BEGINNING_CHAR_TYPE 6
#define FEATURE_MACRO_STRING_ENDING_CHAR_TYPE "%et"
#define FEATURE_MACRO_ENDING_CHAR_TYPE 7
#define FEATURE_MACRO_STRING_FEATURE1 "%f1"
#define FEATURE_MACRO_FEATURE1 8

#define FEATURE_MACRO_STRING_LEFT_WORD "%Lw"
#define FEATURE_MACRO_LEFT_WORD 101
#define FEATURE_MACRO_STRING_LEFT_POS "%Lp"
#define FEATURE_MACRO_LEFT_POS 102
#define FEATURE_MACRO_STRING_LEFT_LENGTH "%Ll"
#define FEATURE_MACRO_LEFT_LENGTH 103
#define FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR "%Lbc"
#define FEATURE_MACRO_LEFT_BEGINNING_CHAR 104
#define FEATURE_MACRO_STRING_LEFT_ENDING_CHAR "%Lec"
#define FEATURE_MACRO_LEFT_ENDING_CHAR 105
#define FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR_TYPE "%Lbt"
#define FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE 106
#define FEATURE_MACRO_STRING_LEFT_ENDING_CHAR_TYPE "%Let"
#define FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE 107

#define FEATURE_MACRO_STRING_RIGHT_WORD "%Rw"
#define FEATURE_MACRO_RIGHT_WORD 201
#define FEATURE_MACRO_STRING_RIGHT_POS "%Rp"
#define FEATURE_MACRO_RIGHT_POS 202
#define FEATURE_MACRO_STRING_RIGHT_LENGTH "%Rl"
#define FEATURE_MACRO_RIGHT_LENGTH 203
#define FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR "%Rbc"
#define FEATURE_MACRO_RIGHT_BEGINNING_CHAR 204
#define FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR "%Rec"
#define FEATURE_MACRO_RIGHT_ENDING_CHAR 205
#define FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR_TYPE "%Rbt"
#define FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE 206
#define FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR_TYPE "%Ret"
#define FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE 207

//dp feature marco
#define DPFEATURE_MACRO_STRING_HEAD_WORD "%hw"
#define DPFEATURE_MACRO_HEAD_WORD 1001
#define DPFEATURE_MACRO_STRING_HEAD_POS "%hp"
#define DPFEATURE_MACRO_HEAD_POS 1002

#define DPFEATURE_MACRO_STRING_MODIFIER_WORD "%mw"
#define DPFEATURE_MACRO_MODIFIER_WORD 2001
#define DPFEATURE_MACRO_STRING_MODIFIER_POS "%mp"
#define DPFEATURE_MACRO_MODIFIER_POS 2002

#define DPFEATURE_MACRO_STRING_SIBLING_WORD "%sw"
#define DPFEATURE_MACRO_SIBLING_WORD 7001
#define DPFEATURE_MACRO_STRING_SIBLING_POS "%sp"
#define DPFEATURE_MACRO_SIBLING_POS 7002

#define DPFEATURE_MACRO_STRING_HEAD_LEFT_POS "%hlp"
#define DPFEATURE_MACRO_HEAD_LEFT_POS 1003
#define DPFEATURE_MACRO_STRING_HEAD_RIGHT_POS "%hrp"
#define DPFEATURE_MACRO_HEAD_RIGHT_POS 1004

#define DPFEATURE_MACRO_STRING_MODIFIER_LEFT_POS "%mlp"
#define DPFEATURE_MACRO_MODIFIER_LEFT_POS 2003
#define DPFEATURE_MACRO_STRING_MODIFIER_RIGHT_POS "%mrp"
#define DPFEATURE_MACRO_MODIFIER_RIGHT_POS 2004

#define DPFEATURE_MACRO_STRING_INBETWEEN_POS "%ibp"
#define DPFEATURE_MACRO_INBETWEEN_POS 3001

#define DPFEATURE_MACRO_STRING_DIRECTION "%dir"
#define DPFEATURE_MACRO_DIRECTION 4001
#define DPFEATURE_MACRO_STRING_DISTANCE "%dis"
#define DPFEATURE_MACRO_DISTANCE 4002


//word-cluster macro
#define DPFEATURE_MACRO_STRING_HEAD_SB "%hc4"
#define DPFEATURE_MACRO_HEAD_SB 11001
#define DPFEATURE_MACRO_STRING_HEAD_MB "%hc6"
#define DPFEATURE_MACRO_HEAD_MB 11002
#define DPFEATURE_MACRO_STRING_HEAD_FB "%hc0"
#define DPFEATURE_MACRO_HEAD_FB 11003

#define DPFEATURE_MACRO_STRING_MODIFIER_SB "%mc4"
#define DPFEATURE_MACRO_MODIFIER_SB 12001
#define DPFEATURE_MACRO_STRING_MODIFIER_MB "%mc6"
#define DPFEATURE_MACRO_MODIFIER_MB 12002
#define DPFEATURE_MACRO_STRING_MODIFIER_FB "%mc0"
#define DPFEATURE_MACRO_MODIFIER_FB 12003

#define DPFEATURE_MACRO_STRING_SIBLING_SB "%sc4"
#define DPFEATURE_MACRO_SIBLING_SB 17001
#define DPFEATURE_MACRO_STRING_SIBLING_MB "%sc6"
#define DPFEATURE_MACRO_SIBLING_MB 17002
#define DPFEATURE_MACRO_STRING_SIBLING_FB "%sc0"
#define DPFEATURE_MACRO_SIBLING_FB 17003



class FeatureTemplate {
	bool is_unigram;
	std::string type;
	std::string name;
	std::vector<unsigned int> features;
public:
	FeatureTemplate(std::string &in_name, std::string &feature_string,
			bool in_is_unigram);
	FeatureTemplate(std::string &in_name, std::string &feature_string,
			std::string &feature_type);

	bool get_is_unigram() {
		return is_unigram;
	}

	unsigned int interpret_macro(std::string &macro);

	std::string &get_name() {
		return name;
	}

	std::string &get_type() {
		return type;
	}

	std::vector<unsigned int> *get_features() {
		return &features;
	}
};

class FeatureTemplateSet {
	std::vector<FeatureTemplate *> templates;
public:
	bool open(const std::string &template_filename);
	FeatureTemplate *interpret_template(std::string &template_string,
			bool is_unigram);
	FeatureTemplate *interpret_template(std::string &template_string,
			std::string &feature_type);
	std::vector<FeatureTemplate *> *get_templates() {
		return &templates;
	}
};

class FeatureSet {
	FeatureTemplateSet *ftmpl;
	FeatureTemplateSet *wcftmpl;
public:
	std::vector<std::string> fset;
	std::set<std::string> rfset;
	//Morph::DptreeNode* chainRoot;
	//std::vector<Morph::DptreeNode*> chain;
	size_t currentLevel;
	Morph::DptreeNode* breathRoot;
	std::vector<Morph::DptreeNode*> breath;
	size_t currentLeftDist;
	size_t currentRightDist;

	FeatureSet(FeatureTemplateSet *in_ftmpl);
	FeatureSet(FeatureTemplateSet *in_ftmpl, FeatureTemplateSet *in_wcftmpl);
	~FeatureSet();
	void extract_unigram_feature(Node *node);
	void extract_bigram_feature(Node *l_node, Node *r_node);
	//extract dpfeature
	void extract_edge_feature(Dptoken* head, Dptoken* modifier);
	void extract_context_feature(Dptoken* head, Dptoken* modifier,
			std::vector<Morph::Dptoken> *dpTokenArray);

	void extract_sibling_feature(Dptoken* head, Dptoken* modifier);
	void extract_sibling_feature(Dptoken* head, Dptoken* modifier,
			Dptoken* sibling);

	void extract_wcedge_feature(Dptoken* head, Dptoken* modifier);
	void extract_wccontext_feature(Dptoken* head, Dptoken* modifier,
			std::vector<Morph::Dptoken> *dpTokenArray);

	void extract_wcsibling_feature(Dptoken* head, Dptoken* modifier);
	void extract_wcsibling_feature(Dptoken* head, Dptoken* modifier,
			Dptoken* sibling);



	void extract_sibling_feature_complex(Dptoken* head, Dptoken* modifier);

	void extract_breath_feature(std::vector<Morph::Dptoken> *dpTokenArray);

//	void extract_rerank_feature(Dptree* dptree, size_t left_boundry,
//				size_t right_boundry, size_t bottom_boundry);
//	void extract_depth_feature(Dptree* dptree, size_t length_limit);

	void extract_rerank_feature(Dptree* dptree, DptreeNode* subroot, size_t left_boundry,
				size_t right_boundry, size_t bottom_boundry);
	void extract_depth_feature(Dptree* dptree, DptreeNode* subroot,size_t length_limit);

	void extract_ancestor_feature(Dptree* dptree, DptreeNode* subroot,size_t length_limit);

	void config_rerank_feature(Dptree* dptree, DptreeNode* subroot, size_t left_boundry,
				size_t right_boundry, size_t bottom_boundry);
	void config_depth_feature(Dptree* dptree, DptreeNode* subroot,size_t length_limit);
	void config_ancestor_feature(Dptree* dptree, DptreeNode* subroot,size_t length_limit);

	//void extract_treeSlice_feature(std::vector<Morph::Dptoken> *dpTokenArray);



//	void loadChain(size_t n) {
//		Morph::DptreeNode *nullTreeNode = NULL;
//		for (size_t i = 0; i < n; i++) {
//			chain.push_back(nullTreeNode);
//		}
//	}

	void findAllChains(Morph::DptreeNode* subRoot, size_t n);

	void depthFirstTravel(std::string &f1, std::string &f2,
			Morph::DptreeNode* rootNode, Morph::DptreeNode* headNode,
			size_t left_boundry, size_t right_boundry, size_t bottom_boundry);

	void depthFirstTravel(Morph::DptreeNode* rootNode, Morph::DptreeNode* headNode);

	void appendNodeFeature(std::string& f, int complexity,
			Morph::DptreeNode* rootNode, Morph::DptreeNode* headNode);

	void appendRootFeature(std::string& f, int complexity,
			Morph::DptreeNode* dpNode);

	void appendDepthNodeFeature(std::string& f, int complexity, Morph::DptreeNode* headNode);

	void appendAncestorNodeFeature(std::string& f, int complexity,
			Morph::DptreeNode* modifier, Morph::DptreeNode* head);

	void appendUnitNodeFeature(std::string& f, Morph::DptreeNode* dpNode);

	double calc_inner_product_with_weight();

	//	int calc_inner_product_with_weight(
	//			std::map<std::string, double> &in_feature_weight);

	double calc_inner_product_with_weight(
			std::map<std::string, double> &in_feature_weight);

	double calc_rerank_inner_product_with_weight(
				std::map<std::string, double> &in_rerank_feature_weight);
	//	inline int calc_inner_product_with_weight(
	//			std::map<std::string, double> &in_feature_weight, std::map<
	//					std::string, double> &in_feature_weight_sum, std::map<
	//					std::string, int> &in_count_update, size_t total_n) {
	//		double sum = 0;
	//		for (std::vector<std::string>::iterator it = fset.begin(); it
	//				!= fset.end(); it++) {
	//			if (in_feature_weight.find(*it) != in_feature_weight.end()) {
	//				sum += (total_n - in_count_update[*it])
	//						* in_feature_weight[*it] + in_feature_weight_sum[*it];
	//			}
	//		}
	//		return sum;
	//	}

	inline bool append_feature(FeatureSet *in) {
		for (std::vector<std::string>::iterator it = in->fset.begin();
				it != in->fset.end(); it++) {
			fset.push_back(*it);
		}
		return true;
	}

	//	inline void minus_feature_from_weight(
	//			std::map<std::string, double> &in_feature_weight, std::map<
	//					std::string, double> &in_feature_weight_sum, std::map<
	//					std::string, int> &in_count_update, size_t total_n,
	//			size_t factor) {
	//		for (std::vector<std::string>::iterator it = fset.begin(); it
	//				!= fset.end(); it++) {
	//
	//			//std::cout << "before total_n:" <<total_n<<"  count_update:" << in_count_update[*it] <<"  feature_weight:"<<in_feature_weight[*it]<<std::endl;
	//
	//			in_feature_weight_sum[*it] += (total_n - in_count_update[*it])
	//					* in_feature_weight[*it];
	//			in_feature_weight[*it] -= factor;
	//			in_count_update[*it] = total_n;
	//
	//			//std::cout << "after  total_n:" <<total_n<<"  count_update:" << in_count_update[*it] <<"  feature_weight:"<<in_feature_weight[*it]<<std::endl;
	//
	//		}
	//	}

	inline void minus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight, size_t factor) {
		for (std::vector<std::string>::iterator it = fset.begin();
				it != fset.end(); it++) {
			in_feature_weight[*it] -= factor;
		}
	}

	inline void minus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight) {
		for (std::vector<std::string>::iterator it = fset.begin();
				it != fset.end(); it++) {
			in_feature_weight[*it] -= 1;
		}

	}

//	inline void minus_feature_from_weight(
//			std::map<std::string, double> &in_feature_weight, size_t factor) {
//		for (std::vector<std::string>::iterator it = fset.begin(); it
//				!= fset.end(); it++) {
//			in_feature_weight[*it] -= factor;
//		}
//	}
//
//	inline void minus_feature_from_weight(
//			std::map<std::string, double> &in_feature_weight) {
//		for (std::vector<std::string>::iterator it = fset.begin(); it
//				!= fset.end(); it++) {
//			in_feature_weight[*it] -= 1;
//		}
//	}

	//	inline void plus_feature_from_weight(
	//			std::map<std::string, double> &in_feature_weight, std::map<
	//					std::string, double> &in_feature_weight_sum, std::map<
	//					std::string, int> &in_count_update, size_t total_n,
	//			size_t factor) {
	//		for (std::vector<std::string>::iterator it = fset.begin(); it
	//				!= fset.end(); it++) {
	//			in_feature_weight_sum[*it] += (total_n - in_count_update[*it])
	//					* in_feature_weight[*it];
	//			in_feature_weight[*it] += factor;
	//			in_count_update[*it] = total_n;
	//		}
	//	}

	inline void plus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight, size_t factor) {
		for (std::vector<std::string>::iterator it = fset.begin();
				it != fset.end(); it++) {
			in_feature_weight[*it] += factor;
		}
	}

	inline void plus_feature_from_weight(
			std::map<std::string, double> &in_feature_weight) {
		for (std::vector<std::string>::iterator it = fset.begin();
				it != fset.end(); it++) {
			in_feature_weight[*it] += 1;
		}
	}

//	inline void plus_feature_from_weight(
//			std::map<std::string, double> &in_feature_weight, size_t factor) {
//		for (std::vector<std::string>::iterator it = fset.begin(); it
//				!= fset.end(); it++) {
//			in_feature_weight[*it] += factor;
//		}
//	}
//
//	inline void plus_feature_from_weight(
//			std::map<std::string, double> &in_feature_weight) {
//		for (std::vector<std::string>::iterator it = fset.begin(); it
//				!= fset.end(); it++) {
//			in_feature_weight[*it] += 1;
//		}
//	}

	inline void plus_rerank_feature_from_weight(
			std::map<std::string, double> &in_feature_weight, double factor) {
		for (std::set<std::string>::iterator it = rfset.begin();
				it != rfset.end(); it++) {
			in_feature_weight[*it] += factor;
		}
	}

	inline void plus_rerank_feature_from_weight(
			std::map<std::string, double> &in_feature_weight) {
		for (std::set<std::string>::iterator it = rfset.begin();
				it != rfset.end(); it++) {
			in_feature_weight[*it] += 1;
		}
	}

	inline void minus_rerank_feature_from_weight(
			std::map<std::string, double> &in_feature_weight, double factor) {
		for (std::set<std::string>::iterator it = rfset.begin();
				it != rfset.end(); it++) {
			in_feature_weight[*it] -= factor;
		}
	}

	inline void minus_rerank_feature_from_weight(
			std::map<std::string, double> &in_feature_weight) {
		for (std::set<std::string>::iterator it = rfset.begin();
				it != rfset.end(); it++) {
			in_feature_weight[*it] -= 1;
		}
	}

	bool print();
};

}

#endif
