#ifndef PARSER_H_
#define PARSER_H_

#include "common.h"
#include "dptoken.h"
#include "dpsentence.h"
#include "parameter.h"

#define PARSE_SCORE_FACTOR 188000
//#define PARSE_SCORE_FACTOR 1

namespace Morph {

class DepParser {

	Parameter *param;
	FeatureTemplateSet ftmpl;
	FeatureTemplateSet wcftmpl;

	bool shuffle_training_data;

	Dpsentence *dpsentence; // current input sentence
	KbestDpsentence *kbest_dpsentences;
	//size_t iteration_num;
	int dpsentences_for_train_num;
	std::vector<Dpsentence *> dpsentences_for_train;
	std::vector<KbestDpsentence *> kbest_dpsentences_for_train;

	int kbest_gold_data_sentence_count;
	//std::vector<Node *> begin_node_list; // position -> list of nodes that begin at this pos
	//std::vector<Node *> end_node_list;   // position -> list of nodes that end at this pos

public:
	DepParser(Parameter *in_param);
	virtual ~DepParser();

	//Sentence *new_sentence_analyze(std::string &in_sentence);
	void sentence_clear();
	bool viterbi_at_position(unsigned int pos, Node *r_node);
	void print_best_path();

	bool train(const std::string &gsd_file, unsigned int order,
			unsigned int iteration);
	bool exhaust_train(const std::string &gsd_file, unsigned int order,
			unsigned int iteration);

	bool test(unsigned int order);
	bool rerank_test(const std::string &gsd_file, unsigned int kbest);
	bool config(const std::string &gsd_file, unsigned int order);
	bool rerank(const std::string &gsd_file, unsigned int kbest,
			unsigned int iteration);
	bool kbest(const std::string &gsd_file, const std::string &kbo_file,
			size_t K, unsigned int order);

	bool kbest_iostream(size_t K, unsigned int order);

	bool output(unsigned int order);
	bool output(const std::string &gsd_file, const std::string &o_file,
			unsigned int order);

	bool output_stream(unsigned int order);

	bool io_stream(unsigned int order);

	bool complete(const std::string &gsd_file, size_t K, unsigned int order);
	bool sequence_parse(const std::string &gsd_file, size_t K,
			unsigned int order);
	bool sequence_rerank(const std::string &gsd_file, size_t K,
			unsigned int order);


	bool write_dependency_tags(std::vector<std::vector<int> >& depTags) {

		if (dpsentences_for_train.size() != depTags.size()) {
			cerr << "E: data not aligned; in write_dependency_tags" << endl;
			return false;
		}

		size_t sentence_count = 0;

		for (std::vector<Dpsentence*>::iterator it =
				dpsentences_for_train.begin();
				it != dpsentences_for_train.end(); it++) {

			if ((*it)->getDpTokenArray()->size() + 1
					!= depTags[sentence_count].size()) {
				cerr << "E: sentences not aligned; in write_dependency_tags"
						<< endl;
				return false;
			}

			for (int p = 0; p < (*it)->getDpTokenArray()->size(); p++) {

				int line_No = p + 1;

				cout << line_No << "\t";
				cout << (*it)->getDpTokenArray()->at(p).word << "\t";
				cout << "_\t";
				cout << (*it)->getDpTokenArray()->at(p).pos << "\t";
				cout << (*it)->getDpTokenArray()->at(p).pos << "\t";
				cout << "_\t";
				cout << depTags[sentence_count][p+1] << "\t";
				cout << "_"<< endl;

			}
			cout << endl;
			sentence_count++;
		}

		return true;

	}


	bool write_dependency_tags(const std::string &o_file,
			std::vector<std::vector<int> >& depTags) {

		std::ofstream gsd_out(o_file.c_str(), std::ios::out);
		if (!gsd_out.is_open()) {
			cerr << "E: cannot open file for writing" << endl;
			return false;
		}

		if (dpsentences_for_train.size() != depTags.size()) {
			cerr << "E: data not aligned; in write_dependency_tags" << endl;
			return false;
		}

		size_t sentence_count = 0;

		for (std::vector<Dpsentence*>::iterator it =
				dpsentences_for_train.begin();
				it != dpsentences_for_train.end(); it++) {

			if ((*it)->getDpTokenArray()->size() + 1
					!= depTags[sentence_count].size()) {
				cerr << "E: sentences not aligned; in write_dependency_tags"
						<< endl;
				return false;
			}

			for (int p = 0; p < (*it)->getDpTokenArray()->size(); p++) {

				int line_No = p + 1;

				gsd_out << line_No << "\t";
				gsd_out << (*it)->getDpTokenArray()->at(p).word << "\t";
				gsd_out << "_\t";
				gsd_out << (*it)->getDpTokenArray()->at(p).pos << "\t";
				gsd_out << (*it)->getDpTokenArray()->at(p).pos << "\t";
				gsd_out << "_\t";
				gsd_out << depTags[sentence_count][p+1] << "\t";
				gsd_out << "_"<< endl;

			}
			gsd_out << endl;
			sentence_count++;
		}

		gsd_out.close();

		return true;

	}

	bool overwrite_dependency_tags(const std::string &gsd_file,
			std::vector<std::vector<int> >& depTags) {

		std::vector<std::vector<std::vector<std::string> > > originalData;
		std::vector<std::vector<std::string> > oneSentence;

		size_t sentence_count = 0;

		std::ifstream gsd_in(gsd_file.c_str(), std::ios::in);
		if (!gsd_in.is_open()) {
			cerr << ";; cannot open gold data for reading" << endl;
			return false;
		}
		// Sentence *new_sentence = new Sentence(strlen(buffer.c_str()), &begin_node_list, &end_node_list, &dic, &ftmpl, param);

		std::string buffer;

//		std::stringstream ss;
//		int goldDep;
//		std::vector<int> depArray;

		size_t line_count = 0;

		while (getline(gsd_in, buffer)) {

			if (buffer == "") {
				originalData.push_back(oneSentence);
				oneSentence.clear();

				line_count = 0;
				sentence_count++;
				continue;
			}

			std::vector<std::string> oneWord;

			split_string(buffer, "\t", oneWord);

			oneWord.at(6) = int2string(depTags[sentence_count][line_count]);

			oneSentence.push_back(oneWord);

			line_count++;
		}

		gsd_in.close();

		std::ofstream gsd_out(gsd_file.c_str(), std::ios::out);
		if (!gsd_out.is_open()) {
			cerr << ";; cannot open file for writing" << endl;
			return false;
		}

		for (std::vector<std::vector<std::vector<std::string> > >::iterator dt_it =
				originalData.begin(); dt_it != originalData.end(); dt_it++) {
			for (std::vector<std::vector<std::string> >::iterator at_it =
					(*dt_it).begin(); at_it != (*dt_it).end(); at_it++) {

				for (std::vector<std::string>::iterator tt_it =
						(*at_it).begin(); tt_it != (*at_it).end(); tt_it++) {
					gsd_out << (*tt_it) << "\t";
				}
				gsd_out << endl;
			}
			gsd_out << endl;
		}

		gsd_out.close();

		return true;

	}

	bool write_temp_model_file(const std::string &model_filename) {
		std::ofstream model_out(model_filename.c_str(), std::ios::out);
		model_out.precision(15);
		if (!model_out.is_open()) {
			cerr << ";; cannot open " << model_filename << " for writing"
					<< endl;
			return false;
		}

		if (WEIGHT_AVERAGED) {
			std::map<std::string, double> feature_temp_weight;

			for (std::map<std::string, double>::iterator it =
					feature_weight_sum_dep.begin();
					it != feature_weight_sum_dep.end(); it++) {

				if (feature_weight_dep.find(it->first)
						== feature_weight_dep.end()) {
					cerr << "error: cannot match feature weight and weight sum"
							<< endl;
				} else {
					feature_temp_weight[it->first] =
							feature_weight_dep[it->first] * total_sentence_num
									- it->second;
				}

			}

			for (std::map<std::string, double>::iterator it =
					feature_temp_weight.begin();
					it != feature_temp_weight.end(); it++) {
				model_out << it->first << " " << it->second << endl;
			}

		} else {
			for (std::map<std::string, double>::iterator it =
					feature_weight_dep.begin(); it != feature_weight_dep.end();
					it++) {
				model_out << it->first << " " << it->second << endl;
			}
		}

		model_out.close();
		return true;
	}

	bool check_file_exist(const std::string &kbest_filename) {

		std::ofstream kbest_check(kbest_filename.c_str(), std::ios::in);
		if (kbest_check.is_open()) {

			cerr << "Overwrite existing file: " << kbest_filename << endl;

			kbest_check.close();
			std::ofstream kbest_out(kbest_filename.c_str(), std::ios::out);
			if (!kbest_out.is_open()) {

				cerr << ";; cannot open " << kbest_filename << " for writing"
						<< endl;
				return false;
			}

			kbest_out.close();
		} else {
			cerr << "New file created: " << kbest_filename << endl;

		}
		return true;
	}

	bool write_kbest_data(std::vector<std::string> &GS,
			const std::string &gsd_file, const std::string &kbest_filename,
			std::vector<std::vector<int> >& predict_vectors,
			std::vector<int> oracle_vector, std::vector<double> score_vector,
			std::vector<double> ratio_vector, size_t K, int sentence_length,
			size_t sentence_num) {

		std::ofstream kbest_app(kbest_filename.c_str(), std::ios::app);
		if (!kbest_app.is_open()) {

			std::ofstream kbest_out(kbest_filename.c_str(), std::ios::out);
			if (!kbest_out.is_open()) {

				cerr << ";; cannot open " << kbest_filename << " for writing"
						<< endl;
				return false;
			}

			for (std::vector<std::string>::iterator str_it = GS.begin();
					str_it != GS.end(); str_it++) {
				kbest_out << (*str_it) << endl;
			}

			kbest_out << "Sentence No." << endl;
			kbest_out << sentence_num << endl;
			for (size_t rank = 0; rank < K; rank++) {
				kbest_out << "------" << endl;
				kbest_out << oracle_vector[rank] << '\t' << score_vector[rank]
						<< '\t' << ratio_vector[rank] << endl;
				for (size_t i = 0; i < sentence_length; i++) {
					kbest_out << predict_vectors[rank][i + 1] << endl;
				}
			}
			kbest_out << "" << endl;
			kbest_out.close();
			return true;

		}

		for (std::vector<std::string>::iterator str_it = GS.begin();
				str_it != GS.end(); str_it++) {
			kbest_app << (*str_it) << endl;
		}

		kbest_app << "Sentence No." << endl;
		kbest_app << sentence_num << endl;
		for (size_t rank = 0; rank < K; rank++) {
			kbest_app << "------" << endl;
			kbest_app << oracle_vector[rank] << '\t' << score_vector[rank]
					<< '\t' << ratio_vector[rank] << endl;
			for (size_t i = 0; i < sentence_length; i++) {
				kbest_app << predict_vectors[rank][i + 1] << endl;
			}
		}
		kbest_app << "" << endl;
		kbest_app.close();
		return true;
	}

	bool read_gold_data();

	bool read_gold_data(const std::string &gsd_file);
	bool read_gold_data(const char *gsd_file);

	bool read_gold_data(const std::string &gsd_file, size_t division,
			size_t spare);
	bool read_gold_data(const char *gsd_file, size_t division, size_t spare);


	bool read_complement_gold_data(size_t division, size_t spare);

	bool read_complement_gold_data(const std::string &gsd_file, size_t division,
			size_t spare);
	bool read_complement_gold_data(const char *gsd_file, size_t division,
			size_t spare);

	bool read_gold_data_slice(const std::string &gsd_file, size_t division,
			size_t spare);
	bool read_gold_data_slice(const char *gsd_file, size_t division,
			size_t spare);


	bool read_kbest_gold_data(std::vector<std::vector<std::string> >& goldSen,
			const std::string &gsd_file, size_t K);
	bool read_kbest_gold_data(std::vector<std::vector<std::string> >& goldSen,
			const char *gsd_file, size_t K);

	bool read_kbest_gold_data(std::vector<std::vector<std::string> >& goldSen,
			const std::string &gsd_file, size_t K, size_t division,
			size_t spare);
	bool read_kbest_gold_data(std::vector<std::vector<std::string> >& goldSen,
			const char *gsd_file, size_t K, size_t division, size_t spare);

	bool read_complement_kbest_gold_data(
			std::vector<std::vector<std::string> >& goldSen,
			const std::string &gsd_file, size_t K, size_t division,
			size_t spare);
	bool read_complement_kbest_gold_data(
			std::vector<std::vector<std::string> >& goldSen,
			const char *gsd_file, size_t K, size_t division, size_t spare);

	bool read_rerank_gold_data(const std::string &gsd_file);
	bool read_rerank_gold_data(const char *gsd_file);

	//	    bool lookup_gold_data(Sentence *sentence, std::string &word_pos_pair);
	//	    bool add_one_sentence_for_train(Sentence *in_sentence);
	bool add_one_sentence_for_train(Dpsentence *in_sentence);
	bool add_one_sentence_for_train(KbestDpsentence *in_sentence);
	void clear_gold_data();
	void clear_kbest_gold_data();
};

}

#endif /* PARSER_H_ */
