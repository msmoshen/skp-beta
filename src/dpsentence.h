#ifndef DPSENTENCE_H
#define DPSENTENCE_H

#include "common.h"
#include "dptoken.h"
#include "feature.h"

#define Lower_bound -1e15
#define Loose_lower_bound -1e14

namespace Morph {

class KbestSearchToken {
public:
	int leftPos;
	int rightPos;
	std::string cKey;
	double cValue;
	int interPosition;
	bool isThroughSibling;

	KbestSearchToken() {

	}
	;
	KbestSearchToken(int lP, int rP) {
		leftPos = lP;
		rightPos = rP;
		cKey = "";
		cKey.append(int2string(leftPos));
		cKey.append(",");
		cKey.append(int2string(rightPos));
		isThroughSibling = false;
	}
	KbestSearchToken(int lP, int rP, int iP) {
		leftPos = lP;
		rightPos = rP;
		cKey = "";
		cKey.append(int2string(leftPos));
		cKey.append(",");
		cKey.append(int2string(rightPos));
		interPosition = iP;
		isThroughSibling = false;
	}
	KbestSearchToken(int lP, int rP, int iP, double cV) {
		leftPos = lP;
		rightPos = rP;
		cKey = "";
		cKey.append(int2string(leftPos));
		cKey.append(",");
		cKey.append(int2string(rightPos));
		interPosition = iP;
		cValue = cV;
		isThroughSibling = false;
	}

	virtual ~KbestSearchToken() {

	}
	;

	bool operator<(const KbestSearchToken &right) const {
		if ((*this).cValue < right.cValue)
			return true;
		else
			return false;
	}

};

class Span {

	double complete_left_eval;
	double complete_right_eval;
	double incomplete_left_eval;
	double incomplete_right_eval;
	double sibling_left_eval;
	double sibling_right_eval;
	int complete_left_interPosition;
	int complete_right_interPosition;
	int incomplete_left_interPosition;
	int incomplete_right_interPosition;
	int sibling_left_interPosition;
	int sibling_right_interPosition;
	bool throughLeftSibling;
	bool throughRightSibling;

public:
	Span();
	Span(double e1, double e2, double e3, double e4, int r1, int r2, int r3,
			int r4);
	Span(double e1, double e2, double e3, double e4, double esl, double esr,
			int r1, int r2, int r3, int r4, int rsl, int rsr);
	Span(double e1, double e2, double e3, double e4, double esl, double esr,
			int r1, int r2, int r3, int r4, int rsl, int rsr, bool lts,
			bool rts);

	virtual ~Span();

	inline void setThroughLeftSibling(bool lts) {
		throughLeftSibling = lts;
	}
	inline bool getThroughLeftSibling() {
		return throughLeftSibling;
	}

	inline void setThroughRightSibling(bool rts) {
		throughRightSibling = rts;
	}
	inline bool getThroughRightSibling() {
		return throughRightSibling;
	}

	inline void setComplete_left_eval(double e1) {
		complete_left_eval = e1;
	}
	inline double getComplete_left_eval() {
		return complete_left_eval;
	}

	inline void setComplete_right_eval(double e2) {
		complete_right_eval = e2;
	}
	inline double getComplete_right_eval() {
		return complete_right_eval;
	}

	inline void setIncomplete_left_eval(double e3) {
		incomplete_left_eval = e3;
	}
	inline double getIncomplete_left_eval() {
		return incomplete_left_eval;
	}

	inline void setIncomplete_right_eval(double e4) {
		incomplete_right_eval = e4;
	}
	inline double getIncomplete_right_eval() {
		return incomplete_right_eval;
	}

	inline void setSibling_left_eval(double esl) {
		sibling_left_eval = esl;
	}
	inline double getSibling_left_eval() {
		return sibling_left_eval;
	}

	inline void setSibling_right_eval(double esr) {
		sibling_right_eval = esr;
	}
	inline double getSibling_right_eval() {
		return sibling_right_eval;
	}

	inline void setComplete_left_interPosition(int r1) {
		complete_left_interPosition = r1;
	}
	inline int getComplete_left_interPosition() {
		return complete_left_interPosition;
	}

	inline void setComplete_right_interPosition(int r2) {
		complete_right_interPosition = r2;
	}
	inline int getComplete_right_interPosition() {
		return complete_right_interPosition;
	}

	inline void setIncomplete_left_interPosition(int r3) {
		incomplete_left_interPosition = r3;
	}
	inline int getIncomplete_left_interPosition() {
		return incomplete_left_interPosition;
	}

	inline void setIncomplete_right_interPosition(int r4) {
		incomplete_right_interPosition = r4;
	}
	inline int getIncomplete_right_interPosition() {
		return incomplete_right_interPosition;
	}

	inline void setSibling_left_interPosition(int rsl) {
		sibling_left_interPosition = rsl;
	}
	inline int getSibling_left_interPosition() {
		return sibling_left_interPosition;
	}

	inline void setSibling_right_interPosition(int rsr) {
		sibling_right_interPosition = rsr;
	}
	inline int getSibling_right_interPosition() {
		return sibling_right_interPosition;
	}

};

class KbestSpan {

	size_t kbest;
	int order;

public:
	std::vector<KbestSearchToken> complete_left_component;
	std::vector<KbestSearchToken> complete_right_component;
	std::vector<KbestSearchToken> incomplete_left_component;
	std::vector<KbestSearchToken> incomplete_right_component;
	std::vector<KbestSearchToken> sibling_left_component;
	std::vector<KbestSearchToken> sibling_right_component;

	KbestSpan() {

	}
	;
	KbestSpan(size_t k) {
		kbest = k;
		order = 1;

		KbestSearchToken tempKbestToken(1, 1, -1, 0);
		complete_left_component.push_back(tempKbestToken);
		complete_right_component.push_back(tempKbestToken);
		incomplete_left_component.push_back(tempKbestToken);
		incomplete_right_component.push_back(tempKbestToken);
	}

	KbestSpan(size_t k, int odr) {
		kbest = k;
		order = odr;

		KbestSearchToken tempKbestToken(1, 1, -1, 0);
		complete_left_component.push_back(tempKbestToken);
		complete_right_component.push_back(tempKbestToken);
		incomplete_left_component.push_back(tempKbestToken);
		incomplete_right_component.push_back(tempKbestToken);

		sibling_left_component.push_back(tempKbestToken);
		sibling_right_component.push_back(tempKbestToken);
	}

	//	KbestSpan(size_t k, std::vector<double> e1, std::vector<double> e2, std::vector<double> e3, std::vector<double> e4, std::vector<int> r1, std::vector<int> r2, std::vector<int> r3,
	//			std::vector<int> r4);
	virtual ~KbestSpan() {

	}
	;

	//	void setK(size_t k) {
	// 		 kbest = k;
	//	}

	void appendComplete_left_component(KbestSearchToken e1) {
		complete_left_component.push_back(e1);
	}
	std::vector<KbestSearchToken> getComplete_left_component() {
		return complete_left_component;
	}

	void appendComplete_right_component(KbestSearchToken e2) {
		complete_right_component.push_back(e2);
	}
	std::vector<KbestSearchToken> getComplete_right_component() {
		return complete_right_component;
	}

	void appendIncomplete_left_component(KbestSearchToken e3) {
		incomplete_left_component.push_back(e3);
	}
	std::vector<KbestSearchToken> getIncomplete_left_component() {
		return incomplete_left_component;
	}

	void appendIncomplete_right_component(KbestSearchToken e4) {
		incomplete_right_component.push_back(e4);
	}
	std::vector<KbestSearchToken> getIncomplete_right_component() {
		return incomplete_right_component;
	}

	void appendSibling_left_component(KbestSearchToken s1) {
		sibling_left_component.push_back(s1);
	}
	std::vector<KbestSearchToken> getSibling_left_component() {
		return sibling_left_component;
	}

	void appendSibling_right_component(KbestSearchToken s2) {
		sibling_right_component.push_back(s2);
	}
	std::vector<KbestSearchToken> getSibling_right_component() {
		return sibling_right_component;
	}

	//	void setComplete_left_interPosition(int r1) {
	//		complete_left_interPosition.push_back(r1);
	//	}
	//	std::vector<int> getComplete_left_interPosition() {
	//		return complete_left_interPosition;
	//	}
	//
	//	void setComplete_right_interPosition(int r2) {
	//		complete_right_interPosition.push_back(r2);
	//	}
	//	std::vector<int> getComplete_right_interPosition() {
	//		return complete_right_interPosition;
	//	}
	//
	//	void setIncomplete_left_interPosition(int r3) {
	//		incomplete_left_interPosition.push_back(r3);
	//	}
	//	std::vector<int> getIncomplete_left_interPosition() {
	//		return incomplete_left_interPosition;
	//	}
	//
	//	void setIncomplete_right_interPosition(int r4) {
	//		incomplete_right_interPosition.push_back(r4);
	//	}
	//	std::vector<int> getIncomplete_right_interPosition() {
	//		return incomplete_right_interPosition;
	//	}

};

class Dpsentence {

	unsigned int length; // length of this sentence
	double** scoreMtx;
	//double*** scoreQb;
	std::vector<std::vector<std::vector<double> > > scoreTriMtx;

	std::vector<int> headIndex;
	std::vector<int> predict;
	std::vector<Morph::Dptoken> dpTokenArray;
	std::vector<std::vector<int> > depTreeList;
	std::vector<int> oracleList;
	std::vector<double> parseScoreList;
	std::vector<double> predictAccuracyList;

public:

	Dpsentence();
	virtual ~Dpsentence();

	int getLength() {
		return length;
	}

	void setLength(size_t t) {
		length = t;
	}

	inline std::vector<Morph::Dptoken> *getDpTokenArray() {
		return &dpTokenArray;
	}

	inline void appendDpToken(Morph::Dptoken* dp) {
		dpTokenArray.push_back(*dp);
	}
	inline void setPredictPointWise(int i, int j) {
		predict[i] = j;
	}

	inline int getPredictPointWise(int i) {
		return predict[i];
	}

	inline std::vector<int>* getPredict() {
		return &(predict);
	}

	inline double** getScoreMtx() {
		return scoreMtx;
	}

//	inline double*** getScoreQb() {
//		return scoreQb;
//	}

	inline void setHeadIndexPointWise(int i, int j) {
		headIndex[i] = j;
	}
	inline std::vector<int>* getHeadIndex() {
		return &(headIndex);
	}

	inline void printPredict() {
		for (size_t i = 0; i < length; i++) {
			std::cout << predict[i + 1] << std::endl;
		}
	}

	inline void printHeadIndex() {
		for (size_t i = 0; i < length; i++) {
			std::cout << headIndex[i] << std::endl;
		}
	}

	inline void pushTreeList(std::vector<int> incoming_tree) {
		depTreeList.push_back(incoming_tree);
	}

	inline void pushOracleList(int incoming_oracle) {
		oracleList.push_back(incoming_oracle);
	}

	inline void pushParseScoreList(double incoming_score) {
		parseScoreList.push_back(incoming_score);
	}

	inline void pushPredAccuList(double incoming_accu) {
		predictAccuracyList.push_back(incoming_accu);
	}

	inline std::vector<std::vector<int> >* getDepTree() {
		return &(depTreeList);
	}

	inline std::vector<int>* getOracle() {
		return &(oracleList);
	}

	inline std::vector<double>* getParseScore() {
		return &(parseScoreList);
	}

	inline std::vector<double>* getPredictAccu() {
		return &(predictAccuracyList);
	}

	inline void printResult() {

		for (size_t i = 0; i < length; i++) {
			int lineNum = i + 1;
			std::cout << lineNum << '\t' << dpTokenArray[i].word << '\t' << "_"
					<< '\t' << dpTokenArray[i].pos << '\t'
					<< dpTokenArray[i].pos << '\t' << "_" << '\t'
					<< predict[i + 1] << '\t' << "_" << std::endl;

		}

		std::cout << std::endl;
	}

	inline void comparePrint() {
		std::cerr << "Predict" << '\t' << "Gold" << std::endl;
		double nom = 0;
		double denom = 0;
		for (size_t i = 0; i < length; i++) {
			std::cerr << predict[i + 1] << '\t' << headIndex[i] << std::endl;
			if (headIndex[i] != 0) {
				if (dpTokenArray[i].pos != "PU" && dpTokenArray[i].pos != ","
						&& dpTokenArray[i].pos != ":"
						&& dpTokenArray[i].pos != "$"
						&& dpTokenArray[i].pos != "#") {
					denom++;
					if (predict[i + 1] == headIndex[i]) {
						nom++;
					}
				}
			}
		}
		double ratio = nom / denom;
		std::cerr << "No." << total_sentence_num << '\t' << nom << '\t' << denom
				<< '\t' << ratio << std::endl;
	}

	inline void comparePrint(std::vector<int> &pickedVec) {
		std::cerr << "Predict" << '\t' << "Gold" << '\t' << "Oracle"
				<< std::endl;
		double nom = 0;
		double denom = 0;
		for (size_t i = 0; i < length; i++) {
			std::cerr << pickedVec[i] << '\t' << headIndex[i] << std::endl;
			if (headIndex[i] != 0) {
				if (dpTokenArray[i].pos != "PU" && dpTokenArray[i].pos != ","
						&& dpTokenArray[i].pos != ":"
						&& dpTokenArray[i].pos != "$"
						&& dpTokenArray[i].pos != "#") {
					denom++;
					if (pickedVec[i] == headIndex[i]) {
						nom++;
					}
				}
			}
		}
		double ratio = nom / denom;
		std::cerr << "No." << total_sentence_num << '\t' << nom << '\t' << denom
				<< '\t' << ratio << std::endl;
	}

	inline void comparePrint(std::vector<int> &pickedVec,
			std::vector<int> &oracleVec) {
		std::cerr << "Predict" << '\t' << "Gold" << '\t' << "Oracle"
				<< std::endl;
		double nom = 0;
		double denom = 0;
		for (size_t i = 0; i < length; i++) {
			std::cerr << pickedVec[i] << '\t' << headIndex[i] << '\t'
					<< oracleVec[i] << std::endl;
			if (headIndex[i] != 0) {
				if (dpTokenArray[i].pos != "PU" && dpTokenArray[i].pos != ","
						&& dpTokenArray[i].pos != ":"
						&& dpTokenArray[i].pos != "$"
						&& dpTokenArray[i].pos != "#") {
					denom++;
					if (pickedVec[i] == headIndex[i]) {
						nom++;
					}
				}
			}
		}
		double ratio = nom / denom;
		std::cerr << "No." << total_sentence_num << '\t' << nom << '\t' << denom
				<< '\t' << ratio << std::endl;
	}

	inline void checkAccuracy(size_t &nom, size_t &denom) {
		for (size_t i = 0; i < length; i++) {
			if (headIndex[i] != 0) {
				if (dpTokenArray[i].pos != "PU" && dpTokenArray[i].pos != ","
						&& dpTokenArray[i].pos != ":"
						&& dpTokenArray[i].pos != "$"
						&& dpTokenArray[i].pos != "#") {
					denom++;
					if (predict[i + 1] == headIndex[i]) {
						nom++;
					}
				}
			}
		}
	}

	inline void checkAccuracy(size_t &nom, size_t &denom,
			std::vector<int> &pickedVec) {
		for (size_t i = 0; i < length; i++) {
			if (headIndex[i] != 0) {
				if (dpTokenArray[i].pos != "PU" && dpTokenArray[i].pos != ","
						&& dpTokenArray[i].pos != ":"
						&& dpTokenArray[i].pos != "$"
						&& dpTokenArray[i].pos != "#") {
					denom++;
					if (pickedVec[i] == headIndex[i]) {
						nom++;
					}
				}
			}
		}
	}

	inline void setSurroundingPos() {
		std::string leftPOS = "BOS";
		std::string rightPOS = "EOS";
		for (std::vector<Morph::Dptoken>::iterator iterator =
				dpTokenArray.begin(); iterator != dpTokenArray.end();
				iterator++) {
			iterator->setLeftPos(leftPOS);
			leftPOS = iterator->pos;
		}
		for (std::vector<Morph::Dptoken>::reverse_iterator reverse_iterator =
				dpTokenArray.rbegin(); reverse_iterator != dpTokenArray.rend();
				reverse_iterator++) {
			reverse_iterator->setRightPos(rightPOS);
			rightPOS = reverse_iterator->pos;
		}
	}

	inline void setHeadIndex(std::vector<int>* depArray) {
		headIndex = *depArray;
		//	for (std::vector<Morph::Dptoken>::iterator iterator = dpTokenArray.begin(); iterator
		//				!= dpTokenArray.end(); iterator++) {
		//			headIndex[(*iterator).index]=(*iterator).dep;
		//		}
	}

	inline void evaluateEdgeScoreMatrix(Morph::FeatureTemplateSet* in_ftmpl,
			size_t total_n) {
		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == j || i == 0 || j == 0) {
					continue;
				} else {

					//				cerr << "head modifier: "
					//						<<i<<":"<< Dpsentence::dpTokenArray.at(i - 1).position << " "
					//						<<j<<":"<< Dpsentence::dpTokenArray.at(j - 1).position << endl;
					FeatureSet *f = new FeatureSet(in_ftmpl);
					f->extract_edge_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));
					f->extract_context_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)),
							&(Dpsentence::dpTokenArray));

					scoreMtx[i][j] = f->calc_inner_product_with_weight(
							feature_weight_dep);

					//					if (WEIGHT_AVERAGED) {
					//						scoreMtx[i][j] = f->calc_inner_product_with_weight(
					//								feature_weight_dep, feature_weight_sum_dep,
					//								feature_count_lastupdate, total_n);
					//					} else {
					//						scoreMtx[i][j] = f->calc_inner_product_with_weight(
					//								feature_weight_dep);
					//					}

					delete f;
				}
			}
		}
	}

	inline void evaluateEdgeScoreMatrixWc(Morph::FeatureTemplateSet* in_ftmpl,
			Morph::FeatureTemplateSet* in_wcftmpl, size_t total_n) {
		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == j || i == 0 || j == 0) {
					continue;
				} else {

					//				cerr << "head modifier: "
					//						<<i<<":"<< Dpsentence::dpTokenArray.at(i - 1).position << " "
					//						<<j<<":"<< Dpsentence::dpTokenArray.at(j - 1).position << endl;
					FeatureSet *f = new FeatureSet(in_ftmpl, in_wcftmpl);
					f->extract_edge_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));
					f->extract_context_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)),
							&(Dpsentence::dpTokenArray));

					f->extract_wcedge_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));

					f->extract_wccontext_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)),
							&(Dpsentence::dpTokenArray));

					scoreMtx[i][j] = f->calc_inner_product_with_weight(
							feature_weight_dep);

					//					if (WEIGHT_AVERAGED) {
					//						scoreMtx[i][j] = f->calc_inner_product_with_weight(
					//								feature_weight_dep, feature_weight_sum_dep,
					//								feature_count_lastupdate, total_n);
					//					} else {
					//						scoreMtx[i][j] = f->calc_inner_product_with_weight(
					//								feature_weight_dep);
					//					}

					delete f;
				}
			}
		}
	}

	inline void initializeScoreMatrix() {
		double score[length + 1][length + 1];

		scoreMtx = (double**) new double[length + 1];

		for (size_t i = 0; i < length + 1; i++) {
			scoreMtx[i] = (double*) new double[length + 1];
		}

		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == 0 || j == 0) {
					score[i][j] = Loose_lower_bound;
				} else {
					score[i][j] = 0;
				}
				scoreMtx[i][j] = score[i][j];
			}
		}

	}

//	inline void evaluateSiblingScoreMatrix(Morph::FeatureTemplateSet* in_ftmpl,
//			size_t total_n) {
//
//		for (size_t i = 0; i < length + 1; i++) {
//			for (size_t j = 0; j < length + 1; j++) {
//				if (i == j || i == 0 || j == 0) {
//					continue;
//				} else {
//
//					FeatureSet f(in_ftmpl);
//
//					f.extract_edge_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)));
//
//					f.extract_context_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)),
//							&(Dpsentence::dpTokenArray));
//
//					f.extract_sibling_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)));
//
//					scoreTriMtx[i][j][0] = f.calc_inner_product_with_weight(
//							feature_weight_dep);
//
//					if (i < j - 1) {
//						for (size_t k = 1; k < j - i; k++) {
//
//							FeatureSet s(in_ftmpl);
//
//							s.extract_edge_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)));
//
//							s.extract_context_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)),
//									&(Dpsentence::dpTokenArray));
//
//
//							s.extract_sibling_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)),
//									&(Dpsentence::dpTokenArray.at(i + k - 1)));
//
//							scoreTriMtx[i][j][k] = s.calc_inner_product_with_weight(
//											feature_weight_dep);
//
//						}
//
//					} else if (j < i - 1) {
//						for (size_t k = 1; k < i - j; k++) {
//
//							FeatureSet s(in_ftmpl);
//
//							s.extract_edge_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)));
//
//							s.extract_context_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)),
//									&(Dpsentence::dpTokenArray));
//
//							s.extract_sibling_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)),
//									&(Dpsentence::dpTokenArray.at(j + k - 1)));
//
//							scoreTriMtx[i][j][k] = s.calc_inner_product_with_weight(
//											feature_weight_dep);
//
//						}
//					}
//				}
//			}
//		}
//	}

//	inline void evaluateSiblingScoreMatrix(Morph::FeatureTemplateSet* in_ftmpl,
//			size_t total_n) {
//
//		for (size_t i = 0; i < length + 1; i++) {
//			for (size_t j = 0; j < length + 1; j++) {
//				if (i == j || i == 0 || j == 0) {
//					continue;
//				} else {
//
//					FeatureSet f(in_ftmpl);
//
//					f.extract_edge_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)));
//
//					f.extract_context_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)),
//							&(Dpsentence::dpTokenArray));
//
//					double tempEdgeScore = f.calc_inner_product_with_weight(
//							feature_weight_dep);
//
//
//					f.extract_sibling_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)));
//
//					scoreTriMtx[i][j][0] = f.calc_inner_product_with_weight(
//							feature_weight_dep);
//
//					if (i < j - 1) {
//						for (size_t k = 1; k < j - i; k++) {
//
//							FeatureSet s(in_ftmpl);
//
//
//							s.extract_sibling_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)),
//									&(Dpsentence::dpTokenArray.at(i + k - 1)));
//
//							scoreTriMtx[i][j][k] = tempEdgeScore
//									+ s.calc_inner_product_with_weight(
//											feature_weight_dep);
//
//						}
//
//					} else if (j < i - 1) {
//						for (size_t k = 1; k < i - j; k++) {
//
//							FeatureSet s(in_ftmpl);
//
//
//							s.extract_sibling_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)),
//									&(Dpsentence::dpTokenArray.at(j + k - 1)));
//
//							scoreTriMtx[i][j][k] = tempEdgeScore
//									+ s.calc_inner_product_with_weight(
//											feature_weight_dep);
//
//						}
//					}
//				}
//			}
//		}
//	}

	inline void evaluateSiblingScoreMatrix(Morph::FeatureTemplateSet* in_ftmpl,
			size_t total_n) {

		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == j || i == 0 || j == 0) {
					continue;
				} else {

					std::string hp = Dpsentence::dpTokenArray.at(i - 1).pos;

					std::string mp = Dpsentence::dpTokenArray.at(j - 1).pos;

					std::string mw = Dpsentence::dpTokenArray.at(j - 1).word;

					FeatureSet f(in_ftmpl);

					f.extract_edge_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));

					f.extract_context_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)),
							&(Dpsentence::dpTokenArray));

					double tempEdgeScore = f.calc_inner_product_with_weight(
							feature_weight_dep);

//					cout<<"linear score:"<<tempEdgeScore <<endl;

					f.extract_sibling_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));

					scoreTriMtx[i][j][0] = f.calc_inner_product_with_weight(
							feature_weight_dep);

//					cout<<"score "<<j<<"\t"<<i<<"\t"<<0<<"\t"<<scoreTriMtx[i][j][0] <<endl;

					if (i < j - 1) {
						for (size_t k = 1; k < j - i; k++) {

							FeatureSet s(in_ftmpl);

							s.extract_sibling_feature(
									&(Dpsentence::dpTokenArray.at(i - 1)),
									&(Dpsentence::dpTokenArray.at(j - 1)),
									&(Dpsentence::dpTokenArray.at(i + k - 1)));

							scoreTriMtx[i][j][k] = tempEdgeScore
									+ s.calc_inner_product_with_weight(
											feature_weight_dep);

//							cout<<"score "<<j<<"\t"<<i<<"\t"<<i + k<<"\t"<<scoreTriMtx[i][j][k] <<endl;

						}

					} else if (j < i - 1) {
						for (size_t k = 1; k < i - j; k++) {

							FeatureSet s(in_ftmpl);

							s.extract_sibling_feature(
									&(Dpsentence::dpTokenArray.at(i - 1)),
									&(Dpsentence::dpTokenArray.at(j - 1)),
									&(Dpsentence::dpTokenArray.at(j + k - 1)));

							scoreTriMtx[i][j][k] = tempEdgeScore
									+ s.calc_inner_product_with_weight(
											feature_weight_dep);

//							cout<<"score "<<j<<"\t"<<i<<"\t"<<j + k<<"\t"<<scoreTriMtx[i][j][k] <<endl;

						}
					}
				}
			}
		}
	}


	inline void evaluateSiblingScoreMatrixWc(Morph::FeatureTemplateSet* in_ftmpl,Morph::FeatureTemplateSet* in_wcftmpl,
			size_t total_n) {

		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == j || i == 0 || j == 0) {
					continue;
				} else {

//					std::string hp = Dpsentence::dpTokenArray.at(i - 1).pos;
//
//					std::string mp = Dpsentence::dpTokenArray.at(j - 1).pos;
//
//					std::string mw = Dpsentence::dpTokenArray.at(j - 1).word;

					FeatureSet f(in_ftmpl, in_wcftmpl);

					f.extract_edge_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));

					f.extract_context_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)),
							&(Dpsentence::dpTokenArray));

					f.extract_wcedge_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));

					f.extract_wccontext_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)),
							&(Dpsentence::dpTokenArray));

					double tempEdgeScore = f.calc_inner_product_with_weight(
							feature_weight_dep);

//					cout<<"linear score:"<<tempEdgeScore <<endl;

					f.extract_sibling_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));

					f.extract_wcsibling_feature(
							&(Dpsentence::dpTokenArray.at(i - 1)),
							&(Dpsentence::dpTokenArray.at(j - 1)));

					scoreTriMtx[i][j][0] = f.calc_inner_product_with_weight(
							feature_weight_dep);

//					cout<<"score "<<j<<"\t"<<i<<"\t"<<0<<"\t"<<scoreTriMtx[i][j][0] <<endl;

					if (i < j - 1) {
						for (size_t k = 1; k < j - i; k++) {

							FeatureSet s(in_ftmpl,in_wcftmpl);

							s.extract_sibling_feature(
									&(Dpsentence::dpTokenArray.at(i - 1)),
									&(Dpsentence::dpTokenArray.at(j - 1)),
									&(Dpsentence::dpTokenArray.at(i + k - 1)));

							s.extract_wcsibling_feature(
									&(Dpsentence::dpTokenArray.at(i - 1)),
									&(Dpsentence::dpTokenArray.at(j - 1)),
									&(Dpsentence::dpTokenArray.at(i + k - 1)));

							scoreTriMtx[i][j][k] = tempEdgeScore
									+ s.calc_inner_product_with_weight(
											feature_weight_dep);

//							cout<<"score "<<j<<"\t"<<i<<"\t"<<i + k<<"\t"<<scoreTriMtx[i][j][k] <<endl;

						}

					} else if (j < i - 1) {
						for (size_t k = 1; k < i - j; k++) {

							FeatureSet s(in_ftmpl,in_wcftmpl);

							s.extract_sibling_feature(
									&(Dpsentence::dpTokenArray.at(i - 1)),
									&(Dpsentence::dpTokenArray.at(j - 1)),
									&(Dpsentence::dpTokenArray.at(j + k - 1)));

							s.extract_wcsibling_feature(
									&(Dpsentence::dpTokenArray.at(i - 1)),
									&(Dpsentence::dpTokenArray.at(j - 1)),
									&(Dpsentence::dpTokenArray.at(j + k - 1)));

							scoreTriMtx[i][j][k] = tempEdgeScore
									+ s.calc_inner_product_with_weight(
											feature_weight_dep);

//							cout<<"score "<<j<<"\t"<<i<<"\t"<<j + k<<"\t"<<scoreTriMtx[i][j][k] <<endl;

						}
					}
				}
			}
		}
	}

//	inline void evaluateSiblingScoreMatrix(Morph::FeatureTemplateSet* in_ftmpl,
//			size_t total_n) {
//
//		std::string h1 = "HpSpMp";
//		std::string h2 = "SpMp";
//		std::string h3 = "SpMw";
//		std::string h4 = "SwMp";
//		std::string h5 = "SwMw";
//
//		std::string hsf = "DirDis";
//
//		for (size_t i = 0; i < length + 1; i++) {
//			for (size_t j = 0; j < length + 1; j++) {
//				if (i == j || i == 0 || j == 0) {
//					continue;
//				} else {
//
//					std::string hp = Dpsentence::dpTokenArray.at(i - 1).pos;
//
//					std::string mp = Dpsentence::dpTokenArray.at(j - 1).pos;
//
//					std::string mw = Dpsentence::dpTokenArray.at(j - 1).word;
//
//					FeatureSet f(in_ftmpl);
//
//					f.extract_edge_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)));
//
//					f.extract_context_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)),
//							&(Dpsentence::dpTokenArray));
//
//					double tempEdgeScore = f.calc_inner_product_with_weight(
//							feature_weight_dep);
//
//					if (i < j) {
//						f.fset.push_back(
//								h1 + ":" + hp + "," + "#FRSP" + "," + mp);
//						f.fset.push_back(h2 + ":" + "#FRSP" + "," + mp);
//						f.fset.push_back(h3 + ":" + "#FRSP" + "," + mw);
//						f.fset.push_back(h4 + ":" + "#FRSW" + "," + mp);
//						f.fset.push_back(h5 + ":" + "#FRSW" + "," + mw);
//
//					} else if (i > j) {
//						f.fset.push_back(
//								h1 + ":" + hp + "," + "#FLSP" + "," + mp);
//						f.fset.push_back(h2 + ":" + "#FLSP" + "," + mp);
//						f.fset.push_back(h3 + ":" + "#FLSP" + "," + mw);
//						f.fset.push_back(h4 + ":" + "#FLSW" + "," + mp);
//						f.fset.push_back(h5 + ":" + "#FLSW" + "," + mw);
//
//					} else {
//						std::cerr << "error: head identical with modifier"
//								<< std::endl;
//					}
//
////										f.extract_sibling_feature(
////												&(Dpsentence::dpTokenArray.at(i - 1)),
////												&(Dpsentence::dpTokenArray.at(j - 1)));
//
//					scoreTriMtx[i][j][0] = f.calc_inner_product_with_weight(
//							feature_weight_dep);
//
//					if (i < j - 1) {
//						for (size_t k = 1; k < j - i; k++) {
//
//							std::string sw = Dpsentence::dpTokenArray.at(
//									i + k - 1).word;
//							std::string sp = Dpsentence::dpTokenArray.at(
//									i + k - 1).pos;
//
//							FeatureSet s(in_ftmpl);
//
//							s.fset.push_back(
//									h1 + ":" + hp + "," + sp + "," + mp);
//							s.fset.push_back(h2 + ":" + sp + "," + mp);
//							s.fset.push_back(h3 + ":" + sp + "," + mw);
//							s.fset.push_back(h4 + ":" + sw + "," + mp);
//							s.fset.push_back(h5 + ":" + sw + "," + mw);
//
//							int tempDist = j - i - k;
//							std::string tempDistStr = int2string(tempDist);
//
//							if (tempDist < 6) {
//								s.fset.push_back(
//										h1 + hsf + ":" + hp + "," + sp + ","
//												+ mp + ",#LEFT" + ","
//												+ tempDistStr);
//								s.fset.push_back(
//										h2 + hsf + ":" + sp + "," + mp
//												+ ",#LEFT" + "," + tempDistStr);
//								s.fset.push_back(
//										h3 + hsf + ":" + sp + "," + mw
//												+ ",#LEFT" + "," + tempDistStr);
//								s.fset.push_back(
//										h4 + hsf + ":" + sw + "," + mp
//												+ ",#LEFT" + "," + tempDistStr);
//								s.fset.push_back(
//										h5 + hsf + ":" + sw + "," + mw
//												+ ",#LEFT" + "," + tempDistStr);
//							} else if (tempDist < 11) {
//
//								s.fset.push_back(
//										h1 + hsf + ":" + hp + "," + sp + ","
//												+ mp + ",#LEFT" + "," + "10");
//								s.fset.push_back(
//										h2 + hsf + ":" + sp + "," + mp
//												+ ",#LEFT" + "," + "10");
//								s.fset.push_back(
//										h3 + hsf + ":" + sp + "," + mw
//												+ ",#LEFT" + "," + "10");
//								s.fset.push_back(
//										h4 + hsf + ":" + sw + "," + mp
//												+ ",#LEFT" + "," + "10");
//								s.fset.push_back(
//										h5 + hsf + ":" + sw + "," + mw
//												+ ",#LEFT" + "," + "10");
//							} else {
//
//								s.fset.push_back(
//										h1 + hsf + ":" + hp + "," + sp + ","
//												+ mp + ",#LEFT" + "," + "50");
//								s.fset.push_back(
//										h2 + hsf + ":" + sp + "," + mp
//												+ ",#LEFT" + "," + "50");
//								s.fset.push_back(
//										h3 + hsf + ":" + sp + "," + mw
//												+ ",#LEFT" + "," + "50");
//								s.fset.push_back(
//										h4 + hsf + ":" + sw + "," + mp
//												+ ",#LEFT" + "," + "50");
//								s.fset.push_back(
//										h5 + hsf + ":" + sw + "," + mw
//												+ ",#LEFT" + "," + "50");
//							}
//
//							scoreTriMtx[i][j][k] = tempEdgeScore
//									+ s.calc_inner_product_with_weight(
//											feature_weight_dep);
//
//						}
//
//					} else if (j < i - 1) {
//						for (size_t k = 1; k < i - j; k++) {
//
//							std::string sw = Dpsentence::dpTokenArray.at(
//									j + k - 1).word;
//							std::string sp = Dpsentence::dpTokenArray.at(
//									j + k - 1).pos;
//
//							FeatureSet s(in_ftmpl);
//
//							s.fset.push_back(
//									h1 + ":" + hp + "," + sp + "," + mp);
//							s.fset.push_back(h2 + ":" + sp + "," + mp);
//							s.fset.push_back(h3 + ":" + sp + "," + mw);
//							s.fset.push_back(h4 + ":" + sw + "," + mp);
//							s.fset.push_back(h5 + ":" + sw + "," + mw);
//
//							int tempDist = i - j - k;
//							std::string tempDistStr = int2string(tempDist);
//
//							if (tempDist < 6) {
//								s.fset.push_back(
//										h1 + hsf + ":" + hp + "," + sp + ","
//												+ mp + ",#RIGHT" + ","
//												+ tempDistStr);
//								s.fset.push_back(
//										h2 + hsf + ":" + sp + "," + mp
//												+ ",#RIGHT" + ","
//												+ tempDistStr);
//								s.fset.push_back(
//										h3 + hsf + ":" + sp + "," + mw
//												+ ",#RIGHT" + ","
//												+ tempDistStr);
//								s.fset.push_back(
//										h4 + hsf + ":" + sw + "," + mp
//												+ ",#RIGHT" + ","
//												+ tempDistStr);
//								s.fset.push_back(
//										h5 + hsf + ":" + sw + "," + mw
//												+ ",#RIGHT" + ","
//												+ tempDistStr);
//							} else if (tempDist < 11) {
//
//								s.fset.push_back(
//										h1 + hsf + ":" + hp + "," + sp + ","
//												+ mp + ",#RIGHT" + "," + "10");
//								s.fset.push_back(
//										h2 + hsf + ":" + sp + "," + mp
//												+ ",#RIGHT" + "," + "10");
//								s.fset.push_back(
//										h3 + hsf + ":" + sp + "," + mw
//												+ ",#RIGHT" + "," + "10");
//								s.fset.push_back(
//										h4 + hsf + ":" + sw + "," + mp
//												+ ",#RIGHT" + "," + "10");
//								s.fset.push_back(
//										h5 + hsf + ":" + sw + "," + mw
//												+ ",#RIGHT" + "," + "10");
//							} else {
//
//								s.fset.push_back(
//										h1 + hsf + ":" + hp + "," + sp + ","
//												+ mp + ",#RIGHT" + "," + "50");
//								s.fset.push_back(
//										h2 + hsf + ":" + sp + "," + mp
//												+ ",#RIGHT" + "," + "50");
//								s.fset.push_back(
//										h3 + hsf + ":" + sp + "," + mw
//												+ ",#RIGHT" + "," + "50");
//								s.fset.push_back(
//										h4 + hsf + ":" + sw + "," + mp
//												+ ",#RIGHT" + "," + "50");
//								s.fset.push_back(
//										h5 + hsf + ":" + sw + "," + mw
//												+ ",#RIGHT" + "," + "50");
//							}
//
//							scoreTriMtx[i][j][k] = tempEdgeScore
//									+ s.calc_inner_product_with_weight(
//											feature_weight_dep);
//
//						}
//					}
//				}
//			}
//		}
//	}

//	inline void evaluateSiblingScoreMatrix(Morph::FeatureTemplateSet* in_ftmpl,
//			size_t total_n) {
//		for (size_t i = 0; i < length + 1; i++) {
//			for (size_t j = 0; j < length + 1; j++) {
//				if (i == j || i == 0 || j == 0) {
//					continue;
//				} else {
//					FeatureSet f(in_ftmpl);
//
//					f.extract_edge_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)));
//
//					f.extract_context_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)),
//							&(Dpsentence::dpTokenArray));
//
//					f.extract_sibling_feature(
//							&(Dpsentence::dpTokenArray.at(i - 1)),
//							&(Dpsentence::dpTokenArray.at(j - 1)));
//
//					scoreQb[i][j][0] = f.calc_inner_product_with_weight(
//							feature_weight_dep);
//
//					if (i < j - 1) {
//						for (size_t k = 1; k < j - i; k++) {
//							FeatureSet s(in_ftmpl);
//
//							s.extract_sibling_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)),
//									&(Dpsentence::dpTokenArray.at(i + k - 1)));
//
//							scoreQb[i][j][k] = scoreQb[i][j][0]
//									+ s.calc_inner_product_with_weight(
//											feature_weight_dep);
//
//						}
//
//					} else if (j < i - 1) {
//						for (size_t k = 1; k < i - j; k++) {
//							FeatureSet s(in_ftmpl);
//
//							s.extract_sibling_feature(
//									&(Dpsentence::dpTokenArray.at(i - 1)),
//									&(Dpsentence::dpTokenArray.at(j - 1)),
//									&(Dpsentence::dpTokenArray.at(j + k - 1)));
//
//							scoreQb[i][j][k] = scoreQb[i][j][0]
//									+ s.calc_inner_product_with_weight(
//											feature_weight_dep);
//
//						}
//					}
//				}
//			}
//		}
//	}

//	inline void initializeScoreCube() {
//		double score[length + 1][length + 1][length];
//
//		scoreQb = (double***) new double[length + 1];
//
//		for (size_t i = 0; i < length + 1; i++) {
//			scoreQb[i] = (double**) new double[length + 1];
//		}
//
//		for (size_t i = 0; i < length + 1; i++) {
//			for (size_t j = 0; j < length + 1; j++) {
//				scoreQb[i][j] = (double*) new double[length];
//			}
//		}
//
//		for (size_t i = 0; i < length + 1; i++) {
//			for (size_t j = 0; j < length + 1; j++) {
//				for (size_t k = 0; k < length; k++) {
//					if (i == 0 || j == 0) {
//						score[i][j][k] = Loose_lower_bound;
//					} else {
//						score[i][j][k] = 0;
//					}
//					scoreQb[i][j][k] = score[i][j][k];
//				}
//			}
//		}
//
//	}

	inline void collectSiblingFeatureStrings(
			Morph::FeatureTemplateSet* in_ftmpl,
			std::vector<std::vector<std::vector<FeatureSet> > > &incoming_f) {

		std::string h1 = "HpSpMp";
		std::string h2 = "SpMp";
		std::string h3 = "SpMw";
		std::string h4 = "SwMp";
		std::string h5 = "SwMw";

		std::string hsf = "DirDis";

		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == j || i == 0 || j == 0) {
					continue;
				} else {

					std::string hp = Dpsentence::dpTokenArray.at(i - 1).pos;

					std::string mp = Dpsentence::dpTokenArray.at(j - 1).pos;

					std::string mw = Dpsentence::dpTokenArray.at(j - 1).word;

					if (abs(i - j) == 1) {

						incoming_f[i - 1][j - 1][0].extract_edge_feature(
								&(Dpsentence::dpTokenArray.at(i - 1)),
								&(Dpsentence::dpTokenArray.at(j - 1)));
						incoming_f[i - 1][j - 1][0].extract_sibling_feature(
								&(Dpsentence::dpTokenArray.at(i - 1)),
								&(Dpsentence::dpTokenArray.at(j - 1)));

					} else if (i < j - 1) {
						for (size_t k = 1; k < j - i; k++) {

							incoming_f[i - 1][j - 1][k].extract_edge_feature(
									&(Dpsentence::dpTokenArray.at(i - 1)),
									&(Dpsentence::dpTokenArray.at(j - 1)));

							std::string sw = Dpsentence::dpTokenArray.at(
									i + k - 1).word;
							std::string sp = Dpsentence::dpTokenArray.at(
									i + k - 1).pos;

							incoming_f[i - 1][j - 1][k].fset.push_back(
									h1 + ":" + hp + "," + sp + "," + mp);
							incoming_f[i - 1][j - 1][k].fset.push_back(
									h2 + ":" + sp + "," + mp);
							incoming_f[i - 1][j - 1][k].fset.push_back(
									h3 + ":" + sp + "," + mw);
							incoming_f[i - 1][j - 1][k].fset.push_back(
									h4 + ":" + sw + "," + mp);
							incoming_f[i - 1][j - 1][k].fset.push_back(
									h5 + ":" + sw + "," + mw);

							int tempDist = j - i - k;
							std::string tempDistStr = int2string(tempDist);

							if (tempDist < 6) {
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h1 + hsf + ":" + hp + "," + sp + ","
												+ mp + ",#LEFT" + ","
												+ tempDistStr);
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h2 + hsf + ":" + sp + "," + mp
												+ ",#LEFT" + "," + tempDistStr);
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h3 + hsf + ":" + sp + "," + mw
												+ ",#LEFT" + "," + tempDistStr);
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h4 + hsf + ":" + sw + "," + mp
												+ ",#LEFT" + "," + tempDistStr);
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h5 + hsf + ":" + sw + "," + mw
												+ ",#LEFT" + "," + tempDistStr);
							} else if (tempDist < 11) {

								incoming_f[i - 1][j - 1][k].fset.push_back(
										h1 + hsf + ":" + hp + "," + sp + ","
												+ mp + ",#LEFT" + "," + "10");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h2 + hsf + ":" + sp + "," + mp
												+ ",#LEFT" + "," + "10");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h3 + hsf + ":" + sp + "," + mw
												+ ",#LEFT" + "," + "10");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h4 + hsf + ":" + sw + "," + mp
												+ ",#LEFT" + "," + "10");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h5 + hsf + ":" + sw + "," + mw
												+ ",#LEFT" + "," + "10");
							} else {

								incoming_f[i - 1][j - 1][k].fset.push_back(
										h1 + hsf + ":" + hp + "," + sp + ","
												+ mp + ",#LEFT" + "," + "50");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h2 + hsf + ":" + sp + "," + mp
												+ ",#LEFT" + "," + "50");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h3 + hsf + ":" + sp + "," + mw
												+ ",#LEFT" + "," + "50");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h4 + hsf + ":" + sw + "," + mp
												+ ",#LEFT" + "," + "50");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h5 + hsf + ":" + sw + "," + mw
												+ ",#LEFT" + "," + "50");
							}

							//							cout<<"score "<<j<<"\t"<<i<<"\t"<<i + k<<"\t"<<scoreTriMtx[i][j][k] <<endl;

						}

					} else if (j < i - 1) {
						for (size_t k = 1; k < i - j; k++) {

							incoming_f[i - 1][j - 1][k].extract_edge_feature(
									&(Dpsentence::dpTokenArray.at(i - 1)),
									&(Dpsentence::dpTokenArray.at(j - 1)));

							std::string sw = Dpsentence::dpTokenArray.at(
									j + k - 1).word;
							std::string sp = Dpsentence::dpTokenArray.at(
									j + k - 1).pos;

							incoming_f[i - 1][j - 1][k].fset.push_back(
									h1 + ":" + hp + "," + sp + "," + mp);
							incoming_f[i - 1][j - 1][k].fset.push_back(
									h2 + ":" + sp + "," + mp);
							incoming_f[i - 1][j - 1][k].fset.push_back(
									h3 + ":" + sp + "," + mw);
							incoming_f[i - 1][j - 1][k].fset.push_back(
									h4 + ":" + sw + "," + mp);
							incoming_f[i - 1][j - 1][k].fset.push_back(
									h5 + ":" + sw + "," + mw);

							int tempDist = i - j - k;
							std::string tempDistStr = int2string(tempDist);

							if (tempDist < 6) {
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h1 + hsf + ":" + hp + "," + sp + ","
												+ mp + ",#RIGHT" + ","
												+ tempDistStr);
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h2 + hsf + ":" + sp + "," + mp
												+ ",#RIGHT" + ","
												+ tempDistStr);
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h3 + hsf + ":" + sp + "," + mw
												+ ",#RIGHT" + ","
												+ tempDistStr);
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h4 + hsf + ":" + sw + "," + mp
												+ ",#RIGHT" + ","
												+ tempDistStr);
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h5 + hsf + ":" + sw + "," + mw
												+ ",#RIGHT" + ","
												+ tempDistStr);
							} else if (tempDist < 11) {

								incoming_f[i - 1][j - 1][k].fset.push_back(
										h1 + hsf + ":" + hp + "," + sp + ","
												+ mp + ",#RIGHT" + "," + "10");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h2 + hsf + ":" + sp + "," + mp
												+ ",#RIGHT" + "," + "10");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h3 + hsf + ":" + sp + "," + mw
												+ ",#RIGHT" + "," + "10");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h4 + hsf + ":" + sw + "," + mp
												+ ",#RIGHT" + "," + "10");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h5 + hsf + ":" + sw + "," + mw
												+ ",#RIGHT" + "," + "10");
							} else {

								incoming_f[i - 1][j - 1][k].fset.push_back(
										h1 + hsf + ":" + hp + "," + sp + ","
												+ mp + ",#RIGHT" + "," + "50");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h2 + hsf + ":" + sp + "," + mp
												+ ",#RIGHT" + "," + "50");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h3 + hsf + ":" + sp + "," + mw
												+ ",#RIGHT" + "," + "50");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h4 + hsf + ":" + sw + "," + mp
												+ ",#RIGHT" + "," + "50");
								incoming_f[i - 1][j - 1][k].fset.push_back(
										h5 + hsf + ":" + sw + "," + mw
												+ ",#RIGHT" + "," + "50");
							}

						}
					}
				}
			}
		}
	}

	inline void evaluateScoreFormCollection(
			std::vector<std::vector<std::vector<FeatureSet> > > &incoming_f) {

		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == j || i == 0 || j == 0) {
					continue;
				} else {

					if (abs(i - j) == 1) {

						scoreTriMtx[i][j][0] =
								incoming_f[i - 1][j - 1][0].calc_inner_product_with_weight(
										feature_weight_dep);

					} else if (i < j - 1) {
						for (size_t k = 1; k < j - i; k++) {

							scoreTriMtx[i][j][k] =
									incoming_f[i - 1][j - 1][k].calc_inner_product_with_weight(
											feature_weight_dep);

						}

					} else if (j < i - 1) {
						for (size_t k = 1; k < i - j; k++) {

							scoreTriMtx[i][j][k] =
									incoming_f[i - 1][j - 1][k].calc_inner_product_with_weight(
											feature_weight_dep);

						}
					}
				}
			}
		}
	}

	inline void initializeScoreCube() {

		std::vector<double> initScoreVec;
		std::vector<double> initLLBVec;

		std::vector<std::vector<double> > initScoreMtx;
		std::vector<std::vector<double> > initLLBMtx;

		for (size_t k = 0; k < length; k++) {
			initScoreVec.push_back(0);
			initLLBVec.push_back(Loose_lower_bound);
		}

		initScoreMtx.push_back(initLLBVec);
		initLLBMtx.push_back(initLLBVec);

		for (size_t k = 0; k < length; k++) {
			initScoreMtx.push_back(initScoreVec);
			initLLBMtx.push_back(initLLBVec);
		}

		scoreTriMtx.push_back(initLLBMtx);

		for (size_t k = 0; k < length; k++) {
			scoreTriMtx.push_back(initScoreMtx);
		}

	}

	void rebuildPredictVector(Span** span, int i, int j, bool isLeft,
			bool isComplete);

	void rebuildPredictVectorNeo(Span** span, int i, int j, bool isLeft,
			bool isComplete, bool isSibling);

	inline void decodeEdgeFactors() {

		if (length < 1) {
			std::cerr << "emtpy sentence!" << std::endl;
		} else if (length == 1) {
			predict.push_back(0);
			predict.push_back(0);
		} else {
			for (int i = 0; i < length + 1; i++) {
				predict.push_back(0);
			}

			//initialize span matrix

			Morph::Span** spanMtx = (Morph::Span**) new Morph::Span[length + 1];

			for (int i = 0; i < length + 1; i++) {
				spanMtx[i] = (Morph::Span*) new Morph::Span[length + 1];
			}

			for (int i = 0; i < length + 1; i++) {
				for (int j = 0; j < length + 1; j++) {
					Morph::Span initSpan = Morph::Span(0, 0, 0, 0, -1, -1, -1,
							-1);
					spanMtx[i][j] = initSpan;
				}
			}

			for (int k = 1; k < length + 1; k++) {
				for (int s = 0; s < length + 1; s++) {
					int t = s + k;
					if (t > length)
						break;

					//Initialize evaluation of valid components
					spanMtx[s][t].setIncomplete_left_eval(Lower_bound);
					spanMtx[s][t].setIncomplete_right_eval(Lower_bound);
					spanMtx[s][t].setComplete_left_eval(Lower_bound);
					spanMtx[s][t].setComplete_right_eval(Lower_bound);
					//Initialize inter-position of valid components
					spanMtx[s][t].setIncomplete_left_interPosition(t - 1);
					spanMtx[s][t].setIncomplete_right_interPosition(s);
					spanMtx[s][t].setComplete_left_interPosition(s);
					spanMtx[s][t].setComplete_right_interPosition(s + 1);

					//build components
					for (int r = s; r <= t - 1; r++) {
						double tempILE = spanMtx[s][r].getComplete_right_eval()
								+ spanMtx[r + 1][t].getComplete_left_eval()
								+ scoreMtx[t][s];
						if (spanMtx[s][t].getIncomplete_left_eval()
								<= tempILE) {
							spanMtx[s][t].setIncomplete_left_eval(tempILE);
							spanMtx[s][t].setIncomplete_left_interPosition(r);
						}
					}
					for (int r = s; r <= t - 1; r++) {
						double tempIRE = spanMtx[s][r].getComplete_right_eval()
								+ spanMtx[r + 1][t].getComplete_left_eval()
								+ scoreMtx[s][t];
						if (spanMtx[s][t].getIncomplete_right_eval()
								< tempIRE) {
							spanMtx[s][t].setIncomplete_right_eval(tempIRE);
							spanMtx[s][t].setIncomplete_right_interPosition(r);
						}
					}
					for (int r = s; r <= t - 1; r++) {
						double tempCLE = spanMtx[s][r].getComplete_left_eval()
								+ spanMtx[r][t].getIncomplete_left_eval();
						if (spanMtx[s][t].getComplete_left_eval() < tempCLE) {
							spanMtx[s][t].setComplete_left_eval(tempCLE);
							spanMtx[s][t].setComplete_left_interPosition(r);
						}
					}
					for (int r = s; r <= t - 1; r++) {
						double tempCRE =
								spanMtx[s][r + 1].getIncomplete_right_eval()
										+ spanMtx[r + 1][t].getComplete_right_eval();
						if (spanMtx[s][t].getComplete_right_eval() < tempCRE) {
							spanMtx[s][t].setComplete_right_eval(tempCRE);
							spanMtx[s][t].setComplete_right_interPosition(
									r + 1);
						}
					}
				}
			}

//			std::cout << "sentence evaluation: "
//					<< spanMtx[0][length].getComplete_right_eval() << std::endl;
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getIncomplete_left_eval()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout
//							<< spanMtx[i][j].getIncomplete_left_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getIncomplete_right_eval()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout
//							<< spanMtx[i][j].getIncomplete_right_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//			//complete eval
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_left_eval() << '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_left_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_right_eval() << '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_right_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << scoreMtx[i][j] << '\t';
//				}
//				std::cout << std::endl;
//			}

			Dpsentence::rebuildPredictVector(spanMtx, 0, length, false, true);

			for (size_t i = 0; i < length + 1; i++) {
				delete[] spanMtx[i];
			}
			//delete spanMtx;

			//		for (int i = 0; i < length + 1; i++) {
			//			for (int j = 0; j < length + 1; j++) {
			//				cerr << scoreMtx[i][j] << " ";
			//			}
			//			cerr << endl;
			//		}
			//  std::cerr << Dpsentence::predict[0] << std::endl;

		}

	}

	inline void decodeSiblingFactors() {

		if (length < 1) {
			std::cerr << "emtpy sentence!" << std::endl;
		} else if (length == 1) {
			predict.push_back(0);
			predict.push_back(0);
		} else {
			for (int i = 0; i < length + 1; i++) {
				predict.push_back(0);
			}

			//initialize span matrix

			Morph::Span** spanMtx = (Morph::Span**) new Morph::Span[length + 1];

			for (int i = 0; i < length + 1; i++) {
				spanMtx[i] = (Morph::Span*) new Morph::Span[length + 1];
			}

			for (int i = 0; i < length + 1; i++) {
				for (int j = 0; j < length + 1; j++) {
					Morph::Span initSpan = Morph::Span(0, 0, 0, 0, 0, 0, -1, -1,
							-1, -1, -1, -1);
					spanMtx[i][j] = initSpan;
				}
			}

			for (int k = 1; k < length + 1; k++) {
				for (int s = 0; s < length + 1; s++) {
					int t = s + k;
					if (t > length)
						break;

					//Initialize evaluation of valid components
					spanMtx[s][t].setIncomplete_left_eval(Lower_bound);
					spanMtx[s][t].setIncomplete_right_eval(Lower_bound);
					spanMtx[s][t].setComplete_left_eval(Lower_bound);
					spanMtx[s][t].setComplete_right_eval(Lower_bound);
					spanMtx[s][t].setSibling_left_eval(Lower_bound);
					spanMtx[s][t].setSibling_right_eval(Lower_bound);
					//Initialize inter-position of valid components
					spanMtx[s][t].setIncomplete_left_interPosition(t - 1);
					spanMtx[s][t].setIncomplete_right_interPosition(s);
					spanMtx[s][t].setComplete_left_interPosition(s);
					spanMtx[s][t].setComplete_right_interPosition(s + 1);
					spanMtx[s][t].setSibling_left_interPosition(t - 1);
					spanMtx[s][t].setSibling_right_interPosition(s);
					//build components
					for (int r = s; r <= t - 1; r++) {

						double tempSLE = spanMtx[s][r].getComplete_right_eval()
								+ spanMtx[r + 1][t].getComplete_left_eval();

						if (spanMtx[s][t].getSibling_left_eval() <= tempSLE) {
							spanMtx[s][t].setSibling_left_eval(tempSLE);
							spanMtx[s][t].setSibling_left_interPosition(r);
						}
					}

					for (int r = s; r <= t - 1; r++) {

						double tempSRE = spanMtx[s][r].getComplete_right_eval()
								+ spanMtx[r + 1][t].getComplete_left_eval();

						if (spanMtx[s][t].getSibling_right_eval() < tempSRE) {
							spanMtx[s][t].setSibling_right_eval(tempSRE);
							spanMtx[s][t].setSibling_right_interPosition(r);
						}
					}

					for (int r = s + 1; r <= t - 1; r++) {
						double tempILE = spanMtx[s][r].getSibling_left_eval()
								+ spanMtx[r][t].getIncomplete_left_eval()
								+ scoreTriMtx[t][s][r - s];

						if (spanMtx[s][t].getIncomplete_left_eval()
								<= tempILE) {
							spanMtx[s][t].setIncomplete_left_eval(tempILE);
							spanMtx[s][t].setIncomplete_left_interPosition(
									spanMtx[s][r].getSibling_left_interPosition());
						}
					}

					double boundILE = spanMtx[s][t - 1].getComplete_right_eval()
							+ spanMtx[t][t].getComplete_left_eval()
							+ scoreTriMtx[t][s][0];

					if (spanMtx[s][t].getIncomplete_left_eval() <= boundILE) {
						spanMtx[s][t].setIncomplete_left_eval(boundILE);
						spanMtx[s][t].setIncomplete_left_interPosition(t - 1);
					}

					double boundIRE = spanMtx[s][s].getComplete_right_eval()
							+ spanMtx[s + 1][t].getComplete_left_eval()
							+ scoreTriMtx[s][t][0];

					if (spanMtx[s][t].getIncomplete_right_eval() < boundIRE) {
						spanMtx[s][t].setIncomplete_right_eval(boundIRE);
						spanMtx[s][t].setIncomplete_right_interPosition(s);
					}

					for (int r = s + 1; r <= t - 1; r++) {
						double tempIRE =
								spanMtx[s][r].getIncomplete_right_eval()
										+ spanMtx[r][t].getSibling_right_eval()
										+ scoreTriMtx[s][t][r - s];

						if (spanMtx[s][t].getIncomplete_right_eval()
								< tempIRE) {
							spanMtx[s][t].setIncomplete_right_eval(tempIRE);
							spanMtx[s][t].setIncomplete_right_interPosition(
									spanMtx[r][t].getSibling_right_interPosition());
						}
					}

					for (int r = s; r <= t - 1; r++) {

						double tempCLE = spanMtx[s][r].getComplete_left_eval()
								+ spanMtx[r][t].getIncomplete_left_eval();
						if (spanMtx[s][t].getComplete_left_eval() < tempCLE) {
							spanMtx[s][t].setComplete_left_eval(tempCLE);
							spanMtx[s][t].setComplete_left_interPosition(r);
						}

					}

					for (int r = s; r <= t - 1; r++) {

						double tempCRE =
								spanMtx[s][r + 1].getIncomplete_right_eval()
										+ spanMtx[r + 1][t].getComplete_right_eval();
						if (spanMtx[s][t].getComplete_right_eval() < tempCRE) {
							spanMtx[s][t].setComplete_right_eval(tempCRE);
							spanMtx[s][t].setComplete_right_interPosition(
									r + 1);
						}

					}

					//					std::cerr << spanMtx[s][t].getComplete_left_eval()
					//							<< std::endl;
					//					std::cerr << spanMtx[s][t].getComplete_right_eval()
					//							<< std::endl;
					//					std::cerr << spanMtx[s][t].getIncomplete_left_eval()
					//							<< std::endl;
					//					std::cerr << spanMtx[s][t].getIncomplete_right_eval()
					//							<< std::endl;
					//					std::cerr << std::endl;

				}
			}

//			std::cout << "sentence evaluation: "
//					<< spanMtx[0][length].getComplete_right_eval() << std::endl;
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getIncomplete_left_eval()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout
//							<< spanMtx[i][j].getIncomplete_left_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getIncomplete_right_eval()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout
//							<< spanMtx[i][j].getIncomplete_right_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//			//complete eval
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_left_eval() << '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_left_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_right_eval() << '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_right_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << scoreTriMtx[i][j][0] << '\t';
//				}
//				std::cout << std::endl;
//			}

			Dpsentence::rebuildPredictVector(spanMtx, 0, length, false, true);

			for (size_t i = 0; i < length + 1; i++) {
				delete[] spanMtx[i];
			}
			//delete spanMtx;

			//		for (int i = 0; i < length + 1; i++) {
			//			for (int j = 0; j < length + 1; j++) {
			//				cerr << scoreMtx[i][j] << " ";
			//			}
			//			cerr << endl;
			//		}
			//  std::cerr << Dpsentence::predict[0] << std::endl;

		}

	}

	inline double decodeSiblingFactorsNeo() {

		if (length < 1) {
			std::cerr << "emtpy sentence!" << std::endl;
			return 0;
		} else if (length == 1) {
			predict.push_back(0);
			predict.push_back(0);
			return 0;
		} else {
			for (int i = 0; i < length + 1; i++) {
				predict.push_back(0);
			}

			//initialize span matrix

			Morph::Span** spanMtx = (Morph::Span**) new Morph::Span[length + 1];

			for (int i = 0; i < length + 1; i++) {
				spanMtx[i] = (Morph::Span*) new Morph::Span[length + 1];
			}

			for (int i = 0; i < length + 1; i++) {
				for (int j = 0; j < length + 1; j++) {
					Morph::Span initSpan = Morph::Span(0, 0, 0, 0, 0, 0, -1, -1,
							-1, -1, -1, -1, false, false);
					spanMtx[i][j] = initSpan;
				}
			}

			for (int k = 1; k < length + 1; k++) {
				for (int s = 0; s < length + 1; s++) {
					int t = s + k;
					if (t > length)
						break;

					//Initialize evaluation of valid components
					spanMtx[s][t].setIncomplete_left_eval(Lower_bound);
					spanMtx[s][t].setIncomplete_right_eval(Lower_bound);
					spanMtx[s][t].setComplete_left_eval(Lower_bound);
					spanMtx[s][t].setComplete_right_eval(Lower_bound);
					spanMtx[s][t].setSibling_left_eval(Lower_bound);
					spanMtx[s][t].setSibling_right_eval(Lower_bound);
					//Initialize inter-position of valid components
					spanMtx[s][t].setIncomplete_left_interPosition(t - 1);
					spanMtx[s][t].setIncomplete_right_interPosition(s);
					spanMtx[s][t].setComplete_left_interPosition(s);
					spanMtx[s][t].setComplete_right_interPosition(s + 1);
					spanMtx[s][t].setSibling_left_interPosition(t - 1);
					spanMtx[s][t].setSibling_right_interPosition(s);
					//build components
					for (int r = s; r <= t - 1; r++) {

						double tempSLE = spanMtx[s][r].getComplete_right_eval()
								+ spanMtx[r + 1][t].getComplete_left_eval();

						if (spanMtx[s][t].getSibling_left_eval() <= tempSLE) {
							spanMtx[s][t].setSibling_left_eval(tempSLE);
							spanMtx[s][t].setSibling_left_interPosition(r);
						}
					}

					for (int r = s; r <= t - 1; r++) {

						double tempSRE = spanMtx[s][r].getComplete_right_eval()
								+ spanMtx[r + 1][t].getComplete_left_eval();

						if (spanMtx[s][t].getSibling_right_eval() < tempSRE) {
							spanMtx[s][t].setSibling_right_eval(tempSRE);
							spanMtx[s][t].setSibling_right_interPosition(r);
						}
					}

					for (int r = s + 1; r <= t - 1; r++) {
						double tempILE = spanMtx[s][r].getSibling_left_eval()
								+ spanMtx[r][t].getIncomplete_left_eval()
								+ scoreTriMtx[t][s][r - s];

						if (spanMtx[s][t].getIncomplete_left_eval()
								<= tempILE) {
							spanMtx[s][t].setIncomplete_left_eval(tempILE);
							spanMtx[s][t].setIncomplete_left_interPosition(r);
							spanMtx[s][t].setThroughLeftSibling(true);
						}
					}

					double boundILE = spanMtx[s][t - 1].getComplete_right_eval()
							+ spanMtx[t][t].getComplete_left_eval()
							+ scoreTriMtx[t][s][0];

					if (spanMtx[s][t].getIncomplete_left_eval() <= boundILE) {
						spanMtx[s][t].setIncomplete_left_eval(boundILE);
						spanMtx[s][t].setIncomplete_left_interPosition(t - 1);
						spanMtx[s][t].setThroughLeftSibling(false);
					}

					double boundIRE = spanMtx[s][s].getComplete_right_eval()
							+ spanMtx[s + 1][t].getComplete_left_eval()
							+ scoreTriMtx[s][t][0];

					if (spanMtx[s][t].getIncomplete_right_eval() < boundIRE) {
						spanMtx[s][t].setIncomplete_right_eval(boundIRE);
						spanMtx[s][t].setIncomplete_right_interPosition(s);
						spanMtx[s][t].setThroughRightSibling(false);
					}

					for (int r = s + 1; r <= t - 1; r++) {
						double tempIRE =
								spanMtx[s][r].getIncomplete_right_eval()
										+ spanMtx[r][t].getSibling_right_eval()
										+ scoreTriMtx[s][t][r - s];

						if (spanMtx[s][t].getIncomplete_right_eval()
								< tempIRE) {
							spanMtx[s][t].setIncomplete_right_eval(tempIRE);
							spanMtx[s][t].setIncomplete_right_interPosition(r);
							spanMtx[s][t].setThroughRightSibling(true);
						}
					}

					for (int r = s; r <= t - 1; r++) {

						double tempCLE = spanMtx[s][r].getComplete_left_eval()
								+ spanMtx[r][t].getIncomplete_left_eval();
						if (spanMtx[s][t].getComplete_left_eval() < tempCLE) {
							spanMtx[s][t].setComplete_left_eval(tempCLE);
							spanMtx[s][t].setComplete_left_interPosition(r);
						}

					}

					for (int r = s; r <= t - 1; r++) {

						double tempCRE =
								spanMtx[s][r + 1].getIncomplete_right_eval()
										+ spanMtx[r + 1][t].getComplete_right_eval();
						if (spanMtx[s][t].getComplete_right_eval() < tempCRE) {
							spanMtx[s][t].setComplete_right_eval(tempCRE);
							spanMtx[s][t].setComplete_right_interPosition(
									r + 1);
						}

					}

					//					std::cerr << spanMtx[s][t].getComplete_left_eval()
					//							<< std::endl;
					//					std::cerr << spanMtx[s][t].getComplete_right_eval()
					//							<< std::endl;
					//					std::cerr << spanMtx[s][t].getIncomplete_left_eval()
					//							<< std::endl;
					//					std::cerr << spanMtx[s][t].getIncomplete_right_eval()
					//							<< std::endl;
					//					std::cerr << std::endl;

				}
			}

//			std::cout << "sentence evaluation: "
//					<< spanMtx[0][length].getComplete_right_eval() << std::endl;
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getIncomplete_left_eval()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout
//							<< spanMtx[i][j].getIncomplete_left_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getIncomplete_right_eval()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout
//							<< spanMtx[i][j].getIncomplete_right_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//			//complete eval
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_left_eval() << '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_left_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_right_eval() << '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << spanMtx[i][j].getComplete_right_interPosition()
//							<< '\t';
//				}
//				std::cout << std::endl;
//			}
//
//			for (size_t i = 0; i < length + 1; i++) {
//				for (size_t j = 0; j < length + 1; j++) {
//					std::cout << scoreTriMtx[i][j][0] << '\t';
//				}
//				std::cout << std::endl;
//			}

			Dpsentence::rebuildPredictVectorNeo(spanMtx, 0, length, false, true,
					false);

			double returnValue =
					spanMtx[0][length].getComplete_right_eval()-Loose_lower_bound;

			for (size_t i = 0; i < length + 1; i++) {
				delete[] spanMtx[i];
			}

			return returnValue;
			//delete spanMtx;

			//		for (int i = 0; i < length + 1; i++) {
			//			for (int j = 0; j < length + 1; j++) {
			//				cerr << scoreMtx[i][j] << " ";
			//			}
			//			cerr << endl;
			//		}
			//  std::cerr << Dpsentence::predict[0] << std::endl;

		}
		return 0;
	}

	inline void dpsentenceClear() {
		for (size_t i = 0; i < length + 1; i++) {
			delete[] scoreMtx[i];
		}
		//delete[] scoreMtx;
	}

//	inline void dpsentenceClear2nd() {
//		for (size_t i = 0; i < length + 1; i++) {
//			for (size_t j = 0; j < length + 1; j++) {
//				delete[] scoreQb[i][j];
//			}
//			delete[] scoreQb[i];
//		}
//		//delete[] scoreMtx;
//	}

	inline void dpsentenceClear2nd() {
		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				scoreTriMtx[i][j].clear();
			}
			scoreTriMtx[i].clear();
		}
		scoreTriMtx.clear();
	}

}
;

class KbestDpsentence {
	size_t kbest;
	int order;

	unsigned int length; // length of this sentence
	double** scoreMtx;
	std::vector<std::vector<std::vector<double> > > scoreTriMtx;

	std::vector<Morph::Dptoken> dpTokenArray;

public:
	std::vector<int> headIndex;
	std::vector<std::vector<int> > predict;

	KbestDpsentence(size_t k) {
		kbest = k;
		order = 1;
	}
	;

	KbestDpsentence(size_t k, int odr) {
		kbest = k;
		order = odr;
	}
	;

	virtual ~KbestDpsentence() {
	}
	;

	size_t getK() {
		return kbest;
	}

	void setK(size_t newK) {
		kbest = newK;
	}

	int getLength() {
		return length;
	}

	void setLength(size_t t) {
		length = t;
	}

	std::vector<Morph::Dptoken> *getDpTokenArray() {
		return &dpTokenArray;
	}

	void appendDpToken(Morph::Dptoken* dp) {
		dpTokenArray.push_back(*dp);
	}

	double** getScoreMtx() {
		return scoreMtx;
	}

	//	void printPredict() {
	//		for (size_t i = 0; i < length; i++) {
	//			std::cerr << predict[i + 1] << std::endl;
	//		}
	//	}
	//
	//	void printHeadIndex() {
	//		for (size_t i = 0; i < length; i++) {
	//			std::cerr << headIndex[i] << std::endl;
	//		}
	//	}
	//
	//	void comparePrint() {
	//		std::cerr << "Predict" << '\t' << "Gold" << std::endl;
	//		for (size_t i = 0; i < length; i++) {
	//			std::cerr << predict[i + 1] << '\t' << headIndex[i] << std::endl;
	//		}
	//	}

	inline void setSurroundingPos() {
		std::string leftPOS = "BOS";
		std::string rightPOS = "EOS";
		for (std::vector<Morph::Dptoken>::iterator iterator =
				dpTokenArray.begin(); iterator != dpTokenArray.end();
				iterator++) {
			iterator->setLeftPos(leftPOS);
			leftPOS = iterator->pos;
		}
		for (std::vector<Morph::Dptoken>::reverse_iterator reverse_iterator =
				dpTokenArray.rbegin(); reverse_iterator != dpTokenArray.rend();
				reverse_iterator++) {
			reverse_iterator->setRightPos(rightPOS);
			rightPOS = reverse_iterator->pos;
		}
	}

	inline void setHeadIndex(std::vector<int>* depArray) {
		headIndex = *depArray;
		//	for (std::vector<Morph::Dptoken>::iterator iterator = dpTokenArray.begin(); iterator
		//				!= dpTokenArray.end(); iterator++) {
		//			headIndex[(*iterator).index]=(*iterator).dep;
		//		}
	}

	inline void setHeadIndexPointWise(int i, int j) {
		headIndex[i] = j;
	}

	inline std::vector<int>* getHeadIndex() {
		return &(headIndex);
	}

	inline std::vector<int>* getPredict(size_t i) {
		return &(predict[i]);
	}

	inline std::vector<std::vector<int> >* getPredict() {
		return &(predict);
	}

	inline void printPredict(int rank) {
		for (size_t i = 0; i < length; i++) {
			std::cout << predict[rank - 1][i + 1] << std::endl;
		}
	}

	inline void printHeadIndex() {
		for (size_t i = 0; i < length; i++) {
			std::cout << headIndex[i] << std::endl;
		}
	}

	inline void comparePrint(int rank) {
		std::cerr << "Predict" << '\t' << "Gold" << '\t' << "Rank:" << rank
				<< std::endl;
		double nom = 0;
		double denom = 0;
		for (size_t i = 0; i < length; i++) {
			std::cerr << predict[rank - 1][i + 1] << '\t' << headIndex[i]
					<< std::endl;
			denom++;
			if (predict[rank - 1][i + 1] == headIndex[i]) {
				nom++;
			}
		}
		double ratio = nom / denom;
		std::cerr << "No." << total_sentence_num << '\t' << nom << '\t' << denom
				<< '\t' << ratio << std::endl;
	}

	inline void checkAccuracy(size_t &nom, size_t &denom,
			std::vector<int> &pickedVec) {
		for (size_t i = 0; i < length; i++) {
			if (headIndex[i] != 0) {
				if (dpTokenArray[i].pos != "PU" && dpTokenArray[i].pos != ","
						&& dpTokenArray[i].pos != ":"
						&& dpTokenArray[i].pos != "$"
						&& dpTokenArray[i].pos != "#") {
					denom++;
					if (pickedVec[i] == headIndex[i]) {
						nom++;
					}
				}
			}
		}
	}

	inline std::vector<int> getOracleList(int topK) {
		double ratio = 0;
		double maxRatio = 0;
		std::vector<int> oracleList;

		for (int rank = 0; rank < topK; rank++) {
			int dummy = 0;
			oracleList.push_back(dummy);
			double nom = 0;
			double denom = 0;

			for (size_t i = 0; i < length; i++) {
				denom++;
				if (predict[rank][i + 1] == headIndex[i]) {
					nom++;
				}
			}
			ratio = nom / denom;
			if (maxRatio < ratio) {
				maxRatio = ratio;
			}
		}

		for (int rank = 0; rank < topK; rank++) {
			double nom = 0;
			double denom = 0;
			for (size_t i = 0; i < length; i++) {
				denom++;
				if (predict[rank][i + 1] == headIndex[i]) {
					nom++;
				}
			}
			ratio = nom / denom;
			if (ratio == maxRatio) {
				oracleList.at(rank) = 1;
			}
		}
		return oracleList;
	}

	inline std::vector<double> getRatioList(int topK) {

		double ratio = 0;
		std::vector<double> ratioList;

		for (int rank = 0; rank < topK; rank++) {
			double nom = 0;
			double denom = 0;

			for (size_t i = 0; i < length; i++) {
				denom++;
				if (predict[rank][i + 1] == headIndex[i]) {
					nom++;
				}
			}
			ratio = nom / denom;
			ratioList.push_back(ratio);
		}

		return ratioList;
	}

	double rankedAccu(int rank) {
		std::cerr << "Predict" << '\t' << "Gold" << '\t' << "Rank:" << rank
				<< std::endl;
		double nom = 0;
		double denom = 0;
		for (size_t i = 0; i < length; i++) {
			std::cerr << predict[rank - 1][i + 1] << '\t' << headIndex[i]
					<< std::endl;
			denom++;
			if (predict[rank - 1][i + 1] == headIndex[i]) {
				nom++;
			}
		}
		double ratio = nom / denom;
		return ratio;
	}

	inline void evaluateEdgeScoreMatrix(Morph::FeatureTemplateSet* in_ftmpl,
			size_t total_n) {
		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == j || i == 0 || j == 0) {
					continue;
				} else {

					//				cerr << "head modifier: "
					//						<<i<<":"<< Dpsentence::dpTokenArray.at(i - 1).position << " "
					//						<<j<<":"<< Dpsentence::dpTokenArray.at(j - 1).position << endl;
					FeatureSet *f = new FeatureSet(in_ftmpl);
					f->extract_edge_feature(
							&(KbestDpsentence::dpTokenArray.at(i - 1)),
							&(KbestDpsentence::dpTokenArray.at(j - 1)));
					f->extract_context_feature(
							&(KbestDpsentence::dpTokenArray.at(i - 1)),
							&(KbestDpsentence::dpTokenArray.at(j - 1)),
							&(KbestDpsentence::dpTokenArray));

					scoreMtx[i][j] = f->calc_inner_product_with_weight(
							feature_weight_dep);

					//					if (WEIGHT_AVERAGED) {
					//						scoreMtx[i][j] = f->calc_inner_product_with_weight(
					//								feature_weight_dep, feature_weight_sum_dep,
					//								feature_count_lastupdate, total_n);
					//					} else {
					//						scoreMtx[i][j] = f->calc_inner_product_with_weight(
					//								feature_weight_dep);
					//					}

					delete f;
				}
			}
		}
	}

	inline void evaluateSiblingScoreMatrix(Morph::FeatureTemplateSet* in_ftmpl,
			size_t total_n) {

		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == j || i == 0 || j == 0) {
					continue;
				} else {

					std::string hp = KbestDpsentence::dpTokenArray.at(i - 1).pos;

					std::string mp = KbestDpsentence::dpTokenArray.at(j - 1).pos;

					std::string mw =
							KbestDpsentence::dpTokenArray.at(j - 1).word;

					FeatureSet f(in_ftmpl);

					f.extract_edge_feature(
							&(KbestDpsentence::dpTokenArray.at(i - 1)),
							&(KbestDpsentence::dpTokenArray.at(j - 1)));

					f.extract_context_feature(
							&(KbestDpsentence::dpTokenArray.at(i - 1)),
							&(KbestDpsentence::dpTokenArray.at(j - 1)),
							&(KbestDpsentence::dpTokenArray));

					double tempEdgeScore = f.calc_inner_product_with_weight(
							feature_weight_dep);

					//					cout<<"linear score:"<<tempEdgeScore <<endl;

					f.extract_sibling_feature(
							&(KbestDpsentence::dpTokenArray.at(i - 1)),
							&(KbestDpsentence::dpTokenArray.at(j - 1)));

					scoreTriMtx[i][j][0] = f.calc_inner_product_with_weight(
							feature_weight_dep);

					//					cout<<"score "<<j<<"\t"<<i<<"\t"<<0<<"\t"<<scoreTriMtx[i][j][0] <<endl;

					if (i < j - 1) {
						for (size_t k = 1; k < j - i; k++) {

							FeatureSet s(in_ftmpl);

							s.extract_sibling_feature(
									&(KbestDpsentence::dpTokenArray.at(i - 1)),
									&(KbestDpsentence::dpTokenArray.at(j - 1)),
									&(KbestDpsentence::dpTokenArray.at(
											i + k - 1)));

							scoreTriMtx[i][j][k] = tempEdgeScore
									+ s.calc_inner_product_with_weight(
											feature_weight_dep);

							//							cout<<"score "<<j<<"\t"<<i<<"\t"<<i + k<<"\t"<<scoreTriMtx[i][j][k] <<endl;

						}

					} else if (j < i - 1) {
						for (size_t k = 1; k < i - j; k++) {

							FeatureSet s(in_ftmpl);

							s.extract_sibling_feature(
									&(KbestDpsentence::dpTokenArray.at(i - 1)),
									&(KbestDpsentence::dpTokenArray.at(j - 1)),
									&(KbestDpsentence::dpTokenArray.at(
											j + k - 1)));

							scoreTriMtx[i][j][k] = tempEdgeScore
									+ s.calc_inner_product_with_weight(
											feature_weight_dep);

							//							cout<<"score "<<j<<"\t"<<i<<"\t"<<j + k<<"\t"<<scoreTriMtx[i][j][k] <<endl;

						}
					}
				}
			}
		}
	}

	inline void initializeScoreMatrix() {
		double score[length + 1][length + 1];

		scoreMtx = (double**) new double[length + 1];

		for (size_t i = 0; i < length + 1; i++) {
			scoreMtx[i] = (double*) new double[length + 1];
		}

		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				if (i == 0 || j == 0) {
					score[i][j] = Loose_lower_bound;
				} else {
					score[i][j] = 0;
				}
				scoreMtx[i][j] = score[i][j];
			}
		}

	}

	inline void initializeScoreCube() {

		std::vector<double> initScoreVec;
		std::vector<double> initLLBVec;

		std::vector<std::vector<double> > initScoreMtx;
		std::vector<std::vector<double> > initLLBMtx;

		for (size_t k = 0; k < length; k++) {
			initScoreVec.push_back(0);
			initLLBVec.push_back(Loose_lower_bound);
		}

		initScoreMtx.push_back(initLLBVec);
		initLLBMtx.push_back(initLLBVec);

		for (size_t k = 0; k < length; k++) {
			initScoreMtx.push_back(initScoreVec);
			initLLBMtx.push_back(initLLBVec);
		}

		scoreTriMtx.push_back(initLLBMtx);

		for (size_t k = 0; k < length; k++) {
			scoreTriMtx.push_back(initScoreMtx);
		}

	}

	void rebuildPredictVector(std::vector<int>* predict,
			Morph::KbestSpan ** kbestSpan, int i, int j, int rank, bool isLeft,
			bool isComplete);

	void rebuildPredictVector2nd(std::vector<int>* predict,
			Morph::KbestSpan ** kbestSpan, int i, int j, int rank, bool isLeft,
			bool isComplete, bool isSibling);

	void rebuildPredictVectorComplete(std::vector<int>* CPpredict,
			Morph::KbestSpan ** kbestSpan, int i, int j, int rank, bool isLeft,
			bool isComplete);

	void rebuildPredictVector2ndComplete(std::vector<int>* CPpredict,
			Morph::KbestSpan ** kbestSpan, int i, int j, int rank, bool isLeft,
			bool isComplete, bool isSibling);

	void naiveRebuildPredictVector(std::vector<int>* predictVec,
			Morph::KbestSpan ** kbestSpan, int i, int j, int rank, bool isLeft,
			bool isComplete);
	std::vector<double> decodeEdgeFactors(size_t bestK);
	std::vector<double> decodeSiblingFactorsNeo(size_t bestK);

	std::vector<double> decodeEdgeFactorsComplete(
			std::vector<std::vector<int> >& completePredict, size_t bestK);
	std::vector<double> decodeSiblingFactorsNeoComplete(
			std::vector<std::vector<int> >& completePredict, size_t bestK);

	void naiveDecodeEdgeFactors(size_t bestK);

	std::vector<KbestSearchToken> getMergedKbestSpan(Morph::KbestSpan leftSpan,
			Morph::KbestSpan rightSpan, double edgeScore, int interPos,
			bool isLeft, bool isComplete);

	std::vector<KbestSearchToken> getMergedKbestSpan2nd(
			Morph::KbestSpan leftSpan, Morph::KbestSpan rightSpan,
			double edgeScore, int interPos, bool isLeft, bool isComplete,
			bool isThroughSibling);

	void dpsentenceClear() {
		for (size_t i = 0; i < length + 1; i++) {
			delete[] scoreMtx[i];
		}
		//delete[] scoreMtx;
	}

	inline void dpsentenceClear2nd() {
		for (size_t i = 0; i < length + 1; i++) {
			for (size_t j = 0; j < length + 1; j++) {
				scoreTriMtx[i][j].clear();
			}
			scoreTriMtx[i].clear();
		}
		scoreTriMtx.clear();
	}

};

}
#endif // DPSENTENCE_H
