#include "dpsentence.h"
#include "common.h"
#include "dptoken.h"
#include "feature.h"

namespace Morph {

Span::Span() {

}

Span::Span(double e1, double e2, double e3, double e4, int r1, int r2, int r3,
		int r4) {
	Span::setComplete_left_eval(e1);
	Span::setComplete_right_eval(e2);
	Span::setIncomplete_left_eval(e3);
	Span::setIncomplete_right_eval(e4);
	Span::setComplete_left_interPosition(r1);
	Span::setComplete_right_interPosition(r2);
	Span::setIncomplete_left_interPosition(r3);
	Span::setIncomplete_right_interPosition(r4);

}

Span::Span(double e1, double e2, double e3, double e4, double esl, double esr,
		int r1, int r2, int r3, int r4, int rsl, int rsr) {
	Span::setComplete_left_eval(e1);
	Span::setComplete_right_eval(e2);
	Span::setIncomplete_left_eval(e3);
	Span::setIncomplete_right_eval(e4);
	Span::setSibling_left_eval(esl);
	Span::setSibling_right_eval(esr);
	Span::setComplete_left_interPosition(r1);
	Span::setComplete_right_interPosition(r2);
	Span::setIncomplete_left_interPosition(r3);
	Span::setIncomplete_right_interPosition(r4);
	Span::setSibling_left_interPosition(rsl);
	Span::setSibling_right_interPosition(rsr);
}
Span::Span(double e1, double e2, double e3, double e4, double esl, double esr,
		int r1, int r2, int r3, int r4, int rsl, int rsr, bool lts, bool rts) {
	Span::setComplete_left_eval(e1);
	Span::setComplete_right_eval(e2);
	Span::setIncomplete_left_eval(e3);
	Span::setIncomplete_right_eval(e4);
	Span::setSibling_left_eval(esl);
	Span::setSibling_right_eval(esr);
	Span::setComplete_left_interPosition(r1);
	Span::setComplete_right_interPosition(r2);
	Span::setIncomplete_left_interPosition(r3);
	Span::setIncomplete_right_interPosition(r4);
	Span::setSibling_left_interPosition(rsl);
	Span::setSibling_right_interPosition(rsr);
	Span::setThroughLeftSibling(lts);
	Span::setThroughRightSibling(rts);
}

Span::~Span() {
	//dtor
}

Dpsentence::Dpsentence() {
	//ctor
	//	Dpsentence::setSurroundingPos(something);
	// Dpsentence::appendDpToken(something);repeat...
	// Dpsentence::setHeadIndexArray(something);

}

Dpsentence::~Dpsentence() {
	//dtor
}

void Dpsentence::rebuildPredictVector(Morph::Span** span, int i, int j,
		bool isLeft, bool isComplete) {
	if (isLeft) {
		if (isComplete) {
			if (span[i][j].getComplete_left_interPosition() >= 0) {
				rebuildPredictVector(span, i,
						span[i][j].getComplete_left_interPosition(), true,
						true);
				rebuildPredictVector(span,
						span[i][j].getComplete_left_interPosition(), j, true,
						false);
			}
		} else {
			Dpsentence::setPredictPointWise(i, j);
			//  std::cerr << Dpsentence::predict[i] << std::endl;
			if (span[i][j].getIncomplete_left_interPosition() >= 0) {
				rebuildPredictVector(span, i,
						span[i][j].getIncomplete_left_interPosition(), false,
						true);
				rebuildPredictVector(span,
						span[i][j].getIncomplete_left_interPosition() + 1, j,
						true, true);
			}
		}
	} else {
		if (isComplete) {
			if (span[i][j].getComplete_right_interPosition() >= 0) {
				rebuildPredictVector(span, i,
						span[i][j].getComplete_right_interPosition(), false,
						false);
				rebuildPredictVector(span,
						span[i][j].getComplete_right_interPosition(), j, false,
						true);
			}
		} else {
			Dpsentence::setPredictPointWise(j, i);
			//std::cerr << Dpsentence::predict[j] << std::endl;
			if (span[i][j].getIncomplete_right_interPosition() >= 0) {
				rebuildPredictVector(span, i,
						span[i][j].getIncomplete_right_interPosition(), false,
						true);
				rebuildPredictVector(span,
						span[i][j].getIncomplete_right_interPosition() + 1, j,
						true, true);
			}
		}
	}

}

void Dpsentence::rebuildPredictVectorNeo(Morph::Span** span, int i, int j,
		bool isLeft, bool isComplete, bool isSibling) {
	if (isLeft) {

		if (isSibling) {

			if (span[i][j].getSibling_left_interPosition() >= 0 && j - i > 1) {

				//std::cerr << "from:"<< j<<" to"<<i<<" interception:"<<span[i][j].getSibling_left_interPosition()<<" sibling"<<std::endl;

				rebuildPredictVectorNeo(span, i,
						span[i][j].getSibling_left_interPosition(), false, true,
						false);
				rebuildPredictVectorNeo(span,
						span[i][j].getSibling_left_interPosition() + 1, j, true,
						true, false);
			}
		} else {

			if (isComplete) {
				if (span[i][j].getComplete_left_interPosition() >= 0) {

					//std::cerr << "from:"<< j<<" to"<<i<<" interception:"<<span[i][j].getComplete_left_interPosition()<<" complete"<<std::endl;

					rebuildPredictVectorNeo(span, i,
							span[i][j].getComplete_left_interPosition(), true,
							true, false);
					rebuildPredictVectorNeo(span,
							span[i][j].getComplete_left_interPosition(), j,
							true, false, false);
				}
			} else {
				Dpsentence::setPredictPointWise(i, j);

				if (span[i][j].getThroughLeftSibling()) {
					if (span[i][j].getIncomplete_left_interPosition() >= 0) {

						//std::cerr << "from:"<< j<<" to"<<i<<" interception:"<<span[i][j].getIncomplete_left_interPosition()<<" incomplete(through sibling)"<<std::endl;

						rebuildPredictVectorNeo(span, i,
								span[i][j].getIncomplete_left_interPosition(),
								true, false, true);
						rebuildPredictVectorNeo(span,
								span[i][j].getIncomplete_left_interPosition(),
								j, true, false, false);
					}
				} else {
					//  std::cerr << Dpsentence::predict[i] << std::endl;
					if (span[i][j].getIncomplete_left_interPosition() >= 0) {

						//std::cerr << "from:"<< j<<" to"<<i<<" interception:"<<span[i][j].getIncomplete_left_interPosition()<<" incomplete"<<std::endl;

						rebuildPredictVectorNeo(span, i,
								span[i][j].getIncomplete_left_interPosition(),
								false, true, false);
						rebuildPredictVectorNeo(
								span,
								span[i][j].getIncomplete_left_interPosition()
										+ 1, j, true, true, false);
					}
				}
			}
		}
	} else {

		if (isSibling) {
			if (span[i][j].getSibling_right_interPosition() >= 0 && j - i > 1) {

				//std::cerr << "from:"<< i<<" to"<<j<<" interception:"<<span[i][j].getSibling_right_interPosition()<<" sibling"<<std::endl;

				rebuildPredictVectorNeo(span, i,
						span[i][j].getSibling_right_interPosition(), false,
						true, false);
				rebuildPredictVectorNeo(span,
						span[i][j].getSibling_right_interPosition() + 1, j,
						true, true, false);
			}
		} else {

			if (isComplete) {
				if (span[i][j].getComplete_right_interPosition() >= 0) {

					//std::cerr << "from:"<< i<<" to"<<j<<" interception:"<<span[i][j].getComplete_right_interPosition()<<" complete"<<std::endl;

					rebuildPredictVectorNeo(span, i,
							span[i][j].getComplete_right_interPosition(), false,
							false, false);
					rebuildPredictVectorNeo(span,
							span[i][j].getComplete_right_interPosition(), j,
							false, true, false);
				}
			} else {
				Dpsentence::setPredictPointWise(j, i);
				if (span[i][j].getThroughRightSibling()) {

					if (span[i][j].getIncomplete_right_interPosition() >= 0) {

						//std::cerr << "from:"<< i<<" to"<<j<<" interception:"<<span[i][j].getIncomplete_right_interPosition()<<" incomplete(through sibling)"<<std::endl;

						rebuildPredictVectorNeo(span, i,
								span[i][j].getIncomplete_right_interPosition(),
								false, false, false);
						rebuildPredictVectorNeo(span,
								span[i][j].getIncomplete_right_interPosition(),
								j, false, false, true);
					}

				} else {
					//std::cerr << Dpsentence::predict[j] << std::endl;
					if (span[i][j].getIncomplete_right_interPosition() >= 0) {

						//std::cerr << "from:"<< i<<" to"<<j<<" interception:"<<span[i][j].getIncomplete_right_interPosition()<<" incomplete"<<std::endl;

						rebuildPredictVectorNeo(span, i,
								span[i][j].getIncomplete_right_interPosition(),
								false, true, false);
						rebuildPredictVectorNeo(
								span,
								span[i][j].getIncomplete_right_interPosition()
										+ 1, j, true, true, false);
					}
				}
			}
		}
	}

}

std::vector<KbestSearchToken> KbestDpsentence::getMergedKbestSpan(
		Morph::KbestSpan leftSpan, Morph::KbestSpan rightSpan, double edgeScore,
		int interPos, bool isLeft, bool isComplete) {

	std::vector<KbestSearchToken> mergedSpan;
	std::map<std::string, double> frontier;
	std::deque<Morph::KbestSearchToken> extension;
	std::priority_queue<Morph::KbestSearchToken> sorting;

	if (!isComplete && isLeft) {

		Morph::KbestSearchToken firstToken(1, 1, interPos);

		firstToken.cValue = leftSpan.getComplete_right_component().at(
				firstToken.leftPos - 1).cValue
				+ rightSpan.getComplete_left_component().at(
						firstToken.rightPos - 1).cValue + edgeScore;

		frontier[firstToken.cKey] = firstToken.cValue;

		sorting.push(firstToken);
		//attention
		while (mergedSpan.capacity() < KbestDpsentence::getK()) {
			if (!extension.empty()) {
				extension.clear();
				break;
			} else if (sorting.empty()) {
				break;
			}

			double maxCvalue = sorting.top().cValue;
			extension.push_back(sorting.top());
			sorting.pop();

			while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
				extension.push_back(sorting.top());
				sorting.pop();
			}

			while (!extension.empty()) {

				std::map<std::string, double>::const_iterator result;
				Morph::KbestSearchToken tempKSToken;
				tempKSToken = extension.front();
				extension.pop_front();
				mergedSpan.push_back(tempKSToken);
				frontier.erase(tempKSToken.cKey);

				if (tempKSToken.leftPos
						< leftSpan.getComplete_right_component().capacity()
						|| tempKSToken.rightPos
								< rightSpan.getComplete_left_component().capacity()) {

					if (tempKSToken.leftPos
							< leftSpan.getComplete_right_component().capacity()) {
						Morph::KbestSearchToken upperKSToken(
								tempKSToken.leftPos + 1, tempKSToken.rightPos,
								interPos);

						upperKSToken.cValue =
								leftSpan.getComplete_right_component().at(
										upperKSToken.leftPos - 1).cValue
										+ rightSpan.getComplete_left_component().at(
												upperKSToken.rightPos - 1).cValue
										+ edgeScore;

						result = frontier.find(upperKSToken.cKey);
						if (result == frontier.end()) {
							frontier[upperKSToken.cKey] = upperKSToken.cValue;
							sorting.push(upperKSToken);
						}
					}

					if (tempKSToken.rightPos
							< rightSpan.getComplete_left_component().capacity()) {
						Morph::KbestSearchToken rightKSToken(
								tempKSToken.leftPos, tempKSToken.rightPos + 1,
								interPos);

						rightKSToken.cValue =
								leftSpan.getComplete_right_component().at(
										rightKSToken.leftPos - 1).cValue
										+ rightSpan.getComplete_left_component().at(
												rightKSToken.rightPos - 1).cValue
										+ edgeScore;

						result = frontier.find(rightKSToken.cKey);
						if (result == frontier.end()) {
							frontier[rightKSToken.cKey] = rightKSToken.cValue;
							sorting.push(rightKSToken);
						}
					}

				} else {
					break;
				}
			}
		}
	}

	if (!isComplete && !isLeft) {

		Morph::KbestSearchToken firstToken(1, 1, interPos);

		firstToken.cValue = leftSpan.getComplete_right_component().at(
				firstToken.leftPos - 1).cValue
				+ rightSpan.getComplete_left_component().at(
						firstToken.rightPos - 1).cValue + edgeScore;

		frontier[firstToken.cKey] = firstToken.cValue;

		sorting.push(firstToken);
		//attention
		while (mergedSpan.capacity() < KbestDpsentence::getK()) {
			if (!extension.empty()) {
				extension.clear();
				break;
			} else if (sorting.empty()) {
				break;
			}

			double maxCvalue = sorting.top().cValue;
			extension.push_back(sorting.top());
			sorting.pop();

			while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
				extension.push_back(sorting.top());
				sorting.pop();
			}

			while (!extension.empty()) {

				std::map<std::string, double>::const_iterator result;
				Morph::KbestSearchToken tempKSToken;
				tempKSToken = extension.front();
				extension.pop_front();
				mergedSpan.push_back(tempKSToken);
				frontier.erase(tempKSToken.cKey);

				if (tempKSToken.leftPos
						< leftSpan.getComplete_right_component().capacity()
						|| tempKSToken.rightPos
								< rightSpan.getComplete_left_component().capacity()) {

					if (tempKSToken.leftPos
							< leftSpan.getComplete_right_component().capacity()) {
						Morph::KbestSearchToken upperKSToken(
								tempKSToken.leftPos + 1, tempKSToken.rightPos,
								interPos);

						upperKSToken.cValue =
								leftSpan.getComplete_right_component().at(
										upperKSToken.leftPos - 1).cValue
										+ rightSpan.getComplete_left_component().at(
												upperKSToken.rightPos - 1).cValue
										+ edgeScore;

						result = frontier.find(upperKSToken.cKey);
						if (result == frontier.end()) {
							frontier[upperKSToken.cKey] = upperKSToken.cValue;
							sorting.push(upperKSToken);
						}
					}

					if (tempKSToken.rightPos
							< rightSpan.getComplete_left_component().capacity()) {
						Morph::KbestSearchToken rightKSToken(
								tempKSToken.leftPos, tempKSToken.rightPos + 1,
								interPos);

						rightKSToken.cValue =
								leftSpan.getComplete_right_component().at(
										rightKSToken.leftPos - 1).cValue
										+ rightSpan.getComplete_left_component().at(
												rightKSToken.rightPos - 1).cValue
										+ edgeScore;

						result = frontier.find(rightKSToken.cKey);
						if (result == frontier.end()) {
							frontier[rightKSToken.cKey] = rightKSToken.cValue;
							sorting.push(rightKSToken);
						}
					}

				} else {
					break;
				}
			}
		}
	}

	if (isComplete && isLeft) {

		Morph::KbestSearchToken firstToken(1, 1, interPos);

		firstToken.cValue = leftSpan.getComplete_left_component().at(
				firstToken.leftPos - 1).cValue
				+ rightSpan.getIncomplete_left_component().at(
						firstToken.rightPos - 1).cValue;

		frontier[firstToken.cKey] = firstToken.cValue;

		sorting.push(firstToken);
		//attention
		while (mergedSpan.capacity() < KbestDpsentence::getK()) {
			if (!extension.empty()) {
				extension.clear();
				break;
			} else if (sorting.empty()) {
				break;
			}

			double maxCvalue = sorting.top().cValue;
			extension.push_back(sorting.top());
			sorting.pop();

			while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
				extension.push_back(sorting.top());
				sorting.pop();
			}

			while (!extension.empty()) {

				std::map<std::string, double>::const_iterator result;
				Morph::KbestSearchToken tempKSToken;
				tempKSToken = extension.front();
				extension.pop_front();
				mergedSpan.push_back(tempKSToken);
				frontier.erase(tempKSToken.cKey);

				if (tempKSToken.leftPos
						< leftSpan.getComplete_left_component().capacity()
						|| tempKSToken.rightPos
								< rightSpan.getIncomplete_left_component().capacity()) {

					if (tempKSToken.leftPos
							< leftSpan.getComplete_left_component().capacity()) {
						Morph::KbestSearchToken upperKSToken(
								tempKSToken.leftPos + 1, tempKSToken.rightPos,
								interPos);

						upperKSToken.cValue =
								leftSpan.getComplete_left_component().at(
										upperKSToken.leftPos - 1).cValue
										+ rightSpan.getIncomplete_left_component().at(
												upperKSToken.rightPos - 1).cValue;

						result = frontier.find(upperKSToken.cKey);
						if (result == frontier.end()) {
							frontier[upperKSToken.cKey] = upperKSToken.cValue;
							sorting.push(upperKSToken);
						}
					}

					if (tempKSToken.rightPos
							< rightSpan.getIncomplete_left_component().capacity()) {
						Morph::KbestSearchToken rightKSToken(
								tempKSToken.leftPos, tempKSToken.rightPos + 1,
								interPos);

						rightKSToken.cValue =
								leftSpan.getComplete_left_component().at(
										rightKSToken.leftPos - 1).cValue
										+ rightSpan.getIncomplete_left_component().at(
												rightKSToken.rightPos - 1).cValue;

						result = frontier.find(rightKSToken.cKey);
						if (result == frontier.end()) {
							frontier[rightKSToken.cKey] = rightKSToken.cValue;
							sorting.push(rightKSToken);
						}
					}

				} else {
					break;
				}
			}
		}
	}

	if (isComplete && !isLeft) {

		Morph::KbestSearchToken firstToken(1, 1, interPos);

		firstToken.cValue = leftSpan.getIncomplete_right_component().at(
				firstToken.leftPos - 1).cValue
				+ rightSpan.getComplete_right_component().at(
						firstToken.rightPos - 1).cValue;

		frontier[firstToken.cKey] = firstToken.cValue;

		sorting.push(firstToken);
		//attention
		while (mergedSpan.capacity() < KbestDpsentence::getK()) {
			if (!extension.empty()) {
				extension.clear();
				break;
			} else if (sorting.empty()) {
				break;
			}

			double maxCvalue = sorting.top().cValue;
			extension.push_back(sorting.top());
			sorting.pop();

			while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
				extension.push_back(sorting.top());
				sorting.pop();
			}

			while (!extension.empty()) {

				std::map<std::string, double>::const_iterator result;
				Morph::KbestSearchToken tempKSToken;
				tempKSToken = extension.front();
				extension.pop_front();
				mergedSpan.push_back(tempKSToken);
				frontier.erase(tempKSToken.cKey);

				if (tempKSToken.leftPos
						< leftSpan.getIncomplete_right_component().capacity()
						|| tempKSToken.rightPos
								< rightSpan.getComplete_right_component().capacity()) {

					if (tempKSToken.leftPos
							< leftSpan.getIncomplete_right_component().capacity()) {
						Morph::KbestSearchToken upperKSToken(
								tempKSToken.leftPos + 1, tempKSToken.rightPos,
								interPos);

						upperKSToken.cValue =
								leftSpan.getIncomplete_right_component().at(
										upperKSToken.leftPos - 1).cValue
										+ rightSpan.getComplete_right_component().at(
												upperKSToken.rightPos - 1).cValue;

						result = frontier.find(upperKSToken.cKey);
						if (result == frontier.end()) {
							frontier[upperKSToken.cKey] = upperKSToken.cValue;
							sorting.push(upperKSToken);
						}
					}

					if (tempKSToken.rightPos
							< rightSpan.getComplete_right_component().capacity()) {
						Morph::KbestSearchToken rightKSToken(
								tempKSToken.leftPos, tempKSToken.rightPos + 1,
								interPos);

						rightKSToken.cValue =
								leftSpan.getIncomplete_right_component().at(
										rightKSToken.leftPos - 1).cValue
										+ rightSpan.getComplete_right_component().at(
												rightKSToken.rightPos - 1).cValue;

						result = frontier.find(rightKSToken.cKey);
						if (result == frontier.end()) {
							frontier[rightKSToken.cKey] = rightKSToken.cValue;
							sorting.push(rightKSToken);
						}
					}

				} else {
					break;
				}
			}
		}
	}

	return mergedSpan;
}

std::vector<KbestSearchToken> KbestDpsentence::getMergedKbestSpan2nd(
		Morph::KbestSpan leftSpan, Morph::KbestSpan rightSpan, double edgeScore,
		int interPos, bool isLeft, bool isComplete, bool isThroughSibling) {

	std::vector<KbestSearchToken> mergedSpan;
	std::map<std::string, double> frontier;
	std::deque<Morph::KbestSearchToken> extension;
	std::priority_queue<Morph::KbestSearchToken> sorting;

	if (!isComplete && isLeft) {
		if (isThroughSibling) {
			Morph::KbestSearchToken firstToken(1, 1, interPos);

			firstToken.cValue = leftSpan.getSibling_left_component().at(
					firstToken.leftPos - 1).cValue
					+ rightSpan.getIncomplete_left_component().at(
							firstToken.rightPos - 1).cValue + edgeScore;

			frontier[firstToken.cKey] = firstToken.cValue;

			sorting.push(firstToken);
			//attention
			while (mergedSpan.capacity() < KbestDpsentence::getK()) {
				if (!extension.empty()) {
					extension.clear();
					break;
				} else if (sorting.empty()) {
					break;
				}

				double maxCvalue = sorting.top().cValue;
				extension.push_back(sorting.top());
				sorting.pop();

				while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
					extension.push_back(sorting.top());
					sorting.pop();
				}

				while (!extension.empty()) {

					std::map<std::string, double>::const_iterator result;
					Morph::KbestSearchToken tempKSToken;
					tempKSToken = extension.front();
					extension.pop_front();

					tempKSToken.isThroughSibling = true;

					mergedSpan.push_back(tempKSToken);
					frontier.erase(tempKSToken.cKey);

					if (tempKSToken.leftPos
							< leftSpan.getSibling_left_component().capacity()
							|| tempKSToken.rightPos
									< rightSpan.getIncomplete_left_component().capacity()) {

						if (tempKSToken.leftPos
								< leftSpan.getSibling_left_component().capacity()) {
							Morph::KbestSearchToken upperKSToken(
									tempKSToken.leftPos + 1,
									tempKSToken.rightPos, interPos);

							upperKSToken.cValue =
									leftSpan.getSibling_left_component().at(
											upperKSToken.leftPos - 1).cValue
											+ rightSpan.getIncomplete_left_component().at(
													upperKSToken.rightPos - 1).cValue
											+ edgeScore;

							result = frontier.find(upperKSToken.cKey);
							if (result == frontier.end()) {
								frontier[upperKSToken.cKey] =
										upperKSToken.cValue;
								sorting.push(upperKSToken);
							}
						}

						if (tempKSToken.rightPos
								< rightSpan.getIncomplete_left_component().capacity()) {
							Morph::KbestSearchToken rightKSToken(
									tempKSToken.leftPos,
									tempKSToken.rightPos + 1, interPos);

							rightKSToken.cValue =
									leftSpan.getSibling_left_component().at(
											rightKSToken.leftPos - 1).cValue
											+ rightSpan.getIncomplete_left_component().at(
													rightKSToken.rightPos - 1).cValue
											+ edgeScore;

							result = frontier.find(rightKSToken.cKey);
							if (result == frontier.end()) {
								frontier[rightKSToken.cKey] =
										rightKSToken.cValue;
								sorting.push(rightKSToken);
							}
						}

					} else {
						break;
					}
				}
			}
		} else {
			Morph::KbestSearchToken firstToken(1, 1, interPos);

			firstToken.cValue = leftSpan.getComplete_right_component().at(
					firstToken.leftPos - 1).cValue
					+ rightSpan.getComplete_left_component().at(
							firstToken.rightPos - 1).cValue + edgeScore;

			frontier[firstToken.cKey] = firstToken.cValue;

			sorting.push(firstToken);
			//attention
			while (mergedSpan.capacity() < KbestDpsentence::getK()) {
				if (!extension.empty()) {
					extension.clear();
					break;
				} else if (sorting.empty()) {
					break;
				}

				double maxCvalue = sorting.top().cValue;
				extension.push_back(sorting.top());
				sorting.pop();

				while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
					extension.push_back(sorting.top());
					sorting.pop();
				}

				while (!extension.empty()) {

					std::map<std::string, double>::const_iterator result;
					Morph::KbestSearchToken tempKSToken;
					tempKSToken = extension.front();
					extension.pop_front();
					mergedSpan.push_back(tempKSToken);
					frontier.erase(tempKSToken.cKey);

					if (tempKSToken.leftPos
							< leftSpan.getComplete_right_component().capacity()
							|| tempKSToken.rightPos
									< rightSpan.getComplete_left_component().capacity()) {

						if (tempKSToken.leftPos
								< leftSpan.getComplete_right_component().capacity()) {
							Morph::KbestSearchToken upperKSToken(
									tempKSToken.leftPos + 1,
									tempKSToken.rightPos, interPos);

							upperKSToken.cValue =
									leftSpan.getComplete_right_component().at(
											upperKSToken.leftPos - 1).cValue
											+ rightSpan.getComplete_left_component().at(
													upperKSToken.rightPos - 1).cValue
											+ edgeScore;

							result = frontier.find(upperKSToken.cKey);
							if (result == frontier.end()) {
								frontier[upperKSToken.cKey] =
										upperKSToken.cValue;
								sorting.push(upperKSToken);
							}
						}

						if (tempKSToken.rightPos
								< rightSpan.getComplete_left_component().capacity()) {
							Morph::KbestSearchToken rightKSToken(
									tempKSToken.leftPos,
									tempKSToken.rightPos + 1, interPos);

							rightKSToken.cValue =
									leftSpan.getComplete_right_component().at(
											rightKSToken.leftPos - 1).cValue
											+ rightSpan.getComplete_left_component().at(
													rightKSToken.rightPos - 1).cValue
											+ edgeScore;

							result = frontier.find(rightKSToken.cKey);
							if (result == frontier.end()) {
								frontier[rightKSToken.cKey] =
										rightKSToken.cValue;
								sorting.push(rightKSToken);
							}
						}

					} else {
						break;
					}
				}
			}
		}
	}

	if (!isComplete && !isLeft) {

		if (isThroughSibling) {
			Morph::KbestSearchToken firstToken(1, 1, interPos);

			firstToken.cValue = leftSpan.getIncomplete_right_component().at(
					firstToken.leftPos - 1).cValue
					+ rightSpan.getSibling_right_component().at(
							firstToken.rightPos - 1).cValue + edgeScore;

			frontier[firstToken.cKey] = firstToken.cValue;

			sorting.push(firstToken);
			//attention
			while (mergedSpan.capacity() < KbestDpsentence::getK()) {
				if (!extension.empty()) {
					extension.clear();
					break;
				} else if (sorting.empty()) {
					break;
				}

				double maxCvalue = sorting.top().cValue;
				extension.push_back(sorting.top());
				sorting.pop();

				while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
					extension.push_back(sorting.top());
					sorting.pop();
				}

				while (!extension.empty()) {

					std::map<std::string, double>::const_iterator result;
					Morph::KbestSearchToken tempKSToken;
					tempKSToken = extension.front();
					extension.pop_front();

					tempKSToken.isThroughSibling = true;

					mergedSpan.push_back(tempKSToken);
					frontier.erase(tempKSToken.cKey);

					if (tempKSToken.leftPos
							< leftSpan.getIncomplete_right_component().capacity()
							|| tempKSToken.rightPos
									< rightSpan.getSibling_right_component().capacity()) {

						if (tempKSToken.leftPos
								< leftSpan.getIncomplete_right_component().capacity()) {
							Morph::KbestSearchToken upperKSToken(
									tempKSToken.leftPos + 1,
									tempKSToken.rightPos, interPos);

							upperKSToken.cValue =
									leftSpan.getIncomplete_right_component().at(
											upperKSToken.leftPos - 1).cValue
											+ rightSpan.getSibling_right_component().at(
													upperKSToken.rightPos - 1).cValue
											+ edgeScore;

							result = frontier.find(upperKSToken.cKey);
							if (result == frontier.end()) {
								frontier[upperKSToken.cKey] =
										upperKSToken.cValue;
								sorting.push(upperKSToken);
							}
						}

						if (tempKSToken.rightPos
								< rightSpan.getSibling_right_component().capacity()) {
							Morph::KbestSearchToken rightKSToken(
									tempKSToken.leftPos,
									tempKSToken.rightPos + 1, interPos);

							rightKSToken.cValue =
									leftSpan.getIncomplete_right_component().at(
											rightKSToken.leftPos - 1).cValue
											+ rightSpan.getSibling_right_component().at(
													rightKSToken.rightPos - 1).cValue
											+ edgeScore;

							result = frontier.find(rightKSToken.cKey);
							if (result == frontier.end()) {
								frontier[rightKSToken.cKey] =
										rightKSToken.cValue;
								sorting.push(rightKSToken);
							}
						}

					} else {
						break;
					}
				}
			}
		} else {
			Morph::KbestSearchToken firstToken(1, 1, interPos);

			firstToken.cValue = leftSpan.getComplete_right_component().at(
					firstToken.leftPos - 1).cValue
					+ rightSpan.getComplete_left_component().at(
							firstToken.rightPos - 1).cValue + edgeScore;

			frontier[firstToken.cKey] = firstToken.cValue;

			sorting.push(firstToken);
			//attention
			while (mergedSpan.capacity() < KbestDpsentence::getK()) {
				if (!extension.empty()) {
					extension.clear();
					break;
				} else if (sorting.empty()) {
					break;
				}

				double maxCvalue = sorting.top().cValue;
				extension.push_back(sorting.top());
				sorting.pop();

				while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
					extension.push_back(sorting.top());
					sorting.pop();
				}

				while (!extension.empty()) {

					std::map<std::string, double>::const_iterator result;
					Morph::KbestSearchToken tempKSToken;
					tempKSToken = extension.front();
					extension.pop_front();
					mergedSpan.push_back(tempKSToken);
					frontier.erase(tempKSToken.cKey);

					if (tempKSToken.leftPos
							< leftSpan.getComplete_right_component().capacity()
							|| tempKSToken.rightPos
									< rightSpan.getComplete_left_component().capacity()) {

						if (tempKSToken.leftPos
								< leftSpan.getComplete_right_component().capacity()) {
							Morph::KbestSearchToken upperKSToken(
									tempKSToken.leftPos + 1,
									tempKSToken.rightPos, interPos);

							upperKSToken.cValue =
									leftSpan.getComplete_right_component().at(
											upperKSToken.leftPos - 1).cValue
											+ rightSpan.getComplete_left_component().at(
													upperKSToken.rightPos - 1).cValue
											+ edgeScore;

							result = frontier.find(upperKSToken.cKey);
							if (result == frontier.end()) {
								frontier[upperKSToken.cKey] =
										upperKSToken.cValue;
								sorting.push(upperKSToken);
							}
						}

						if (tempKSToken.rightPos
								< rightSpan.getComplete_left_component().capacity()) {
							Morph::KbestSearchToken rightKSToken(
									tempKSToken.leftPos,
									tempKSToken.rightPos + 1, interPos);

							rightKSToken.cValue =
									leftSpan.getComplete_right_component().at(
											rightKSToken.leftPos - 1).cValue
											+ rightSpan.getComplete_left_component().at(
													rightKSToken.rightPos - 1).cValue
											+ edgeScore;

							result = frontier.find(rightKSToken.cKey);
							if (result == frontier.end()) {
								frontier[rightKSToken.cKey] =
										rightKSToken.cValue;
								sorting.push(rightKSToken);
							}
						}

					} else {
						break;
					}
				}
			}
		}
	}

	if (isComplete && isLeft) {

		Morph::KbestSearchToken firstToken(1, 1, interPos);

		firstToken.cValue = leftSpan.getComplete_left_component().at(
				firstToken.leftPos - 1).cValue
				+ rightSpan.getIncomplete_left_component().at(
						firstToken.rightPos - 1).cValue;

		frontier[firstToken.cKey] = firstToken.cValue;

		sorting.push(firstToken);
		//attention
		while (mergedSpan.capacity() < KbestDpsentence::getK()) {
			if (!extension.empty()) {
				extension.clear();
				break;
			} else if (sorting.empty()) {
				break;
			}

			double maxCvalue = sorting.top().cValue;
			extension.push_back(sorting.top());
			sorting.pop();

			while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
				extension.push_back(sorting.top());
				sorting.pop();
			}

			while (!extension.empty()) {

				std::map<std::string, double>::const_iterator result;
				Morph::KbestSearchToken tempKSToken;
				tempKSToken = extension.front();
				extension.pop_front();
				mergedSpan.push_back(tempKSToken);
				frontier.erase(tempKSToken.cKey);

				if (tempKSToken.leftPos
						< leftSpan.getComplete_left_component().capacity()
						|| tempKSToken.rightPos
								< rightSpan.getIncomplete_left_component().capacity()) {

					if (tempKSToken.leftPos
							< leftSpan.getComplete_left_component().capacity()) {
						Morph::KbestSearchToken upperKSToken(
								tempKSToken.leftPos + 1, tempKSToken.rightPos,
								interPos);

						upperKSToken.cValue =
								leftSpan.getComplete_left_component().at(
										upperKSToken.leftPos - 1).cValue
										+ rightSpan.getIncomplete_left_component().at(
												upperKSToken.rightPos - 1).cValue;

						result = frontier.find(upperKSToken.cKey);
						if (result == frontier.end()) {
							frontier[upperKSToken.cKey] = upperKSToken.cValue;
							sorting.push(upperKSToken);
						}
					}

					if (tempKSToken.rightPos
							< rightSpan.getIncomplete_left_component().capacity()) {
						Morph::KbestSearchToken rightKSToken(
								tempKSToken.leftPos, tempKSToken.rightPos + 1,
								interPos);

						rightKSToken.cValue =
								leftSpan.getComplete_left_component().at(
										rightKSToken.leftPos - 1).cValue
										+ rightSpan.getIncomplete_left_component().at(
												rightKSToken.rightPos - 1).cValue;

						result = frontier.find(rightKSToken.cKey);
						if (result == frontier.end()) {
							frontier[rightKSToken.cKey] = rightKSToken.cValue;
							sorting.push(rightKSToken);
						}
					}

				} else {
					break;
				}
			}
		}
	}

	if (isComplete && !isLeft) {

		Morph::KbestSearchToken firstToken(1, 1, interPos);

		firstToken.cValue = leftSpan.getIncomplete_right_component().at(
				firstToken.leftPos - 1).cValue
				+ rightSpan.getComplete_right_component().at(
						firstToken.rightPos - 1).cValue;

		frontier[firstToken.cKey] = firstToken.cValue;

		sorting.push(firstToken);
		//attention
		while (mergedSpan.capacity() < KbestDpsentence::getK()) {
			if (!extension.empty()) {
				extension.clear();
				break;
			} else if (sorting.empty()) {
				break;
			}

			double maxCvalue = sorting.top().cValue;
			extension.push_back(sorting.top());
			sorting.pop();

			while (!sorting.empty() && sorting.top().cValue == maxCvalue) {
				extension.push_back(sorting.top());
				sorting.pop();
			}

			while (!extension.empty()) {

				std::map<std::string, double>::const_iterator result;
				Morph::KbestSearchToken tempKSToken;
				tempKSToken = extension.front();
				extension.pop_front();
				mergedSpan.push_back(tempKSToken);
				frontier.erase(tempKSToken.cKey);

				if (tempKSToken.leftPos
						< leftSpan.getIncomplete_right_component().capacity()
						|| tempKSToken.rightPos
								< rightSpan.getComplete_right_component().capacity()) {

					if (tempKSToken.leftPos
							< leftSpan.getIncomplete_right_component().capacity()) {
						Morph::KbestSearchToken upperKSToken(
								tempKSToken.leftPos + 1, tempKSToken.rightPos,
								interPos);

						upperKSToken.cValue =
								leftSpan.getIncomplete_right_component().at(
										upperKSToken.leftPos - 1).cValue
										+ rightSpan.getComplete_right_component().at(
												upperKSToken.rightPos - 1).cValue;

						result = frontier.find(upperKSToken.cKey);
						if (result == frontier.end()) {
							frontier[upperKSToken.cKey] = upperKSToken.cValue;
							sorting.push(upperKSToken);
						}
					}

					if (tempKSToken.rightPos
							< rightSpan.getComplete_right_component().capacity()) {
						Morph::KbestSearchToken rightKSToken(
								tempKSToken.leftPos, tempKSToken.rightPos + 1,
								interPos);

						rightKSToken.cValue =
								leftSpan.getIncomplete_right_component().at(
										rightKSToken.leftPos - 1).cValue
										+ rightSpan.getComplete_right_component().at(
												rightKSToken.rightPos - 1).cValue;

						result = frontier.find(rightKSToken.cKey);
						if (result == frontier.end()) {
							frontier[rightKSToken.cKey] = rightKSToken.cValue;
							sorting.push(rightKSToken);
						}
					}

				} else {
					break;
				}
			}
		}
	}

	return mergedSpan;
}

void KbestDpsentence::rebuildPredictVector(std::vector<int>* predictVec,
		Morph::KbestSpan ** kbestSpan, int i, int j, int rank, bool isLeft,
		bool isComplete) {
	if (isLeft) {
		if (isComplete) {
			if (kbestSpan[i][j].complete_left_component[rank - 1].interPosition
					>= 0) {
				rebuildPredictVector(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
						kbestSpan[i][j].complete_left_component[rank - 1].leftPos,
						true, true);
				rebuildPredictVector(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
						j,
						kbestSpan[i][j].complete_left_component[rank - 1].rightPos,
						true, false);
			}
		} else {
			(*predictVec)[i] = j;
			//  std::cerr << Dpsentence::predict[i] << std::endl;
			if (kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
					>= 0) {
				rebuildPredictVector(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition,
						kbestSpan[i][j].incomplete_left_component[rank - 1].leftPos,
						false, true);
				rebuildPredictVector(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
								+ 1,
						j,
						kbestSpan[i][j].incomplete_left_component[rank - 1].rightPos,
						true, true);
			}
		}
	} else {
		if (isComplete) {
			if (kbestSpan[i][j].complete_right_component[rank - 1].interPosition
					>= 0) {
				rebuildPredictVector(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
						kbestSpan[i][j].complete_right_component[rank - 1].leftPos,
						false, false);
				rebuildPredictVector(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
						j,
						kbestSpan[i][j].complete_right_component[rank - 1].rightPos,
						false, true);
			}
		} else {
			(*predictVec)[j] = i;
			//std::cerr << Dpsentence::predict[j] << std::endl;
			if (kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
					>= 0) {
				rebuildPredictVector(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition,
						kbestSpan[i][j].incomplete_right_component[rank - 1].leftPos,
						false, true);
				rebuildPredictVector(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
								+ 1,
						j,
						kbestSpan[i][j].incomplete_right_component[rank - 1].rightPos,
						true, true);
			}
		}
	}
}

void KbestDpsentence::rebuildPredictVector2nd(std::vector<int>* predictVec,
		Morph::KbestSpan ** kbestSpan, int i, int j, int rank, bool isLeft,
		bool isComplete, bool isSibling) {
	if (isLeft) {

		if (isSibling) {

			if (kbestSpan[i][j].sibling_left_component[rank - 1].interPosition
					>= 0 && j - i > 1) {

				//std::cerr << "from:"<< j<<" to"<<i<<" interception:"<<span[i][j].getSibling_left_interPosition()<<" sibling"<<std::endl;

				rebuildPredictVector2nd(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].sibling_left_component[rank - 1].interPosition,
						kbestSpan[i][j].sibling_left_component[rank - 1].leftPos,
						false, true, false);
				rebuildPredictVector2nd(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].sibling_left_component[rank - 1].interPosition
								+ 1,
						j,
						kbestSpan[i][j].sibling_left_component[rank - 1].rightPos,
						true, true, false);
			}

		} else {

			if (isComplete) {
				if (kbestSpan[i][j].complete_left_component[rank - 1].interPosition
						>= 0) {
					rebuildPredictVector2nd(
							predictVec,
							kbestSpan,
							i,
							kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
							kbestSpan[i][j].complete_left_component[rank - 1].leftPos,
							true, true, false);
					rebuildPredictVector2nd(
							predictVec,
							kbestSpan,
							kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
							j,
							kbestSpan[i][j].complete_left_component[rank - 1].rightPos,
							true, false, false);
				}
			} else {
				(*predictVec)[i] = j;
				//  std::cerr << Dpsentence::predict[i] << std::endl;

				if (kbestSpan[i][j].incomplete_left_component[rank - 1].isThroughSibling) {

					if (kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
							>= 0) {

						rebuildPredictVector2nd(
								predictVec,
								kbestSpan,
								i,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].interPosition,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].leftPos, true, false, true);
						rebuildPredictVector2nd(
								predictVec,
								kbestSpan,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].interPosition,
								j,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].rightPos, true, false, false);
					}

				} else {

					if (kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
							>= 0) {
						rebuildPredictVector2nd(
								predictVec,
								kbestSpan,
								i,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].interPosition,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].leftPos, false, true, false);
						rebuildPredictVector2nd(
								predictVec,
								kbestSpan,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].interPosition + 1,
								j,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].rightPos, true, true, false);
					}

				}

			}

		}

	} else {

		if (isSibling) {

			if (kbestSpan[i][j].sibling_right_component[rank - 1].interPosition
					>= 0 && j - i > 1) {

				//std::cerr << "from:"<< j<<" to"<<i<<" interception:"<<span[i][j].getSibling_left_interPosition()<<" sibling"<<std::endl;

				rebuildPredictVector2nd(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].sibling_right_component[rank - 1].interPosition,
						kbestSpan[i][j].sibling_right_component[rank - 1].leftPos,
						false, true, false);
				rebuildPredictVector2nd(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].sibling_right_component[rank - 1].interPosition
								+ 1,
						j,
						kbestSpan[i][j].sibling_right_component[rank - 1].rightPos,
						true, true, false);
			}

		} else {
			if (isComplete) {
				if (kbestSpan[i][j].complete_right_component[rank - 1].interPosition
						>= 0) {
					rebuildPredictVector2nd(
							predictVec,
							kbestSpan,
							i,
							kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
							kbestSpan[i][j].complete_right_component[rank - 1].leftPos,
							false, false, false);
					rebuildPredictVector2nd(
							predictVec,
							kbestSpan,
							kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
							j,
							kbestSpan[i][j].complete_right_component[rank - 1].rightPos,
							false, true, false);
				}
			} else {
				(*predictVec)[j] = i;
				//std::cerr << Dpsentence::predict[j] << std::endl;

				if (kbestSpan[i][j].incomplete_right_component[rank - 1].isThroughSibling) {

					if (kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
							>= 0) {
						rebuildPredictVector2nd(
								predictVec,
								kbestSpan,
								i,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].interPosition,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].leftPos, false, false, false);
						rebuildPredictVector2nd(
								predictVec,
								kbestSpan,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].interPosition,
								j,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].rightPos, false, false, true);
					}

				} else {

					if (kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
							>= 0) {
						rebuildPredictVector2nd(
								predictVec,
								kbestSpan,
								i,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].interPosition,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].leftPos, false, true, false);
						rebuildPredictVector2nd(
								predictVec,
								kbestSpan,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].interPosition + 1,
								j,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].rightPos, true, true, false);
					}

				}

			}
		}

	}
}

void KbestDpsentence::rebuildPredictVectorComplete(
		std::vector<int>* CPpredictVec, Morph::KbestSpan ** kbestSpan, int i,
		int j, int rank, bool isLeft, bool isComplete) {
	if (isLeft) {
		if (isComplete) {
			if (kbestSpan[i][j].complete_left_component[rank - 1].interPosition
					>= 0) {
				rebuildPredictVectorComplete(
						CPpredictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
						kbestSpan[i][j].complete_left_component[rank - 1].leftPos,
						true, true);
				rebuildPredictVectorComplete(
						CPpredictVec,
						kbestSpan,
						kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
						j,
						kbestSpan[i][j].complete_left_component[rank - 1].rightPos,
						true, false);
			}
		} else {
			(*CPpredictVec)[i - 1] = j;
			//  std::cerr << Dpsentence::predict[i] << std::endl;
			if (kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
					>= 0) {
				rebuildPredictVectorComplete(
						CPpredictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition,
						kbestSpan[i][j].incomplete_left_component[rank - 1].leftPos,
						false, true);
				rebuildPredictVectorComplete(
						CPpredictVec,
						kbestSpan,
						kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
								+ 1,
						j,
						kbestSpan[i][j].incomplete_left_component[rank - 1].rightPos,
						true, true);
			}
		}
	} else {
		if (isComplete) {
			if (kbestSpan[i][j].complete_right_component[rank - 1].interPosition
					>= 0) {
				rebuildPredictVectorComplete(
						CPpredictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
						kbestSpan[i][j].complete_right_component[rank - 1].leftPos,
						false, false);
				rebuildPredictVectorComplete(
						CPpredictVec,
						kbestSpan,
						kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
						j,
						kbestSpan[i][j].complete_right_component[rank - 1].rightPos,
						false, true);
			}
		} else {
			(*CPpredictVec)[j - 1] = i;
			//std::cerr << Dpsentence::predict[j] << std::endl;
			if (kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
					>= 0) {
				rebuildPredictVectorComplete(
						CPpredictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition,
						kbestSpan[i][j].incomplete_right_component[rank - 1].leftPos,
						false, true);
				rebuildPredictVectorComplete(
						CPpredictVec,
						kbestSpan,
						kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
								+ 1,
						j,
						kbestSpan[i][j].incomplete_right_component[rank - 1].rightPos,
						true, true);
			}
		}
	}
}

void KbestDpsentence::rebuildPredictVector2ndComplete(
		std::vector<int>* CPpredictVec, Morph::KbestSpan ** kbestSpan, int i,
		int j, int rank, bool isLeft, bool isComplete, bool isSibling) {
	if (isLeft) {

		if (isSibling) {

			if (kbestSpan[i][j].sibling_left_component[rank - 1].interPosition
					>= 0 && j - i > 1) {

				//std::cerr << "from:"<< j<<" to"<<i<<" interception:"<<span[i][j].getSibling_left_interPosition()<<" sibling"<<std::endl;

				rebuildPredictVector2ndComplete(
						CPpredictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].sibling_left_component[rank - 1].interPosition,
						kbestSpan[i][j].sibling_left_component[rank - 1].leftPos,
						false, true, false);
				rebuildPredictVector2ndComplete(
						CPpredictVec,
						kbestSpan,
						kbestSpan[i][j].sibling_left_component[rank - 1].interPosition
								+ 1,
						j,
						kbestSpan[i][j].sibling_left_component[rank - 1].rightPos,
						true, true, false);
			}

		} else {

			if (isComplete) {
				if (kbestSpan[i][j].complete_left_component[rank - 1].interPosition
						>= 0) {
					rebuildPredictVector2ndComplete(
							CPpredictVec,
							kbestSpan,
							i,
							kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
							kbestSpan[i][j].complete_left_component[rank - 1].leftPos,
							true, true, false);
					rebuildPredictVector2ndComplete(
							CPpredictVec,
							kbestSpan,
							kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
							j,
							kbestSpan[i][j].complete_left_component[rank - 1].rightPos,
							true, false, false);
				}
			} else {
				(*CPpredictVec)[i - 1] = j;
				//  std::cerr << Dpsentence::predict[i] << std::endl;

				if (kbestSpan[i][j].incomplete_left_component[rank - 1].isThroughSibling) {

					if (kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
							>= 0) {

						rebuildPredictVector2ndComplete(
								CPpredictVec,
								kbestSpan,
								i,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].interPosition,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].leftPos, true, false, true);
						rebuildPredictVector2ndComplete(
								CPpredictVec,
								kbestSpan,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].interPosition,
								j,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].rightPos, true, false, false);
					}

				} else {

					if (kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
							>= 0) {
						rebuildPredictVector2ndComplete(
								CPpredictVec,
								kbestSpan,
								i,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].interPosition,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].leftPos, false, true, false);
						rebuildPredictVector2ndComplete(
								CPpredictVec,
								kbestSpan,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].interPosition + 1,
								j,
								kbestSpan[i][j].incomplete_left_component[rank
										- 1].rightPos, true, true, false);
					}

				}

			}

		}

	} else {

		if (isSibling) {

			if (kbestSpan[i][j].sibling_right_component[rank - 1].interPosition
					>= 0 && j - i > 1) {

				//std::cerr << "from:"<< j<<" to"<<i<<" interception:"<<span[i][j].getSibling_left_interPosition()<<" sibling"<<std::endl;

				rebuildPredictVector2ndComplete(
						CPpredictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].sibling_right_component[rank - 1].interPosition,
						kbestSpan[i][j].sibling_right_component[rank - 1].leftPos,
						false, true, false);
				rebuildPredictVector2ndComplete(
						CPpredictVec,
						kbestSpan,
						kbestSpan[i][j].sibling_right_component[rank - 1].interPosition
								+ 1,
						j,
						kbestSpan[i][j].sibling_right_component[rank - 1].rightPos,
						true, true, false);
			}

		} else {
			if (isComplete) {
				if (kbestSpan[i][j].complete_right_component[rank - 1].interPosition
						>= 0) {
					rebuildPredictVector2ndComplete(
							CPpredictVec,
							kbestSpan,
							i,
							kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
							kbestSpan[i][j].complete_right_component[rank - 1].leftPos,
							false, false, false);
					rebuildPredictVector2ndComplete(
							CPpredictVec,
							kbestSpan,
							kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
							j,
							kbestSpan[i][j].complete_right_component[rank - 1].rightPos,
							false, true, false);
				}
			} else {
				(*CPpredictVec)[j - 1] = i;
				//std::cerr << Dpsentence::predict[j] << std::endl;

				if (kbestSpan[i][j].incomplete_right_component[rank - 1].isThroughSibling) {

					if (kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
							>= 0) {
						rebuildPredictVector2ndComplete(
								CPpredictVec,
								kbestSpan,
								i,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].interPosition,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].leftPos, false, false, false);
						rebuildPredictVector2ndComplete(
								CPpredictVec,
								kbestSpan,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].interPosition,
								j,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].rightPos, false, false, true);
					}

				} else {

					if (kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
							>= 0) {
						rebuildPredictVector2ndComplete(
								CPpredictVec,
								kbestSpan,
								i,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].interPosition,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].leftPos, false, true, false);
						rebuildPredictVector2ndComplete(
								CPpredictVec,
								kbestSpan,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].interPosition + 1,
								j,
								kbestSpan[i][j].incomplete_right_component[rank
										- 1].rightPos, true, true, false);
					}

				}

			}
		}

	}
}

void KbestDpsentence::naiveRebuildPredictVector(std::vector<int>* predictVec,
		Morph::KbestSpan ** kbestSpan, int i, int j, int rank, bool isLeft,
		bool isComplete) {
	if (isLeft) {
		if (isComplete) {
			if (kbestSpan[i][j].complete_left_component[rank - 1].interPosition
					>= 0) {
				naiveRebuildPredictVector(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
						1, true, true);
				naiveRebuildPredictVector(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].complete_left_component[rank - 1].interPosition,
						j, 1, true, false);
			}
		} else {
			(*predictVec)[i] = j;
			//  std::cerr << Dpsentence::predict[i] << std::endl;
			if (kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
					>= 0) {
				naiveRebuildPredictVector(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition,
						1, false, true);
				naiveRebuildPredictVector(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].incomplete_left_component[rank - 1].interPosition
								+ 1, j, 1, true, true);
			}
		}
	} else {
		if (isComplete) {
			if (kbestSpan[i][j].complete_right_component[rank - 1].interPosition
					>= 0) {
				naiveRebuildPredictVector(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
						1, false, false);
				naiveRebuildPredictVector(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].complete_right_component[rank - 1].interPosition,
						j, 1, false, true);
			}
		} else {
			(*predictVec)[j] = i;
			//std::cerr << Dpsentence::predict[j] << std::endl;
			if (kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
					>= 0) {
				naiveRebuildPredictVector(
						predictVec,
						kbestSpan,
						i,
						kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition,
						1, false, true);
				naiveRebuildPredictVector(
						predictVec,
						kbestSpan,
						kbestSpan[i][j].incomplete_right_component[rank - 1].interPosition
								+ 1, j, 1, true, true);
			}
		}
	}
}

std::vector<double> KbestDpsentence::decodeEdgeFactors(size_t bestK) {
	std::vector<double> scoreList;

	if (length < 1) {
		std::cerr << "empty sentence!" << std::endl;
	} else if (length == 1) {
		std::vector<int> tempPredict;
		tempPredict.push_back(0);
		tempPredict.push_back(0);

		for (int i = 0; i < bestK; i++) {
			predict.push_back(tempPredict);
		}
		scoreList.push_back(0);

	} else {
		std::vector<int> tempPredict;
		for (int i = 0; i < length + 1; i++) {
			tempPredict.push_back(0);
		}
		for (int i = 0; i < bestK; i++) {
			predict.push_back(tempPredict);
		}
		//initialize span matrix

		Morph::KbestSpan** kbestSpanMtx =
				(Morph::KbestSpan**) new Morph::KbestSpan[length + 1];

		for (int i = 0; i < length + 1; i++) {
			kbestSpanMtx[i] = (Morph::KbestSpan*) new Morph::KbestSpan[length
					+ 1];
		}

		for (int i = 0; i < length + 1; i++) {
			for (int j = 0; j < length + 1; j++) {
				Morph::KbestSpan initSpan = Morph::KbestSpan(bestK);
				kbestSpanMtx[i][j] = initSpan;
			}
		}

		for (int k = 1; k < length + 1; k++) {
			for (int s = 0; s < length + 1; s++) {
				int t = s + k;
				if (t > length)
					break;

				kbestSpanMtx[s][t].incomplete_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].incomplete_right_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_right_component.front().cValue =
						Lower_bound;

				kbestSpanMtx[s][t].incomplete_left_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].incomplete_right_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].complete_left_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].complete_right_component.front().interPosition =
						s + 1;

				//build components
				std::priority_queue<Morph::KbestSearchToken> IL_Heap;

				std::priority_queue<Morph::KbestSearchToken> IR_Heap;

				std::priority_queue<Morph::KbestSearchToken> CL_Heap;

				std::priority_queue<Morph::KbestSearchToken> CR_Heap;

				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> IL_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									scoreMtx[t][s], r, true, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IL_KbestToken.begin(); iter != IL_KbestToken.end();
							iter++) {
						IL_Heap.push(*iter);
					}

					std::vector<Morph::KbestSearchToken> IR_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									scoreMtx[s][t], r, false, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IR_KbestToken.begin(); iter != IR_KbestToken.end();
							iter++) {
						IR_Heap.push(*iter);
					}
				}

				kbestSpanMtx[s][t].incomplete_left_component.pop_back();
				kbestSpanMtx[s][t].incomplete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(IL_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_left_component.push_back(
								IL_Heap.top());
						IL_Heap.pop();
					}

					if (!(IR_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_right_component.push_back(
								IR_Heap.top());
						IR_Heap.pop();
					}
				}

				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> CL_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t], 0,
									r, true, true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CL_KbestToken.begin(); iter != CL_KbestToken.end();
							iter++) {
						CL_Heap.push(*iter);
					}

					std::vector<Morph::KbestSearchToken> CR_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r + 1],
									kbestSpanMtx[r + 1][t], 0, r + 1, false,
									true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CR_KbestToken.begin(); iter != CR_KbestToken.end();
							iter++) {
						CR_Heap.push(*iter);
					}
				}

				kbestSpanMtx[s][t].complete_left_component.pop_back();
				kbestSpanMtx[s][t].complete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(CL_Heap.empty())) {
						kbestSpanMtx[s][t].complete_left_component.push_back(
								CL_Heap.top());
						CL_Heap.pop();
					}

					if (!(CR_Heap.empty())) {
						kbestSpanMtx[s][t].complete_right_component.push_back(
								CR_Heap.top());
						CR_Heap.pop();
					}
				}

			}
		}

//		std::cerr << "ready to rebuild; capacity:"
//				<< kbestSpanMtx[0][length].complete_right_component.capacity()
//				<< std::endl;

		size_t bound_of_output = bestK;

		if (bound_of_output
				> kbestSpanMtx[0][length].complete_right_component.capacity())
			bound_of_output =
					kbestSpanMtx[0][length].complete_right_component.capacity();

//		std::cerr << "ready to rebuild; bound of output:" << bound_of_output
//				<< std::endl;

		for (size_t i = 0; i < bound_of_output; i++) {

			double parseScore =
					kbestSpanMtx[0][length].getComplete_right_component().at(i).cValue
							- Loose_lower_bound;
			scoreList.push_back(parseScore);

			KbestDpsentence::rebuildPredictVector(&(predict[i]), kbestSpanMtx,
					0, length, i + 1, false, true);
		}

		for (size_t i = 0; i < length + 1; i++) {
			delete[] kbestSpanMtx[i];
		}
		//delete spanMtx;

		//		for (int i = 0; i < length + 1; i++) {
		//			for (int j = 0; j < length + 1; j++) {
		//				cerr << scoreMtx[i][j] << " ";
		//			}
		//			cerr << endl;
		//		}
		//  std::cerr << Dpsentence::predict[0] << std::endl;

	}

	return scoreList;
}

std::vector<double> KbestDpsentence::decodeSiblingFactorsNeo(size_t bestK) {
	std::vector<double> scoreList;

	if (length < 1) {
		std::cerr << "empty sentence!" << std::endl;
	} else if (length == 1) {
		std::vector<int> tempPredict;
		tempPredict.push_back(0);
		tempPredict.push_back(0);

		for (int i = 0; i < bestK; i++) {
			predict.push_back(tempPredict);
		}
		scoreList.push_back(0);

	} else {
		std::vector<int> tempPredict;
		for (int i = 0; i < length + 1; i++) {
			tempPredict.push_back(0);
		}
		for (int i = 0; i < bestK; i++) {
			predict.push_back(tempPredict);
		}
		//initialize span matrix

		Morph::KbestSpan** kbestSpanMtx =
				(Morph::KbestSpan**) new Morph::KbestSpan[length + 1];

		for (int i = 0; i < length + 1; i++) {
			kbestSpanMtx[i] = (Morph::KbestSpan*) new Morph::KbestSpan[length
					+ 1];
		}

		for (int i = 0; i < length + 1; i++) {
			for (int j = 0; j < length + 1; j++) {
				Morph::KbestSpan initSpan = Morph::KbestSpan(bestK, 2);
				kbestSpanMtx[i][j] = initSpan;
			}
		}

		for (int k = 1; k < length + 1; k++) {
			for (int s = 0; s < length + 1; s++) {
				int t = s + k;
				if (t > length)
					break;

				kbestSpanMtx[s][t].incomplete_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].incomplete_right_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_right_component.front().cValue =
						Lower_bound;

				kbestSpanMtx[s][t].sibling_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].sibling_right_component.front().cValue =
						Lower_bound;

				kbestSpanMtx[s][t].incomplete_left_component.front().interPosition =
						t - 1;
				kbestSpanMtx[s][t].incomplete_right_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].complete_left_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].complete_right_component.front().interPosition =
						s + 1;

				kbestSpanMtx[s][t].sibling_left_component.front().interPosition =
						t - 1;
				kbestSpanMtx[s][t].sibling_right_component.front().interPosition =
						s;

				//build components
				std::priority_queue<Morph::KbestSearchToken> SL_Heap;

				std::priority_queue<Morph::KbestSearchToken> SR_Heap;

				std::priority_queue<Morph::KbestSearchToken> IL_Heap;

				std::priority_queue<Morph::KbestSearchToken> IR_Heap;

				std::priority_queue<Morph::KbestSearchToken> CL_Heap;

				std::priority_queue<Morph::KbestSearchToken> CR_Heap;

//sibling components
				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> SL_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									0, r, true, false, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							SL_KbestToken.begin(); iter != SL_KbestToken.end();
							iter++) {
						SL_Heap.push(*iter);
					}
				}

				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> SR_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									0, r, false, false, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							SR_KbestToken.begin(); iter != SR_KbestToken.end();
							iter++) {
						SR_Heap.push(*iter);
					}
				}

				kbestSpanMtx[s][t].sibling_left_component.pop_back();
				kbestSpanMtx[s][t].sibling_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(SL_Heap.empty())) {
						kbestSpanMtx[s][t].sibling_left_component.push_back(
								SL_Heap.top());
						SL_Heap.pop();
					}

					if (!(SR_Heap.empty())) {
						kbestSpanMtx[s][t].sibling_right_component.push_back(
								SR_Heap.top());
						SR_Heap.pop();
					}
				}

//incomplete components
				//from
				for (int r = s + 1; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> IL_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t],
									scoreTriMtx[t][s][r - s], r, true, false,
									true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IL_KbestToken.begin(); iter != IL_KbestToken.end();
							iter++) {
						IL_Heap.push(*iter);
					}
				}

				std::vector<Morph::KbestSearchToken> IL_KbestToken =
						KbestDpsentence::getMergedKbestSpan2nd(
								kbestSpanMtx[s][t - 1], kbestSpanMtx[t][t],
								scoreTriMtx[t][s][0], t - 1, true, false,
								false);

				for (std::vector<Morph::KbestSearchToken>::iterator iter =
						IL_KbestToken.begin(); iter != IL_KbestToken.end();
						iter++) {
					IL_Heap.push(*iter);
				}

				std::vector<Morph::KbestSearchToken> IR_KbestToken =
						KbestDpsentence::getMergedKbestSpan2nd(
								kbestSpanMtx[s][s], kbestSpanMtx[s + 1][t],
								scoreTriMtx[s][t][0], s, false, false, false);

				for (std::vector<Morph::KbestSearchToken>::iterator iter =
						IR_KbestToken.begin(); iter != IR_KbestToken.end();
						iter++) {
					IR_Heap.push(*iter);
				}

				for (int r = s + 1; r <= t - 1; r++) {

					std::vector<Morph::KbestSearchToken> IR_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t],
									scoreTriMtx[s][t][r - s], r, false, false,
									true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IR_KbestToken.begin(); iter != IR_KbestToken.end();
							iter++) {
						IR_Heap.push(*iter);
					}
				}
				//to

				kbestSpanMtx[s][t].incomplete_left_component.pop_back();
				kbestSpanMtx[s][t].incomplete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(IL_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_left_component.push_back(
								IL_Heap.top());
						IL_Heap.pop();
					}

					if (!(IR_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_right_component.push_back(
								IR_Heap.top());
						IR_Heap.pop();
					}
				}

//complete components
				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> CL_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t], 0,
									r, true, true, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CL_KbestToken.begin(); iter != CL_KbestToken.end();
							iter++) {
						CL_Heap.push(*iter);
					}

					std::vector<Morph::KbestSearchToken> CR_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r + 1],
									kbestSpanMtx[r + 1][t], 0, r + 1, false,
									true, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CR_KbestToken.begin(); iter != CR_KbestToken.end();
							iter++) {
						CR_Heap.push(*iter);
					}
				}

				kbestSpanMtx[s][t].complete_left_component.pop_back();
				kbestSpanMtx[s][t].complete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(CL_Heap.empty())) {
						kbestSpanMtx[s][t].complete_left_component.push_back(
								CL_Heap.top());
						CL_Heap.pop();
					}

					if (!(CR_Heap.empty())) {
						kbestSpanMtx[s][t].complete_right_component.push_back(
								CR_Heap.top());
						CR_Heap.pop();
					}
				}

			}
		}

//		std::cerr << "ready to rebuild; capacity:"
//				<< kbestSpanMtx[0][length].complete_right_component.capacity()
//				<< std::endl;

		size_t bound_of_output = bestK;

		if (bound_of_output
				> kbestSpanMtx[0][length].complete_right_component.capacity())
			bound_of_output =
					kbestSpanMtx[0][length].complete_right_component.capacity();

//		std::cerr << "ready to rebuild; bound of output:" << bound_of_output
//				<< std::endl;

		for (size_t i = 0; i < bound_of_output; i++) {

			double parseScore =
					kbestSpanMtx[0][length].getComplete_right_component().at(i).cValue
							- Loose_lower_bound;
			scoreList.push_back(parseScore);

			KbestDpsentence::rebuildPredictVector2nd(&(predict[i]),
					kbestSpanMtx, 0, length, i + 1, false, true, false);
		}

		for (size_t i = 0; i < length + 1; i++) {
			delete[] kbestSpanMtx[i];
		}
		//delete spanMtx;

		//		for (int i = 0; i < length + 1; i++) {
		//			for (int j = 0; j < length + 1; j++) {
		//				cerr << scoreMtx[i][j] << " ";
		//			}
		//			cerr << endl;
		//		}
		//  std::cerr << Dpsentence::predict[0] << std::endl;

	}

	return scoreList;
}

std::vector<double> KbestDpsentence::decodeEdgeFactorsComplete(
		std::vector<std::vector<int> >& completePredict, size_t bestK) {
	std::vector<double> scoreList;

	if (length < 1) {
		std::cerr << "empty sentence!" << std::endl;
	} else if (length == 1) {
		std::vector<int> tempPredict;

		tempPredict.push_back(0);

		for (int i = 0; i < bestK; i++) {
			completePredict.push_back(tempPredict);
		}

		scoreList.push_back(0);

	} else {
		std::vector<int> tempPredict;

		for (int i = 0; i < length; i++) {
			tempPredict.push_back(0);
		}

		for (int i = 0; i < bestK; i++) {
			completePredict.push_back(tempPredict);
		}
		//initialize span matrix

		Morph::KbestSpan** kbestSpanMtx =
				(Morph::KbestSpan**) new Morph::KbestSpan[length + 1];

		for (int i = 0; i < length + 1; i++) {
			kbestSpanMtx[i] = (Morph::KbestSpan*) new Morph::KbestSpan[length
					+ 1];
		}

		for (int i = 0; i < length + 1; i++) {
			for (int j = 0; j < length + 1; j++) {
				Morph::KbestSpan initSpan = Morph::KbestSpan(bestK);
				kbestSpanMtx[i][j] = initSpan;
			}
		}

		for (int k = 1; k < length + 1; k++) {
			for (int s = 0; s < length + 1; s++) {
				int t = s + k;
				if (t > length)
					break;

				kbestSpanMtx[s][t].incomplete_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].incomplete_right_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_right_component.front().cValue =
						Lower_bound;

				kbestSpanMtx[s][t].incomplete_left_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].incomplete_right_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].complete_left_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].complete_right_component.front().interPosition =
						s + 1;

				//build components
				std::priority_queue<Morph::KbestSearchToken> IL_Heap;

				std::priority_queue<Morph::KbestSearchToken> IR_Heap;

				std::priority_queue<Morph::KbestSearchToken> CL_Heap;

				std::priority_queue<Morph::KbestSearchToken> CR_Heap;

				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> IL_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									scoreMtx[t][s], r, true, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IL_KbestToken.begin(); iter != IL_KbestToken.end();
							iter++) {
						IL_Heap.push(*iter);
					}

					std::vector<Morph::KbestSearchToken> IR_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									scoreMtx[s][t], r, false, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IR_KbestToken.begin(); iter != IR_KbestToken.end();
							iter++) {
						IR_Heap.push(*iter);
					}
				}

				kbestSpanMtx[s][t].incomplete_left_component.pop_back();
				kbestSpanMtx[s][t].incomplete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(IL_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_left_component.push_back(
								IL_Heap.top());
						IL_Heap.pop();
					}

					if (!(IR_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_right_component.push_back(
								IR_Heap.top());
						IR_Heap.pop();
					}
				}

				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> CL_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t], 0,
									r, true, true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CL_KbestToken.begin(); iter != CL_KbestToken.end();
							iter++) {
						CL_Heap.push(*iter);
					}

					std::vector<Morph::KbestSearchToken> CR_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r + 1],
									kbestSpanMtx[r + 1][t], 0, r + 1, false,
									true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CR_KbestToken.begin(); iter != CR_KbestToken.end();
							iter++) {
						CR_Heap.push(*iter);
					}
				}

				kbestSpanMtx[s][t].complete_left_component.pop_back();
				kbestSpanMtx[s][t].complete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(CL_Heap.empty())) {
						kbestSpanMtx[s][t].complete_left_component.push_back(
								CL_Heap.top());
						CL_Heap.pop();
					}

					if (!(CR_Heap.empty())) {
						kbestSpanMtx[s][t].complete_right_component.push_back(
								CR_Heap.top());
						CR_Heap.pop();
					}
				}

			}
		}

//		std::cerr << "ready to rebuild; capacity:"
//				<< kbestSpanMtx[0][length].complete_right_component.capacity()
//				<< std::endl;

		size_t bound_of_output = bestK;

		if (bound_of_output
				> kbestSpanMtx[0][length].complete_right_component.capacity())
			bound_of_output =
					kbestSpanMtx[0][length].complete_right_component.capacity();

//		std::cerr << "ready to rebuild; bound of output:" << bound_of_output
//				<< std::endl;

		for (size_t i = 0; i < bound_of_output; i++) {

			double parseScore =
					kbestSpanMtx[0][length].getComplete_right_component().at(i).cValue
							- Loose_lower_bound;
			scoreList.push_back(parseScore);

			KbestDpsentence::rebuildPredictVector(&(completePredict[i]), kbestSpanMtx,
					0, length, i + 1, false, true);
		}

		for (size_t i = 0; i < length + 1; i++) {
			delete[] kbestSpanMtx[i];
		}
		//delete spanMtx;

		//		for (int i = 0; i < length + 1; i++) {
		//			for (int j = 0; j < length + 1; j++) {
		//				cerr << scoreMtx[i][j] << " ";
		//			}
		//			cerr << endl;
		//		}
		//  std::cerr << Dpsentence::predict[0] << std::endl;

	}

	return scoreList;
}

std::vector<double> KbestDpsentence::decodeSiblingFactorsNeoComplete(
		std::vector<std::vector<int> >& completePredict, size_t bestK) {
	std::vector<double> scoreList;

	if (length < 1) {
		std::cerr << "empty sentence!" << std::endl;
	} else if (length == 1) {
		std::vector<int> tempPredict;

		tempPredict.push_back(0);

		for (int i = 0; i < bestK; i++) {
			completePredict.push_back(tempPredict);
		}

		scoreList.push_back(0);

	} else {
		std::vector<int> tempPredict;

		for (int i = 0; i < length; i++) {
			tempPredict.push_back(0);
		}
		for (int i = 0; i < bestK; i++) {
			completePredict.push_back(tempPredict);
		}
		//initialize span matrix

		Morph::KbestSpan** kbestSpanMtx =
				(Morph::KbestSpan**) new Morph::KbestSpan[length + 1];

		for (int i = 0; i < length + 1; i++) {
			kbestSpanMtx[i] = (Morph::KbestSpan*) new Morph::KbestSpan[length
					+ 1];
		}

		for (int i = 0; i < length + 1; i++) {
			for (int j = 0; j < length + 1; j++) {
				Morph::KbestSpan initSpan = Morph::KbestSpan(bestK, 2);
				kbestSpanMtx[i][j] = initSpan;
			}
		}

		for (int k = 1; k < length + 1; k++) {
			for (int s = 0; s < length + 1; s++) {
				int t = s + k;
				if (t > length)
					break;

				kbestSpanMtx[s][t].incomplete_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].incomplete_right_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_right_component.front().cValue =
						Lower_bound;

				kbestSpanMtx[s][t].sibling_left_component.front().cValue =
						Lower_bound;
				kbestSpanMtx[s][t].sibling_right_component.front().cValue =
						Lower_bound;

				kbestSpanMtx[s][t].incomplete_left_component.front().interPosition =
						t - 1;
				kbestSpanMtx[s][t].incomplete_right_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].complete_left_component.front().interPosition =
						s;
				kbestSpanMtx[s][t].complete_right_component.front().interPosition =
						s + 1;

				kbestSpanMtx[s][t].sibling_left_component.front().interPosition =
						t - 1;
				kbestSpanMtx[s][t].sibling_right_component.front().interPosition =
						s;

				//build components
				std::priority_queue<Morph::KbestSearchToken> SL_Heap;

				std::priority_queue<Morph::KbestSearchToken> SR_Heap;

				std::priority_queue<Morph::KbestSearchToken> IL_Heap;

				std::priority_queue<Morph::KbestSearchToken> IR_Heap;

				std::priority_queue<Morph::KbestSearchToken> CL_Heap;

				std::priority_queue<Morph::KbestSearchToken> CR_Heap;

//sibling components
				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> SL_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									0, r, true, false, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							SL_KbestToken.begin(); iter != SL_KbestToken.end();
							iter++) {
						SL_Heap.push(*iter);
					}
				}

				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> SR_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									0, r, false, false, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							SR_KbestToken.begin(); iter != SR_KbestToken.end();
							iter++) {
						SR_Heap.push(*iter);
					}
				}

				kbestSpanMtx[s][t].sibling_left_component.pop_back();
				kbestSpanMtx[s][t].sibling_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(SL_Heap.empty())) {
						kbestSpanMtx[s][t].sibling_left_component.push_back(
								SL_Heap.top());
						SL_Heap.pop();
					}

					if (!(SR_Heap.empty())) {
						kbestSpanMtx[s][t].sibling_right_component.push_back(
								SR_Heap.top());
						SR_Heap.pop();
					}
				}

//incomplete components
				//from
				for (int r = s + 1; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> IL_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t],
									scoreTriMtx[t][s][r - s], r, true, false,
									true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IL_KbestToken.begin(); iter != IL_KbestToken.end();
							iter++) {
						IL_Heap.push(*iter);
					}
				}

				std::vector<Morph::KbestSearchToken> IL_KbestToken =
						KbestDpsentence::getMergedKbestSpan2nd(
								kbestSpanMtx[s][t - 1], kbestSpanMtx[t][t],
								scoreTriMtx[t][s][0], t - 1, true, false,
								false);

				for (std::vector<Morph::KbestSearchToken>::iterator iter =
						IL_KbestToken.begin(); iter != IL_KbestToken.end();
						iter++) {
					IL_Heap.push(*iter);
				}

				std::vector<Morph::KbestSearchToken> IR_KbestToken =
						KbestDpsentence::getMergedKbestSpan2nd(
								kbestSpanMtx[s][s], kbestSpanMtx[s + 1][t],
								scoreTriMtx[s][t][0], s, false, false, false);

				for (std::vector<Morph::KbestSearchToken>::iterator iter =
						IR_KbestToken.begin(); iter != IR_KbestToken.end();
						iter++) {
					IR_Heap.push(*iter);
				}

				for (int r = s + 1; r <= t - 1; r++) {

					std::vector<Morph::KbestSearchToken> IR_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t],
									scoreTriMtx[s][t][r - s], r, false, false,
									true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IR_KbestToken.begin(); iter != IR_KbestToken.end();
							iter++) {
						IR_Heap.push(*iter);
					}
				}
				//to

				kbestSpanMtx[s][t].incomplete_left_component.pop_back();
				kbestSpanMtx[s][t].incomplete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(IL_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_left_component.push_back(
								IL_Heap.top());
						IL_Heap.pop();
					}

					if (!(IR_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_right_component.push_back(
								IR_Heap.top());
						IR_Heap.pop();
					}
				}

//complete components
				for (int r = s; r <= t - 1; r++) {
					std::vector<Morph::KbestSearchToken> CL_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t], 0,
									r, true, true, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CL_KbestToken.begin(); iter != CL_KbestToken.end();
							iter++) {
						CL_Heap.push(*iter);
					}

					std::vector<Morph::KbestSearchToken> CR_KbestToken =
							KbestDpsentence::getMergedKbestSpan2nd(
									kbestSpanMtx[s][r + 1],
									kbestSpanMtx[r + 1][t], 0, r + 1, false,
									true, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CR_KbestToken.begin(); iter != CR_KbestToken.end();
							iter++) {
						CR_Heap.push(*iter);
					}
				}

				kbestSpanMtx[s][t].complete_left_component.pop_back();
				kbestSpanMtx[s][t].complete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(CL_Heap.empty())) {
						kbestSpanMtx[s][t].complete_left_component.push_back(
								CL_Heap.top());
						CL_Heap.pop();
					}

					if (!(CR_Heap.empty())) {
						kbestSpanMtx[s][t].complete_right_component.push_back(
								CR_Heap.top());
						CR_Heap.pop();
					}
				}

			}
		}

//		std::cerr << "ready to rebuild; capacity:"
//				<< kbestSpanMtx[0][length].complete_right_component.capacity()
//				<< std::endl;

		size_t bound_of_output = bestK;

		if (bound_of_output
				> kbestSpanMtx[0][length].complete_right_component.capacity())
			bound_of_output =
					kbestSpanMtx[0][length].complete_right_component.capacity();

//		std::cerr << "ready to rebuild; bound of output:" << bound_of_output
//				<< std::endl;

		for (size_t i = 0; i < bound_of_output; i++) {

			double parseScore =
					kbestSpanMtx[0][length].getComplete_right_component().at(i).cValue
							- Loose_lower_bound;
			scoreList.push_back(parseScore);

			KbestDpsentence::rebuildPredictVector2nd(&(completePredict[i]),
					kbestSpanMtx, 0, length, i + 1, false, true, false);
		}

		for (size_t i = 0; i < length + 1; i++) {
			delete[] kbestSpanMtx[i];
		}

		//delete spanMtx;

		//		for (int i = 0; i < length + 1; i++) {
		//			for (int j = 0; j < length + 1; j++) {
		//				cerr << scoreMtx[i][j] << " ";
		//			}
		//			cerr << endl;
		//		}
		//  std::cerr << Dpsentence::predict[0] << std::endl;

	}

	return scoreList;
}

void KbestDpsentence::naiveDecodeEdgeFactors(size_t bestK) {

	if (length < 1) {
		std::cerr << "empty sentence!" << std::endl;
	} else if (length == 1) {
		std::vector<int> tempPredict;
		tempPredict.push_back(0);
		tempPredict.push_back(0);
		for (int i = 0; i < bestK; i++) {
			predict.push_back(tempPredict);
		}

	} else {
		std::vector<int> tempPredict;
		for (int i = 0; i < length + 1; i++) {
			tempPredict.push_back(0);
		}
		for (int i = 0; i < bestK; i++) {
			predict.push_back(tempPredict);
		}
		//initialize span matrix

		Morph::KbestSpan** kbestSpanMtx =
				(Morph::KbestSpan**) new Morph::KbestSpan[length + 1];

		for (int i = 0; i < length + 1; i++) {
			kbestSpanMtx[i] = (Morph::KbestSpan*) new Morph::KbestSpan[length
					+ 1];
		}

		for (int i = 0; i < length + 1; i++) {
			for (int j = 0; j < length + 1; j++) {
				Morph::KbestSpan initSpan = Morph::KbestSpan(bestK);
				kbestSpanMtx[i][j] = initSpan;
			}
		}

		for (int k = 1; k < length + 1; k++) {
			for (int s = 0; s < length + 1; s++) {
				int t = s + k;
				if (t > length)
					break;

				kbestSpanMtx[s][t].incomplete_left_component[0].cValue =
						Lower_bound;
				kbestSpanMtx[s][t].incomplete_right_component[0].cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_left_component[0].cValue =
						Lower_bound;
				kbestSpanMtx[s][t].complete_right_component[0].cValue =
						Lower_bound;

				kbestSpanMtx[s][t].incomplete_left_component[0].interPosition =
						s;
				kbestSpanMtx[s][t].incomplete_right_component[0].interPosition =
						s;
				kbestSpanMtx[s][t].complete_left_component[0].interPosition = s;
				kbestSpanMtx[s][t].complete_right_component[0].interPosition = s
						+ 1;

				//build components
				std::priority_queue<Morph::KbestSearchToken> IL_Heap;

				std::priority_queue<Morph::KbestSearchToken> IR_Heap;

				std::priority_queue<Morph::KbestSearchToken> CL_Heap;

				std::priority_queue<Morph::KbestSearchToken> CR_Heap;

				for (int r = s; r <= t - 1; r++) {

					std::vector<Morph::KbestSearchToken> IL_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									scoreMtx[t][s], r, true, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IL_KbestToken.begin(); iter != IL_KbestToken.end();
							iter++) {
						IL_Heap.push(*iter);
					}

					for (int p = 0; p < bestK; p++) {

						if (!(IL_Heap.empty())) {
							kbestSpanMtx[s][t].incomplete_left_component.push_back(
									IL_Heap.top());
							IL_Heap.pop();
						}
					}

					std::vector<Morph::KbestSearchToken> IR_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r + 1][t],
									scoreMtx[s][t], r, false, false);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							IR_KbestToken.begin(); iter != IR_KbestToken.end();
							iter++) {
						IR_Heap.push(*iter);
					}

					std::vector<Morph::KbestSearchToken> CL_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r], kbestSpanMtx[r][t], 0,
									r, true, true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CL_KbestToken.begin(); iter != CL_KbestToken.end();
							iter++) {
						CL_Heap.push(*iter);
					}

					std::vector<Morph::KbestSearchToken> CR_KbestToken =
							KbestDpsentence::getMergedKbestSpan(
									kbestSpanMtx[s][r + 1],
									kbestSpanMtx[r + 1][t], 0, r + 1, false,
									true);

					for (std::vector<Morph::KbestSearchToken>::iterator iter =
							CR_KbestToken.begin(); iter != CR_KbestToken.end();
							iter++) {
						CR_Heap.push(*iter);
					}

					double tempILE =
							kbestSpanMtx[s][r].complete_right_component[0].cValue
									+ kbestSpanMtx[r + 1][t].complete_left_component[0].cValue
									+ scoreMtx[t][s];

					if (kbestSpanMtx[s][t].incomplete_left_component[0].cValue
							< tempILE) {
						kbestSpanMtx[s][t].incomplete_left_component[0].cValue =
								tempILE;
						kbestSpanMtx[s][t].incomplete_left_component[0].interPosition =
								r;
					}

					double tempIRE =
							kbestSpanMtx[s][r].complete_right_component[0].cValue
									+ kbestSpanMtx[r + 1][t].complete_left_component[0].cValue
									+ scoreMtx[s][t];

					if (kbestSpanMtx[s][t].incomplete_right_component[0].cValue
							< tempIRE) {
						kbestSpanMtx[s][t].incomplete_right_component[0].cValue =
								tempIRE;
						kbestSpanMtx[s][t].incomplete_right_component[0].interPosition =
								r;
					}

					double tempCLE =
							kbestSpanMtx[s][r].complete_left_component[0].cValue
									+ kbestSpanMtx[r][t].incomplete_left_component[0].cValue;

					if (kbestSpanMtx[s][t].complete_left_component[0].cValue
							< tempCLE) {
						kbestSpanMtx[s][t].complete_left_component[0].cValue =
								tempCLE;
						kbestSpanMtx[s][t].complete_left_component[0].interPosition =
								r;
					}

					double tempCRE =
							kbestSpanMtx[s][r + 1].incomplete_right_component[0].cValue
									+ kbestSpanMtx[r + 1][t].incomplete_right_component[0].cValue;

					if (kbestSpanMtx[s][t].complete_right_component[0].cValue
							< tempCRE) {
						kbestSpanMtx[s][t].complete_right_component[0].cValue =
								tempCRE;
						kbestSpanMtx[s][t].complete_right_component[0].interPosition =
								r + 1;
					}

				}

				if (IL_Heap.top().cValue
						!= kbestSpanMtx[s][t].incomplete_left_component[0].cValue) {
					//kbestSpanMtx[s][t].incomplete_left_component[0].cValue=IL_Heap.top();
					std::cerr
							<< "IL not equal "
							<< IL_Heap.top().cValue
							<< " "
							<< kbestSpanMtx[s][t].incomplete_left_component[0].cValue
							<< std::endl;
				}
				if (IR_Heap.top().cValue
						!= kbestSpanMtx[s][t].incomplete_right_component[0].cValue) {
					//kbestSpanMtx[s][t].incomplete_right_component[0].cValue=IR_Heap.top();
					std::cerr
							<< "IR not equal "
							<< IR_Heap.top().cValue
							<< " "
							<< kbestSpanMtx[s][t].incomplete_right_component[0].cValue
							<< std::endl;
				}
				if (CL_Heap.top().cValue
						!= kbestSpanMtx[s][t].complete_left_component[0].cValue) {
					//kbestSpanMtx[s][t].complete_left_component[0].cValue=CL_Heap.top();
					std::cerr
							<< "CL not equal "
							<< CL_Heap.top().cValue
							<< " "
							<< kbestSpanMtx[s][t].complete_left_component[0].cValue
							<< std::endl;
				}
				if (CR_Heap.top().cValue
						!= kbestSpanMtx[s][t].complete_right_component[0].cValue) {
					//kbestSpanMtx[s][t].complete_right_component[0].cValue=CR_Heap.top();
					std::cerr
							<< "CR not equal "
							<< CR_Heap.top().cValue
							<< " "
							<< kbestSpanMtx[s][t].complete_right_component[0].cValue
							<< std::endl;
				}

//				kbestSpanMtx[s][t].incomplete_left_component.pop_back();
//				kbestSpanMtx[s][t].incomplete_right_component.pop_back();
//				kbestSpanMtx[s][t].complete_left_component.pop_back();
//				kbestSpanMtx[s][t].complete_right_component.pop_back();

				for (int p = 0; p < bestK; p++) {

					if (!(IL_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_left_component.push_back(
								IL_Heap.top());
						IL_Heap.pop();
					}

					if (!(IR_Heap.empty())) {
						kbestSpanMtx[s][t].incomplete_right_component.push_back(
								IR_Heap.top());
						IR_Heap.pop();
					}

					if (!(CL_Heap.empty())) {
						kbestSpanMtx[s][t].complete_left_component.push_back(
								CL_Heap.top());
						CL_Heap.pop();
					}

					if (!(CR_Heap.empty())) {
						kbestSpanMtx[s][t].complete_right_component.push_back(
								CR_Heap.top());
						CR_Heap.pop();
					}
				}

			}
		}

//		std::cerr << "ready to rebuild; capacity:"
//				<< kbestSpanMtx[0][length].complete_right_component.capacity()
//				<< std::endl;

//		size_t bound_of_output = bestK;
//
//		if (bound_of_output
//				> kbestSpanMtx[0][length].complete_right_component.capacity())
//			bound_of_output =
//					kbestSpanMtx[0][length].complete_right_component.capacity();

//		std::cerr << "entered" << std::endl;

		KbestDpsentence::naiveRebuildPredictVector(&(predict[0]), kbestSpanMtx,
				0, length, 1, false, true);

//			std::cerr << "exited" << std::endl;

		for (size_t i = 0; i < length + 1; i++) {
			delete[] kbestSpanMtx[i];
		}
		//delete spanMtx;

		//		for (int i = 0; i < length + 1; i++) {
		//			for (int j = 0; j < length + 1; j++) {
		//				cerr << scoreMtx[i][j] << " ";
		//			}
		//			cerr << endl;
		//		}
		//  std::cerr << Dpsentence::predict[0] << std::endl;

	}

}

}
